/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月11日
**  名   称：lib.h
**  摘   要：
*******************************************************************************/
#ifndef _LIB_H
#define _LIB_H

void Lib_Init(void);

void Lib_DelayUs(uint32 us);
void Lib_DelayMs(uint32 ms);
void Lib_DelaySecond(uint32 second);

uint16 Lib_htons(uint16 n);

bool CRC16_Check(uint8 *buff, uint8 *crc, uint16 len);
uint16 CRC16_Process(uint8 *buff, uint16 len);

const _Time* Lib_GetTime(void);
void Lib_RenewCalendar(void);

uint16 Lib_GetEarlyTime(_Time time[], uint16 count);
uint16 Lib_GetLateTime(_Time time[], uint16 count);
bool Lib_TimeIsValid(_Time *time);

uint8 Lib_NumToBCD(uint8 num);
uint8 Lib_BCDToNum(uint8 BCD);
uint8 Lib_HexToChar(uint8 hex);

void Lib_BuffShiftBytes(uint8 *dest, uint8 *src, uint16 len, uint8 opt);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0          2016年05月11日   ggggggg      创建文件
*******************************************************************************/
