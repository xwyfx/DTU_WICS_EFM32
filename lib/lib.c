/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月10日
**  名   称：lib.c
**  摘   要：
*******************************************************************************/
#include "system.h"
#include "PHY_Timer.h"
#include "PHY_WatchDog.h"

#define asm  __ASM

/*CRC计算表*/
const unsigned char  CrCTabH[256] = {
    0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70,
    0x81, 0x91, 0xA1, 0xB1, 0xC1, 0xD1, 0xE1, 0xF1,
    0x12, 0x02, 0x32, 0x22, 0x52, 0x42, 0x72, 0x62,
    0x93, 0x83, 0xB3, 0xA3, 0xD3, 0xC3, 0xF3, 0xE3,
    0x24, 0x34, 0x04, 0x14, 0x64, 0x74, 0x44, 0x54,
    0xA5, 0xB5, 0x85, 0x95, 0xE5, 0xF5, 0xC5, 0xD5,
    0x36, 0x26, 0x16, 0x06, 0x76, 0x66, 0x56, 0x46,
    0xB7, 0xA7, 0x97, 0x87, 0xF7, 0xE7, 0xD7, 0xC7,
    0x48, 0x58, 0x68, 0x78, 0x08, 0x18, 0x28, 0x38,
    0xC9, 0xD9, 0xE9, 0xF9, 0x89, 0x99, 0xA9, 0xB9,
    0x5A, 0x4A, 0x7A, 0x6A, 0x1A, 0x0A, 0x3A, 0x2A,
    0xDB, 0xCB, 0xFB, 0xEB, 0x9B, 0x8B, 0xBB, 0xAB,
    0x6C, 0x7C, 0x4C, 0x5C, 0x2C, 0x3C, 0x0C, 0x1C,
    0xED, 0xFD, 0xCD, 0xDD, 0xAD, 0xBD, 0x8D, 0x9D,
    0x7E, 0x6E, 0x5E, 0x4E, 0x3E, 0x2E, 0x1E, 0x0E,
    0xFF, 0xEF, 0xDF, 0xCF, 0xBF, 0xAF, 0x9F, 0x8F,
    0x91, 0x81, 0xB1, 0xA1, 0xD1, 0xC1, 0xF1, 0xE1,
    0x10, 0x00, 0x30, 0x20, 0x50, 0x40, 0x70, 0x60,
    0x83, 0x93, 0xA3, 0xB3, 0xC3, 0xD3, 0xE3, 0xF3,
    0x02, 0x12, 0x22, 0x32, 0x42, 0x52, 0x62, 0x72,
    0xB5, 0xA5, 0x95, 0x85, 0xF5, 0xE5, 0xD5, 0xC5,
    0x34, 0x24, 0x14, 0x04, 0x74, 0x64, 0x54, 0x44,
    0xA7, 0xB7, 0x87, 0x97, 0xE7, 0xF7, 0xC7, 0xD7,
    0x26, 0x36, 0x06, 0x16, 0x66, 0x76, 0x46, 0x56,
    0xD9, 0xC9, 0xF9, 0xE9, 0x99, 0x89, 0xB9, 0xA9,
    0x58, 0x48, 0x78, 0x68, 0x18, 0x08, 0x38, 0x28,
    0xCB, 0xDB, 0xEB, 0xFB, 0x8B, 0x9B, 0xAB, 0xBB,
    0x4A, 0x5A, 0x6A, 0x7A, 0x0A, 0x1A, 0x2A, 0x3A,
    0xFD, 0xED, 0xDD, 0xCD, 0xBD, 0xAD, 0x9D, 0x8D,
    0x7C, 0x6C, 0x5C, 0x4C, 0x3C, 0x2C, 0x1C, 0x0C,
    0xEF, 0xFF, 0xCF, 0xDF, 0xAF, 0xBF, 0x8F, 0x9F,
    0x6E, 0x7E, 0x4E, 0x5E, 0x2E, 0x3E, 0x0E, 0x1E,
};

/*CRC计算表*/
const unsigned char CrCTabL[256] = {
    0x00, 0x21, 0x42, 0x63, 0x84, 0xA5, 0xC6, 0xE7,
    0x08, 0x29, 0x4A, 0x6B, 0x8C, 0xAD, 0xCE, 0xEF,
    0x31, 0x10, 0x73, 0x52, 0xB5, 0x94, 0xF7, 0xD6,
    0x39, 0x18, 0x7B, 0x5A, 0xBD, 0x9C, 0xFF, 0xDE,
    0x62, 0x43, 0x20, 0x01, 0xE6, 0xC7, 0xA4, 0x85,
    0x6A, 0x4B, 0x28, 0x09, 0xEE, 0xCF, 0xAC, 0x8D,
    0x53, 0x72, 0x11, 0x30, 0xD7, 0xF6, 0x95, 0xB4,
    0x5B, 0x7A, 0x19, 0x38, 0xDF, 0xFE, 0x9D, 0xBC,
    0xC4, 0xE5, 0x86, 0xA7, 0x40, 0x61, 0x02, 0x23,
    0xCC, 0xED, 0x8E, 0xAF, 0x48, 0x69, 0x0A, 0x2B,
    0xF5, 0xD4, 0xB7, 0x96, 0x71, 0x50, 0x33, 0x12,
    0xFD, 0xDC, 0xBF, 0x9E, 0x79, 0x58, 0x3B, 0x1A,
    0xA6, 0x87, 0xE4, 0xC5, 0x22, 0x03, 0x60, 0x41,
    0xAE, 0x8F, 0xEC, 0xCD, 0x2A, 0x0B, 0x68, 0x49,
    0x97, 0xB6, 0xD5, 0xF4, 0x13, 0x32, 0x51, 0x70,
    0x9F, 0xBE, 0xDD, 0xFC, 0x1B, 0x3A, 0x59, 0x78,
    0x88, 0xA9, 0xCA, 0xEB, 0x0C, 0x2D, 0x4E, 0x6F,
    0x80, 0xA1, 0xC2, 0xE3, 0x04, 0x25, 0x46, 0x67,
    0xB9, 0x98, 0xFB, 0xDA, 0x3D, 0x1C, 0x7F, 0x5E,
    0xB1, 0x90, 0xF3, 0xD2, 0x35, 0x14, 0x77, 0x56,
    0xEA, 0xCB, 0xA8, 0x89, 0x6E, 0x4F, 0x2C, 0x0D,
    0xE2, 0xC3, 0xA0, 0x81, 0x66, 0x47, 0x24, 0x05,
    0xDB, 0xFA, 0x99, 0xB8, 0x5F, 0x7E, 0x1D, 0x3C,
    0xD3, 0xF2, 0x91, 0xB0, 0x57, 0x76, 0x15, 0x34,
    0x4C, 0x6D, 0x0E, 0x2F, 0xC8, 0xE9, 0x8A, 0xAB,
    0x44, 0x65, 0x06, 0x27, 0xC0, 0xE1, 0x82, 0xA3,
    0x7D, 0x5C, 0x3F, 0x1E, 0xF9, 0xD8, 0xBB, 0x9A,
    0x75, 0x54, 0x37, 0x16, 0xF1, 0xD0, 0xB3, 0x92,
    0x2E, 0x0F, 0x6C, 0x4D, 0xAA, 0x8B, 0xE8, 0xC9,
    0x26, 0x07, 0x64, 0x45, 0xA2, 0x83, 0xE0, 0xC1,
    0x1F, 0x3E, 0x5D, 0x7C, 0x9B, 0xBA, 0xD9, 0xF8,
    0x17, 0x36, 0x55, 0x74, 0x93, 0xB2, 0xD1, 0xF0,
};

static _Time now;

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void lib_DelayOneSecond(void)
{
    uint16 time;
    uint16 now;

    time = PHY_GetSysTick();
    time = time + SYS_SECOND_TICK_NUM;

    while (1)
    {
        now = PHY_GetTickReg();

        if (now >= time)
        {
            break;
        }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Lib_DelaySecond(uint32 second)
{
    second = second;
    while (second > 0)
    {
        lib_DelayOneSecond();
        second--;

        SysFeedWatchdogCounter = 1;
        PHY_FeedWatchDog();
    }

    return;
}
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Lib_DelayMs(uint32 ms)
{
    if (ms >= 500)
    {
        while (ms >= 500)
        {
            Lib_DelayUs(500 * 1000);
            ms = ms - 500;
        }

        if (ms > 0)
        {
            Lib_DelayUs(ms * 1000);
        }
    }
    else
    {
        Lib_DelayUs(ms * 1000);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Lib_DelayUs(uint32 us)
{
    while (us > 0)
    {
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");

        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");

        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");

        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");

        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");
        asm("nop");

        us--;
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Lib_htons(uint16 n)
{
    return ((n & 0xff) << 8) | ((n & 0xff00) >> 8);
}

/*******************************************************************
*功能：采用crc - ibm生成CRC
*参数：数据首地址，数据长度
*返回值：CRC
*******************************************************************/
uint16 CRC16_Process(uint8 *buff, uint16 len)
{
    uint16 crc;                                                                 //存放CRC
    uint16  i;                                                                  //循环变量
    uint8  t;                                                                   //CRC计算中间变量
    uint8  a;                                                                   //CRC计算中间变量
    uint8  b;                                                                   //CRC计算中间变量
    uint8  CrCH;                                                                //CRC计算中间变量
    uint8  CrCL;                                                                //CRC计算中间变量

    a = b = 0;
    for (i = 0; i < len; i++)                                                   //CRC计算
    {
        t = a ^ buff[i];
        a = b ^ CrCTabH[t];
        b = CrCTabL[t];
    }
    CrCH = a;
    CrCL = b;

    crc = CrCH;
    crc = crc << 8;
    crc = crc + CrCL;

    return crc;                                                                 //返回CRC
}

/*******************************************************************************
*功能：CRC校验
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool CRC16_Check(uint8 *buff, uint8 *crc, uint16 len)
{
    uint16 crc_check;                                                           //本地生成的CRC
    uint16 crc_pkt;                                                             //报文中的CRC

    crc_check = CRC16_Process(buff, len);
    memcpy(&crc_pkt, crc, 2);

    if (crc_check == crc_pkt)                                                   //如果CRC相同
    {
        return true;                                                            //返回成功标识
    }
    else                                                                        //如果CRC不相同
    {
        return false;                                                           //返回失败标识
    }
}

/*-----------------------万年历部分----------------------*/

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：闰年返回29, 平年返回28
*******************************************************************************/
static uint8 Fun_Leap(uint16 year)                                              //判断是否闰年并返回二月份的天数
{
    if (year % 4 == 0)
    {
        if (year % 100 == 0)
        {
            if (year / 400 == 0)
            {
                return 29;
            }
            else
            {
                return 28;
            }

        }
        else
        {
            return 29;
        }

    }
    else
    {
        return 28;
    }
}
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static uint8 Fun_GetDay(uint8 month)                                            //计算并返回每个月的天数
{
    switch (month)
    {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            {
                return 31;                                                      //1, 3, 5, 7, 8, 10, 12月每月31天
            }
        case 4:
        case 6:
        case 9:
        case 11:
            {
                return 30;
            }                                                                   //4, 6, 9, 11月每月30天
        case 2:
            {
                return Fun_Leap(now.year);                                      //返回二月份的天数
            }
        default:
            {
                break;
            }
    }

    return now.month;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Lib_RenewCalendar(void)
{
    uint8 day;

    if (now.second == 59)
    {
        now.second = 0;

        if (now.minute >= 59)
        {
            now.minute = 0;

            if (now.hour >= 23)
            {
                now.hour = 0;
                day = Fun_GetDay(now.month);                                    //取每月的天数
                if (now.day >= day)                                             //计时一个月
                {
                    now.day = 1;

                    if (now.month >= 12)                                        //计时一年
                    {
                        now.month = 1;
                        now.year++;
                    }
                    else
                    {
                        now.month++;
                    }
                }
                else
                {
                    now.day++;
                }
            }
            else
            {
                now.hour++;
            }
        }
        else
        {
            now.minute++;
        }
    }
    else
    {
        now.second++;
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
const _Time* Lib_GetTime(void)
{
    return &now;
}
/*-----------------------万年历部分结束-------------------*/

/*******************************************************************************
*功能：时间是否合法有效
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Lib_TimeIsValid(_Time *time)
{
    if (time->year > 2300 || time->year < 1900)
    {
        return false;
    }

    if (time->month > 12)
    {
        return false;
    }

    if (time->day > 32)
    {
        return false;
    }

    if (time->hour > 23)
    {
        return false;
    }

    if (time->second > 59)
    {
        return false;
    }

    return true;
}


/*******************************************************************************
*功能：  比较时间，获取最早时间
*参数：  无
*返回值：无
*说明：
*******************************************************************************/
uint16 Lib_GetEarlyTime(_Time time[], uint16 count)
{
    uint16 i;
    uint16 index;

    index = 0xFFFF;
    _Time tmp;
    memset(&tmp, 0x00, sizeof(_Time));

    for (i = 0; i < count; i++)
    {
        if (Lib_TimeIsValid(&time[i]) == false)
        {
            continue;
        }

        if (time[i].year > tmp.year)
        {
            index = i;
            tmp.year = time[i].year;
            continue;
        }
        else if (time[i].year < tmp.year)
        {
            continue;
        }

        if (time[i].month > tmp.month)
        {
            index = i;
            tmp.month = time[i].month;
            continue;
        }
        else if (time[i].month < tmp.month)
        {
            continue;
        }

        if (time[i].day > tmp.day)
        {
            index = i;
            tmp.day = time[i].day;
            continue;
        }
        else if (time[i].day < tmp.day)
        {
            continue;
        }

        if (time[i].hour > tmp.hour)
        {
            index = i;
            tmp.hour = time[i].hour;
            continue;
        }
        else if (time[i].hour < tmp.hour)
        {
            continue;
        }

        if (time[i].minute > tmp.minute)
        {
            index = i;
            tmp.minute = time[i].minute;
            continue;
        }
        else if (time[i].minute < tmp.minute)
        {
            continue;
        }

        if (time[i].second > tmp.second)
        {
            index = i;
            tmp.second = time[i].second;
            continue;
        }
        else if (time[i].second < tmp.second)
        {
            continue;
        }
    }

    return index;
}

/*******************************************************************************
*功能：  比较时间，获取最早时间
*参数：  无
*返回值：无
*说明：
*******************************************************************************/
uint16 Lib_GetLateTime(_Time time[], uint16 count)
{
    uint16 i;
    uint16 index;

    index = 0xFFFF;
    _Time tmp;
    memset(&tmp, 0xFF, sizeof(_Time));

    for (i = 0; i < count; i++)
    {
        if (Lib_TimeIsValid(&time[i]) == false)
        {
            continue;
        }

        if (time[i].year < tmp.year)
        {
            index = i;
            tmp.year = time[i].year;
            continue;
        }
        else if (time[i].year > tmp.year)
        {
            continue;
        }

        if (time[i].month < tmp.month)
        {
            index = i;
            tmp.month = time[i].month;
            continue;
        }
        else if (time[i].month > tmp.month)
        {
            continue;
        }

        if (time[i].day < tmp.day)
        {
            index = i;
            tmp.day = time[i].day;
            continue;
        }
        else if (time[i].day > tmp.day)
        {
            continue;
        }

        if (time[i].hour < tmp.hour)
        {
            index = i;
            tmp.hour = time[i].hour;
            continue;
        }
        else if (time[i].hour > tmp.hour)
        {
            continue;
        }

        if (time[i].minute < tmp.minute)
        {
            index = i;
            tmp.minute = time[i].minute;
            continue;
        }
        else if (time[i].minute > tmp.minute)
        {
            continue;
        }

        if (time[i].second < tmp.second)
        {
            index = i;
            tmp.second = time[i].second;
            continue;
        }
        else if (time[i].second > tmp.second)
        {
            continue;
        }
    }

    return index;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Lib_Init(void)
{
    now.year = 2016;
    now.month = 11;
    now.day = 16;
    now.hour = 15;
    now.minute = 34;
    now.second = 55;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 Lib_HexToChar(uint8 hex)
{
    if (hex <= 9)
    {
        hex += 0x30;
    }
    else if ((hex >= 10) && (hex <= 15))                                        //Capital
    {
        hex += 0x37;
    }

    return hex;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 Lib_CharToHex(uint8 bChar)
{
    if ((bChar >= 0x30) && (bChar <= 0x39))
    {
        bChar -= 0x30;
    }
    else if ((bChar >= 0x41) && (bChar <= 0x46))                                // Capital
    {
        bChar -= 0x37;
    }
    else if ((bChar >= 0x61) && (bChar <= 0x66))                                //littlecase
    {
        bChar -= 0x57;
    }
    else
    {
        bChar = 0xff;
    }
    
    return bChar;
}

/*******************************************************************************
*功能：将数字转化为BCD码
*参数：num - 要转换的数字
*返回：无
*说明：
*******************************************************************************/
uint8 Lib_NumToBCD(uint8 num)
{
    return (num / 10) << 4 | (num % 10);
}

/*******************************************************************************
*功能：将BCD码转化为数字
*参数：BCD - 要转化的BCD码
*返回：无
*说明：
*******************************************************************************/
uint8 Lib_BCDToNum(uint8 BCD)
{
    return ((BCD & 0x70) >> 4) * 10 + (BCD & 0x0F);
}


/*******************************************************************************
*功能：
*参数：  无
*返回值：无
*说明：  opt:0 向左偏移  1:向右偏移
*******************************************************************************/
void Lib_BuffShiftBytes(uint8 *dest, uint8 *src, uint16 len, uint8 opt)
{
    int16 i;

    if (opt == 0)                                                               //向左移动
    {
        for (i = 0; i < len; i++)
        {
            dest[i] = src[i];
        }
    }
    else if (opt == 1)                                                          //向右移动
    {
        for (i = len; i >= 0; i--)
        {
            dest[i] = src[i];
        }
    }

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年05月10日   ggggggg      创建文件
*******************************************************************************/

