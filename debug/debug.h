/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年05月20日
**  名   称：debug.h
**  摘   要：
*******************************************************************************/
#ifndef _DEBUG_H
#define _DEBUG_H

#pragma pack(1)
typedef struct
{
    uint8  DeviceType;                                                          //设备类型
    uint8  version;                                                             //版本
    uint16 reset;                                                               //复位次数
    uint8 online;                                                               //上线次数
    uint8 offline;                                                              //离线次数
    uint8 Exception_485_over_time;
    uint8 Exception_pump_work;
    uint8 Exception_forward;
    uint8 Exception_parameter;
    uint8 Exception_circuit_protect;
    uint8 Exception_net_commute;
    uint8 Exception_wireless_commute;
    uint8 Exception_coordinator;
    uint8 Exception_valve;
    uint8 Exception_flowmeter;
    uint8 Exception_cmd_lock;
    uint8 Exception_logic_lock;
    uint8 beat;                                                                 //发送心跳包次数
    uint8 ResBeat;
    uint8 Register;
    uint8 ResRegister;
    uint8 ResetException;
    uint8 ResResetException;
    uint8 ReportException;
    uint8 ResReportException;
    uint8 transparent;
    uint8 ResTransparent;
    uint8 SetTask;
    uint8 ResTask;
    uint8 TaskReport;
    uint8 privilege;
    uint8 ResPrivilege;
    uint8 CancelPrivilege;
    uint8 ResCancelPrivilege;
    uint8 forward;
    uint8 PlcRxForwardRequest;                                                  //plc收到其他dtu产生的、服务器转发的请求
    uint8 ResPlcRxForwardRequest;                                               //plc对于服务器转发的请求进行回复响应
    uint8 MicRxForwardResponse;                                                 //mic收到plc发出的、服务器转发的响应
    uint8 MicRxServerForwardRes;                                                //mic收到server产生的、转发请求的响应
    uint8 WtCfg;
    uint8 ResWtCfg;
    uint8 RdCfg;
    uint8 ResRdCfg;
    uint8 GprsStart;
    uint8 OverTimeNum;
    uint8 MaintainTmr;
    uint8 ProtectTmr;
    uint8 TrigProtect;
    uint8 NoGprsSendBuff;
    uint8 GprsSendParaErr;
    uint8 SearchGprsBuffErr;
    uint8 RdValve;                                                              //读阀门次数
    uint8 ResValve;                                                             //读阀门响应次数
    uint8 ValveChanged;
    uint8 RdFlowMeter;                                                          //读流量计次数
    uint8 ResFlowMeter;                                                         //读流量计响应次数
    uint8 RdTask;
    uint8 ResSubTask;
    uint8 SubTaskWait;
    uint8 SubTaskExecute;
    uint8 SubTaskOver;
    uint8 RdCommute;                                                            //读通信状态次数
    uint8 ResCommute;                                                           //读通信状态响应次数
    uint8 CloseValve;
    uint8 OpenPump;
    uint8 ClosePump;
    uint8 RdTransformer;
    uint8 ResTransformer;
    uint8 RdMotorBind;
    uint8 ResMotorBind;
    uint8 RS485RxErr;
    uint8 RS485RxTimeout;
    uint8 Send485Err;
    uint8 No485SendBuff;
    uint8 L0ProSlow;
    uint8 L1ProSlow;
    uint8 U0ProSlow;
}_Statics;
#pragma pack()

extern _Statics statics;

void Debug_Init(void);
void Debug_Printf(char *s, uint8 *buff, uint16 len, uint8 level, uint8 opt);
void Debug_Statics(uint16 offset, uint16 count, uint8 bytes);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0          2016年05月20日   巍图      创建文件
*******************************************************************************/
