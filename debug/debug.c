/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年05月20日
**  名   称：debug.c
**  摘   要：
*******************************************************************************/
#include "system.h"
#include "PHY_Usart.h"
#include "PHY_Timer.h"
#include "PHY_GprsSendTask.h"
#include "PHY_Gprs.h"
#include "debug.h"

#define SYS_VERSION                          1

/*测试报文缓存数量：1*/
#define MAX_TEST_BUFF_NUM                    1

typedef struct
{
    uint8 buff[200];
    uint16 len;
}_TestBuff;


_Statics statics;
static OS_TMR TestTmrId;

static uint16 TestTmrFiredNum;
static uint16 DebugSendCounter;

_TestBuff TestBuff[MAX_TEST_BUFF_NUM];

extern _Gprs Gprs;

/*******************************************************************************
*功能：
*参数：opt：0 输入为十六进制数字；1 输入为ASCII码
*返回：无
*说明：
*******************************************************************************/
void Debug_Printf(char *s, uint8 *buff, uint16 len, uint8 level, uint8 opt)
{
    uint16 length;
    uint16 i, j;
    _TestBuff *SendBuff;
    char StringEndFlg[] = "\r\n";

    if ((level & SysDebugInfoOut) == 0)
    {
        return;
    }

    SendBuff = &TestBuff[0];

    if (s != NULL && buff != NULL && len != 0)
    {
        length = strlen(s);

        if (opt == 0)
        {
            if ((length + len * 2 + 3) >= sizeof(SendBuff->buff))
            {
                return;
            }
        }
        else
        {
            if ((length + len + 3) >= sizeof(SendBuff->buff))                   //3个字符为 / r / n 0(字符串结束符
            {
                return;
            }
        }

        memset(SendBuff->buff, 0x00, sizeof(SendBuff->buff));
        sprintf((char *)SendBuff, "%s", s);

        for (i = 0, j = 0; i < len; i++)
        {
            if (opt == 0)
            {
                SendBuff->buff[j + length] = Lib_HexToChar(buff[i] >> 4);
                j++;

                SendBuff->buff[j + length] = Lib_HexToChar(buff[i] & 0x0F);
                j++;
            }
            else
            {
                SendBuff->buff[j + length] = buff[i];

                j++;
            }
        }

        SendBuff->len = j + length;
        memcpy(&SendBuff->buff[SendBuff->len], StringEndFlg, 2);
        SendBuff->len = SendBuff->len + 2;

        PHY_UsartSend(USART_232_CHANNEL, SendBuff->buff, SendBuff->len);

        return;
    }

    if (s != NULL && buff == NULL)
    {
        PHY_UsartSend(USART_232_CHANNEL, (uint8 *)s, strlen(s));

        return;
    }

    if (s == NULL && buff != NULL)
    {
        PHY_UsartSend(USART_232_CHANNEL, buff, len);
        PHY_UsartSend(USART_232_CHANNEL, "\r\n", sizeof("\r\n"));

        return;
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：根据初始化，定时器的触发周期为20秒
*******************************************************************************/
static void TestTmrHandler(void *p_tmr, void *p_arg)
{
    _IF msg;
    uint16 len;

    len = sizeof(statics);

    msg.head.version = CMD_VERSION;
    msg.head.cmd = CMD_DEBUG_MSG;
    msg.head.len = Lib_htons(len);
    memcpy(msg.buff, &statics, sizeof(statics));

    statics.DeviceType = cfg.MainDeviceType;

    /*每隔1小时，向服务器发送一帧调试信息*/
    if (DebugSendCounter > 180)
    {
        DebugSendCounter = 0;
        PHY_RequestGprsSendMsg((uint8 *)&msg, offsetof(_IF, buff) + len, CMD_DEBUG_MSG, 0xFF, 1, 0, false);
    }
    else
    {
        DebugSendCounter++;
    }

    statics.version = SYS_VERSION;

    Debug_Printf("test:", (uint8 *)&msg, offsetof(_IF, buff) + len, DEBUG_TEST_INFO, 0);

    TestTmrFiredNum++;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Debug_Init(void)
{
    OS_ERR err;

    OSTmrCreate(&TestTmrId,
                NULL,
                5 * SYS_SECOND_TICK_NUM,
                20 * SYS_SECOND_TICK_NUM,
                OS_OPT_TMR_PERIODIC,
                TestTmrHandler,
                NULL,
                &err);

    OSTmrStart(&TestTmrId, &err);

    DebugSendCounter = 0;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Debug_Statics(uint16 offset, uint16 count, uint8 bytes)
{
    uint8 *p_uint8;
    uint16 *p_uint16;

    switch (bytes)
    {
        case 1:
            {
                p_uint8 = (uint8 *)&statics + offset;

                *p_uint8 = *p_uint8 + count;

                break;
            }
        case 2:
            {
                p_uint16 = (uint16 *)&statics + offset;
                *p_uint16 = *p_uint16 + count;

                break;
            }
    }


    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0          2016年05月20日   巍图      创建文件
*******************************************************************************/
