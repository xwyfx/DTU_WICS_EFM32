/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年04月25日
**  名   称：update.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "update.h"

#include "PHY_GprsSendTask.h"
#include "PHY_Reset.h"
#include "PHY_Flash.h"

enum MessageType
{
    CMD_REQUEST_START_UPDATE     = 0x12,                                        //终端请求下载升级文件
    CMD_REQUEST_UPDATE_DATA      = 0x13,                                        //终端请求下载升级文件第N包
    CMD_UPDATE_FINISH            = 0x14,                                        //终端通知平台下载包全部接收完毕
    CMD_SERVER_UPDATE_RESPONSE   = 0x15,                                        //平台回复终端请求下载升级文件的请求
    CMD_SERVER_UPDATE_DATA       = 0x16                                         //平台回复终端请求下载升级文件第N包的请求，并发送下载包:
};

//要升级总的报文包数
uint16 TotalPackets = 0;

//升级报文每包的字节数
uint16 PktLen = 0;

//当前请求升级报文的序号
uint16 UpdateMsgSeq = 1;

#define HIBYTE(w)           ((uint8)(((uint16)(w) >> 8) & 0xFF))

const uint8 UpdateValidFlg[16] =  { 0xAA, 0xAB, 0xAC, 0xAD, 0x9A, 0x9B, 0x9C, 0x9D, 0x8A, 0x8B, 0x8C, 0x8D, 0x7A, 0x7B,   0x7C, 0x7D };

/*******************************************************************************
*功能：计算crc
*参数：char *ptr - 要校验字符串的缓冲区地址、uint32 len - 字符产长度
*返回：校验和
*说明：
*******************************************************************************/
static uint16 crc16(char *ptr, uint32 len)
{
    uint8 i;
    uint16 crc = 0xffff;
    while (len--)
    {
        crc = HIBYTE(crc) ^ *ptr;
        for (i = 0x80; i != 0; i >>= 1)
        {
            if ((crc & 0x0001) != 0)
            {
                crc >>= 1;
                crc ^= 0xa001;
            }
            else
            {
                crc >>= 1;
            }
        }
        ptr++;
    }

    return (crc);
}

/*******************************************************************************
*功能：发送请求下载更新文件第i包
*参数：请求下载文件的包号
*返回：无
*说明：
*******************************************************************************/
static void Fun_RequestUpdateMsg(uint16 seq)
{
    _UPDATE_REQUEST_DATA *UpdateRequestData;
    uint16 len;
    _IF msg;

    msg.head.version = CMD_VERSION;
    msg.head.cmd = CMD_REQUEST_UPDATE_DATA;                                     //请求升级报文
    msg.head.len = sizeof(_UPDATE_REQUEST_DATA);
    msg.head.len = Lib_htons(msg.head.len);

    UpdateRequestData = (_UPDATE_REQUEST_DATA *)(msg.buff);
    UpdateRequestData->seq = seq;

    len = offsetof(_IF, buff) + sizeof(_UPDATE_REQUEST_DATA);
    PHY_RequestGprsSendMsg((uint8 *)&msg, len, CMD_REQUEST_UPDATE_DATA, 0xFF, 5, 5, false);

    return;

}

/*******************************************************************************
*功能：发送下载更新文件完成
*参数：无
*返回：无
*说明：在下载全部更新文件包后，发送该报文
*******************************************************************************/
static void Fun_UpdateFinish(void)
{
    _IF msg;
    uint16 len;

    msg.head.version = CMD_VERSION;
    msg.head.cmd = CMD_UPDATE_FINISH;                                           //请求升级报文
    msg.head.len = 0;

    len = offsetof(_IF, buff);
    PHY_RequestGprsSendMsg((uint8 *)&msg, len, CMD_UPDATE_FINISH, 0xFF, 1, 0, true);

    return;
}

/*******************************************************************************
*功能：开始升级
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_StartUpdate(void)
{
    uint16 len;
    _UPDATE_REQUEST *UpdateRequest;
    _IF msg;

    if (cfg.update == false)
    {
        return;
    }

    /*初始化升级模块局部变量*/
    UpdateMsgSeq = 1;

    /*请求升级*/
    UpdateRequest = (_UPDATE_REQUEST *)msg.buff;

    msg.head.version = CMD_VERSION;
    msg.head.cmd = CMD_REQUEST_START_UPDATE;                                    //请求升级报文
    msg.head.len = sizeof(_UPDATE_REQUEST);
    msg.head.len = Lib_htons(msg.head.len);

    memcpy(UpdateRequest->IMEI, Gprs.IMEI, sizeof(Gprs.IMEI));
    UpdateRequest->RequestVersion = UpdateVersion;
    UpdateRequest->CurrentVersion = CMD_VERSION;

    len = offsetof(_IF, buff) + sizeof(_UPDATE_REQUEST);
    PHY_RequestGprsSendMsg((uint8 *)&msg, len, CMD_REQUEST_START_UPDATE, 0xFF, 5, 5, false);

    return;
}

/*******************************************************************************
*功能：升级报文处理程序
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_UpdateMessageHandler(_IF *msg)
{
    uint16 crc;
    uint16 i;
    uint8 buff[MAX_GPRS_RX_BUFF_LEN];
    _UPDATE_CFG UpdateCfg;

    _UPDATE_INFO *UpdateInfo;                                                   //服务器升级请求应答
    _UPDATE_DATA *UpdateData;                                                   //服务器升级数据包

    switch (msg->head.cmd)
    {
        case CMD_SERVER_UPDATE_RESPONSE:                                        //升级总体信息
        {
            PHY_SearchGprsSendBuff(CMD_REQUEST_START_UPDATE, NULL, 0);

            UpdateInfo = (_UPDATE_INFO *)(msg->buff);

            /* 判断版本号是否正确 */
            if (UpdateVersion == UpdateInfo->version)                           //如果版本号正确
            {
                TotalPackets = UpdateInfo->TotalPackets;
                PktLen =  UpdateInfo->PktLen;

                /*擦除代码缓存区*/
                for (i = 0x42; i < 0x80; i++)
                {
                    PHY_InterFlashErasePage(i);
                }

                /*请求第一包*/
                UpdateMsgSeq = 1;
                Fun_RequestUpdateMsg(UpdateMsgSeq);
            }
            else
            {
                PHY_Reset(0);
            }

            break;
        }
        case CMD_SERVER_UPDATE_DATA:                                            //升级报文体
        {
            PHY_SearchGprsSendBuff(CMD_REQUEST_UPDATE_DATA, NULL, 0);

            UpdateData = (_UPDATE_DATA *)(msg->buff);

            crc = crc16((char *)(UpdateData->data), PktLen);

            if (UpdateData->crc != crc)
            {
                Fun_RequestUpdateMsg(UpdateMsgSeq);

                break;
            }

            if (UpdateData->seq != UpdateMsgSeq)
            {
                Fun_RequestUpdateMsg(UpdateMsgSeq);

                break;
            }

            if (UpdateMsgSeq < TotalPackets)
            {
                /*保存升级报文到FLASH中*/
                PHY_InterFlashWrite(FIRMWARE_BUFF_ADDR + (UpdateMsgSeq - 1) * PktLen, UpdateData->data,  PktLen);
                PHY_InterFlashRead(FIRMWARE_BUFF_ADDR + (UpdateMsgSeq - 1) * PktLen, buff, PktLen);
                if (memcmp(buff, (uint8 *)UpdateData->data, PktLen) != 0)
                {
                    App_StartUpdate();

                    break;
                }

                /*请求下一包*/
                ++UpdateMsgSeq;
                Fun_RequestUpdateMsg(UpdateMsgSeq);
            }
            else if (UpdateMsgSeq == TotalPackets)
            {
                /* 保存最后一包数据 */
                PHY_InterFlashWrite(FIRMWARE_BUFF_ADDR + (UpdateMsgSeq - 1) * PktLen, UpdateData->data,  PktLen);


                /* 保存固件程序长度 */
                memcpy(UpdateCfg.UpdateValidFlg, UpdateValidFlg, sizeof(UpdateValidFlg));
                UpdateCfg.UpdateVersion = UpdateVersion;
                UpdateCfg.CurrentVersion = CMD_VERSION;
                UpdateCfg.CodeLen =  PktLen * TotalPackets;

                PHY_InterFlashErasePage(2);
                PHY_InterFlashWrite(UPDATE_CFG_ADDR,  (uint8 *)(&UpdateCfg),  sizeof(UpdateCfg));

                /*升级结束*/
                Fun_UpdateFinish();

                PHY_Reset(0);
            }

            break;
        }
        default:
        {
            break;
        }
    }

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年04月25日    巍图      创建文件
*******************************************************************************/


