/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年05月02日
**  名   称：update.h
**  摘   要：
*******************************************************************************/
#ifndef _UPDATE_H
#define _UPDATE_H

/*升级代码缓存存放位置*/
#define	FIRMWARE_BUFF_ADDR                0x00021000

/*升级配置信息存放地址*/
#define UPDATE_CFG_ADDR                   0x00001000

/*存储起来的的升级信息格式*/
typedef struct _UPDATE_CFG
{
    uint8  UpdateValidFlg[16];                                                  //存储的变量是否有效标志
    uint16 CurrentVersion;                                                      //正在运行的固件版本
    uint16 UpdateVersion;                                                       //升级程序空间中的目标固件版本
    uint32 CodeLen;                                                             //目标固件长度
}_UPDATE_CFG;


/*升级请求*/
typedef struct _UPDATE_REQUEST
{
    char IMEI[15];                                                              //终端地址
    uint8 CurrentVersion;                                                       //当前版本
    uint8 RequestVersion;                                                       //请求版本
}_UPDATE_REQUEST;


/*平台回复终端请求下载升级文件的请求*/
#pragma pack(1)
typedef struct _UPDATE_INFO
{
    uint16 PktLen;                                                              //每包字节数
    uint16 TotalPackets;                                                        //总包数
    uint8  version;                                                             //请求的版本
}_UPDATE_INFO;
#pragma pack()

/* 终端请求下载升级文件第N包 */
typedef struct _SERVER_INFO
{
    uint16 seq;                                                                 //请求数据包的序列号
}_UPDATE_REQUEST_DATA;


/* 平台回复终端请求下载升级文件第N包的请求，并发送下载包 */
#pragma pack(1)
typedef struct _UPDATE_DATA
{
    uint16 seq;                                                                 //数据包编号
    uint8  data[512];                                                           //每包字节数
    uint16 crc;
}_UPDATE_DATA;
#pragma pack()

void App_StartUpdate(void);
void App_UpdateMessageHandler(_IF *pMessage);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年05月02日    巍图      创建文件
*******************************************************************************/
