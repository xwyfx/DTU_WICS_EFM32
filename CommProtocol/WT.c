/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年12月20日
**  名   称：WT.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#ifdef ENABLE_PROTOCOL_MODBUS

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool WT_ReceivedRrocess(_SysMsg *msg)
{
    bool result;

    if (msg->len <= 2)
    {
        return false;
    }

    result = CRC16_Check(msg->buff, &msg->buff[msg->len - 2],  msg->len - 2);

    if (result == true)
    {
        msg->len = msg->len - 2;
        msg->protocol = PROTOCOL_WT;
    }

    return result;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool WT_SendProcess(_SysMsg *msg)
{
    uint16 crc;

    if ((msg->len + 2) >= sizeof(msg->buff))
    {
        return false;
    }

    crc = CRC16_Process(msg->buff, msg->len);
    memcpy(&msg->buff[msg->len], &crc, 2);

    msg->len = msg->len + 2;

    return true;
}

#endif

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年12月20日    巍图      创建文件
*******************************************************************************/
