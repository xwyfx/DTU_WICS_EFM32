/*******************************************************************************
**                 中科院沈阳自动化研究所 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作    者：巍图
**  日    期：2013年03月20日
**  文件名称：CommProtocol.c
**  摘    要：
*******************************************************************************/
#include "system.h"
#include "ModBusRtu.h"
#include "WT.h"

/*物理层通信接口结构体声明*/
typedef struct _PCIF
{
    uint16 id;
    bool (*ReceiveProcess)(_SysMsg *msg);
    bool (*SendProcess)(_SysMsg *msg);
}_PCIF;

#define IF_FUN_LIST(id, received, send) {id, received, send},

/*物理层接口结构体数组*/
const _PCIF PCIF[] =
{
#include "ModBusRtu_def.h"
#include "WT_def.h"
};

/*******************************************************************************
*功能：
*参数：  无
*返回值：无
*******************************************************************************/
bool CPR_ReceivedProcess(_SysMsg *msg)
{
    uint8 i;
    bool result;
    uint8 num;

    num = sizeof(PCIF) / sizeof(_PCIF);
    result = false;

    for (i = 0; i < num; i++)
    {
        if (PCIF[i].ReceiveProcess != NULL)
        {
            result = (*(PCIF[i].ReceiveProcess))(msg);
        }

        if (result == true)
        {
            return true;
        }
    }

    msg->protocol = PROTOCOL_NONE;

    return false;
}

/*******************************************************************************
*功能：
*参数：  无
*返回值：无
*******************************************************************************/
bool CPR_SendProcess(_SysMsg *msg)
{
    uint8 i;
    bool result;
    uint8 num;

    num = sizeof(PCIF) / sizeof(_PCIF);
    result = false;

    if (msg->protocol == PROTOCOL_NONE)                                         //无通信协议处理
    {
        return true;
    }

    for (i = 0; i < num; i++)
    {
        if (PCIF[i].id != msg->protocol)
        {
            continue;
        }

        if (PCIF[i].SendProcess != NULL)
        {
            result = (*(PCIF[i].SendProcess))(msg);

            break;
        }
    }

    if (i >= num)
    {
        return false;
    }

    return result;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0          2013年03月20日   巍图      创建文件
*******************************************************************************/

