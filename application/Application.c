/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年01月10日
**  名   称：Application.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "PHY_GprsSendTask.h"
#include "PHY_Gprs.h"

#include "Application.h"

#include "debug.h"

/*
单次下发的子任务最大数量：20
*/

_WorkTaskList WorkTaskList;
_485BusStatics BusStatics;                                                      //485总线统计

/*******************************************************************************
*功能：将异常上报server
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_ReportExceptionToServer(uint16 ExceptionNum, uint8 *buff, uint16 len)
{
    _IF msg;
    _ExceptionReport *ExceptionReport;

    if (len >= MAX_BUFF_LEN && buff != NULL)
    {
        return;
    }

    msg.head.version = CMD_VERSION;
    msg.head.cmd = CMD_REPORT_EXCEPTION;

    ExceptionReport = (_ExceptionReport *)msg.buff;

    ExceptionReport->ExceptionNum = ExceptionNum;
    if (buff != NULL && len != 0)
    {
        msg.head.len = Lib_htons(len + 1);
        memcpy(ExceptionReport->buff, buff, len);
        len = offsetof(_IF, buff) + offsetof(_ExceptionReport, buff) + len;
    }
    else
    {
        msg.head.len = Lib_htons(1);
        len = offsetof(_IF, buff) + offsetof(_ExceptionReport, buff);
    }

    PHY_RequestGprsSendMsg((uint8 *)&msg, len, CMD_REPORT_EXCEPTION, 0xFF, 5, 20, true);

    /*统计异常状态*/
    switch (ExceptionNum)
    {
        case EXCEPTION_485_OVER_TIME:                                           //485通信超时异常；严重异常
            {
                statics.Exception_485_over_time = 1;

                break;
            }
        case EXCEPTION_PUMP_WORK:                                               //水泵工作异常；严重异常
            {
                statics.Exception_pump_work = 1;

                break;
            }
        case EXCEPTION_FORWARD:                                                 //转发异常；普通异常
            {
                statics.Exception_forward = 1;

                break;
            }
        case EXCEPTION_PARAMETER:                                               //参数异常；严重异常
            {
                statics.Exception_parameter = 1;

                break;
            }
        case EXCEPTION_CIRCUIT_PROTECT:                                         //电路保护异常；严重异常
            {
                statics.Exception_circuit_protect = 1;

                break;
            }
        case EXCEPTION_NET_COMMUTE:                                             //网络通信异常；普通异常
            {
                statics.Exception_net_commute = 1;

                break;
            }
        case EXCEPTION_WIRELESS_COMMUTE:                                        //无线通信异常；普通异常
            {
                statics.Exception_wireless_commute = 1;

                break;
            }
        case EXCEPTION_COORDINATOR:                                             //协调器异常；普通异常
            {
                statics.Exception_coordinator = 1;

                break;
            }
        case EXCEPTION_VALVE:                                                   //阀门异常；普通异常
            {
                statics.Exception_valve = 1;

                break;
            }
        case EXCEPTION_FLOWMETER:                                               //流量计异常；普通异常
            {
                statics.Exception_flowmeter = 1;

                break;
            }
        case EXCEPTION_CMD_LOCK:                                                //指令锁定异常；普通异常
            {
                statics.Exception_cmd_lock= 1;

                break;
            }
        case EXCEPTION_LOGIC_LOCK:                                              //逻辑锁定异常；普通异常
            {
                statics.Exception_logic_lock = 1;

                break;
            }
        default:
            {
                break;
            }
    }

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年01月10日    巍图      创建文件
*******************************************************************************/
