/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年12月05日
**  名   称：App_MIC.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "Application.h"

#include "PHY_GprsSendTask.h"
#include "PHY_Gprs.h"

#include "PHY_Reset.h"
#include "PHY_Usart.h"

#include "ModBusRtu.h"
#include "Modbus.h"
#include "debug.h"

#define DEFAULT_MIC_SLAVE_ADDR                         200
#define MIC_POLL_PERIOD                                20
#define MAX_GPRS_COMMUTE_FAIL_NUM                      10


/*寄存器地址定义*/
enum
{
    REG_SUB_TASK_STATE    = 39,                                                 //MIC子任务状态
    REG_VALVE_STATE       = 19,                                                 //电磁阀状态
    REG_FLOWMETER         = 13,                                                 //流量计
    REG_DO1_OUTPUT_MODE   = 20,
    REG_DO3_OUTPUT_MODE   = 28,
};


/*MIC工作状态格式*/
typedef struct
{
    uint16 ValveState;                                                          //水泵状态
    uint16 FlowMeterValue;                                                      //流量计值
    bool   FirstGetFlowMeter;
}_MicState;


static OS_TMR MicPollTmr;                                                       //MIC轮询定时器

static bool MicModuleIsInit;                                                    //MIC模块是否被初始化

static _MicState MicState;                                                      //MIC设备状态

/*异常计数器*/
static uint16 Exception_485OverTimeCounter;                                     //485通信异常次数；严重异常
static uint16 Exception_ValveCounter;                                           //流量计异常；普通异常
static uint16 Exception_FlowMeterCounter;                                       //流量计异常；普通异常
static uint16 Exception_LockCmdCounter;
static uint16 Exception_PrivilegeCmdCounter;
static uint16 Exception_GprsCommuteCounter;


/*异常处理方式*/
static uint8 ProcessMode_485OverTimeException;                                  //485总线超时的处理方式
static uint8 ProcessMode_ForwardException;                                      //转发异常的处理方式
static uint8 ProcessMode_NetCommuteException;                                   //网络通信异常处理方式
static uint8 ProcessMode_ValveException;                                        //阀门异常处理方式
static uint8 ProcessMode_FlowMeterException;                                    //流量计异常处理方式
static uint8 ProcessMode_CmdLockException;                                      //指令锁定异常处理方式
static uint8 ProcessMode_LogicLockException;                                    //逻辑锁定异常

/*锁定*/
static bool Lock_485OverTime;
static bool Lock_PrivilegeCmd;
static bool Lock_ParameterException;

/*转发报文序号*/
static uint8 ForwardToBindDtuSeq;

static void Fun_TerminalSubTask(uint8 reason);
static void Fun_SendSubTaskReport(void);
static void Fun_CheckGprsCommuteException(void);
static void Fun_ReportExceptionToServerFail(_GprsSendBuff *SrcMsg);

/*-------------------485总线统计部分------------------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SendStatics(void)
{
    BusStatics.send++;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CorrectReceiveStatics()
{
    BusStatics.CorrectReceive++;
    BusStatics.ContinueErr = 0;

    /*正确接收：将超时参数清零*/
    Exception_485OverTimeCounter = 0;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ErrReceiveStatics(void)
{
    BusStatics.ErrReceive++;

    if (BusStatics.ContinueErr < MAX_CONTINUE_RECEIVE_ERR)
    {
        BusStatics.ContinueErr++;
    }

    Debug_Statics(offsetof(_Statics, RS485RxErr), 1, sizeof(statics.RS485RxErr));

    return;
}

/*******************************************************************************
*功能：超时统计
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_TimeOverStatics(void)
{
    BusStatics.TimeOver++;

    Debug_Statics(offsetof(_Statics, RS485RxTimeout), 1, sizeof(statics.RS485RxTimeout));

    if (ProcessMode_485OverTimeException == EXCEPTION_NO_MONITOR)
    {
        Exception_485OverTimeCounter = 0;

        return;
    }

    if (Exception_485OverTimeCounter >= EXCEPTION_485_OVER_TIME_NUM)
    {
        /*锁定*/
        Lock_485OverTime = true;

        /*发生485总线通信异常时，停止当前灌溉逻辑*/
        Fun_TerminalSubTask(TASK_STATE_OVER_TIME);

        /*向服务器发送通信异常*/
        App_ReportExceptionToServer(EXCEPTION_485_OVER_TIME, NULL, 0);
    }
    else
    {
        Exception_485OverTimeCounter++;
    }

    return;
}

/*-------------------485部分------------------*/
/*******************************************************************************
*功能：请求485发送
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_Request485BusSendMsg(_SysMsg *msg)
{
    bool result;

    /*发生485总线超时锁定时，不再向下发送485报文*/
    if (Lock_485OverTime == true)
    {
        return false;
    }

    msg->port = USART_485_CHANNEL;
    result = PHY_UsartSendMsg(msg);

    Fun_SendStatics();

    if (result == false)
    {
        Debug_Statics(offsetof(_Statics, Send485Err), 1, sizeof(statics.Send485Err));
    }

    if (result == false)
    {
        BusStatics.GetBuffErr++;
    }
    else
    {
        BusStatics.GetBuffErr = 0;
    }

    if (BusStatics.GetBuffErr >= 100)
    {
        PHY_Reset(0);
    }

    return result;
}

/*-------------------子任务部分------------------*/
/*******************************************************************************
*功能：查找下一个需要执行的子任务
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_GetNextSubTask(void)
{
    OS_ERR err;

    if (WorkTaskList.HaveSubTask == false)
    {
        return false;
    }

    if (WorkTaskList.TaskListPtr >= WorkTaskList.len)                           //所有子任务处理完成
    {
        WorkTaskList.HaveSubTask = false;

        return false;
    }

    /*读取待工作子任务*/
    WorkTaskList.ServerTaskPtr = (_ServerTask *)&WorkTaskList.TaskList[WorkTaskList.TaskListPtr];

    if (WorkTaskList.ServerTaskPtr->OverTime != 0)                              //具有超时控制
    {
        WorkTaskList.SubTaskOverTime = WorkTaskList.ServerTaskPtr->OverTime * 60 * SYS_SECOND_TICK_NUM;
        WorkTaskList.SubTaskStartTime = OSTimeGet(&err);
    }
    else
    {
        WorkTaskList.SubTaskOverTime = 0;
        WorkTaskList.SubTaskStartTime = 0;
    }

    WorkTaskList.SubTaskIndex++;
    WorkTaskList.IsExecuteSubTask = true;
    WorkTaskList.TaskListPtr = WorkTaskList.TaskListPtr + WorkTaskList.ServerTaskPtr->len + offsetof(_ServerTask, buff);

    return true;
}

/*******************************************************************************
*功能：执行子任务
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ExecuteSubTask(void)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    uint16 SubTaskRegValue;

    /*清空子任务状态寄存器*/
    SendMsg = (_TxBuff *)msg.buff;

    SubTaskRegValue = STATE_REG_WAITING;

    msg.protocol = PROTOCOL_MODBUS;
    SendMsg->type = SEND_CLEAR_TASK_STATE_MSG;
    Modbus_WriteReg(&SendMsg->buff, DEFAULT_MIC_SLAVE_ADDR, REG_SUB_TASK_STATE, &SubTaskRegValue, 1, MODBUS_RTU, &msg.len);
    Fun_Request485BusSendMsg(&msg);

    /*请求485总线发送*/
    SendMsg = (_TxBuff *)msg.buff;
    SendMsg->len = WorkTaskList.ServerTaskPtr->len;
    SendMsg->type = SEND_TASK_MSG;
    memcpy(&SendMsg->buff, WorkTaskList.ServerTaskPtr->buff, SendMsg->len);

    msg.protocol = PROTOCOL_NONE;
    msg.len = SendMsg->len;
    Fun_Request485BusSendMsg(&msg);

    return;
}

/*******************************************************************************
*功能：中止子任务
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_TerminalSubTask(uint8 reason)
{
    if (WorkTaskList.IsExecuteSubTask == false)
    {
        return;
    }

    WorkTaskList.IsExecuteSubTask = false;

    if (WorkTaskList.SubTaskIndex >= MAX_SUB_TASK_NUM)
    {
        PHY_Reset(0);                                                           //异常
    }

    /*填充异常响应*/
    WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].result = reason;
    WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].SubTaskStateReg = 0;

    /*填充未被执行的子任务状态*/
    while (Fun_GetNextSubTask() == true)
    {
        if (WorkTaskList.SubTaskIndex >= MAX_SUB_TASK_NUM)
        {
            PHY_Reset(0);                                                       //异常
        }

        WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].result = TASK_STATE_CANCEL;
        WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].SubTaskStateReg = 0;
    }

    WorkTaskList.HaveSubTask = false;
    WorkTaskList.IsExecuteSubTask = false;

    Fun_SendSubTaskReport();

    return;
}

/*******************************************************************************
*功能：发送子任务完成报告
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SendSubTaskReport(void)
{
    _IF msg;
    uint16 len;

    len = (WorkTaskList.SubTaskIndex + 1) * sizeof(_SubTaskState) + 1;
    msg.head.version = CMD_VERSION;
    msg.head.cmd = CMD_SET_WORK_TASK_REPORT;
    msg.head.len = Lib_htons(len);

    if (WorkTaskList.SubTaskIndex >= MAX_SUB_TASK_NUM)
    {
        return;
    }

    len = len + offsetof(_IF, buff);

    WorkTaskList.WorkTaskState.WorkId = WorkTaskList.id;
    memcpy(msg.buff, &WorkTaskList.WorkTaskState, len - 1);
    PHY_RequestGprsSendMsg((uint8 *)&msg,
                           len,
                           CMD_SET_WORK_TASK_REPORT,
                           0xFF,
                           1,
                           0,
                           false);

    return;
}

/*******************************************************************************
*功能：读取子任务状态寄存器回调函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ReadSubTaskRegHandler(_SysMsg *msg)
{
    _Modbus *reply;
    _CMD3Reply *Cmd3Reply;
    uint16 SubTaskState;

    reply = (_Modbus *)msg->buff;
    Cmd3Reply = (_CMD3Reply *)reply->UsartType.pdu.data;
    SubTaskState = Cmd3Reply->data[0];

    if (WorkTaskList.IsExecuteSubTask == false)
    {
        return;
    }

    SubTaskState = Lib_htons(SubTaskState);

    switch (SubTaskState)
    {
        case STATE_REG_WAITING:
            {
                Debug_Statics(offsetof(_Statics, SubTaskWait), 1, sizeof(statics.SubTaskWait));

                return;
            }
        case STATE_REG_EXECUTING:
            {
                Debug_Statics(offsetof(_Statics, SubTaskExecute), 1, sizeof(statics.SubTaskExecute));

                return;
            }
        case STATE_REG_OVER:
            {
                Debug_Statics(offsetof(_Statics, SubTaskOver), 1, sizeof(statics.SubTaskOver));

                if (WorkTaskList.SubTaskIndex < MAX_SUB_TASK_NUM)
                {
                    WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].result = TASK_STATE_SUCCESS;
                    WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].SubTaskStateReg = STATE_REG_OVER;
                }
                else
                {
                    PHY_Reset(0);
                }

                if (Fun_GetNextSubTask() == false)
                {
                    WorkTaskList.IsExecuteSubTask = false;
                    Fun_SendSubTaskReport();                                    //发送子任务报告
                }
                else
                {
                    Fun_ExecuteSubTask();                                       //执行下一个子任务
                }

                return;
            }
    }

    return;
}

/*-------------------MIC部分------------------*/
/*******************************************************************************
*功能：读取流量计
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ReadFlowMeter(void)
{
    _SysMsg msg;
    _TxBuff *SendMsg;

    SendMsg = (_TxBuff *)msg.buff;

    /*读流量计状态*/
    Modbus_ReadReg(&SendMsg->buff, DEFAULT_MIC_SLAVE_ADDR, REG_FLOWMETER, 2, MODBUS_RTU, &msg.len);

    msg.protocol = PROTOCOL_MODBUS;
    SendMsg->type = SEND_POLL_MSG;
    Fun_Request485BusSendMsg(&msg);

    return;
}

/*******************************************************************************
*功能：关闭阀门
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CloseValve()
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    uint16 CloseValveAction[6];

    /*尝试关闭阀门*/
    Debug_Statics(offsetof(_Statics, CloseValve), 1, sizeof(statics.CloseValve));

    /*关闭阀门0*/
    SendMsg = (_TxBuff *)msg.buff;

    CloseValveAction[0] = 1;                                                    //定时方式

    CloseValveAction[1] = 0;                                                    //预约时间
    CloseValveAction[2] = 0;
    CloseValveAction[3] = 0;

    CloseValveAction[4] = 0;                                                    //开启至关闭时间

    msg.protocol = PROTOCOL_MODBUS;
    SendMsg->type = SEND_CLEAR_TASK_STATE_MSG;
    Modbus_WriteReg(&SendMsg->buff, DEFAULT_MIC_SLAVE_ADDR, REG_DO1_OUTPUT_MODE, CloseValveAction, 5, MODBUS_RTU, &msg.len);
    Fun_Request485BusSendMsg(&msg);

    /*关闭阀门1*/
    SendMsg = (_TxBuff *)msg.buff;

    CloseValveAction[0] = 1;                                                    //定时方式

    CloseValveAction[1] = 0;                                                    //预约时间
    CloseValveAction[2] = 0;
    CloseValveAction[3] = 0;

    CloseValveAction[4] = 0;                                                    //开启至关闭时间

    msg.protocol = PROTOCOL_MODBUS;
    SendMsg->type = SEND_CLEAR_TASK_STATE_MSG;
    Modbus_WriteReg(&SendMsg->buff, DEFAULT_MIC_SLAVE_ADDR, REG_DO3_OUTPUT_MODE, CloseValveAction, 5, MODBUS_RTU, &msg.len);
    Fun_Request485BusSendMsg(&msg);

    return;
}


/*******************************************************************************
*功能：电磁阀状态改变事件
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ValveStateChangedEvent(_SysMsg *msg)
{
    _Modbus *reply;
    _CMD3Reply *Cmd3Reply;
    uint16 ValveState;
    _IF buff;
    uint16 len;
    _GprsSendBuff *TxBuff;

    _ForwardToBindDTU *ForwardToBindDTU;

    reply = (_Modbus *)msg->buff;
    Cmd3Reply = (_CMD3Reply *)reply->UsartType.pdu.data;

    ValveState = Cmd3Reply->data[0];
    ValveState = Lib_htons(ValveState);

    /*将两个阀统一看成一个*/
    if (ValveState)
    {
        ValveState = 1;
    }
    else
    {
        ValveState = 0;
    }

    if (MicState.ValveState != ValveState)
    {
        MicState.ValveState = ValveState;

        Debug_Statics(offsetof(_Statics, ValveChanged), 1, sizeof(statics.ValveChanged));


        /*为了防止plc端误统计，将上次阀位状态改变事件删除，不再进行发送*/
        ForwardToBindDTU = (_ForwardToBindDTU *)buff.buff;
        memcpy(ForwardToBindDTU->IMEI, cfg.BindDtuList, sizeof(ForwardToBindDTU->IMEI));
        ForwardToBindDTU->seq = ForwardToBindDtuSeq;

        if (ValveState == 0)
        {
            ForwardToBindDTU->ValveState = 1;
        }
        else
        {
            ForwardToBindDTU->ValveState = 0;
        }

        TxBuff = PHY_SearchGprsSendBuff(CMD_FORWARD_TO_BIND_DTU, (uint8 *)ForwardToBindDTU, sizeof(_ForwardToBindDTU));

        while (TxBuff != NULL)
        {
            TxBuff = PHY_SearchGprsSendBuff(CMD_FORWARD_TO_BIND_DTU, (uint8 *)ForwardToBindDTU, sizeof(_ForwardToBindDTU));
        }

        /*转发至关联dtu*/
        buff.head.version = CMD_VERSION;
        buff.head.cmd = CMD_FORWARD_TO_BIND_DTU;
        buff.head.len = Lib_htons(sizeof(_ForwardToBindDTU));

        ForwardToBindDtuSeq = ForwardToBindDtuSeq + 1;

        ForwardToBindDTU = (_ForwardToBindDTU *)buff.buff;

        memcpy(ForwardToBindDTU->IMEI, cfg.BindDtuList, sizeof(ForwardToBindDTU->IMEI));
        ForwardToBindDTU->seq = ForwardToBindDtuSeq;
        if (ValveState == 0)
        {
            ForwardToBindDTU->ValveState = 0;
        }
        else
        {
            ForwardToBindDTU->ValveState = 1;
        }

        len = offsetof(_IF, buff) + sizeof(_ForwardToBindDTU);
        PHY_RequestGprsSendMsg((uint8 *)&buff, len, CMD_FORWARD_TO_BIND_DTU, 0xFF, 5, 20, true);
    }

    return;
}

/*-------------------异常检查部分------------------*/

/*******************************************************************************
*功能：检查特权指令异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckPrivilegeCmdException(void)
{
    if (ProcessMode_LogicLockException == EXCEPTION_NO_MONITOR)
    {
        Exception_PrivilegeCmdCounter = 0;

        return;
    }

    if (Exception_PrivilegeCmdCounter >= EXCEPTION_PRIVILEGE_CMD_NUM)           //检查是否超过特权指令上限
    {
        App_ReportExceptionToServer(EXCEPTION_LOGIC_LOCK, NULL, 0);             //向服务器发送特权指令锁定异常

        Exception_PrivilegeCmdCounter = 0;
    }
    else
    {
        Exception_PrivilegeCmdCounter++;
    }

    return;
}

/*******************************************************************************
*功能：检查指令锁定异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckLockCmdException(void)
{
    if (ProcessMode_CmdLockException == EXCEPTION_NO_MONITOR)
    {
        Exception_LockCmdCounter = 0;

        return;
    }

    if (Exception_LockCmdCounter >= EXCEPTION_LOCK_CMD_NUM)                     //判断是否超过指令锁定上限
    {
        App_ReportExceptionToServer(EXCEPTION_CMD_LOCK, NULL, 0);               //向服务器发送指令锁定异常

        Exception_LockCmdCounter = 0;
    }
    else
    {
        Exception_LockCmdCounter++;
    }

    return;
}

/*******************************************************************************
*功能：检查阀门异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckValveExcetion(_SysMsg *SrcMsg)
{
    _Modbus *reply;
    _CMD3Reply *Cmd3Reply;
    uint16 value1;
    uint16 value2;
    uint16 value;

    reply = (_Modbus *)SrcMsg->buff;
    Cmd3Reply = (_CMD3Reply *)reply->UsartType.pdu.data;

    value1 = Cmd3Reply->data[0];
    value1 = Lib_htons(value1);

    value2 = Cmd3Reply->data[1];
    value2 = Lib_htons(value2);

    value = value1 + value2;

    if (MicState.FirstGetFlowMeter == true)
    {
        MicState.FlowMeterValue = value;
        MicState.FirstGetFlowMeter = false;

        return;
    }

    if (MicState.ValveState == 0)
    {
        if (ProcessMode_ValveException == EXCEPTION_NO_MONITOR)
        {
            Exception_ValveCounter = 0;
            MicState.FlowMeterValue = value;

            return;
        }

        /*阀已经关闭，但流量计值依然在增长*/
        if (MicState.FlowMeterValue != value)
        {
            if (Exception_ValveCounter >= cfg.ValveExceptionLimit)
            {
                Exception_ValveCounter = 0;

                /*向服务器发送阀门异常*/
                App_ReportExceptionToServer(EXCEPTION_VALVE, NULL, 0);
            }
            else
            {
                Exception_ValveCounter++;
            }
        }
        else
        {
            Exception_ValveCounter = 0;
        }
    }
    else
    {
        if (ProcessMode_FlowMeterException == EXCEPTION_NO_MONITOR)
        {
            Exception_FlowMeterCounter = 0;
            MicState.FlowMeterValue = value;

            return;
        }

        /*阀已经打开，但流量计值不增长*/
        if (MicState.FlowMeterValue == value)
        {
            if (Exception_FlowMeterCounter >= cfg.FlowMeterExceptionLimit)
            {
                Exception_FlowMeterCounter = 0;
                Fun_CloseValve();

                /*向服务器发送流量计异常*/
                App_ReportExceptionToServer(EXCEPTION_FLOWMETER, NULL, 0);
            }
            else
            {
                Exception_FlowMeterCounter++;
            }
        }
        else
        {
            Exception_FlowMeterCounter = 0;
        }
    }

    MicState.FlowMeterValue = value;

    return;
}

/*******************************************************************************
*功能：检查gprs通信异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckGprsCommuteException(void)
{
    if (ProcessMode_NetCommuteException == EXCEPTION_NO_MONITOR)
    {
        Exception_GprsCommuteCounter = 0;

        return;
    }

    if (Exception_GprsCommuteCounter <= MAX_GPRS_COMMUTE_FAIL_NUM)
    {
        Exception_GprsCommuteCounter++;
    }
    else
    {
        App_ReportExceptionToServer(EXCEPTION_NET_COMMUTE, NULL, 0);

        Exception_GprsCommuteCounter = 0;
    }

    return;
}

/*******************************************************************************
*功能：轮询mic定时器回调函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_MicPollTmrHandler(void *p_tmr, void *p_arg)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    OS_ERR err;
    uint32 tick;

    if (Lock_PrivilegeCmd == true)
    {
        return;
    }

    SendMsg = (_TxBuff *)msg.buff;

    /*读电磁阀状态*/
    Modbus_ReadReg(&SendMsg->buff, DEFAULT_MIC_SLAVE_ADDR, REG_VALVE_STATE, 1, MODBUS_RTU, &msg.len);

    msg.protocol = PROTOCOL_MODBUS;
    SendMsg->type = SEND_POLL_MSG;
    Fun_Request485BusSendMsg(&msg);

    Debug_Statics(offsetof(_Statics, RdValve), 1, sizeof(statics.RdValve));

    /*读流量计状态*/
    Debug_Statics(offsetof(_Statics, RdFlowMeter), 1, sizeof(statics.RdFlowMeter));
    Fun_ReadFlowMeter();

    /*根据当前是否正在执行子任务来决定是否读子任务状态寄存器*/
    if (WorkTaskList.IsExecuteSubTask == true)
    {
        if (WorkTaskList.ServerTaskPtr->TaskType == SUB_TASK_TIMING)            //定时任务
        {
            if (WorkTaskList.ServerTaskPtr->OverTime != 0)                      //超时时间有效
            {
                tick = OSTimeGet(&err);
                if (tick >= WorkTaskList.SubTaskStartTime)                      //判断是否超时
                {
                    if ((tick - WorkTaskList.SubTaskStartTime) >= WorkTaskList.SubTaskOverTime)
                    {
                        /*子任务超时*/
                        Fun_TerminalSubTask(TASK_STATE_OVER_TIME);              //结束所有子任务逻辑

                        return;
                    }
                }
                else
                {
                    if ((WorkTaskList.SubTaskStartTime - tick) >= WorkTaskList.SubTaskOverTime)
                    {
                        /*子任务超时*/
                        Fun_TerminalSubTask(TASK_STATE_OVER_TIME);              //结束所有子任务逻辑

                        return;
                    }
                }
            }
        }

        Debug_Statics(offsetof(_Statics, RdTask), 1, sizeof(statics.RdTask));

        Modbus_ReadReg(&SendMsg->buff, DEFAULT_MIC_SLAVE_ADDR, REG_SUB_TASK_STATE, 1, MODBUS_RTU, &msg.len);
        Fun_Request485BusSendMsg(&msg);
    }

    return;
}

/*-------------------异常处理部分------------------*/

/*******************************************************************************
*功能：向服务器上报异常失败
*参数：无
*返回：无
*说明：严重异常不处理；普通异常清空异常计数器
*******************************************************************************/
static void Fun_ReportExceptionToServerFail(_GprsSendBuff *SrcMsg)
{
    _IF *msg;
    _ExceptionReport *ExceptionReport;

    msg = (_IF *)SrcMsg->buff;

    ExceptionReport = (_ExceptionReport *)msg->buff;

    switch (ExceptionReport->ExceptionNum)
    {
        case EXCEPTION_485_OVER_TIME:                                           //485通信超时异常；严重异常
            {
                break;
            }
        case EXCEPTION_PUMP_WORK:                                               //水泵工作异常；严重异常
            {
                break;
            }
        case EXCEPTION_FORWARD:                                                 //转发异常；普通异常
            {
                break;
            }
        case EXCEPTION_PARAMETER:                                               //参数异常；严重异常
            {
                break;
            }
        case EXCEPTION_CIRCUIT_PROTECT:                                         //电路保护异常；严重异常
            {
                break;
            }
        case EXCEPTION_NET_COMMUTE:                                             //网络通信异常；普通异常
            {
                break;
            }
        case EXCEPTION_WIRELESS_COMMUTE:                                        //无线通信异常；普通异常
            {
                break;
            }
        case EXCEPTION_COORDINATOR:                                             //协调器异常；普通异常
            {
                break;
            }
        case EXCEPTION_VALVE:                                                   //阀门异常；普通异常
            {
                break;
            }
        case EXCEPTION_FLOWMETER:                                               //流量计异常；普通异常
            {
                break;
            }
        case EXCEPTION_CMD_LOCK:                                                //指令锁定异常；普通异常
            {
                break;
            }
        case EXCEPTION_LOGIC_LOCK:                                              //逻辑锁定异常；普通异常
            {
                break;
            }
        default:
            {
                break;
            }
    }

    return;
}

/*---------------------外部接口函数-------------------*/

/*******************************************************************************
*功能：转发响应（服务器下发）
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_MicForwardToBindDtuRespose(_IF *msg, uint16 len)
{
    _ForwardToBindDTU *ForwardToBindDtuResponse;
    _GprsSendBuff *TxBuff;

    if (MicModuleIsInit == false)
    {
        return;
    }

    msg->head.len = Lib_htons(msg->head.len);
    if ((msg->head.len + offsetof(_IF, buff)) != len)
    {
        return;
    }

    if (msg->head.len != sizeof(_ForwardToBindDTU))
    {
        return;
    }

    /*搜索搜索gprs发送缓存，停止重传*/
    ForwardToBindDtuResponse = (_ForwardToBindDTU *)msg->buff;
    TxBuff = PHY_SearchGprsSendBuff(CMD_FORWARD_TO_BIND_DTU, (uint8 *)ForwardToBindDtuResponse, sizeof(_ForwardToBindDTU));

    Debug_Statics(offsetof(_Statics, MicRxForwardResponse), 1, sizeof(statics.MicRxForwardResponse));

    if (TxBuff == NULL)
    {
        return;
    }

    return;
}

/*******************************************************************************
*功能：异常上报响应（服务器下发）
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_MicReportExceptionRespose(_IF *msg, uint16 len)
{
    _ExceptionReportResponse *ExceptionReportResponse;
    _GprsSendBuff *TxBuff;

    if (MicModuleIsInit == false)
    {
        return;
    }

    msg->head.len = Lib_htons(msg->head.len);
    if ((msg->head.len + offsetof(_IF, buff)) != len)
    {
        return;
    }

    if (msg->head.len != sizeof(_ExceptionReportResponse))
    {
        return;
    }

    Debug_Statics(offsetof(_Statics, ResReportException), 1, sizeof(statics.ResReportException));

    /*搜索gprs发送缓存，停止重传*/
    ExceptionReportResponse = (_ExceptionReportResponse *)msg->buff;
    TxBuff = PHY_SearchGprsSendBuff(CMD_REPORT_EXCEPTION, NULL, 0);

    if (TxBuff == NULL)
    {
        return;
    }

    switch (ExceptionReportResponse->ExceptionNum)
    {
        case EXCEPTION_NET_COMMUTE:
            {
                Exception_GprsCommuteCounter = 0;

                break;
            }
    }

    return;
}

/*******************************************************************************
*功能：server下发的特权指令
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_MicPrivilegeCmd(_IF *SrcMsg, uint16 len)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    bool IsException;
    bool valid;
    _IF buff;

    if (MicModuleIsInit == false)
    {
        return;
    }

    valid = true;

    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);
    if ((SrcMsg->head.len + offsetof(_IF, buff)) != len)
    {
        valid = false;
    }

    if (valid == true)
    {
        IsException = false;
        /*处于485总线超时锁定状态*/
        if (Lock_485OverTime == true)
        {
            IsException = true;
        }

        /*处于参数异常锁定状态*/
        if (Lock_ParameterException == true)
        {
            IsException = true;
        }

        /*处于严重状态*/
        if (IsException == true)
        {
            Fun_CheckLockCmdException();
        }

        /*特权指令锁定*/
        Lock_PrivilegeCmd = true;

        if (IsException == false)
        {
            /*取消当前的灌溉逻辑，原因：人为取消*/
            Fun_TerminalSubTask(TASK_STATE_CANCEL);

            /*请求485总线发送特权指令*/
            SendMsg = (_TxBuff *)msg.buff;

            msg.protocol = PROTOCOL_NONE;
            msg.len = len - offsetof(_IF, buff);

            if (msg.len >= sizeof(msg.buff))
            {
                return;
            }

            memcpy(&SendMsg->buff, SrcMsg->buff, msg.len);
            SendMsg->type = SEND_PRIVILEGE_CMD;

            Debug_Statics(offsetof(_Statics, privilege), 1, sizeof(statics.privilege));

            Fun_Request485BusSendMsg(&msg);
        }
    }

    /*回复响应，无负载*/
    buff.head.version = CMD_VERSION;
    buff.head.cmd = CMD_PRIVILEGE_CMD_RESPOSE;

    buff.head.len = 1;
    buff.head.len = Lib_htons(buff.head.len);
    buff.buff[0] = valid;

    len = offsetof(_IF, buff) + 1;
    PHY_RequestGprsSendMsg((uint8 *)&buff, len, CMD_PRIVILEGE_CMD_RESPOSE, 0xFF, 1, 0, false);

    return;
}

/*******************************************************************************
*功能：server取消下发的特权指令
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_MicCancelPrivilegeCmd(_IF *msg, uint16 len)
{
    _IF buff;

    if (MicModuleIsInit == false)
    {
        return;
    }

    msg->head.len = Lib_htons(msg->head.len);
    if ((msg->head.len + offsetof(_IF, buff)) != len)
    {
        return;
    }

    Debug_Statics(offsetof(_Statics, CancelPrivilege), 1, sizeof(statics.CancelPrivilege));

    /*取消特权指令模式*/
    Lock_PrivilegeCmd = false;

    Exception_PrivilegeCmdCounter = 0;

    /*回复响应，无负载*/
    buff.head.version = CMD_VERSION;
    buff.head.cmd = CMD_CANCEL_PRIVILEGE_CMD_RESPOSE;
    buff.head.len = 0;

    len = offsetof(_IF, buff);
    PHY_RequestGprsSendMsg((uint8 *)&buff, len, CMD_CANCEL_PRIVILEGE_CMD_RESPOSE, 0xFF, 1, 0, false);

    return;
}

/*******************************************************************************
*功能：server下发任务
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_SetMicWorkTask(_IF *msg, uint16 len)
{
    _IF buff;
    uint16 SubTaskPtr;
    uint16 offset;                                                              //偏移量
    _ServerTask *ServerTask;
    bool valid;
    bool IsException;

    if (MicModuleIsInit == false)
    {
        return;
    }

    msg->head.len = Lib_htons(msg->head.len);

    IsException = false;


    /*处于485总线超时锁定状态*/
    if (Lock_485OverTime == true)
    {
        IsException = true;
    }

    /*处于参数异常锁定状态*/
    if (Lock_ParameterException == true)
    {
        IsException = true;
    }

    /*处于严重状态*/
    if (IsException == true)
    {
        Fun_CheckLockCmdException();
    }

    /*处于特权指令锁定状态*/
    if (Lock_PrivilegeCmd == true)
    {
        Fun_CheckPrivilegeCmdException();
        IsException = true;
    }

    valid = true;

    /*参数检查*/
    if (len >= sizeof(WorkTaskList.TaskList))
    {
        valid = false;

        return;
    }

    if (len <= (sizeof(msg->head) + 1))
    {
        valid = false;

        return;
    }

    if (len != msg->head.len + offsetof(_IF, buff))
    {
        valid = false;

        return;
    }

    if (IsException == false && valid == true)
    {
        /*检查发送的任务报文的正确性*/
        offset = offsetof(_IF, buff);
        SubTaskPtr = 1;
        ServerTask = (_ServerTask *)&msg->buff[SubTaskPtr];                     //msg->buff[0]为任务id

        while (true)
        {
            ServerTask->len = Lib_htons(ServerTask->len);

            if (ServerTask->TaskType != SUB_TASK_TIMING && ServerTask->TaskType != SUB_TASK_RATION)
            {
                /*下达的任务列表参数不正确*/
                valid = false;

                break;
            }

            /*检查报文负载的crc是否正确*/
            if (Modbus_CheckCRC(ServerTask->buff, ServerTask->len) == false)
            {
                valid = false;

                break;
            }

            if ((SubTaskPtr + offset + offsetof(_ServerTask, buff) + ServerTask->len) == len)
            {
                valid = true;

                break;
            }
            else if ((SubTaskPtr + offset + offsetof(_ServerTask, buff) + ServerTask->len) > len)
            {
                /*下达的任务列表不正确*/
                valid = false;

                break;
            }

            SubTaskPtr += ServerTask->len + offsetof(_ServerTask, buff);
            ServerTask = (_ServerTask *)&msg->buff[SubTaskPtr];
        }
    }

    /*回复响应，无负载*/
    buff.head.version = CMD_VERSION;
    buff.head.cmd = CMD_SET_WORK_TASK_RESPONSE;
    buff.head.len = 1;

    if (IsException == true)
    {
        buff.buff[0] = 2;
    }
    else
    {
        buff.buff[0] = valid;
    }

    buff.head.len = Lib_htons(buff.head.len);
    PHY_RequestGprsSendMsg((uint8 *)&buff, offsetof(_IF, buff) + 1, CMD_SET_WORK_TASK_RESPONSE, 0xFF, 1, 0, false);

    if (valid == false)
    {
        return;
    }

    if (IsException == true)
    {
        return;
    }

    /*开始执行子任务*/
    memcpy(WorkTaskList.TaskList, &msg->buff[1], len - sizeof(msg->head) - 1);
    WorkTaskList.id = msg->buff[0];
    WorkTaskList.IsExecuteSubTask = false;
    WorkTaskList.HaveSubTask = true;
    WorkTaskList.SubTaskIndex =  - 1;

    WorkTaskList.len = len - sizeof(msg->head) - 1;
    WorkTaskList.TaskListPtr = 0;

    Debug_Statics(offsetof(_Statics, SetTask), 1, sizeof(statics.SetTask));

    /*获取首个子任务*/
    if (Fun_GetNextSubTask() == false)
    {
        WorkTaskList.IsExecuteSubTask = false;
        Fun_SendSubTaskReport();
    }
    else
    {
        Fun_ExecuteSubTask();                                                   //开始执行第一个子任务
    }

    return;
}

/*******************************************************************************
*功能：注册响应
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_MicRegisterToServerResponse(_IF *SrcMsg, uint16 len)
{
    _GprsSendBuff *TxBuff;

    if (MicModuleIsInit == false)
    {
        return;
    }

    if (SrcMsg->head.len != 0)
    {
        return;
    }

    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);
    if (len != SrcMsg->head.len + offsetof(_IF, buff))
    {
        return;
    }

    Debug_Statics(offsetof(_Statics, ResRegister), 1, sizeof(statics.ResRegister));

    TxBuff = PHY_SearchGprsSendBuff(CMD_REGISTER_TO_SERVER, NULL, 0);

    if (TxBuff == NULL)
    {
        return;
    }

    return;
}

/*******************************************************************************
*功能：复位异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_MicResetException(_SysMsg *msg, uint16 len)
{
    _ResetException *ResetException;
    bool result;
    _IF *response;
    _IF *SrcMsg;

    if (MicModuleIsInit == false)
    {
        return;
    }

    result = true;

    SrcMsg = (_IF *)msg->buff;
    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);

    /*报文参数检查*/
    if (SrcMsg->head.len != sizeof(_ResetException))
    {
        result = false;
    }

    if ((offsetof(_IF, buff) + sizeof(_ResetException)) != len)
    {
        result = false;
    }

    ResetException = (_ResetException *)SrcMsg->buff;

    if (result == true)
    {
        if (ResetException->ProcessMode != EXCEPTION_MONITOR
            && ResetException->ProcessMode != EXCEPTION_NO_MONITOR)
        {
            result = false;
        }
    }

    if (result == true)
    {
        Debug_Statics(offsetof(_Statics, ResetException), 1, sizeof(statics.ResetException));

        switch (ResetException->ExceptionNum)
        {
            case EXCEPTION_485_OVER_TIME:                                       //485通信超时异常；严重异常
                {
                    Lock_485OverTime = false;
                    ProcessMode_485OverTimeException = ResetException->ProcessMode;
                    Exception_485OverTimeCounter = 0;

                    statics.Exception_485_over_time = 0;

                    break;
                }
            case EXCEPTION_FORWARD:                                             //转发异常
                {
                    ProcessMode_ForwardException = ResetException->ProcessMode;

                    statics.Exception_forward = 0;

                    break;
                }
            case EXCEPTION_PARAMETER:                                           //参数异常；严重异常
                {
                    Lock_ParameterException = false;

                    statics.Exception_parameter = 0;

                    break;
                }
            case EXCEPTION_NET_COMMUTE:                                         //网络通信异常
                {
                    ProcessMode_NetCommuteException = ResetException->ProcessMode;

                    statics.Exception_net_commute = 0;

                    break;
                }
            case EXCEPTION_VALVE:                                               //阀门异常
                {
                    ProcessMode_ValveException = ResetException->ProcessMode;

                    statics.Exception_valve = 0;

                    break;
                }
            case EXCEPTION_FLOWMETER:                                           //流量计异常
                {
                    ProcessMode_FlowMeterException = ResetException->ProcessMode;

                    statics.Exception_flowmeter = 0;

                    break;
                }
            case EXCEPTION_CMD_LOCK:                                            //指令锁定异常
                {
                    ProcessMode_CmdLockException = ResetException->ProcessMode;

                    statics.Exception_cmd_lock = 0;

                    break;
                }
            case EXCEPTION_LOGIC_LOCK:                                          //逻辑锁定异常
                {
                    ProcessMode_LogicLockException = ResetException->ProcessMode;

                    statics.Exception_logic_lock = 0;

                    break;
                }
            default:
                {
                    result = false;

                    break;
                }
        }
    }

    if (result == false)
    {
        return;
    }

    /*回复响应，无负载*/
    response = (_IF *)msg->buff;

    response->head.version = CMD_VERSION;
    response->head.cmd = CMD_RESET_EXCEPTION_RESPONSE;
    response->head.len = 1;
    response->head.len = Lib_htons(response->head.len);

    response->buff[0] = ResetException->ExceptionNum;

    len = offsetof(_IF, buff) + 1;

    if (msg->port == USART_GPRS_CHANNEL)
    {
        PHY_RequestGprsSendMsg((uint8 *)response, len, CMD_RESET_EXCEPTION_RESPONSE, 0xFF, 1, 0, false);
    }
    else
    {
        msg->len = len;
        msg->protocol = PROTOCOL_NONE;

        PHY_UsartSendMsg(msg);
    }

    return;
}

/*******************************************************************************
*功能：服务器下发的透明传输报文
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_MicTransparentToMainDevice(_IF *SrcMsg, uint16 len)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    bool IsException;
    bool result;

    _IF *response;

    if (MicModuleIsInit == false)
    {
        return;
    }

    result = true;

    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);
    if (len != SrcMsg->head.len + offsetof(_IF, buff))
    {
        result = false;

        goto function_end;
    }

    IsException = false;

    /*处于485总线超时锁定状态*/
    if (Lock_485OverTime == true)
    {
        IsException = true;
    }

    /*处于参数异常锁定状态*/
    if (Lock_ParameterException == true)
    {
        IsException = true;
    }


    /*处于严重状态*/
    if (IsException == true)
    {
        Fun_CheckLockCmdException();
    }

    /*处于特权指令锁定状态*/
    if (Lock_PrivilegeCmd == true)
    {
        /*在特权指令模式下，仅放行modbus功能码3*/
        if (((_Modbus *)(SrcMsg->buff))->UsartType.pdu.cmd != 3)
        {
            Fun_CheckPrivilegeCmdException();
            IsException = true;
        }
    }

    SendMsg = (_TxBuff *)msg.buff;

    msg.protocol = PROTOCOL_NONE;
    msg.len = len - offsetof(_IF, buff);

    if (msg.len >= sizeof(msg.buff))
    {
        result = false;

        goto function_end;
    }

    if (SrcMsg->head.len != msg.len)
    {
        result = false;

        goto function_end;
    }

    Debug_Statics(offsetof(_Statics, transparent), 1, sizeof(statics.transparent));

    if (IsException == true)
    {
        result = false;

        goto function_end;
    }

function_end:

    if (result == true)
    {
        memcpy(&SendMsg->buff, SrcMsg->buff, msg.len);

        if (SrcMsg->head.cmd == CMD_TRANSPARENT_TO_MAIN_DEVICE_1)
        {
            SendMsg->type = SEND_TRANSPARENT_MSG_1;
        }
        else
        {
            SendMsg->type = SEND_TRANSPARENT_MSG_2;
        }

        Fun_Request485BusSendMsg(&msg);
    }
    else                                                                        //封装负响应
    {
        response = SrcMsg;

        /*回复响应，无负载*/
        response->head.version = CMD_VERSION;
        response->head.cmd = CMD_TRANSPARENT_NEGATIVE_RESPONSE;
        response->head.len = 0;

        len = offsetof(_IF, buff);
        PHY_RequestGprsSendMsg((uint8 *)response, len, CMD_TRANSPARENT_NEGATIVE_RESPONSE, 0xFF, 1, 0, false);
    }

    return;
}

/*******************************************************************************
*功能：485接收处理
*参数：无
*返回：无
*说明：在接收到响应或发送超时时被调用
*******************************************************************************/
void App_MicRS485ReceiveHandler(_SysMsg *msg, _TxBuff *SrcMsg, uint8 result)
{
    _Modbus *SendMsg;
    _CMD3PDU *Cmd3Request;
    uint8 CheckResult;
    _IF response;
    uint16 StartAddr;
    uint16 len;

    if (MicModuleIsInit == false)
    {
        return;
    }

    /*发送超时*/
    if (msg == NULL && result == SEND_MSG_FAIL)
    {
        Fun_TimeOverStatics();

        return;
    }

    SendMsg = &SrcMsg->buff;

    /*接收正确性检查*/
    CheckResult =  Modbus_CheckResponse(&SrcMsg->buff, (_Modbus *)msg->buff, msg->len);

    switch (CheckResult)
    {
        case MODBUS_ERR_RESPONSE:                                               //负响应
            {
                Fun_ErrReceiveStatics();

                return;
            }
        case MODBUS_NON_REQUEST_CMD_ERR:                                        //检查接收命令号
            {
                Fun_ErrReceiveStatics();

                return;
            }
        case MODBUS_RESPONSE_LEN_ERR:                                           //响应字节长度不对
            {
                Fun_ErrReceiveStatics();

                return;
            }
        case MOBUS_CORRECT_RESPONSE:                                            //正确接收
            {
                Fun_CorrectReceiveStatics();

                break;
            }
        default:
            {
                return;
            }
    }

    switch (SrcMsg->type)
    {
        /*轮询指令响应处理*/
        case SEND_POLL_MSG:
            {
                Cmd3Request = (_CMD3PDU *)&SendMsg->UsartType.pdu.data;
                StartAddr = Lib_htons(Cmd3Request->StartAddr);
                switch (StartAddr)
                {
                    case REG_VALVE_STATE:                                       //电磁阀
                        {
                            Debug_Statics(offsetof(_Statics, ResValve), 1, sizeof(statics.ResValve));
                            Fun_ValveStateChangedEvent(msg);

                            break;
                        }
                    case REG_SUB_TASK_STATE:                                    //子任务状态
                        {
                            Debug_Statics(offsetof(_Statics, ResSubTask), 1, sizeof(statics.ResSubTask));
                            Fun_ReadSubTaskRegHandler(msg);

                            break;
                        }
                    case REG_FLOWMETER:                                         //流量计
                        {
                            Debug_Statics(offsetof(_Statics, ResFlowMeter), 1, sizeof(statics.ResFlowMeter));
                            Fun_CheckValveExcetion(msg);

                            break;
                        }
                    default:
                        {
                            return;
                        }
                }

                return;
            }
        case SEND_TRANSPARENT_MSG_1:
        case SEND_TRANSPARENT_MSG_2:
            {
                /*对于透明传输的响应，无论是正响应还是负响应，均发送给服务器*/
                if (msg->len >= sizeof(response.buff))
                {
                    break;
                }

                ModBus_SendProcess(msg);

                if (SrcMsg->type == SEND_TRANSPARENT_MSG_1)
                {
                    response.head.cmd = CMD_TRANSPARENT_TO_MAIN_DEVICE_1;
                }
                else
                {
                    response.head.cmd = CMD_TRANSPARENT_TO_MAIN_DEVICE_2;
                }

                response.head.version = CMD_VERSION;
                response.head.len =  msg->len;

                len = msg->len + offsetof(_IF, buff);
                response.head.len = Lib_htons(response.head.len);

                memcpy(response.buff, msg->buff, msg->len);

                Debug_Statics(offsetof(_Statics, ResTransparent), 1, sizeof(statics.ResTransparent));
                PHY_RequestGprsSendMsg((uint8 *)&response, len, response.head.cmd, 0xFF, 1, 0, false);

                break;
            }
        default:
            {
                break;
            }
    }

    return;
}


/*-------------------GPRS处理部分------------------*/
/*******************************************************************************
*功能：gprs发送完成回调函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool App_MicGprsSendResultHandler(_GprsSendBuff *TxBuff, uint8 SendResult)
{
    bool result;

    if (MicModuleIsInit == false)
    {
        return false;
    }

    if (SendResult == SEND_MSG_SUCCESS)
    {
        Exception_GprsCommuteCounter = 0;

        return true;
    }

    if (SendResult == SEND_MSG_FAIL)
    {
        result = false;

        /*统计gprs发送失败次数*/
        Fun_CheckGprsCommuteException();

        switch (TxBuff->cmd)
        {
            case CMD_FORWARD_TO_BIND_DTU:
                {
                    if (ProcessMode_ForwardException == EXCEPTION_NO_MONITOR)
                    {
                        break;
                    }

                    App_ReportExceptionToServer(EXCEPTION_FORWARD, (uint8 *)cfg.BindDtuList, 15);//转发异常

                    break;
                }
            case CMD_REPORT_EXCEPTION:                                          //上报异常失败
                {
                    Fun_ReportExceptionToServerFail(TxBuff);

                    break;
                }
            case CMD_REGISTER_TO_SERVER:
                {
                    TxBuff->ResendNum = 200;
                    result = true;

                    break;
                }
        }

        return result;
    }

    return false;
}

/*******************************************************************************
*功能：设备初始化
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_MicMainDeviceInit(bool ParameterException)
{
    OS_ERR err;

    if (cfg.MainDeviceType == MAIN_DEVICE_MIC)
    {
        MicModuleIsInit = true;
    }
    else
    {
        MicModuleIsInit = false;

        return;
    }

    Lock_ParameterException = ParameterException;

    /*创建轮询定时器*/
    OSTmrCreate(&MicPollTmr,
                NULL,
                3 * SYS_SECOND_TICK_NUM,
                MIC_POLL_PERIOD * SYS_SECOND_TICK_NUM,
                OS_OPT_TMR_PERIODIC,
                Fun_MicPollTmrHandler,
                NULL,
                &err);

    /*启动定时器*/
    OSTmrStart(&MicPollTmr, &err);

    /*初始化变量*/
    Exception_485OverTimeCounter = 0;
    Exception_LockCmdCounter = 0;
    Exception_ValveCounter = 0;

    ProcessMode_485OverTimeException = EXCEPTION_MONITOR;
    ProcessMode_ForwardException = EXCEPTION_MONITOR;
    ProcessMode_NetCommuteException = EXCEPTION_MONITOR;
    ProcessMode_ValveException = EXCEPTION_MONITOR;
    ProcessMode_FlowMeterException = EXCEPTION_MONITOR;
    ProcessMode_CmdLockException = EXCEPTION_MONITOR;
    ProcessMode_LogicLockException = EXCEPTION_MONITOR;

    ForwardToBindDtuSeq = 0;

    Lock_485OverTime = false;
    Lock_PrivilegeCmd = false;

    MicState.FirstGetFlowMeter = true;

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年12月05日    巍图      创建文件
*******************************************************************************/

/*
485超时锁定：锁定透明传输、锁定下发子任务、锁定特权指令
*/

