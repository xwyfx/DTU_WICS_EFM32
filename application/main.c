/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月10日
**  名   称：main.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "Dri_BSP.h"
#include "Dri_Driver.h"

#include "PHY.h"
#include "PHY_Flash.h"
#include "PHY_Timer.h"
#include "PHY_Usart.h"
#include "PHY_Flash.h"
#include "PHY_Led.h"

#include "PHY_GprsSendTask.h"
#include "PHY_Gprs.h"

#include "IF.h"

#include "App_MIC.h"
#include "App_PLC.h"

#include "debug.h"


static OS_TMR SysHeartTmrId;

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SysHeartTmrHandler(void *p_tmr, void *p_arg)
{
    PHY_LedSet(SYS_HEART_LED, LED_TOGGLE);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CreateSysHeartTmr(void)
{
    OS_ERR err;

    OSTmrCreate(&SysHeartTmrId,
                NULL,
                2 * SYS_SECOND_TICK_NUM,
                1 * SYS_SECOND_TICK_NUM,
                OS_OPT_TMR_PERIODIC,
                Fun_SysHeartTmrHandler,
                NULL,
                &err);
    OSTmrStart(&SysHeartTmrId, &err);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
int main(void)
{
    OS_ERR  err;
    bool ParameterErr;

    __disable_irq();

    /*芯片级初始化，必须最先调用*/
    Dri_BSPInit();

    OSInit(&err);

    Lib_Init();

    Dri_DriverInit();

    statics.reset = PHY_ReadResetNum();
    PHY_WriteResetNum(statics.reset + 1);

    ParameterErr = IF_ReadCfgFromFlash();                                       //读取配置
    Debug_Init();

    PHY_UsartInit();
    PHY_GprsSendTaskInit();
    PHY_GprsInit();

    PHY_StartGprs();

    (void)&err;

    if (cfg.update == false)
    {
        App_MicMainDeviceInit(!ParameterErr);
        App_PlcMainDeviceInit(!ParameterErr);
    }

    if (ParameterErr == false)
    {
        SysParameterException = true;
    }

    Fun_CreateSysHeartTmr();

    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年05月10日   巍图      创建文件
*******************************************************************************/
