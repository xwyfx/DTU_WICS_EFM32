/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年01月10日
**  名   称：Application.h
**  摘   要：
*******************************************************************************/
#ifndef _APPLICATION_H
#define _APPLICATION_H

#define MAX_CONTINUE_RECEIVE_ERR                       5                      //最大连续接收错误
#define MAX_SUB_TASK_NUM                               16                     //最大的子任务数量

/*子任务类型定义*/
enum
{
    SUB_TASK_TIMING = 1,                                                        //定时任务
    SUB_TASK_RATION = 2,                                                        //定量任务
};

/*子任务状态寄存器定义*/
enum
{
    STATE_REG_WAITING   = 0,                                                    //等待
    STATE_REG_EXECUTING = 1,                                                    //执行中
    STATE_REG_OVER      = 2,                                                    //结束
};


/*子任务执行结果定义*/
enum
{
    TASK_STATE_SUCCESS   = 0,                                                   //子任务执行成功
    TASK_STATE_OVER_TIME = 1,                                                   //子任务执行超时
    TASK_STATE_CANCEL    = 2,                                                   //子任务被人为取消
};

/*异常门限定义*/
enum
{
    EXCEPTION_485_OVER_TIME_NUM    = 20,                                        //485通信超时异常次数
    EXCEPTION_LOCK_CMD_NUM         = 20,                                        //指令锁定时超限次数
    EXCEPTION_PRIVILEGE_CMD_NUM    = 20,                                        //特权指令状态下超限次数
};


/*通过485下发的报文统计格式*/
typedef struct
{
    uint16 send;
    uint16 CorrectReceive;
    uint16 ErrReceive;
    uint16 ContinueErr;
    uint16 TimeOver;
    uint8 GetBuffErr;
}_485BusStatics;

/*------------------子任务相关格式定义-----------------*/
/*Server下发的任务格式*/
#pragma pack(1)
typedef struct
{
    uint16 len;                                                                 //长度
    uint8  TaskType;                                                            //任务类型
    uint8  OverTime;                                                            //单位：分钟
    uint8  buff[MAX_BUFF_LEN];                                                  //缓存
}_ServerTask;
#pragma pack()

/*子任务执行结果格式*/
#pragma pack(1)
typedef struct
{
    uint8 result;
    uint16 SubTaskStateReg;                                                     //子任务状态寄存器
}_SubTaskState;
#pragma pack()

/*任务执行结果响应格式*/
typedef struct
{
    uint8 WorkId;                                                               //任务id
    _SubTaskState SubTaskState[MAX_SUB_TASK_NUM];                               //子任务状态寄存器
}_WorkTaskState;

/*Server下发的任务列表格式*/
typedef struct
{
    bool IsExecuteSubTask;                                                      //是否正在执行子任务
    bool SendState;
    bool HaveSubTask;                                                           //是否有子任务要执行
    int8 SubTaskIndex;                                                          //正在执行的子任务数组索引
    _WorkTaskState WorkTaskState;                                               //任务执行结果
    uint8  TaskList[MAX_BUFF_LEN];                                              //任务类型
    uint8  id;                                                                  //任务id
    _ServerTask *ServerTaskPtr;                                                 //子任务指针
    uint16 TaskListPtr;                                                         //任务列表偏移量
    uint16 len;                                                                 //总长度
    uint32 SubTaskOverTime;                                                     //子任务超时时间
    uint32 SubTaskStartTime;                                                    //子任务启动时间
    uint8 LandPieceSeq;
}_WorkTaskList;

/*Server下发的任务格式*/
typedef struct
{
    uint8 ExceptionNum;                                                         //异常码
    uint8 ProcessMode;                                                          //后续该异常的处理方式
}_ResetException;

extern _WorkTaskList WorkTaskList;
extern _485BusStatics BusStatics;                                               //485总线统计

void App_ReportExceptionToServer(uint16 ExceptionNum, uint8 *buff, uint16 len);

#endif

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年01月10日    巍图      创建文件
*******************************************************************************/

