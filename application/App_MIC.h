/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年12月05日
**  名   称：App_MIC.h
**  摘   要：
*******************************************************************************/
#ifndef _APP_MIC_H
#define _APP_MIC_H

void App_MicMainDeviceInit(bool ParameterException);
void App_SetMicWorkTask(_IF *msg, uint16 len);
void App_MicTransparentToMainDevice(_IF *SrcMsg, uint16 len);
void App_MicReportExceptionRespose(_IF *msg, uint16 len);
void App_MicForwardToBindDtuRespose(_IF *msg, uint16 len);
void App_MicPrivilegeCmd(_IF *msg, uint16 len);
void App_MicCancelPrivilegeCmd(_IF *msg, uint16 len);
void App_MicResetException(_SysMsg *msg, uint16 len);
void App_MicRegisterToServerResponse(_IF *SrcMsg, uint16 len);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年12月05日    巍图      创建文件
*******************************************************************************/
