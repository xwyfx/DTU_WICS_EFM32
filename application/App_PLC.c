/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年12月05日
**  名   称：App_PLC.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "Application.h"

#include "PHY_GprsSendTask.h"
#include "PHY_Gprs.h"

#include "PHY_Reset.h"
#include "PHY_Usart.h"

#include "ModBusRtu.h"
#include "Modbus.h"

#include "debug.h"

#define MAX_POLL_LAND_PIECE_NUM                        4
#define DEFAULT_PLC_SLAVE_ADDR                         2                       //默认PLC从站地址
#define DEFAULT_TRANSFORMER_ADDR                       4                       //默认互感器轮询地址

#define PLC_POLL_PERIOD                                20                      //PLC轮询周期，单位：秒
#define PUMP_EXCEPTIONAL_TRY_CLOSE_LIMIT               5                       //异常状态下，尝试关泵次数
#define MAX_GPRS_COMMUTE_FAIL_NUM                      10                      //GPRS通信异常阈值
#define MAX_LAND_PIECE_NUM                             16                      //系统支持的做多地块数量

#define MAX_BOUND_DTUS_NUM                             50
#define RX_TRANS_TABLE_TMR_PERIOD                      60
#define IMEI_LEN                                       6
#define CLEAN_RX_TRANS_TABLE_TIME                      10                       //清空接收传输表时间，单位：分钟

/*水泵状态*/
enum
{
    PUMP_CLOSE = 0,                                                             //水泵：关闭状态
    PUMP_OPEN  = 1,                                                             //水泵：打开状态
};

/*寄存器地址定义*/
enum
{
    REG_SEND_STATE                    = 681,

    REG_CTL_PUMP                      = 678,                                    //0x02A6 水泵控制寄存器
    REG_TRANSFORMER_CURRENT           = 73,                                     //互感器电流，从站地址使用4
    REG_MOTOR_BIND                    = 678,                                    //电机关联寄存器

    REG_COMMUTE_STATE                 = 691,
};

enum
{
    REQUEST_FLOWMETER_ADDR   = 1,                                               //请求获取流量计地址
    REQUEST_CLOSE_VALVE_ADDR = 2,                                               //请求获取关闭阀门地址
    REQUEST_TASK_STATE_ADDR  = 3,                                               //请求获取子任务状态地址
    REQUEST_TASK_START_ADDR  = 4,                                               //灌溉任务起始地址
};

typedef union
{
    uint8  buff[IMEI_LEN];

    struct
    {
        uint16 L;
        uint32 H;
    };
}_IMEI;

typedef struct
{
    uint8  seq;
    uint8  time;
    _IMEI imei;
}_TransTable;

/*PLC工作状态格式*/
typedef struct
{
    uint32 ValveState;                                                          //电磁阀状态
    uint8  PumpState;                                                           //水泵状态
    bool  SendState;
    uint16 CommuteState;
    _TransTable TransTable[MAX_BOUND_DTUS_NUM];
}_PlcState;

/*地块状态格式*/
typedef struct
{
    bool   FirstGetFlowMeter;
    bool   ValveState;                                                          //电磁阀状态
    uint16 Exception_ValveCounter;                                              //阀门异常；普通异常
    uint16 Exception_FlowMeterCounter;                                          //流量计异常；普通异常
    uint16 FlowMeterValue;                                                      //流量计值//流量计
}_LandPiece;

static OS_TMR PlcPollTmr;                                                       //PLC轮询定时器
static OS_TMR RxTransTableTmr;                                                  //接收传输表维护定时器
static uint8 RxTrarnsTableTmrCounter;

static bool PlcModuleIsInit;                                                    //PLC模块是否被初始化

static _PlcState PlcState;                                                      //PLC设备状态

static uint8 SysValveState;                                                     //其它MIC上报的电磁阀状态


/*异常计数器*/
static uint16 Exception_485OverTimeCounter;                                     //485通信异常次数；严重异常
static uint16 Exception_LockCmdCounter;                                         //指令锁定异常；普通异常
static uint16 Exception_PrivilegeCmdCounter;                                    //逻辑锁定异常；普通异常
static uint16 Exception_PumpCouter;                                             //水泵异常；严重异常
static uint16 Exception_CircuitProtectCounter;                                  //电路保护异常；严重异常
static uint16 Exception_GprsCommuteCounter;
static uint16 PumpExceptionalTryCloseCounter;


/*异常处理方式*/
static uint8 ProcessMode_485OverTimeException;                                  //485总线超时
static uint8 ProcessMode_CircuitProtectException;                               //电路保护异常
static uint8 ProcessMode_PumpException;                                         //水泵异常
static uint8 ProcessMode_ForwardException;                                      //转发异常的处理方式
static uint8 ProcessMode_NetCommuteException;                                   //网络通信异常处理方式
static uint8 ProcessMode_WirelessCommnuteException;                             //无线通信异常处理方式
static uint8 ProcessMode_CoordinatorException;                                  //协调器异常处理方式
static uint8 ProcessMode_ValveException;                                        //阀门异常处理方式
static uint8 ProcessMode_FlowMeterException;                                    //流量计异常处理方式
static uint8 ProcessMode_CmdLockException;                                      //指令锁定异常处理方式
static uint8 ProcessMode_LogicLockException;                                    //逻辑锁定异常


/*锁定*/
static bool Lock_485OverTime;                                                   //485超时锁定
static bool Lock_PrivilegeCmd;                                                  //特权指令锁定
static bool Lock_Pump;                                                          //水泵锁定
static bool Lock_CircuitProtect;                                                //电路保护锁定
static bool Lock_ParameterException;                                            //参数异常锁定

/*转发报文序号*/
static uint8 ForwardToBindDtuSeq;

static _LandPiece LandPiece[MAX_LAND_PIECE_NUM];
static uint8 PollLandPieceStateSeq;

static void Fun_TerminalSubTask(uint8 reason);
static void Fun_SendSubTaskReport(void);
static void Fun_CheckGprsCommuteException(void);
static void Fun_ReportExceptionToServerFail(_GprsSendBuff *SrcMsg);
static uint16 Fun_GetLandPieceStateReg(uint8 LandPieceSeq, uint8 RequestSeq);
static uint8 Fun_GetLandPieceSeq(uint16 RequestAddr, uint8 RequestSeq);

/*-------------------485总线统计部分------------------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SendStatics(void)
{
    BusStatics.send++;

    return;
}

/*******************************************************************************
*功能：rs485正确接收统计
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CorrectReceiveStatics()
{
    BusStatics.CorrectReceive++;
    BusStatics.ContinueErr = 0;

    /*正确接收：将超时参数清零*/
    Exception_485OverTimeCounter = 0;

    return;
}

/*******************************************************************************
*功能：rs485接收错误统计
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ErrReceiveStatics(void)
{
    BusStatics.ErrReceive++;

    if (BusStatics.ContinueErr < MAX_CONTINUE_RECEIVE_ERR)
    {
        BusStatics.ContinueErr++;
    }

    Debug_Statics(offsetof(_Statics, RS485RxErr), 1, sizeof(statics.RS485RxErr));

    return;
}

/*******************************************************************************
*功能：超时统计
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_TimeOverStatics(void)
{
    BusStatics.TimeOver++;

    Debug_Statics(offsetof(_Statics, RS485RxTimeout), 1, sizeof(statics.RS485RxTimeout));

    if (ProcessMode_485OverTimeException == EXCEPTION_NO_MONITOR)
    {
        Exception_485OverTimeCounter = 0;

        return;
    }

    if (Exception_485OverTimeCounter >= EXCEPTION_485_OVER_TIME_NUM)
    {
        /*锁定*/
        Lock_485OverTime = true;

        /*发生485总线通信异常时，停止当前灌溉逻辑*/
        Fun_TerminalSubTask(TASK_STATE_OVER_TIME);

        /*向服务器发送通信异常*/
        App_ReportExceptionToServer(EXCEPTION_485_OVER_TIME, NULL, 0);
    }
    else
    {
        Exception_485OverTimeCounter++;
    }

    return;
}

/*-------------------接收传输表维护部分-------------------*/
/*******************************************************************************
*功能：接收传输表定时器回调函数
*参数：无
*返回：无
*说明：1、触发周期为一分钟；2、清空超过10分钟的接收缓存
*******************************************************************************/
static void Fun_RxTrarnsTableTmrHandler(void *p_tmr, void *p_arg)
{
    uint16 i;

    RxTrarnsTableTmrCounter++;

    /*遍历对比*/
    for (i = 0; i < MAX_BOUND_DTUS_NUM; i++)
    {
        if (PlcState.TransTable[i].imei.H != 0
            || PlcState.TransTable[i].imei.L != 0)
        {
            if (abs(RxTrarnsTableTmrCounter - PlcState.TransTable[i].time) > CLEAN_RX_TRANS_TABLE_TIME)
            {
                PlcState.TransTable[i].time = 0;
                PlcState.TransTable[i].seq = 0;

                PlcState.TransTable[i].imei.L = 0;
                PlcState.TransTable[i].imei.H = 0;
            }
        }
    }

    return;
}

/*******************************************************************************
*功能：检查接收传输表
*参数：无
*返回：无
*说明：1、不存在，则写入；2、存在且报文序号不一致，更新报文序号；3、存在且报文序号一致，则返回false
*******************************************************************************/
static bool Fun_CheckRxTransTable(uint8 *buff, uint8 seq)
{
    uint16 i;
    _IMEI imei;
    uint16 empty;

    memcpy(imei.buff, buff, IMEI_LEN);

    empty = 0xFF;

    /*遍历对比*/
    for (i = 0; i < MAX_BOUND_DTUS_NUM; i++)
    {
        /*查找空索引*/
        if (empty == 0xFF
            && PlcState.TransTable[i].imei.H == 0
            && PlcState.TransTable[i].imei.L == 0)
        {
            empty = i;
        }

        if (PlcState.TransTable[i].imei.H == imei.H
            && PlcState.TransTable[i].imei.L == imei.L)
        {
            if (PlcState.TransTable[i].seq == seq)
            {
                /*重传报文*/
                return true;
            }
            else
            {
                PlcState.TransTable[i].seq = seq;
                PlcState.TransTable[i].time = RxTrarnsTableTmrCounter;

                return false;
            }
        }
    }

    if (empty != 0xFF)
    {
        PlcState.TransTable[empty].imei.H = imei.H;
        PlcState.TransTable[empty].imei.L = imei.L;

        PlcState.TransTable[empty].seq = seq;
        PlcState.TransTable[empty].time = RxTrarnsTableTmrCounter;

        return false;
    }

    /*表已满，无处可容纳，迫不得已，只能返回true*/
    return true;
}

/*-------------------485部分------------------*/
/*******************************************************************************
*功能：请求485发送
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_Request485BusSendMsg(_SysMsg *msg)
{
    bool result;

    /*发生485总线超时锁定时，不再向下发送485报文*/
    if (Lock_485OverTime == true)
    {
        return false;
    }

    msg->port = USART_485_CHANNEL;
    result = PHY_UsartSendMsg(msg);

    Fun_SendStatics();

    if (result == false)
    {
        Debug_Statics(offsetof(_Statics, Send485Err), 1, sizeof(statics.Send485Err));
    }

    if (result == false)
    {
        BusStatics.GetBuffErr++;
    }
    else
    {
        BusStatics.GetBuffErr = 0;
    }

    if (BusStatics.GetBuffErr >= 100)
    {
        PHY_Reset(0);
    }

    return result;
}

/*-------------------子任务部分------------------*/
/*******************************************************************************
*功能：查找下一个需要执行的子任务
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_GetNextSubTask(void)
{
    OS_ERR err;
    uint16 StartAddr;
    uint16 LandPieceSeq;

    if (WorkTaskList.HaveSubTask == false)
    {
        return false;
    }

    if (WorkTaskList.TaskListPtr >= WorkTaskList.len)                           //所有子任务处理完成
    {
        WorkTaskList.HaveSubTask = false;

        return false;
    }

    /*读取待工作子任务*/
    WorkTaskList.ServerTaskPtr = (_ServerTask *)&WorkTaskList.TaskList[WorkTaskList.TaskListPtr];

    if (WorkTaskList.ServerTaskPtr->OverTime != 0)                              //具有超时控制
    {
        WorkTaskList.SubTaskOverTime = WorkTaskList.ServerTaskPtr->OverTime * 60 * SYS_SECOND_TICK_NUM;
        WorkTaskList.SubTaskStartTime = OSTimeGet(&err);
    }
    else
    {
        WorkTaskList.SubTaskOverTime = 0;
        WorkTaskList.SubTaskStartTime = 0;
    }

    memcpy(&StartAddr, ((_Modbus *)WorkTaskList.ServerTaskPtr->buff)->UsartType.pdu.data, 2);
    StartAddr = Lib_htons(StartAddr);

    LandPieceSeq = Fun_GetLandPieceSeq(StartAddr, REQUEST_TASK_START_ADDR);
    if (LandPieceSeq == 0xFF)
    {
        return false;
    }

    WorkTaskList.LandPieceSeq = LandPieceSeq;
    WorkTaskList.SubTaskIndex++;
    WorkTaskList.IsExecuteSubTask = true;
    WorkTaskList.SendState = true;
    WorkTaskList.TaskListPtr = WorkTaskList.TaskListPtr + WorkTaskList.ServerTaskPtr->len + offsetof(_ServerTask, buff);

    return true;
}

/*******************************************************************************
*功能：执行子任务
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ExecuteSubTask(void)
{
    _SysMsg msg;
    _TxBuff *SendMsg;

    /*请求485总线发送*/
    SendMsg = (_TxBuff *)msg.buff;
    SendMsg->len = WorkTaskList.ServerTaskPtr->len;
    SendMsg->type = SEND_TASK_MSG;
    memcpy(&SendMsg->buff, WorkTaskList.ServerTaskPtr->buff, SendMsg->len);

    msg.protocol = PROTOCOL_NONE;
    msg.len = SendMsg->len;
    Fun_Request485BusSendMsg(&msg);

    return;
}

/*******************************************************************************
*功能：中止子任务
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_TerminalSubTask(uint8 reason)
{
    if (WorkTaskList.IsExecuteSubTask == false)
    {
        return;
    }

    WorkTaskList.IsExecuteSubTask = false;

    if (WorkTaskList.SubTaskIndex >= MAX_SUB_TASK_NUM)
    {
        PHY_Reset(0);                                                           //异常
    }

    /*填充异常响应*/
    WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].result = reason;
    WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].SubTaskStateReg = 0;

    /*填充未被执行的子任务状态*/
    while (Fun_GetNextSubTask() == true)
    {
        if (WorkTaskList.SubTaskIndex >= MAX_SUB_TASK_NUM)
        {
            PHY_Reset(0);                                                       //异常
        }

        WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].result = TASK_STATE_CANCEL;
        WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].SubTaskStateReg = 0;
    }

    WorkTaskList.HaveSubTask = false;
    WorkTaskList.IsExecuteSubTask = false;

    Fun_SendSubTaskReport();

    return;
}

/*******************************************************************************
*功能：发送子任务完成报告
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SendSubTaskReport(void)
{
    _IF msg;
    uint16 len;

    len = (WorkTaskList.SubTaskIndex + 1) * sizeof(_SubTaskState) + 1;
    msg.head.version = CMD_VERSION;
    msg.head.cmd = CMD_SET_WORK_TASK_REPORT;
    msg.head.len = Lib_htons(len);

    if (WorkTaskList.SubTaskIndex >= MAX_SUB_TASK_NUM)
    {
        return;
    }

    len = len + offsetof(_IF, buff);

    WorkTaskList.WorkTaskState.WorkId = WorkTaskList.id;
    memcpy(msg.buff, &WorkTaskList.WorkTaskState, len - 1);
    PHY_RequestGprsSendMsg((uint8 *)&msg,
                           len,
                           CMD_SET_WORK_TASK_REPORT,
                           0xFF,
                           1,
                           0,
                           false);

    return;
}

/*******************************************************************************
*功能：读取子任务状态寄存器回调函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ReadSubTaskRegHandler(_SysMsg *msg)
{
    _Modbus *reply;
    _CMD3Reply *Cmd3Reply;
    uint16 SubTaskState;

    reply = (_Modbus *)msg->buff;
    Cmd3Reply = (_CMD3Reply *)reply->UsartType.pdu.data;
    SubTaskState = Cmd3Reply->data[0];

    if (WorkTaskList.IsExecuteSubTask == false)
    {
        return;
    }

    SubTaskState = Lib_htons(SubTaskState);

    switch (SubTaskState)
    {
        case STATE_REG_WAITING:
            {
                Debug_Statics(offsetof(_Statics, SubTaskWait), 1, sizeof(statics.SubTaskWait));

                return;
            }
        case STATE_REG_EXECUTING:
            {
                Debug_Statics(offsetof(_Statics, SubTaskExecute), 1, sizeof(statics.SubTaskExecute));

                return;
            }
        case STATE_REG_OVER:
            {
                Debug_Statics(offsetof(_Statics, SubTaskOver), 1, sizeof(statics.SubTaskOver));

                if (WorkTaskList.SubTaskIndex < MAX_SUB_TASK_NUM)
                {
                    WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].result = TASK_STATE_SUCCESS;
                    WorkTaskList.WorkTaskState.SubTaskState[WorkTaskList.SubTaskIndex].SubTaskStateReg = STATE_REG_OVER;
                }
                else
                {
                    PHY_Reset(0);
                }

                if (Fun_GetNextSubTask() == false)
                {
                    WorkTaskList.IsExecuteSubTask = false;
                    Fun_SendSubTaskReport();                                    //发送子任务报告
                }
                else
                {
                    Fun_ExecuteSubTask();                                       //执行下一个子任务
                }

                return;
            }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static uint16 Fun_GetLandPieceStateReg(uint8 LandPieceSeq, uint8 RequestSeq)
{
    uint16 addr;
    uint16 start;
    uint16 offset;

    addr = 0xFFFF;

    if (LandPieceSeq >= MAX_LAND_PIECE_NUM)
    {
        return addr;
    }

    switch (RequestSeq)
    {
        case REQUEST_FLOWMETER_ADDR:
            {
                start = 13;
                offset = 20;

                break;
            }
        case REQUEST_CLOSE_VALVE_ADDR:
            {
                start = 320;
                offset = 20;

                break;
            }
        case REQUEST_TASK_STATE_ADDR:
            {
                start = 339;
                offset = 20;

                break;
            }
        default:
            {
                return addr;
            }
    }

    addr = start + LandPieceSeq * offset;

    return addr;
}

/*******************************************************************************
*功能：获取地块序号
*参数：无
*返回：无
*说明：
*******************************************************************************/
static uint8 Fun_GetLandPieceSeq(uint16 RequestAddr, uint8 RequestSeq)
{
    uint8 seq;
    uint16 start;
    uint16 offset;
    uint16 i;
    uint16 addr;

    seq = 0xFF;

    switch (RequestSeq)
    {
        case REQUEST_FLOWMETER_ADDR:                                            //流量计地址
            {
                start = 13;
                offset = 20;

                break;
            }
        case REQUEST_CLOSE_VALVE_ADDR:                                          //关闭阀门地址
            {
                start = 320;
                offset = 20;

                break;
            }
        case REQUEST_TASK_STATE_ADDR:                                           //子任务状态寄存器
            {
                start = 339;
                offset = 20;

                break;
            }
        case REQUEST_TASK_START_ADDR:                                           //任务起始地址
            {
                /*DO1地址匹配过程*/
                start = 320;
                offset = 20;

                for (i = 0; i < MAX_LAND_PIECE_NUM; i++)
                {
                    addr = start + i * offset;
                    if (addr == RequestAddr)
                    {
                        seq = i;

                        return seq;
                    }
                }

                /*DO3地址匹配过程*/
                start = 328;
                offset = 20;

                for (i = 0; i < MAX_LAND_PIECE_NUM; i++)
                {
                    addr = start + i * offset;
                    if (addr == RequestAddr)
                    {
                        seq = i;

                        return seq;
                    }
                }

                return seq;
            }
        default:
            {
                return seq;
            }
    }

    for (i = 0; i < MAX_LAND_PIECE_NUM; i++)
    {
        addr = start + i * offset;
        if (addr == RequestAddr)
        {
            seq = i;

            break;
        }
    }

    return seq;
}

/*-------------------PLC部分------------------*/
/*******************************************************************************
*功能：读取流量计
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ReadLandPieceState(void)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    uint16 addr;
    uint16 offset;
    uint8 i;

    SendMsg = (_TxBuff *)msg.buff;

    PollLandPieceStateSeq++;
    PollLandPieceStateSeq = PollLandPieceStateSeq % MAX_POLL_LAND_PIECE_NUM;
    offset = 20;
    addr = 13;

    addr = PollLandPieceStateSeq * 4 * offset + addr;

    for (i = 0; i < 4; i++)
    {
        /*读地块状态*/
        Modbus_ReadReg(&SendMsg->buff, DEFAULT_PLC_SLAVE_ADDR, addr, 7, MODBUS_RTU, &msg.len);

        msg.protocol = PROTOCOL_MODBUS;
        SendMsg->type = SEND_POLL_MSG;
        Fun_Request485BusSendMsg(&msg);

        addr = addr + offset;

        Debug_Statics(offsetof(_Statics, RdValve), 1, sizeof(statics.RdValve)); //读阀门统计
        Debug_Statics(offsetof(_Statics, RdFlowMeter), 1, sizeof(statics.RdFlowMeter));//读流量计统计
    }

    return;
}

/*******************************************************************************
*功能：打开水泵
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_OpenPump(void)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    uint16 CtlPumpRegValue;

    Debug_Statics(offsetof(_Statics, OpenPump), 1, sizeof(statics.OpenPump));

    SendMsg = (_TxBuff *)msg.buff;

    CtlPumpRegValue = PUMP_OPEN;

    msg.protocol = PROTOCOL_MODBUS;
    SendMsg->type = SEND_CTL_PUMP_CMD;
    Modbus_WriteReg(&SendMsg->buff, DEFAULT_PLC_SLAVE_ADDR, REG_CTL_PUMP, &CtlPumpRegValue, 1, MODBUS_RTU, &msg.len);
    Fun_Request485BusSendMsg(&msg);

    PlcState.PumpState = PUMP_OPEN;

    return;
}

/*******************************************************************************
*功能：关闭水泵
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ClosePump(void)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    uint16 CtlPumpRegValue;

    Debug_Statics(offsetof(_Statics, ClosePump), 1, sizeof(statics.ClosePump));

    SendMsg = (_TxBuff *)msg.buff;

    CtlPumpRegValue = PUMP_CLOSE;

    msg.protocol = PROTOCOL_MODBUS;
    SendMsg->type = SEND_CTL_PUMP_CMD;
    Modbus_WriteReg(&SendMsg->buff, DEFAULT_PLC_SLAVE_ADDR, REG_CTL_PUMP, &CtlPumpRegValue, 1, MODBUS_RTU, &msg.len);
    Fun_Request485BusSendMsg(&msg);

    return;
}

/*******************************************************************************
*功能：关闭阀门
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CloseValve(uint8 LandPieceSeq)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    uint16 CloseValveAction[11];
    uint16 addr;

    /*尝试关闭阀门*/
    Debug_Statics(offsetof(_Statics, CloseValve), 1, sizeof(statics.CloseValve));

    /*关闭阀门：DO1*/
    SendMsg = (_TxBuff *)msg.buff;

    CloseValveAction[0] = 1;                                                    //定时方式
    CloseValveAction[1] = 0;                                                    //预约时间
    CloseValveAction[2] = 0;
    CloseValveAction[3] = 0;
    CloseValveAction[4] = 0;                                                    //开启至关闭时间

    /*关闭阀门：DO3*/
    CloseValveAction[5] = 1;                                                    //定时方式
    CloseValveAction[6] = 0;                                                    //预约时间
    CloseValveAction[7] = 0;
    CloseValveAction[8] = 0;
    CloseValveAction[9] = 0;                                                    //开启至关闭时间

    addr = Fun_GetLandPieceStateReg(LandPieceSeq, REQUEST_CLOSE_VALVE_ADDR);
    if (addr == 0xFFFF)
    {
        return;
    }

    msg.protocol = PROTOCOL_MODBUS;
    SendMsg->type = SEND_CLEAR_TASK_STATE_MSG;
    Modbus_WriteReg(&SendMsg->buff, DEFAULT_PLC_SLAVE_ADDR, addr, CloseValveAction, 10, MODBUS_RTU, &msg.len);
    Fun_Request485BusSendMsg(&msg);

    PlcState.SendState = true;

    return;
}


/*******************************************************************************
*功能：电磁阀状态改变事件
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ValveStateChangedEvent(uint16 ValveState, uint8 LandPieceSeq, bool *ValveChanged)
{
    _IF buff;
    _ForwardToBindDTU *ForwardToBindDTU;
    uint16 len;

    *ValveChanged = false;

    if (LandPieceSeq >= MAX_LAND_PIECE_NUM)
    {
        return;
    }

    /*地块的阀门变化状态*/
    ValveState = Lib_htons(ValveState);

    if ((ValveState > 0 && LandPiece[LandPieceSeq].ValveState == false)
        || (ValveState == 0 && LandPiece[LandPieceSeq].ValveState == true))
    {
        if (LandPiece[LandPieceSeq].ValveState == false)
        {
            LandPiece[LandPieceSeq].ValveState = true;
            PlcState.ValveState |= (uint32)1 << LandPieceSeq;
        }
        else
        {
            LandPiece[LandPieceSeq].ValveState = false;
            PlcState.ValveState &= ~((uint32)1 << LandPieceSeq);
        }

        *ValveChanged = true;                                                   //阀门状态发生改变
    }

    /*整个系统的阀门状态*/
    if (SysValveState == 0 && PlcState.ValveState == 0)
    {
        if (memcmp(cfg.BindDtuList, Gprs.IMEI, sizeof(Gprs.IMEI)) == 0)         //水泵处于非关闭状态下，关闭水泵
        {
            Fun_ClosePump();

            PlcState.PumpState = PUMP_CLOSE;
        }
    }

    if (SysValveState != 0 || PlcState.ValveState > 0)
    {
        if (memcmp(cfg.BindDtuList, Gprs.IMEI, sizeof(Gprs.IMEI)) == 0)         //检查是否关联的是自己
        {
            Fun_OpenPump();
            PlcState.PumpState = PUMP_OPEN;                                     //打开水泵
        }
    }

    if (memcmp(cfg.BindDtuList, Gprs.IMEI, sizeof(Gprs.IMEI)) != 0)             //检查是否关联的是自己
    {
        if (*ValveChanged == false)
        {
            return;
        }

        /*转发至关联dtu*/
        buff.head.version = CMD_VERSION;
        buff.head.cmd = CMD_FORWARD_TO_BIND_DTU;
        buff.head.len = Lib_htons(sizeof(_ForwardToBindDTU));

        ForwardToBindDTU = (_ForwardToBindDTU *)buff.buff;

        memcpy(ForwardToBindDTU->IMEI, cfg.BindDtuList, sizeof(ForwardToBindDTU->IMEI));
        ForwardToBindDTU->seq = ForwardToBindDtuSeq++;
        if (ValveState == 0)
        {
            ForwardToBindDTU->ValveState = 0;
        }
        else
        {
            ForwardToBindDTU->ValveState = 1;
        }

        len = offsetof(_IF, buff) + sizeof(_ForwardToBindDTU);
        PHY_RequestGprsSendMsg((uint8 *)&buff, len, CMD_FORWARD_TO_BIND_DTU, 0xFF, 5, 20, true);
    }

    return;
}

/*-------------------异常检查部分------------------*/

/*******************************************************************************
*功能：检查特权指令异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckPrivilegeCmdException(void)
{
    if (ProcessMode_LogicLockException == EXCEPTION_NO_MONITOR)
    {
        Exception_PrivilegeCmdCounter = 0;

        return;
    }

    if (Exception_PrivilegeCmdCounter >= EXCEPTION_PRIVILEGE_CMD_NUM)           //检查是否超过特权指令上限
    {
        App_ReportExceptionToServer(EXCEPTION_LOGIC_LOCK, NULL, 0);             //向服务器发送特权指令锁定异常

        Exception_PrivilegeCmdCounter = 0;
    }
    else
    {
        Exception_PrivilegeCmdCounter++;
    }

    return;
}

/*******************************************************************************
*功能：检查指令锁定异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckLockCmdException(void)
{
    if (ProcessMode_CmdLockException == EXCEPTION_NO_MONITOR)
    {
        Exception_LockCmdCounter = 0;

        return;
    }

    if (Exception_LockCmdCounter >= EXCEPTION_LOCK_CMD_NUM)                     //判断是否超过指令锁定上限
    {
        App_ReportExceptionToServer(EXCEPTION_CMD_LOCK, NULL, 0);               //向服务器发送指令锁定异常
    }
    else
    {
        Exception_LockCmdCounter++;
    }

    return;
}

/*******************************************************************************
*功能：检查阀门异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckValveAndFlowMeterExcetion(uint16 FlowValue1, uint16 FlowValue2, uint8 LandPieceSeq, bool ValveChanged)
{
    uint16 FlowValue;
    uint8 buff[5];

    FlowValue = FlowValue1 + FlowValue2;

    if (LandPieceSeq >= MAX_LAND_PIECE_NUM)
    {
        return;
    }

    if (LandPiece[LandPieceSeq].FirstGetFlowMeter == true
        || ValveChanged == true)
    {
        LandPiece[LandPieceSeq].FlowMeterValue = FlowValue;
        LandPiece[LandPieceSeq].FirstGetFlowMeter = false;

        return;
    }

    if (LandPiece[LandPieceSeq].ValveState == false)
    {
        if (ProcessMode_ValveException == EXCEPTION_NO_MONITOR)
        {
            LandPiece[LandPieceSeq].Exception_ValveCounter = 0;

            return;
        }

        /*阀已经关闭，但流量计值依然在增长*/
        if (LandPiece[LandPieceSeq].FlowMeterValue != FlowValue)
        {
            if (LandPiece[LandPieceSeq].Exception_ValveCounter >= cfg.ValveExceptionLimit)
            {
                LandPiece[LandPieceSeq].Exception_ValveCounter = 0;

                /*封装负载*/
                buff[0] = LandPieceSeq + 1;

                FlowValue1 = Lib_htons(FlowValue1);
                memcpy(&buff[1], &FlowValue1, 2);

                FlowValue2 = Lib_htons(FlowValue2);
                memcpy(&buff[3], &FlowValue2, 2);

                /*向服务器发送阀门异常*/
                App_ReportExceptionToServer(EXCEPTION_VALVE, buff, sizeof(buff));
            }
            else
            {
                LandPiece[LandPieceSeq].Exception_ValveCounter++;
            }

            LandPiece[LandPieceSeq].FlowMeterValue = FlowValue;
        }
        else
        {
            LandPiece[LandPieceSeq].Exception_ValveCounter = 0;
        }
    }
    else
    {
        if (ProcessMode_FlowMeterException == EXCEPTION_NO_MONITOR)
        {
            LandPiece[LandPieceSeq].Exception_FlowMeterCounter = 0;

            return;
        }

        /*阀已经打开，但流量计值不增长*/
        if (LandPiece[LandPieceSeq].FlowMeterValue == FlowValue)
        {
            if (LandPiece[LandPieceSeq].Exception_FlowMeterCounter > cfg.FlowMeterExceptionLimit)
            {
                LandPiece[LandPieceSeq].Exception_FlowMeterCounter = 0;

                Fun_CloseValve(LandPieceSeq);

                /*向服务器发送流量计异常*/
                App_ReportExceptionToServer(EXCEPTION_FLOWMETER, NULL, 0);
            }
            else
            {
                LandPiece[LandPieceSeq].Exception_FlowMeterCounter++;
            }
        }
        else
        {
            LandPiece[LandPieceSeq].FlowMeterValue = FlowValue;
            LandPiece[LandPieceSeq].Exception_ValveCounter = 0;
        }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckLandPieceState(_SysMsg *SrcMsg, uint8 LandPieceSeq)
{
    _Modbus *reply;
    bool ValveChanged;
    _CMD3Reply *Cmd3Reply;

    reply = (_Modbus *)SrcMsg->buff;
    Cmd3Reply = (_CMD3Reply *)reply->UsartType.pdu.data;

    ValveChanged = false;
    Fun_ValveStateChangedEvent(Cmd3Reply->data[6], LandPieceSeq, &ValveChanged);

    Debug_Statics(offsetof(_Statics, ResValve), 1, sizeof(statics.ResValve));   //读阀门响应统计
    Debug_Statics(offsetof(_Statics, ResFlowMeter), 1, sizeof(statics.ResFlowMeter));//读流量计响应统计

    Fun_CheckValveAndFlowMeterExcetion(Lib_htons(Cmd3Reply->data[0]), Lib_htons(Cmd3Reply->data[1]), LandPieceSeq, ValveChanged);

    return;
}

/*******************************************************************************
*功能：通过读取互感器电流值的方式来检查水泵异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckPumpException(_SysMsg *SrcMsg)
{
    _Modbus *reply;
    _CMD3Reply *Cmd3Reply;
    uint16 current;

    if (ProcessMode_PumpException == EXCEPTION_NO_MONITOR)
    {
        Exception_PumpCouter = 0;
        PumpExceptionalTryCloseCounter = 0;

        return;
    }

    reply = (_Modbus *)SrcMsg->buff;
    Cmd3Reply = (_CMD3Reply *)reply->UsartType.pdu.data;

    current = Cmd3Reply->data[0];
    current = Lib_htons(current);

    if (PlcState.PumpState == PUMP_OPEN)                                        //水泵处于打开状态
    {
        if (current == 0)                                                       //异常：电流值为零
        {
            if (Exception_PumpCouter <= cfg.PumpExcetionLimit)                  //未达到尝试开泵次数
            {
                Exception_PumpCouter++;
                Fun_OpenPump();
            }
            else
            {
                if (PumpExceptionalTryCloseCounter == 0)
                {
                    Fun_TerminalSubTask(TASK_STATE_CANCEL);                     //结束子任务

                    current = Lib_htons(current);
                    App_ReportExceptionToServer(EXCEPTION_PUMP_WORK, (uint8 *)&current, sizeof(current));//上报异常
                }

                if (PumpExceptionalTryCloseCounter < PUMP_EXCEPTIONAL_TRY_CLOSE_LIMIT)
                {
                    Fun_ClosePump();                                            //尝试关泵
                    PumpExceptionalTryCloseCounter++;
                    Lock_Pump = true;
                }
                else
                {
                    PlcState.PumpState = PUMP_CLOSE;
                    PumpExceptionalTryCloseCounter = PUMP_EXCEPTIONAL_TRY_CLOSE_LIMIT;
                }
            }
        }
        else                                                                    //正常
        {
            PumpExceptionalTryCloseCounter = 0;
            Exception_PumpCouter = 0;
        }
    }
    else
    {
        if (current > 0)                                                        //电流值不为零
        {
            /*尝试关泵*/
            if (Exception_PumpCouter < cfg.PumpExcetionLimit)
            {
                Exception_PumpCouter++;
                Fun_ClosePump();
                PlcState.PumpState = PUMP_CLOSE;
            }
            else
            {
                if (PumpExceptionalTryCloseCounter == 0)
                {
                    /*结束子任务*/
                    Fun_TerminalSubTask(TASK_STATE_CANCEL);

                    current = Lib_htons(current);
                    App_ReportExceptionToServer(EXCEPTION_PUMP_WORK, (uint8 *)&current, sizeof(current));
                }

                if (PumpExceptionalTryCloseCounter < PUMP_EXCEPTIONAL_TRY_CLOSE_LIMIT)
                {
                    Fun_ClosePump();
                    PlcState.PumpState = PUMP_CLOSE;
                    PumpExceptionalTryCloseCounter++;
                    Lock_Pump = true;
                }
                else
                {
                    PlcState.PumpState = PUMP_CLOSE;
                    PumpExceptionalTryCloseCounter = PUMP_EXCEPTIONAL_TRY_CLOSE_LIMIT;
                }
            }
        }
        else                                                                    //正常
        {
            PumpExceptionalTryCloseCounter = 0;
            Exception_PumpCouter = 0;
        }
    }

    return;
}

/*******************************************************************************
*功能：检查电路保护异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheakCircuitProtectException(_SysMsg *SrcMsg)
{
    _Modbus *reply;
    _CMD3Reply *Cmd3Reply;
    uint16 PumpState;

    if (ProcessMode_CircuitProtectException == EXCEPTION_NO_MONITOR)
    {
        Exception_CircuitProtectCounter = 0;

        return;
    }

    reply = (_Modbus *)SrcMsg->buff;
    Cmd3Reply = (_CMD3Reply *)reply->UsartType.pdu.data;

    PumpState = Cmd3Reply->data[0];
    PumpState = Lib_htons(PumpState);

    if (PlcState.PumpState == PUMP_OPEN)
    {
        if (PumpState == PUMP_CLOSE)
        {
            if (Exception_CircuitProtectCounter <= cfg.CircuitProtectExceptionLimit)
            {
                Exception_CircuitProtectCounter++;
            }
            else
            {
                if (Lock_CircuitProtect == true)
                {
                    return;
                }

                Lock_CircuitProtect = true;

                App_ReportExceptionToServer(EXCEPTION_CIRCUIT_PROTECT, NULL, 0);
                Fun_TerminalSubTask(TASK_STATE_CANCEL);
            }
        }
        else
        {
            Exception_CircuitProtectCounter = 0;
        }
    }

    return;
}

/*******************************************************************************
*功能：检查gprs通信异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckGprsCommuteException(void)
{
    if (ProcessMode_NetCommuteException == EXCEPTION_NO_MONITOR)
    {
        Exception_GprsCommuteCounter = 0;

        return;
    }

    if (Exception_GprsCommuteCounter < MAX_GPRS_COMMUTE_FAIL_NUM)
    {
        Exception_GprsCommuteCounter++;
    }
    else
    {
        App_ReportExceptionToServer(EXCEPTION_NET_COMMUTE, NULL, 0);

        Exception_GprsCommuteCounter = 0;
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CheckCommuteException(_SysMsg *SrcMsg)
{
    _Modbus *reply;
    _CMD3Reply *Cmd3Reply;
    uint16 CommuteState;

    reply = (_Modbus *)SrcMsg->buff;
    Cmd3Reply = (_CMD3Reply *)reply->UsartType.pdu.data;

    CommuteState = Cmd3Reply->data[0];
    CommuteState = Lib_htons(CommuteState);

    if (PlcState.CommuteState == CommuteState)
    {
        return;
    }

    PlcState.CommuteState = CommuteState;

    if (CommuteState > 0 && CommuteState != 0xFFFF)
    {
        if (ProcessMode_WirelessCommnuteException == EXCEPTION_NO_MONITOR)
        {
            return;
        }

        CommuteState = Lib_htons(CommuteState);
        App_ReportExceptionToServer(EXCEPTION_WIRELESS_COMMUTE, (uint8 *)&CommuteState, sizeof(CommuteState));
    }

    if (CommuteState == 0xFFFF)
    {
        if (ProcessMode_CoordinatorException == EXCEPTION_NO_MONITOR)
        {
            return;
        }

        CommuteState = Lib_htons(CommuteState);
        App_ReportExceptionToServer(EXCEPTION_COORDINATOR, (uint8 *)&CommuteState, sizeof(CommuteState));
    }

    return;
}

/*******************************************************************************
*功能：plc轮询定时器回调函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_PlcPollTmrHandler(void *p_tmr, void *p_arg)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    OS_ERR err;
    uint32 tick;
    uint16 addr;
    uint16 SendState;

    if (Lock_PrivilegeCmd == true)
    {
        return;
    }

    SendMsg = (_TxBuff *)msg.buff;

    /*在接互感器的情况下，读互感器电流，识别水泵异常*/
    if (cfg.ConnectedMutualInductor == true)
    {
        Modbus_ReadReg(&SendMsg->buff, DEFAULT_TRANSFORMER_ADDR, REG_TRANSFORMER_CURRENT, 1, MODBUS_RTU, &msg.len);

        msg.protocol = PROTOCOL_MODBUS;
        SendMsg->type = SEND_POLL_MSG;
        Fun_Request485BusSendMsg(&msg);

        Debug_Statics(offsetof(_Statics, RdTransformer), 1, sizeof(statics.RdTransformer));
    }

    /*在开启水泵的情况下，查询电机关联寄存器，识别电路保护异常*/
    if (PlcState.PumpState == PUMP_OPEN)
    {
        Debug_Statics(offsetof(_Statics, RdMotorBind), 1, sizeof(statics.RdMotorBind));

        Modbus_ReadReg(&SendMsg->buff, DEFAULT_PLC_SLAVE_ADDR, REG_MOTOR_BIND, 1, MODBUS_RTU, &msg.len);

        msg.protocol = PROTOCOL_MODBUS;
        SendMsg->type = SEND_POLL_MSG;
        Fun_Request485BusSendMsg(&msg);
    }


    /*一包轮询地块*/
    Fun_ReadLandPieceState();


    /*轮询通信状态*/
    Modbus_ReadReg(&SendMsg->buff, DEFAULT_PLC_SLAVE_ADDR, REG_COMMUTE_STATE, 1, MODBUS_RTU, &msg.len);

    msg.protocol = PROTOCOL_MODBUS;
    SendMsg->type = SEND_POLL_MSG;
    Fun_Request485BusSendMsg(&msg);

    Debug_Statics(offsetof(_Statics, RdCommute), 1, sizeof(statics.RdCommute)); //读通信状态统计次数

    if (PlcState.SendState == true)
    {
        /*发送 发送状态*/
        SendMsg = (_TxBuff *)msg.buff;

        SendState = 1;

        msg.protocol = PROTOCOL_MODBUS;
        SendMsg->type = SEND_CLEAR_TASK_STATE_MSG;
        Modbus_WriteReg(&SendMsg->buff, DEFAULT_PLC_SLAVE_ADDR, REG_SEND_STATE, &SendState, 1, MODBUS_RTU, &msg.len);
        Fun_Request485BusSendMsg(&msg);

        PlcState.SendState = false;
    }

    /*根据当前是否正在执行子任务来决定是否读子任务状态寄存器*/
    if (WorkTaskList.IsExecuteSubTask == true)
    {
        if (WorkTaskList.SendState == true)
        {
            /*发送 发送状态*/
            SendMsg = (_TxBuff *)msg.buff;

            SendState = 1;

            msg.protocol = PROTOCOL_MODBUS;
            SendMsg->type = SEND_CLEAR_TASK_STATE_MSG;
            Modbus_WriteReg(&SendMsg->buff, DEFAULT_PLC_SLAVE_ADDR, REG_SEND_STATE, &SendState, 1, MODBUS_RTU, &msg.len);
            Fun_Request485BusSendMsg(&msg);

            WorkTaskList.SendState = false;

            return;
        }

        if (WorkTaskList.ServerTaskPtr->TaskType == SUB_TASK_TIMING)            //定时任务
        {
            if (WorkTaskList.ServerTaskPtr->OverTime != 0)                      //超时时间有效
            {
                tick = OSTimeGet(&err);
                if (tick >= WorkTaskList.SubTaskStartTime)                      //判断是否超时
                {
                    if ((tick - WorkTaskList.SubTaskStartTime) >= WorkTaskList.SubTaskOverTime)
                    {
                        /*子任务超时*/
                        Fun_TerminalSubTask(TASK_STATE_OVER_TIME);              //结束所有子任务逻辑

                        return;
                    }
                }
                else
                {
                    if ((WorkTaskList.SubTaskStartTime - tick) >= WorkTaskList.SubTaskOverTime)
                    {
                        /*子任务超时*/
                        Fun_TerminalSubTask(TASK_STATE_OVER_TIME);              //结束所有子任务逻辑

                        return;
                    }
                }
            }
        }

        Debug_Statics(offsetof(_Statics, RdTask), 1, sizeof(statics.RdTask));

        addr = Fun_GetLandPieceStateReg(WorkTaskList.LandPieceSeq, REQUEST_TASK_STATE_ADDR);
        Modbus_ReadReg(&SendMsg->buff, DEFAULT_PLC_SLAVE_ADDR, addr, 1, MODBUS_RTU, &msg.len);

        msg.protocol = PROTOCOL_MODBUS;
        SendMsg->type = SEND_POLL_MSG;
        Fun_Request485BusSendMsg(&msg);
    }

    return;
}

/*-------------------异常处理部分------------------*/

/*******************************************************************************
*功能：向服务器上报异常失败
*参数：无
*返回：无
*说明：严重异常不处理；普通异常清空异常计数器
*******************************************************************************/
static void Fun_ReportExceptionToServerFail(_GprsSendBuff *SrcMsg)
{
    _IF *msg;
    _ExceptionReport *ExceptionReport;

    msg = (_IF *)SrcMsg->buff;

    ExceptionReport = (_ExceptionReport *)msg->buff;

    switch (ExceptionReport->ExceptionNum)
    {
        case EXCEPTION_485_OVER_TIME:                                           //485通信超时异常；严重异常
            {
                break;
            }
        case EXCEPTION_PUMP_WORK:                                               //水泵工作异常；严重异常
            {
                break;
            }
        case EXCEPTION_FORWARD:                                                 //转发异常；普通异常
            {
                break;
            }
        case EXCEPTION_PARAMETER:                                               //参数异常；严重异常
            {
                break;
            }
        case EXCEPTION_CIRCUIT_PROTECT:                                         //电路保护异常；严重异常
            {
                break;
            }
        case EXCEPTION_NET_COMMUTE:                                             //网络通信异常；普通异常
            {
                break;
            }
        case EXCEPTION_WIRELESS_COMMUTE:                                        //无线通信异常；普通异常
            {
                break;
            }
        case EXCEPTION_COORDINATOR:                                             //协调器异常；普通异常
            {
                break;
            }
        case EXCEPTION_VALVE:                                                   //阀门异常；普通异常
            {
                break;
            }
        case EXCEPTION_FLOWMETER:                                               //流量计异常；普通异常
            {
                break;
            }
        case EXCEPTION_CMD_LOCK:                                                //指令锁定异常；普通异常
            {
                break;
            }
        case EXCEPTION_LOGIC_LOCK:                                              //逻辑锁定异常；普通异常
            {
                break;
            }
        default:
            {
                break;
            }
    }

    return;
}

/*---------------------外部接口函数-------------------*/

/*******************************************************************************
*功能：收到服务器转发过来的、其他设备的阀位状态
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_PlcReceivedServerForwardRequest(_IF *msg, uint16 len)
{
    _ForwardToBindDTU *ForwardToBindDtuRequest;
    bool result;
    bool repetitive;
    uint8 ImeiPart[IMEI_LEN];

    result = true;

    if (PlcModuleIsInit == false)
    {
        return;
    }

    msg->head.len = Lib_htons(msg->head.len);
    if ((msg->head.len + offsetof(_IF, buff)) != len)
    {
        result = false;
    }

    if (msg->head.len != sizeof(_ForwardToBindDTU))
    {
        result = false;
    }

    if (result == true)
    {
        ForwardToBindDtuRequest = (_ForwardToBindDTU *)msg->buff;

        repetitive = true;

        /*根据IMEI的规则，截取生产序列号部分*/
        memcpy(ImeiPart, &ForwardToBindDtuRequest->IMEI[8], sizeof(ImeiPart));

        repetitive = Fun_CheckRxTransTable(ImeiPart, ForwardToBindDtuRequest->seq);

        if (repetitive == false)
        {
            Debug_Statics(offsetof(_Statics, PlcRxForwardRequest), 1, sizeof(statics.PlcRxForwardRequest));

            if (ForwardToBindDtuRequest->ValveState > 0)
            {
                SysValveState++;

                Fun_OpenPump();
            }
            else
            {
                if (SysValveState > 0)
                {
                    SysValveState--;
                }

                if (SysValveState == 0 && PlcState.ValveState == 0)
                {
                    Fun_ClosePump();
                    PlcState.PumpState = PUMP_CLOSE;
                }
            }
        }
    }

    Debug_Statics(offsetof(_Statics, ResPlcRxForwardRequest), 1, sizeof(statics.ResPlcRxForwardRequest));

    msg->head.len = Lib_htons(msg->head.len);
    msg->head.cmd = CMD_RECEIVED_SERVER_FORWARD_REPONSE;
    PHY_RequestGprsSendMsg((uint8 *)msg, len, CMD_RECEIVED_SERVER_FORWARD_REPONSE, 0xFF, 1, 0, false);

    return;
}

/*******************************************************************************
*功能：异常上报响应（服务器下发）
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_PlcReportExceptionRespose(_IF *msg, uint16 len)
{
    _ExceptionReportResponse *ExceptionReportResponse;
    _GprsSendBuff *TxBuff;

    if (PlcModuleIsInit == false)
    {
        return;
    }

    msg->head.len = Lib_htons(msg->head.len);
    if ((msg->head.len + offsetof(_IF, buff)) != len)
    {
        return;
    }

    if (msg->head.len != sizeof(_ExceptionReportResponse))
    {
        return;
    }

    Debug_Statics(offsetof(_Statics, ResReportException), 1, sizeof(statics.ResReportException));

    /*搜索gprs发送缓存，停止重传*/
    ExceptionReportResponse = (_ExceptionReportResponse *)msg->buff;
    TxBuff = PHY_SearchGprsSendBuff(CMD_REPORT_EXCEPTION, NULL, 0);

    if (TxBuff == NULL)
    {
        return;
    }

    switch (ExceptionReportResponse->ExceptionNum)
    {
        case EXCEPTION_NET_COMMUTE:
            {
                Exception_GprsCommuteCounter = 0;

                break;
            }
    }

    return;
}

/*******************************************************************************
*功能：server下发的特权指令
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_PlcPrivilegeCmd(_IF *SrcMsg, uint16 len)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    bool IsException;
    bool valid;
    _IF buff;

    if (PlcModuleIsInit == false)
    {
        return;
    }

    valid = true;

    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);
    if ((SrcMsg->head.len + offsetof(_IF, buff)) != len)
    {
        valid = false;
    }

    if (valid == true)
    {
        IsException = false;

        /*水泵锁定状态*/
        if (Lock_Pump == true)
        {
            IsException = true;
        }

        /*电路保护异常锁定状态*/
        if (Lock_CircuitProtect == true)
        {
            IsException = true;
        }

        /*处于485总线超时锁定状态*/
        if (Lock_485OverTime == true)
        {
            IsException = true;
        }

        /*处于参数异常锁定状态*/
        if (Lock_ParameterException == true)
        {
            IsException = true;
        }

        /*处于严重状态*/
        if (IsException == true)
        {
            Fun_CheckLockCmdException();
        }

        /*特权指令锁定*/
        Lock_PrivilegeCmd = true;

        if (IsException == false)
        {
            /*取消当前的灌溉逻辑，原因：人为取消*/
            Fun_TerminalSubTask(TASK_STATE_CANCEL);

            /*请求485总线发送特权指令*/
            SendMsg = (_TxBuff *)msg.buff;

            msg.protocol = PROTOCOL_NONE;
            msg.len = len - offsetof(_IF, buff);

            if (msg.len >= sizeof(msg.buff))
            {
                return;
            }

            memcpy(&SendMsg->buff, SrcMsg->buff, msg.len);
            SendMsg->type = SEND_PRIVILEGE_CMD;

            Debug_Statics(offsetof(_Statics, privilege), 1, sizeof(statics.privilege));

            Fun_Request485BusSendMsg(&msg);
        }
    }

    /*回复响应，无负载*/
    buff.head.version = CMD_VERSION;
    buff.head.cmd = CMD_PRIVILEGE_CMD_RESPOSE;

    buff.head.len = 1;
    buff.head.len = Lib_htons(buff.head.len);
    buff.buff[0] = valid;

    len = offsetof(_IF, buff) + 1;
    PHY_RequestGprsSendMsg((uint8 *)&buff, len, CMD_PRIVILEGE_CMD_RESPOSE, 0xFF, 1, 0, false);

    return;
}

/*******************************************************************************
*功能：server取消下发的特权指令
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_PlcCancelPrivilegeCmd(_IF *msg, uint16 len)
{
    _IF buff;

    if (PlcModuleIsInit == false)
    {
        return;
    }

    msg->head.len = Lib_htons(msg->head.len);
    if ((msg->head.len + offsetof(_IF, buff)) != len)
    {
        return;
    }

    Debug_Statics(offsetof(_Statics, CancelPrivilege), 1, sizeof(statics.CancelPrivilege));

    /*取消特权指令模式*/
    Lock_PrivilegeCmd = false;

    Exception_PrivilegeCmdCounter = 0;

    /*回复响应，无负载*/
    buff.head.version = CMD_VERSION;
    buff.head.cmd = CMD_CANCEL_PRIVILEGE_CMD_RESPOSE;
    buff.head.len = 0;

    len = offsetof(_IF, buff);
    PHY_RequestGprsSendMsg((uint8 *)&buff, len, CMD_CANCEL_PRIVILEGE_CMD_RESPOSE, 0xFF, 1, 0, false);

    return;
}

/*******************************************************************************
*功能：server下发任务
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_SetPlcWorkTask(_IF *msg, uint16 len)
{
    _IF buff;
    uint16 SubTaskPtr;
    uint16 offset;                                                              //偏移量
    _ServerTask *ServerTask;
    bool valid;
    bool IsException;
    uint16 StartAddr;
    uint8 LandPieceSeq;

    if (PlcModuleIsInit == false)
    {
        return;
    }

    msg->head.len = Lib_htons(msg->head.len);

    IsException = false;

    /*水泵锁定状态*/
    if (Lock_Pump == true)
    {
        IsException = true;
    }

    /*电路保护异常锁定状态*/
    if (Lock_CircuitProtect == true)
    {
        IsException = true;
    }

    /*处于485总线超时锁定状态*/
    if (Lock_485OverTime == true)
    {
        IsException = true;
    }

    /*处于参数异常锁定状态*/
    if (Lock_ParameterException == true)
    {
        IsException = true;
    }

    /*处于严重状态*/
    if (IsException == true)
    {
        Fun_CheckLockCmdException();
    }

    /*处于特权指令锁定状态*/
    if (Lock_PrivilegeCmd == true)
    {
        Fun_CheckPrivilegeCmdException();
        IsException = true;
    }

    valid = true;

    /*参数检查*/
    if (len >= sizeof(WorkTaskList.TaskList))
    {
        valid = false;

        return;
    }

    if (len <= (sizeof(msg->head) + 1))
    {
        valid = false;

        return;
    }

    if (len != msg->head.len + offsetof(_IF, buff))
    {
        valid = false;

        return;
    }

    if (IsException == false && valid == true)
    {
        /*检查发送的任务报文的正确性*/
        offset = offsetof(_IF, buff);
        SubTaskPtr = 1;
        ServerTask = (_ServerTask *)&msg->buff[SubTaskPtr];                     //msg->buff[0]为任务id

        while (true)
        {
            ServerTask->len = Lib_htons(ServerTask->len);

            if (ServerTask->TaskType != SUB_TASK_TIMING && ServerTask->TaskType != SUB_TASK_RATION)
            {
                /*下达的任务列表参数不正确*/
                valid = false;

                break;
            }

            /*检查报文负载的crc是否正确*/
            if (Modbus_CheckCRC(ServerTask->buff, ServerTask->len) == false)
            {
                valid = false;

                break;
            }

            /*检查起始地址是否正确*/
            memcpy(&StartAddr, ((_Modbus *)ServerTask->buff)->UsartType.pdu.data, 2);
            StartAddr = Lib_htons(StartAddr);
            LandPieceSeq = Fun_GetLandPieceSeq(StartAddr, REQUEST_TASK_START_ADDR);
            if (LandPieceSeq == 0xFF)
            {
                /*下达的任务列表不正确*/
                valid = false;

                break;
            }

            if ((SubTaskPtr + offset + offsetof(_ServerTask, buff) + ServerTask->len) == len)
            {
                valid = true;

                break;
            }
            else if ((SubTaskPtr + offset + offsetof(_ServerTask, buff) + ServerTask->len) > len)
            {
                /*下达的任务列表不正确*/
                valid = false;

                break;
            }

            SubTaskPtr += ServerTask->len + offsetof(_ServerTask, buff);
            ServerTask = (_ServerTask *)&msg->buff[SubTaskPtr];
        }
    }

    /*回复响应，无负载*/
    buff.head.version = CMD_VERSION;
    buff.head.cmd = CMD_SET_WORK_TASK_RESPONSE;
    buff.head.len = 1;

    if (IsException == true)
    {
        buff.buff[0] = 2;
    }
    else
    {
        buff.buff[0] = valid;
    }

    buff.head.len = Lib_htons(buff.head.len);
    PHY_RequestGprsSendMsg((uint8 *)&buff, offsetof(_IF, buff) + 1, CMD_SET_WORK_TASK_RESPONSE, 0xFF, 1, 0, false);

    if (valid == false)
    {
        return;
    }

    if (IsException == true)
    {
        return;
    }

    /*开始执行子任务*/
    memcpy(WorkTaskList.TaskList, &msg->buff[1], len - sizeof(msg->head) - 1);
    WorkTaskList.id = msg->buff[0];
    WorkTaskList.IsExecuteSubTask = false;
    WorkTaskList.HaveSubTask = true;
    WorkTaskList.SubTaskIndex =  - 1;

    WorkTaskList.len = len - sizeof(msg->head) - 1;
    WorkTaskList.TaskListPtr = 0;

    Debug_Statics(offsetof(_Statics, SetTask), 1, sizeof(statics.SetTask));

    /*获取首个子任务*/
    if (Fun_GetNextSubTask() == false)
    {
        WorkTaskList.IsExecuteSubTask = false;
        Fun_SendSubTaskReport();
    }
    else
    {
        Fun_ExecuteSubTask();                                                   //开始执行第一个子任务
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_PlcRegisterToServerResponse(_IF *SrcMsg, uint16 len)
{
    _GprsSendBuff *TxBuff;

    if (PlcModuleIsInit == false)
    {
        return;
    }

    if (SrcMsg->head.len != 0)
    {
        return;
    }

    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);
    if (len != SrcMsg->head.len + offsetof(_IF, buff))
    {
        return;
    }

    Debug_Statics(offsetof(_Statics, ResRegister), 1, sizeof(statics.ResRegister));

    TxBuff = PHY_SearchGprsSendBuff(CMD_REGISTER_TO_SERVER, NULL, 0);

    if (TxBuff == NULL)
    {
        return;
    }

    return;
}

/*******************************************************************************
*功能：复位异常
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_PlcResetException(_SysMsg *msg, uint16 len)
{
    _ResetException *ResetException;
    bool result;
    _IF *response;
    _IF *SrcMsg;

    if (PlcModuleIsInit == false)
    {
        return;
    }

    result = true;

    SrcMsg = (_IF *)msg->buff;
    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);

    /*报文参数检查*/
    if (SrcMsg->head.len != sizeof(_ResetException))
    {
        result = false;
    }

    if ((offsetof(_IF, buff) + sizeof(_ResetException)) != len)
    {
        result = false;
    }

    ResetException = (_ResetException *)SrcMsg->buff;

    if (result == true)
    {
        if (ResetException->ProcessMode != EXCEPTION_MONITOR
            && ResetException->ProcessMode != EXCEPTION_NO_MONITOR)
        {
            result = false;
        }
    }

    if (result == true)
    {
        Debug_Statics(offsetof(_Statics, ResetException), 1, sizeof(statics.ResetException));

        switch (ResetException->ExceptionNum)
        {
            case EXCEPTION_485_OVER_TIME:                                       //485通信超时异常；严重异常
                {
                    Lock_485OverTime = false;
                    ProcessMode_485OverTimeException = ResetException->ProcessMode;
                    Exception_485OverTimeCounter = 0;

                    statics.Exception_485_over_time = 0;

                    break;
                }
            case EXCEPTION_PUMP_WORK:                                           //水泵工作异常；严重异常
                {
                    Lock_Pump = false;
                    ProcessMode_PumpException = ResetException->ProcessMode;
                    Exception_PumpCouter = 0;
                    PumpExceptionalTryCloseCounter = 0;

                    statics.Exception_pump_work = 0;

                    break;
                }
            case EXCEPTION_FORWARD:                                             //转发异常
                {
                    ProcessMode_ForwardException = ResetException->ProcessMode;

                    statics.Exception_forward = 0;

                    break;
                }
            case EXCEPTION_PARAMETER:                                           //参数异常；严重异常
                {
                    statics.Exception_parameter = 0;

                    break;
                }
            case EXCEPTION_CIRCUIT_PROTECT:                                     //电路保护异常；严重异常
                {
                    Lock_CircuitProtect = false;
                    ProcessMode_CircuitProtectException = ResetException->ProcessMode;
                    Exception_CircuitProtectCounter = 0;

                    statics.Exception_circuit_protect = 0;

                    break;
                }

            case EXCEPTION_NET_COMMUTE:                                         //网络通信异常
                {
                    ProcessMode_NetCommuteException = ResetException->ProcessMode;
                    Exception_GprsCommuteCounter = 0;

                    statics.Exception_net_commute = 0;

                    break;
                }
            case EXCEPTION_WIRELESS_COMMUTE:                                    //无线通信异常
                {
                    ProcessMode_WirelessCommnuteException = ResetException->ProcessMode;
                    PlcState.CommuteState = 0;

                    statics.Exception_wireless_commute = 0;

                    break;
                }
            case EXCEPTION_COORDINATOR:                                         //协调器异常
                {
                    ProcessMode_CoordinatorException = ResetException->ProcessMode;

                    statics.Exception_coordinator = 0;

                    break;
                }
            case EXCEPTION_VALVE:                                               //阀门异常
                {
                    ProcessMode_ValveException = ResetException->ProcessMode;

                    statics.Exception_valve = 0;

                    break;
                }
            case EXCEPTION_FLOWMETER:                                           //流量计异常
                {
                    ProcessMode_FlowMeterException = ResetException->ProcessMode;

                    statics.Exception_flowmeter = 0;

                    break;
                }
            case EXCEPTION_CMD_LOCK:                                            //指令锁定异常
                {
                    ProcessMode_CmdLockException = ResetException->ProcessMode;

                    statics.Exception_cmd_lock = 0;

                    break;
                }
            case EXCEPTION_LOGIC_LOCK:                                          //逻辑锁定异常
                {
                    ProcessMode_LogicLockException = ResetException->ProcessMode;

                    statics.Exception_logic_lock = 0;

                    break;
                }
            default:
                {
                    result = false;

                    break;
                }
        }
    }

    if (result == false)
    {
        return;
    }

    /*回复响应，无负载*/
    response = (_IF *)msg->buff;

    response->head.version = CMD_VERSION;
    response->head.cmd = CMD_RESET_EXCEPTION_RESPONSE;
    response->head.len = 1;
    response->head.len = Lib_htons(response->head.len);

    response->buff[0] = ResetException->ExceptionNum;

    len = offsetof(_IF, buff) + 1;

    if (msg->port == USART_GPRS_CHANNEL)
    {
        PHY_RequestGprsSendMsg((uint8 *)response, len, CMD_RESET_EXCEPTION_RESPONSE, 0xFF, 1, 0, false);
    }
    else
    {
        msg->len = len;
        msg->protocol = PROTOCOL_NONE;

        PHY_UsartSendMsg(msg);
    }

    return;
}

/*******************************************************************************
*功能：服务器下发的透明传输报文
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_PlcTransparentToMainDevice(_IF *SrcMsg, uint16 len)
{
    _SysMsg msg;
    _TxBuff *SendMsg;
    bool IsException;
    bool result;

    _IF *response;

    if (PlcModuleIsInit == false)
    {
        return;
    }

    result = true;

    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);
    if (len != SrcMsg->head.len + offsetof(_IF, buff))
    {
        result = false;

        goto function_end;
    }

    IsException = false;

    /*水泵锁定状态*/
    if (Lock_Pump == true)
    {
        IsException = true;
    }

    /*电路保护异常锁定状态*/
    if (Lock_CircuitProtect == true)
    {
        IsException = true;
    }

    /*处于485总线超时锁定状态*/
    if (Lock_485OverTime == true)
    {
        IsException = true;
    }

    /*处于参数异常锁定状态*/
    if (Lock_ParameterException == true)
    {
        IsException = true;
    }

    /*处于严重状态*/
    if (IsException == true)
    {
        Fun_CheckLockCmdException();
    }

    /*处于特权指令锁定状态*/
    if (Lock_PrivilegeCmd == true)
    {
        /*在特权指令模式下，仅放行modbus功能码3*/
        if (((_Modbus *)(SrcMsg->buff))->UsartType.pdu.cmd != 3)
        {
            Fun_CheckPrivilegeCmdException();
            IsException = true;
        }
    }

    SendMsg = (_TxBuff *)msg.buff;

    msg.protocol = PROTOCOL_NONE;
    msg.len = len - offsetof(_IF, buff);

    if (msg.len >= sizeof(msg.buff))
    {
        result = false;

        goto function_end;
    }

    if (SrcMsg->head.len != msg.len)
    {
        result = false;

        goto function_end;
    }

    Debug_Statics(offsetof(_Statics, transparent), 1, sizeof(statics.transparent));

    if (IsException == true)
    {
        result = false;

        goto function_end;
    }

function_end:

    if (result == true)
    {
        memcpy(&SendMsg->buff, SrcMsg->buff, msg.len);

        if (SrcMsg->head.cmd == CMD_TRANSPARENT_TO_MAIN_DEVICE_1)
        {
            SendMsg->type = SEND_TRANSPARENT_MSG_1;
        }
        else
        {
            SendMsg->type = SEND_TRANSPARENT_MSG_2;
        }

        Fun_Request485BusSendMsg(&msg);
    }
    else                                                                        //封装负响应
    {
        response = SrcMsg;

        /*回复响应，无负载*/
        response->head.version = CMD_VERSION;
        response->head.cmd = CMD_TRANSPARENT_NEGATIVE_RESPONSE;
        response->head.len = 0;

        len = offsetof(_IF, buff);
        PHY_RequestGprsSendMsg((uint8 *)response, len, CMD_TRANSPARENT_NEGATIVE_RESPONSE, 0xFF, 1, 0, false);
    }

    return;
}

/*******************************************************************************
*功能：485接收处理
*参数：无
*返回：无
*说明：在接收到响应或发送超时时被调用
*******************************************************************************/
void App_PlcRS485ReceiveHandler(_SysMsg *msg, _TxBuff *SrcMsg, uint8 result)
{
    _Modbus *SendMsg;
    _CMD3PDU *Cmd3Request;
    uint8 CheckResult;
    _IF response;
    uint16 StartAddr;
    uint16 len;
    uint8 LandPieceSeq;

    if (PlcModuleIsInit == false)
    {
        return;
    }

    /*发送超时*/
    if (msg == NULL && result == SEND_MSG_FAIL)
    {
        Fun_TimeOverStatics();

        return;
    }

    SendMsg = &SrcMsg->buff;

    /*接收正确性检查*/
    CheckResult =  Modbus_CheckResponse(&SrcMsg->buff, (_Modbus *)msg->buff, msg->len);

    switch (CheckResult)
    {
        case MODBUS_ERR_RESPONSE:                                               //负响应
            {
                Fun_ErrReceiveStatics();

                return;
            }
        case MODBUS_NON_REQUEST_CMD_ERR:                                        //检查接收命令号
            {
                Fun_ErrReceiveStatics();

                return;
            }
        case MODBUS_RESPONSE_LEN_ERR:                                           //响应字节长度不对
            {
                Fun_ErrReceiveStatics();

                return;
            }
        case MOBUS_CORRECT_RESPONSE:                                            //正确接收
            {
                Fun_CorrectReceiveStatics();

                break;
            }
        default:
            {
                return;
            }
    }

    switch (SrcMsg->type)
    {
        /*轮询指令响应处理*/
        case SEND_POLL_MSG:
            {
                Cmd3Request = (_CMD3PDU *)&SendMsg->UsartType.pdu.data;
                StartAddr = Lib_htons(Cmd3Request->StartAddr);

                /*由于两个地址重复，都是73，因此，需要增加从站地址判断----互感器电流*/
                if (SendMsg->UsartType.addr == DEFAULT_TRANSFORMER_ADDR && StartAddr == REG_TRANSFORMER_CURRENT)
                {
                    Debug_Statics(offsetof(_Statics, ResTransformer), 1, sizeof(statics.ResTransformer));
                    Fun_CheckPumpException(msg);                                //检查水泵异常

                    return;
                }

                switch (StartAddr)
                {
                    case REG_MOTOR_BIND:                                        //电机关联
                        {
                            Debug_Statics(offsetof(_Statics, ResMotorBind), 1, sizeof(statics.ResMotorBind));
                            Fun_CheakCircuitProtectException(msg);              //检查电路保护异常

                            break;
                        }
                    case REG_COMMUTE_STATE:
                        {
                            Debug_Statics(offsetof(_Statics, ResCommute), 1, sizeof(statics.ResCommute));//读通信状态响应统计次数
                            Fun_CheckCommuteException(msg);

                            break;
                        }
                    default:
                        {
                            LandPieceSeq = Fun_GetLandPieceSeq(StartAddr, REQUEST_FLOWMETER_ADDR);
                            if (LandPieceSeq != 0xFF)
                            {
                                Fun_CheckLandPieceState(msg, LandPieceSeq);

                                break;
                            }

                            if (StartAddr == Fun_GetLandPieceStateReg(WorkTaskList.LandPieceSeq, REQUEST_TASK_STATE_ADDR))//子任务状态
                            {
                                Debug_Statics(offsetof(_Statics, ResSubTask), 1, sizeof(statics.ResSubTask));
                                Fun_ReadSubTaskRegHandler(msg);

                                break;
                            }

                            return;
                        }
                }

                return;
            }
        case SEND_TRANSPARENT_MSG_1:
        case SEND_TRANSPARENT_MSG_2:
            {
                /*对于透明传输的响应，无论是正响应还是负响应，均发送给服务器*/
                if (msg->len >= sizeof(response.buff))
                {
                    break;
                }

                ModBus_SendProcess(msg);

                if (SrcMsg->type == SEND_TRANSPARENT_MSG_1)
                {
                    response.head.cmd = CMD_TRANSPARENT_TO_MAIN_DEVICE_1;
                }
                else
                {
                    response.head.cmd = CMD_TRANSPARENT_TO_MAIN_DEVICE_2;
                }

                response.head.version = CMD_VERSION;
                response.head.len =  msg->len;

                len = msg->len + offsetof(_IF, buff);
                response.head.len = Lib_htons(response.head.len);

                memcpy(response.buff, msg->buff, msg->len);

                Debug_Statics(offsetof(_Statics, ResTransparent), 1, sizeof(statics.ResTransparent));
                PHY_RequestGprsSendMsg((uint8 *)&response, len, response.head.cmd, 0xFF, 1, 0, false);

                break;
            }
        default:
            {
                break;
            }
    }

    return;
}


/*-------------------GPRS处理部分------------------*/
/*******************************************************************************
*功能：gprs发送完成回调函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool App_PlcGprsSendResultHandler(_GprsSendBuff *TxBuff, uint8 SendResult)
{
    bool result;

    if (PlcModuleIsInit == false)
    {
        return false;
    }

    if (SendResult == SEND_MSG_SUCCESS)
    {
        Exception_GprsCommuteCounter = 0;

        return true;
    }

    if (SendResult == SEND_MSG_FAIL)
    {
        result = false;

        /*统计gprs发送失败次数*/
        Fun_CheckGprsCommuteException();

        switch (TxBuff->cmd)
        {
            case CMD_FORWARD_TO_BIND_DTU:
                {
                    if (ProcessMode_ForwardException == EXCEPTION_NO_MONITOR)
                    {
                        break;
                    }

                    App_ReportExceptionToServer(EXCEPTION_FORWARD, (uint8 *)cfg.BindDtuList, 15);//转发异常

                    break;
                }
            case CMD_REPORT_EXCEPTION:                                          //上报异常失败
                {
                    Fun_ReportExceptionToServerFail(TxBuff);

                    break;
                }
            case CMD_REGISTER_TO_SERVER:
                {
                    TxBuff->ResendNum = 200;
                    result = true;

                    break;
                }
        }

        return result;
    }

    return false;
}

/*******************************************************************************
*功能：设备初始化
*参数：无
*返回：无
*说明：
*******************************************************************************/
void App_PlcMainDeviceInit(bool ParameterException)
{
    OS_ERR err;
    uint16 i;

    if (cfg.MainDeviceType == MAIN_DEVICE_PLC)
    {
        PlcModuleIsInit = true;
    }
    else
    {
        PlcModuleIsInit = false;

        return;
    }

    Lock_ParameterException = ParameterException;

    /*创建轮询定时器*/
    OSTmrCreate(&PlcPollTmr,
                NULL,
                3 * SYS_SECOND_TICK_NUM,
                PLC_POLL_PERIOD * SYS_SECOND_TICK_NUM,
                OS_OPT_TMR_PERIODIC,
                Fun_PlcPollTmrHandler,
                NULL,
                &err);

    /*启动定时器*/
    OSTmrStart(&PlcPollTmr, &err);

    /*创建接收传输表维护定时器*/
    OSTmrCreate(&RxTransTableTmr,
                NULL,
                RX_TRANS_TABLE_TMR_PERIOD * SYS_SECOND_TICK_NUM,
                RX_TRANS_TABLE_TMR_PERIOD * SYS_SECOND_TICK_NUM,
                OS_OPT_TMR_PERIODIC,
                Fun_RxTrarnsTableTmrHandler,
                NULL,
                &err);

    /*启动定时器*/
    OSTmrStart(&RxTransTableTmr, &err);

    /*初始化变量*/
    Exception_485OverTimeCounter = 0;
    Exception_LockCmdCounter = 0;

    ProcessMode_485OverTimeException = EXCEPTION_MONITOR;
    ProcessMode_ForwardException = EXCEPTION_MONITOR;
    ProcessMode_NetCommuteException = EXCEPTION_MONITOR;
    ProcessMode_ValveException = EXCEPTION_MONITOR;
    ProcessMode_FlowMeterException = EXCEPTION_MONITOR;
    ProcessMode_CmdLockException = EXCEPTION_MONITOR;
    ProcessMode_LogicLockException = EXCEPTION_MONITOR;
    ProcessMode_PumpException = EXCEPTION_MONITOR;
    ProcessMode_CircuitProtectException = EXCEPTION_MONITOR;
    ProcessMode_WirelessCommnuteException = EXCEPTION_MONITOR;
    ProcessMode_CoordinatorException = EXCEPTION_MONITOR;

    PlcState.PumpState = PUMP_CLOSE;
    SysValveState = 0;

    ForwardToBindDtuSeq = 0;

    Lock_485OverTime = false;
    Lock_PrivilegeCmd = false;

    PollLandPieceStateSeq = false;

    /*将阀位状态恢复成关闭状态*/
    for (i = 0; i < MAX_LAND_PIECE_NUM; i++)
    {
        LandPiece[i].FirstGetFlowMeter = true;
        LandPiece[i].FlowMeterValue = 0;
        LandPiece[i].ValveState = false;
    }

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年12月05日    巍图      创建文件
*******************************************************************************/
/*
关泵状态：电流值应为0；开泵状态：电流值应为大于0
异常计数为非累计值

485超时锁定、水泵锁定：锁定透明传输、锁定下发子任务、锁定特权指令
*/

