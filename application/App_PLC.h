/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年12月05日
**  名   称：App_PLC.h
**  摘   要：
*******************************************************************************/
#ifndef _APP_PLC_H
#define _APP_PLC_H

void App_PlcMainDeviceInit(bool ParameterException);
void App_SetPlcWorkTask(_IF *msg, uint16 len);
void App_PlcTransparentToMainDevice(_IF *SrcMsg, uint16 len);
void App_PlcReportExceptionRespose(_IF *msg, uint16 len);
void App_PlcPrivilegeCmd(_IF *msg, uint16 len);
void App_PlcCancelPrivilegeCmd(_IF *msg, uint16 len);
void App_PlcResetException(_SysMsg *msg, uint16 len);
void App_PlcReceivedServerForwardRequest(_IF *msg, uint16 len);
void App_PlcRegisterToServerResponse(_IF *SrcMsg, uint16 len);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年12月05日    巍图      创建文件
*******************************************************************************/

