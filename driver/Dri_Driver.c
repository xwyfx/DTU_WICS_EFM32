/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_Driver.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_rmu.h"
#include "em_chip.h"

#include "Dri_Driver.h"
#include "Dri_Rtc.h"
#include "Dri_Spi.h"
#include "Dri_Burtc.h"
#include "Dri_WatchDog.h"
#include "Dri_Adc.h"
#include "Dri_Led.h"
#include "Dri_Dma.h"
#include "Dri_Timer.h"
#include "Dri_Usart.h"
#include "Dri_I2C.h"
#include "Dri_DS1302.h"
#include "Dri_InterFlash.h"

/*******************************************************************************
*功能：全局驱动初始化
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_DriverInit(void)
{
    NVIC_SetPriorityGrouping(0);
    RMU_ResetControl(rmuResetBU, false);                                        //关闭复位管理单元

    Dri_DmaInit();
    Dri_TimerInit();
    Dri_UsartInit();
    Dri_RtcInit();                                                              //配置RTC

    Dri_BuRtcInit();
    Dri_LedInit();
    Dri_AdcInit();
    Dri_I2CInit();
    Dri_InterFlashInit();
    Dri_DS1302Init();
    Dri_WatchdogTimerInit();
    Dri_StartWatchdogTimer();

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_ResetAllReg(void)
{
    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_ResetNumRecord(void)
{
//    IF_ResetNumRecord();
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/
