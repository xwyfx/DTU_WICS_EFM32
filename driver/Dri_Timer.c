/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_Timer.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_burtc.h"
#include "em_letimer.h"
#include "em_cmu.h"
#include "em_timer.h"

#include "Dri_Timer.h"
#include "Dri_Dma.h"
#include "Dri_Rtc.h"
#include "Dri_Usart.h"

static _STimerAttribute STimer[STIMER_NUM];
static uint32 tick;

extern void PHY_TimerFiredHandler(uint8 channel);

/*----------------STimer软件定时器部分---------*/

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_SetSTimer(uint8 channel, uint16 top, bool shot)
{
    if (channel >= STIMER_NUM)
    {
        return;
    }

    OS_ENTER_CRITICAL();
    STimer[channel].enable = 1;
    STimer[channel].fired = 0;
    STimer[channel].top = top;
    STimer[channel].count = 0;
    STimer[channel].shot = shot;
    OS_EXIT_CRITICAL();

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_CloseSTimer(uint8 channel)
{
    if (channel >= STIMER_NUM)
    {
        return;
    }

    OS_ENTER_CRITICAL();
    STimer[channel].enable = 0;
    STimer[channel].fired = 0;
    STimer[channel].top = 0;
    STimer[channel].count = 0;
    OS_EXIT_CRITICAL();

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_RegisterSTimer(uint8 channel, void (*handler)(void))
{
    if (channel >= STIMER_NUM)
    {
        return;
    }

    STimer[channel].handler = handler;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Fun_STimerInit(void)
{
    uint16 i;

    for (i = 0; i < STIMER_NUM; i++)
    {
        STimer[i].top = 0;
        STimer[i].count = 0;
        STimer[i].enable = 0;
        STimer[i].fired = 0;
        STimer[i].shot = 0;

        STimer[i].handler = NULL;
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：轮询所有的软件定时器，计数器加一，并判断计时器是否到达设定的时间，从而触发相应的处理程序
*******************************************************************************/
void Fun_STimerFiredIsr(void)
{
    uint16 i;

    for (i = 0; i < STIMER_NUM; i++)
    {
        if (STimer[i].enable == true)
        {
            if (STimer[i].count >= STimer[i].top)
            {
                if (STimer[i].shot == true)
                {
                    STimer[i].enable = false;
                    STimer[i].fired = true;
                    STimer[i].count = 0;
                    STimer[i].top = 0;
                }
                else
                {
                    STimer[i].fired = true;
                    STimer[i].count = 0;
                }
            }

            STimer[i].count++;
        }
    }

    return;
}

/*----------------LeTimer部分-----------------*/

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void LETIMER0_IRQHandler(void)
{
    uint32 LeTimerCnt = 0;
    uint16 TimerReg = 0;
    uint8 EnableFlg = 0;
    LeTimerCnt = LETIMER_IntGet(LETIMER0);
    EnableFlg = LETIMER0->IEN;

    CPU_SR  cpu_sr = 0;

    if ((LeTimerCnt & EnableFlg & 0x01) == LETIMER_IF_COMP0)
    {
        CPU_CRITICAL_ENTER();
        LETIMER_IntClear(LETIMER0, LETIMER_IF_COMP0);                           //清除溢出中断标志位
        LETIMER_IntDisable(LETIMER0, LETIMER_IF_COMP0);

        Dri_TimerFiredHandler(LETIMER_CHANNEL_0);

        CPU_CRITICAL_EXIT();
    }

    if ((LeTimerCnt & EnableFlg & 0x02) == LETIMER_IF_COMP1)
    {
        LETIMER_IntClear(LETIMER0, LETIMER_IF_COMP1);

        tick++;

        OS_CPU_SysTickHandler();
        TimerReg = LETIMER_CounterGet(LETIMER0);
        TimerReg = TimerReg - SYS_TICK_SCALE;
        LETIMER0->COMP1 = TimerReg;

        /*看门狗计数器累加*/
        SysFeedWatchdogCounter++;
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_LetimerInit(void)
{
    CMU_ClockEnable(cmuClock_LETIMER0, true);                                   //开启LETIMER0时钟

    /*
    * LETIMER初始化结构体
    */
    const LETIMER_Init_TypeDef tLetimerInit =
    {
        .enable         = true,                                                 //初始化完成后使能
        .debugRun       = false,                                                //调试期间不运行
        .rtcComp0Enable = false,                                                //禁能RTC匹配0触发启动
        .rtcComp1Enable = true,                                                 //禁能RTC匹配1触发启动
        .comp0Top       = false,                                                //COMP0作为计数器顶端值
        .bufTop         = false,                                                //禁能COMP1作为COMP0缓冲
        .out0Pol        = 0,                                                    //OUT0空闲输出低电平
        .out1Pol        = 0,                                                    //OUT1空闲输出低电平
        .ufoa0          = letimerUFOANone,                                      //OUT0输出脉冲
        .ufoa1          = letimerUFOANone,                                      //OUT1无输出
        .repMode        = letimerRepeatFree                                     //自由模式
    };
    LETIMER_Init(LETIMER0, &tLetimerInit);

    LETIMER_CompareSet(LETIMER0, 1, 0xFFFF - SYS_TICK_SCALE);
    /*
     *  REP0值需大于0才会有输出
     */
    LETIMER_RepeatSet(LETIMER0, 0, 1);

    /*
     *  使能LETIMER0的输出0，并使用LOC1映
     */
    LETIMER0->ROUTE = LETIMER_ROUTE_LOCATION_LOC0;

    LETIMER_IntEnable(LETIMER0, LETIMER_IF_COMP1);

    NVIC_EnableIRQ(LETIMER0_IRQn);

    NVIC_SetPriority(LETIMER0_IRQn, NVIC_EncodePriority(0, 1, 1));

    return;
}

/*------------------Timer3部分----------------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_Timer3Init(void)
{
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_TIMER3, true);                                     //使能TIMER0模块时钟

    /*
    *  配置为向上计数，当到达TOP值溢出，重新加载计数
    */
    TIMER_Init_TypeDef tTimerInit = {
        .enable     = false,                                                    //初始化结束不使能
        .debugRun   = false,                                                    //调试Halt时停止计数
        .prescale   = timerPrescale1024,                                        //1024分频
        .clkSel     = timerClkSelHFPerClk,                                      //时钟源选择高频外设时钟
        .count2x    = false,                                                    //不使用2倍计数模式，GG / TG才有
        /*
        *  该配置位若置位，则STATUS寄存器中CCPOL位会始终跟踪输入通道的状态
        */
        .ati        = false,
        /*
        *  计数器可以由CC0的输入（外部引脚或PRS信号）控制
        *  通过配置CTRL寄存器中FALLA和RISEA位域选择行为：启动、停止、重载并启动
        */
        .fallAction = timerInputActionNone,                                     //配置下降沿行为
        .riseAction = timerInputActionNone,                                     //配置上升沿行为
        .mode       = timerModeUp,                                              //配置计数模式
        /*
        *  当使用DMA功能时，该配置若置位则允许在DMA请求信号发出后自动清除请求信号
        */
        .dmaClrAct  = false,
        .quadModeX4 = false,                                                    //在正交编码中是否使用X4模式
        .oneShot    = false,                                                    //是否使用单次模式
        .sync       = false,                                                    //定时器由其它定时器控制
    };

    TIMER_Init(TIMER3, &tTimerInit);                                            //初始化TIMER1计数器

    /*
    *  写TIMER1的TOP值，每隔1秒发生一次中断
    */
    TIMER_TopSet(TIMER3, CMU_ClockFreqGet(cmuClock_TIMER3) / (1024 * 1000));    //0.001S一次

    TIMER_IntEnable(TIMER3, TIMER_IF_OF);                                       //使能TIMER1溢出中断
    NVIC_EnableIRQ(TIMER3_IRQn);                                                //使能TIMER1中断

    TIMER3->CMD = TIMER_CMD_START;                                              //启动计数器

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_DisableTimer3(void)
{
    NVIC_DisableIRQ(TIMER3_IRQn);
    TIMER_Enable(TIMER3, false);
    CMU_ClockEnable(cmuClock_TIMER3, false);                                    //使能TIMER0模块时钟

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：1毫秒产生一次中断
*******************************************************************************/
void TIMER3_IRQHandler(void)
{
    uint16 i;

    TIMER_IntClear(TIMER3, TIMER_IF_OF);                                        //清除溢出中断标志位

    //软件定时器计数
    Fun_STimerFiredIsr();

    for (i = 0; i < STIMER_NUM; i++)
    {
        if (STimer[i].fired == true)
        {
            STimer[i].fired = false;
        }
        else
        {
            continue;
        }

        if (STimer[i].handler != NULL)
        {
//            STimer[i].enable = 0;
//            STimer[i].fired = 0;
//            STimer[i].top = 0;
//            STimer[i].count = 0;

            (*(STimer[i].handler))();
        }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Dri_StartLeTimerChannel(uint8 channel, uint16 time)
{
    uint16 TimerReg = 0;
    CPU_SR  cpu_sr = 0;

    switch (channel)
    {
        case LETIMER_CHANNEL_0:
        {
            CPU_CRITICAL_ENTER();

            TimerReg = LETIMER_CounterGet(LETIMER0);
            TimerReg = TimerReg - time;
            LETIMER0->COMP0 = TimerReg;

            LETIMER_IntClear(LETIMER0, LETIMER_IF_COMP0);                       //清除溢出中断标志位
            LETIMER_IntEnable(LETIMER0, LETIMER_IF_COMP0);

            CPU_CRITICAL_EXIT();

            return true;
        }
        case LETIMER_CHANNEL_1:                                                 //通道0用作系统tick
        {
            return false;
        }
        default:
        {
            return false;
        }
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Dri_StopLeTimerChannel(uint8 channel)
{
    CPU_SR  cpu_sr = 0;

    switch (channel)
    {
        case LETIMER_CHANNEL_0:
        {
            CPU_CRITICAL_ENTER();

            LETIMER_IntClear(LETIMER0, LETIMER_IF_COMP0);                       //清除溢出中断标志位
            LETIMER_IntDisable(LETIMER0, LETIMER_IF_COMP0);

            CPU_CRITICAL_EXIT();

            return true;
        }
        case LETIMER_CHANNEL_1:                                                 //通道0用作系统tick
        {
            return false;
        }
        default:
        {
            return false;
        }
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dri_GetTimeReg(void)
{
    return BURTC_CounterGet();
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_StartTimer(uint8 channel, uint16 time)
{
    switch (channel)
    {
        case RTC_TIMER_CHANNEL_0:
        {
            Dri_StartRtcTimerChannel(RTC_TIMER_CHANNEL_0, time);

            break;
        }
        case LETIMER_CHANNEL_0:
        {
            Dri_StartLeTimerChannel(LETIMER_CHANNEL_0, time);

            break;
        }
        default:
        {
            break;
        }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_DisableTimer(uint8 channel)
{
    switch (channel)
    {
        case RTC_TIMER_CHANNEL_0:
        {
            Dri_StopRtcTimerChannel(RTC_TIMER_CHANNEL_0);

            break;
        }
        case LETIMER_CHANNEL_0:
        {
            Dri_StopLeTimerChannel(LETIMER_CHANNEL_0);

            break;
        }
        case TIMER3_CHANNEL_0:
        {
            Dri_DisableTimer3();

            break;
        }
        default:
        {
            break;
        }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_TimerFiredHandler(uint8 channel)
{
    switch (channel)
    {
        case LETIMER_CHANNEL_0:
        {
            break;
        }
        case RTC_TIMER_CHANNEL_0:
        {
            break;
        }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_TimerInit(void)
{
    Dri_LetimerInit();                                                          //LETIMER初始化
    Dri_Timer3Init();
    Fun_STimerInit();

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint32 Dri_GetSysTick(void)
{
    return tick;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint32 Dri_GetTickReg(void)
{
    return LETIMER_CounterGet(LETIMER0);
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/
