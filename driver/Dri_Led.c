/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_Led.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_gpio.h"

#include "Dri_Led.h"

/*******************************************************************************
*功能：指示灯端口初始化
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_LedInit(void)
{
    GPIO_PinModeSet(gpioPortE, 10, gpioModePushPull, 0);
    GPIO_PinModeSet(gpioPortE, 11, gpioModePushPull, 0);
    GPIO_PinModeSet(gpioPortE, 12, gpioModePushPull, 0);
    GPIO_PinModeSet(gpioPortE, 13, gpioModePushPull, 0);

    return;
}
/*******************************************************************************
*功能：闪烁某个LED
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_LedToggle(uint8 num)
{
    switch (num)
    {
        case 1:
        {
            GPIO_PinOutToggle(gpioPortE, 13);

            break;
        }
        case 2:
        {
            GPIO_PinOutToggle(gpioPortE, 12);

            break;
        }
        case 3:
        {
            GPIO_PinOutToggle(gpioPortE, 11);

            break;
        }
        case 4:
        {
            GPIO_PinOutToggle(gpioPortE, 10);

            break;
        }
        default:
        {
            break;
        }
    }

    return;
}

/*******************************************************************************
*功能：打开某个LED
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_LedOn(uint8 num)
{
    switch (num)
    {
        case 1:
        {
            GPIO_PinOutClear(gpioPortE, 13);

            break;
        }
        case 2:
        {
            GPIO_PinOutClear(gpioPortE, 12);

            break;
        }
        case 3:
        {
            GPIO_PinOutClear(gpioPortE, 11);

            break;
        }
        case 4:
        {
            GPIO_PinOutClear(gpioPortE, 10);

            break;
        }
        default:
        {
            break;
        }
    }

    return;
}

/*******************************************************************************
*功能：关闭某个LED
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_LedOff(uint8  num)
{
    switch (num)
    {
        case 1:
        {
            GPIO_PinOutSet(gpioPortE, 13);

            break;
        }
        case 2:
        {
            GPIO_PinOutSet(gpioPortE, 12);

            break;
        }
        case 3:
        {
            GPIO_PinOutSet(gpioPortE, 11);

            break;
        }
        case 4:
        {
            GPIO_PinOutSet(gpioPortE, 10);

            break;
        }
        default:
        {
            break;
        }
    }

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/

