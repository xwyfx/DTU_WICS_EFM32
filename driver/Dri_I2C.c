/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年03月13日
**  名   称：Dri_I2C.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_gpio.h"

#define I2C_SLAVE_ADDR				    0xA2
#define I2C_DELAY				    20//

#define AT24C256_PAGE_SIZE			    64//64bytes
#define AT24C256_PAGE_NUM			    512
#define AT24C256_DEVICE_SIZE		    AT24C256_PAGE_SIZE*AT24C256_PAGE_NUM

#define I2C_PIN_SCL                    1
#define I2C_PIN_SDA                    0

typedef enum
{
    I2C_LOW  = 0,
    I2C_HIGH = 1
}ENUM_PIN_LEVEL;

typedef enum
{
    NACK	= 0,
    ACK		= 1
}ENUM_ACK;

static volatile uint8 timeout = 13;

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void I2C_PinModeSet(GPIO_Port_TypeDef port,
                    unsigned int pin,
                    GPIO_Mode_TypeDef mode)
{

    /* There are two registers controlling the pins for each port. The MODEL
     * register controls pins 0 - 7 and MODEH controls pins 8 - 15. */
    if (pin < 8)
    {
        GPIO->P[port].MODEL = (GPIO->P[port].MODEL & ~(0xF << (pin * 4)))
            | (mode << (pin * 4));
    }
    else
    {
        GPIO->P[port].MODEH = (GPIO->P[port].MODEH & ~(0xF << ((pin - 8) * 4)))
            | (mode << ((pin - 8) * 4));
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_I2CInit(void)
{
    GPIO_PinModeSet(gpioPortE, 15, gpioModePushPull, 0);                        //WP
    GPIO_PinModeSet(gpioPortA, I2C_PIN_SCL, gpioModePushPull, 0);               //SCL
    GPIO_PinModeSet(gpioPortA, I2C_PIN_SDA, gpioModePushPull, 1);               //SDA

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_I2C_Sda(ENUM_PIN_LEVEL BitLvl)
{
    if (BitLvl == I2C_HIGH)
    {
        GPIO_PinOutSet(gpioPortA, I2C_PIN_SDA);
    }
    else
    {
        GPIO_PinOutClear(gpioPortA, I2C_PIN_SDA);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_I2C_Scl(ENUM_PIN_LEVEL BitLvl)
{
    if (BitLvl == I2C_HIGH)
    {
        GPIO_PinOutSet(gpioPortA, I2C_PIN_SCL);
    }
    else
    {
        GPIO_PinOutClear(gpioPortA, I2C_PIN_SCL);
    }

    return;
}

/*******************************************************************************
*功能：设置SDA为输出
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_Set_Sda_Out(void)
{
    I2C_PinModeSet(gpioPortA, I2C_PIN_SDA, gpioModeWiredAndPullUp);

    return;
}

/*******************************************************************************
*功能：设置SDA为输入
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_Set_Sda_In(void)
{
    I2C_PinModeSet(gpioPortA, I2C_PIN_SDA, gpioModeInput);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
unsigned char drv_Get_Sda_In(void)
{
    return GPIO_PinInGet(gpioPortA, I2C_PIN_SDA);
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void delay_5us(void)
{
    uint8 i;

    for (i = 0; i < timeout; i++) ;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_Eeprom_Delay(void)
{
    unsigned short t = I2C_DELAY;

    while (t--)
    {
        delay_5us();
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_Eeprom_Delay10ms(void)
{
    unsigned short t = 2000;

    while (t--)
    {
        delay_5us();
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_Eeprom_Start(void)
{
    drv_Set_Sda_Out();

    drv_I2C_Sda(I2C_HIGH);                                                      //SDA = 1
    drv_Eeprom_Delay();
    drv_I2C_Scl(I2C_HIGH);                                                      //SCL = 1
    drv_Eeprom_Delay();
    drv_I2C_Sda(I2C_LOW);                                                       //SDA = 0
    drv_Eeprom_Delay();
    drv_I2C_Scl(I2C_LOW);                                                       //SCL = 0
    drv_Eeprom_Delay();

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_Eeprom_Stop(void)
{
    drv_Set_Sda_Out();
    drv_Eeprom_Delay();
    drv_I2C_Sda(I2C_LOW);                                                       //SDA = 0
    drv_Eeprom_Delay();
    drv_I2C_Scl(I2C_HIGH);                                                      //SCL = 1
    drv_Eeprom_Delay();
    drv_I2C_Sda(I2C_HIGH);                                                      //SDA = 1
    drv_Eeprom_Delay();
    drv_I2C_Scl(I2C_LOW);                                                       //SCL = 0
    drv_Eeprom_Delay();

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
unsigned char drv_Eeprom_GetAck(void)
{
    unsigned char ack = 0;

    drv_I2C_Sda(I2C_HIGH);                                                      //SDA = 1
//drv_Eeprom_Delay();
    drv_Set_Sda_In();
    drv_Eeprom_Delay();
    drv_I2C_Scl(I2C_HIGH);                                                      //SCL = 1
    drv_Eeprom_Delay();
    if (drv_Get_Sda_In())
    {
        ack = ACK;
    }
    else
    {
        ack = NACK;
    }
    drv_I2C_Scl(I2C_LOW);                                                       //SCL = 0
    drv_Eeprom_Delay();

    return ack;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_Eeprom_PutAck(ENUM_ACK ack)
{
    drv_Set_Sda_Out();
    drv_Eeprom_Delay();
    if (ack == ACK)
    {
        drv_I2C_Sda(I2C_LOW);                                                   //SDA = 1
    }
    else
    {
        drv_I2C_Sda(I2C_HIGH);                                                  //SDA = 0
    }
    drv_I2C_Scl(I2C_LOW);                                                       //SCL = 1
    drv_Eeprom_Delay();
    drv_I2C_Scl(I2C_HIGH);                                                      //SCL = 0
    drv_Eeprom_Delay();
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void drv_Eeprom_Write_Byte(unsigned char data)
{
    unsigned char i;

    drv_Set_Sda_Out();
    for (i = 0; i < 8; i++)
    {
        drv_I2C_Scl(I2C_LOW);                                                   //SCL = 0
        if (data & 0x80)
        {
            drv_I2C_Sda(I2C_HIGH);                                              //SDA = 1
        }
        else
        {
            drv_I2C_Sda(I2C_LOW);                                               //SDA = 0
        }
        drv_Eeprom_Delay();
        drv_I2C_Scl(I2C_HIGH);                                                  //SCL = 1
        drv_Eeprom_Delay();
        data <<= 1;
    }
    drv_I2C_Scl(I2C_LOW);                                                       //SCL = 0

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
unsigned char drv_Eeprom_Read_Byte(void)
{
    unsigned char data;
    unsigned char i;

    data = 0;

    drv_I2C_Sda(I2C_HIGH);                                                      //SDA = 1
    drv_Set_Sda_In();
    for (i = 0; i < 8; i++)
    {
        data <<= 1;
        drv_I2C_Scl(I2C_LOW);                                                   //SCL = 0
        drv_Eeprom_Delay();
        drv_I2C_Scl(I2C_HIGH);                                                  //SCL = 1
        drv_Eeprom_Delay();
        if (drv_Get_Sda_In())
        {
            data |= 0x01;
        }
        else
        {
            data &= 0xfe;
        }
    }
    drv_I2C_Scl(I2C_LOW);                                                       //SCL = 0

    return data;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
unsigned char drv_Write_Single(unsigned short address, unsigned char data)
{
    unsigned char addr_h = (unsigned char)((address & 0xff00) >> 8);
    unsigned char addr_l = (unsigned char)(address & 0x00ff);

    drv_Eeprom_Start();
    drv_Eeprom_Write_Byte(I2C_SLAVE_ADDR);

    if (drv_Eeprom_GetAck())
    {
        drv_Eeprom_Stop();
        return false;
    }
    drv_Eeprom_Write_Byte(addr_h);
    if (drv_Eeprom_GetAck())
    {
        drv_Eeprom_Stop();
        return false;
    }
    drv_Eeprom_Write_Byte(addr_l);
    if (drv_Eeprom_GetAck())
    {
        drv_Eeprom_Stop();
        return false;
    }
    drv_Eeprom_Write_Byte(data);
    if (drv_Eeprom_GetAck())
    {
        drv_Eeprom_Stop();
        return false;
    }
    drv_Eeprom_Stop();

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
unsigned char drv_Read_Single(unsigned short address, unsigned char *pData)
{
    unsigned char addr_h = (unsigned char)((address & 0xff00) >> 8);
    unsigned char addr_l = (unsigned char)(address & 0x00ff);

    drv_Eeprom_Start();
    drv_Eeprom_Write_Byte(I2C_SLAVE_ADDR);
    if (drv_Eeprom_GetAck())
    {
        drv_Eeprom_Stop();
        return false;
    }

    drv_Eeprom_Write_Byte(addr_h);
    if (drv_Eeprom_GetAck())
    {
        drv_Eeprom_Stop();
        return false;
    }
    drv_Eeprom_Write_Byte(addr_l);
    if (drv_Eeprom_GetAck())
    {
        drv_Eeprom_Stop();
        return false;
    }
    drv_Eeprom_Start();
    drv_Eeprom_Write_Byte(I2C_SLAVE_ADDR + 1);
    if (drv_Eeprom_GetAck())
    {
        drv_Eeprom_Stop();
        return false;
    }
    *pData = drv_Eeprom_Read_Byte();
    drv_Eeprom_PutAck(ACK);
    drv_Eeprom_Stop();

    return  true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
unsigned char drv_Read_Continuous(unsigned short addr, unsigned char *p_buff, unsigned short number)
{
    if (number == 0)
    {
        return false;
    }

    while (number--)
    {
//      feedDog();
        if (!drv_Read_Single(addr, p_buff))
        {
            return false;
        }
        addr++;
        p_buff++;
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
unsigned char drv_Write_Continuous(unsigned short addr, unsigned char *p_buff, unsigned short number)
{
    unsigned char addr_h = (unsigned char)((addr & 0xff00) >> 8);
    unsigned char addr_l = (unsigned char)(addr & 0x00ff);

    if (number == 0)
    {
        return false;
    }
    if (number > 1)
    {
        drv_Eeprom_Start();

        drv_Eeprom_Write_Byte(I2C_SLAVE_ADDR);
        if (drv_Eeprom_GetAck())
        {
            drv_Eeprom_Stop();
            return false;
        }

        drv_Eeprom_Write_Byte(addr_h);
        if (drv_Eeprom_GetAck())
        {
            drv_Eeprom_Stop();
            return false;
        }

        drv_Eeprom_Write_Byte(addr_l);
        if (drv_Eeprom_GetAck())
        {
            drv_Eeprom_Stop();
            return false;
        }
        while (number--)
        {
//          feedDog();
            drv_Eeprom_Write_Byte(*p_buff);
            p_buff++;
            if (drv_Eeprom_GetAck())
            {
                drv_Eeprom_Stop();
                return false;
            }
        }
        drv_Eeprom_Stop();
    }
    else
    {
        drv_Write_Single(addr, *p_buff);
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
unsigned char drv_AT24C256_Write(unsigned short usAddr, unsigned char *pBuff, unsigned short usLen)
{
    unsigned short usTempAddr = usAddr;
    unsigned short usTempLen = usLen;
    unsigned short i, j;

    if ((usAddr + usLen) > AT24C256_DEVICE_SIZE - 1)
    {
        return false;
    }

    if (usAddr % AT24C256_PAGE_SIZE)                                            //地址在页中间，先把页剩余部分填满
    {
        if (usTempLen > (AT24C256_PAGE_SIZE - usAddr % AT24C256_PAGE_SIZE))     //页剩余位置不满足长度需要
        {
            if (!drv_Write_Continuous(usTempAddr, pBuff, AT24C256_PAGE_SIZE - usAddr % AT24C256_PAGE_SIZE))
            {
                return false;
            }
            //1.8v系统需要延时20m
            drv_Eeprom_Delay10ms();
            usTempAddr += (AT24C256_PAGE_SIZE - usAddr % AT24C256_PAGE_SIZE);
            pBuff += (AT24C256_PAGE_SIZE - usAddr % AT24C256_PAGE_SIZE);
            usTempLen -= (AT24C256_PAGE_SIZE - usAddr % AT24C256_PAGE_SIZE);
        }
        else
        {
            if (!drv_Write_Continuous(usTempAddr, pBuff, usTempLen))
            {
                return false;
            }
            //1.8v系统需要延时20m
            drv_Eeprom_Delay10ms();
            usTempAddr += usTempLen;
            pBuff += usTempLen;
            usTempLen -= usTempLen;
        }
    }

    if (usTempLen / AT24C256_PAGE_SIZE)                                         //再看一下是否有多个页数据需要写
    {
        j = usTempLen / AT24C256_PAGE_SIZE;
        for (i = 0; i < j; i++)
        {
            if (!drv_Write_Continuous(usTempAddr, pBuff, AT24C256_PAGE_SIZE))
            {
                return false;
            }
            //1.8v系统需要延时20ms
            drv_Eeprom_Delay10ms();
            usTempAddr += AT24C256_PAGE_SIZE;
            pBuff += AT24C256_PAGE_SIZE;
            usTempLen -= AT24C256_PAGE_SIZE;
        }
    }

    if (usTempLen % AT24C256_PAGE_SIZE)                                         //若还有剩余
    {
        if (!drv_Write_Continuous(usTempAddr, pBuff, usTempLen % AT24C256_PAGE_SIZE))
        {
            return false;
        }
        //1.8v系统需要延时20m
        drv_Eeprom_Delay10ms();
        usTempAddr += usTempLen % AT24C256_PAGE_SIZE;
        pBuff += usTempLen % AT24C256_PAGE_SIZE;
        usTempLen -= usTempLen % AT24C256_PAGE_SIZE;
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
unsigned char drv_AT24C256_Read(unsigned short usAddr, unsigned char *pBuff, unsigned short usLen)
{
    if ((usAddr + usLen) > AT24C256_DEVICE_SIZE - 1)
    {
        return false;
    }

    if (!drv_Read_Continuous(usAddr, pBuff, usLen))
    {
        return false;
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Dri_I2CRead(uint16 addr, uint8 *buf, uint16 num)
{
    if (drv_AT24C256_Read(addr, buf, num) == true)
    {
        return false;
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Dri_I2CWrite(uint16 addr, uint8 *buf, uint16 num)
{
    if (drv_AT24C256_Write(addr, buf, num) == true)
    {
        return false;
    }

    return true;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年03月13日    巍图      创建文件
*******************************************************************************/
