/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            IW(2008-2011)
**  作   者：巍图
**  日   期：2017年03月21日
**  名   称：Dri_DS1302.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_chip.h"
#include "em_gpio.h"

#include "Dri_DS1302.h"

/*****************************************************变量声明********************************************/
#define RdSec                                 0x81
#define RdMin                                 0x83
#define RdHour                                0x85
#define RdDate                                0x87
#define RdMonth                               0x89
#define RdWeek                                0x8b
#define RdYear                                0x8d
#define RdControl                             0x8f
#define RdTrickleCharge                       0x91
#define RdClockBurst                          0xbf
#define WrSec                                 0x80
#define WrMin                                 0x82
#define WrHour                                0x84
#define WrDate                                0x86
#define WrMonth                               0x88
#define WrWeek                                0x8a
#define WrYear                                0x8c
#define WrControl                             0x8e
#define WrTrickleCharge                       0x90
#define WrClockBurst                          0xbe


/* 芯片驱动用到的IO定义 */
#define DS1302_PORT                 gpioPortC

#define DS1302_SCK_PIN              3
#define DS1302_IO_PIN               4
#define DS1302_CE_PIN               5

#define DS1302_SETSCK()             (GPIO_PinOutSet(DS1302_PORT, DS1302_SCK_PIN))
#define DS1302_CLRSCK()             (GPIO_PinOutClear(DS1302_PORT, DS1302_SCK_PIN))

#define DS1302_SETIO()              (GPIO_PinOutSet(DS1302_PORT, DS1302_IO_PIN))
#define DS1302_CLRIO()              (GPIO_PinOutClear(DS1302_PORT, DS1302_IO_PIN))

#define DS1302_SETCE()              (GPIO_PinOutSet(DS1302_PORT, DS1302_CE_PIN))
#define DS1302_CLRCE()              (GPIO_PinOutClear(DS1302_PORT, DS1302_CE_PIN))

#define DS1302_READ_SDA()           (GPIO_PinInGet(DS1302_PORT, DS1302_IO_PIN))

#define DS1302_IO_IN()              (GPIO_PinModeSet(DS1302_PORT, DS1302_IO_PIN, gpioModeInputPull,1))
#define DS1302_IO_OUT()             (GPIO_PinModeSet(DS1302_PORT, DS1302_IO_PIN, gpioModePushPull,0))

/*******************************************************************************
*功能：DS1302芯片写一个字节
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void ds1302_write8bit(uint8 code)
{
    uint8 i;

    DS1302_IO_OUT();
    DS1302_CLRSCK();

    for (i = 0; i < 8; i++)
    {
        Lib_DelayUs(5);

        if (code & 0x01)
        {
            (DS1302_SETIO());
        }
        else
        {
            (DS1302_CLRIO());
        }

        Lib_DelayUs(5);
        DS1302_SETSCK();

        Lib_DelayUs(5);
        DS1302_CLRSCK();

        code = code >> 1;
    }

    return;
}

/*******************************************************************************
*功能：DS1302芯片读一个字节
*参数：无
*返回：无
*说明：
*******************************************************************************/
static uint8 ds1302_read8bit(void)
{
    uint8 i, code;

    DS1302_IO_IN();

    code = 0;

    DS1302_CLRSCK();
    Lib_DelayUs(5);

    for (i = 0; i < 8; i++)
    {
        code = code >> 1;

        if (DS1302_READ_SDA())
        {
            code = code | 0x80;
        }

        Lib_DelayUs(5);
        DS1302_SETSCK();

        Lib_DelayUs(5);
        DS1302_CLRSCK();
    }

    return code;
}

/*******************************************************************************
*功能：DS1302芯片读某个寄存器值
*参数：con-指令
*返回：无
*说明：
*******************************************************************************/
static uint8 ds1302_readbyte(uint8 con)
{
    uint8 code;

    DS1302_CLRCE();
    Lib_DelayUs(5);

    DS1302_CLRSCK();
    Lib_DelayUs(5);

    DS1302_SETCE();
    Lib_DelayUs(5);

    ds1302_write8bit(con);

    code = ds1302_read8bit();

    Lib_DelayUs(5);
    DS1302_SETSCK();

    Lib_DelayUs(5);
    DS1302_CLRCE();

    return code;
}

/*******************************************************************************
*功能：DS1302芯片写某个寄存器值
*参数：con-指令
*返回：无
*说明：
*******************************************************************************/
static void ds1302_writebyte(uint8 con, uint8 code)
{
    DS1302_CLRCE();
    Lib_DelayUs(5);

    DS1302_CLRSCK();
    Lib_DelayUs(5);

    DS1302_SETCE();
    Lib_DelayUs(5);

    ds1302_write8bit(con);
    ds1302_write8bit(code);
    Lib_DelayUs(5);

    DS1302_SETSCK();
    Lib_DelayUs(5);

    DS1302_CLRCE();

    return;
}

/*******************************************************************************
*功能：设置实时时间
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void ds1302_writetime(_Time *time)
{
    ds1302_writebyte(WrControl, 0x00);

    ds1302_writebyte(WrYear, time->year);
    ds1302_writebyte(WrMonth, time->month);
    ds1302_writebyte(WrDate, time->day);
    ds1302_writebyte(WrHour, time->hour);
    ds1302_writebyte(WrMin, time->minute);
    ds1302_writebyte(WrSec, time->second);

    ds1302_writebyte(WrControl, 0x80);

    return;
}

/*******************************************************************************
*功能：读取实时时间
*参数：time-读取到的实时时间值
*返回：无
*说明：
*******************************************************************************/
void Dri_DS1302ReadTime(_Time *time)
{
    time->year     = ds1302_readbyte(RdYear);
    time->month    = ds1302_readbyte(RdMonth);
    time->day      = ds1302_readbyte(RdDate);
    time->hour     = ds1302_readbyte(RdHour);
    time->minute   = ds1302_readbyte(RdMin);
    time->second   = ds1302_readbyte(RdSec);

    /* 将BCD码转换为数字值 */
    time->year     = Lib_BCDToNum(time->year);
    time->month    = Lib_BCDToNum(time->month);
    time->day      = Lib_BCDToNum(time->day);
    time->hour     = Lib_BCDToNum(time->hour);
    time->minute   = Lib_BCDToNum(time->minute);
    time->second   = Lib_BCDToNum(time->second);

    return;
}

/*******************************************************************************
*功能：读取实时时间,并以BCD码的形式返回
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_DS1302ReadBCDTime(_Time *time)
{
    time->year     = ds1302_readbyte(RdYear);
    time->month    = ds1302_readbyte(RdMonth);
    time->day      = ds1302_readbyte(RdDate);
    time->hour     = ds1302_readbyte(RdHour);
    time->minute   = ds1302_readbyte(RdMin);
    time->second   = ds1302_readbyte(RdSec);

    return;
}

/*******************************************************************************
*功能：设置DS1302的实时时间
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_DS1302SetTime(_Time *time)
{
    _Time rtc_time;

    /* 关闭写保护 */
    ds1302_writebyte(WrControl, 0x00);
    Lib_DelayUs(10);

    rtc_time.year     = Lib_NumToBCD(time->year);
    rtc_time.month    = Lib_NumToBCD(time->month);
    rtc_time.day      = Lib_NumToBCD(time->day);
    rtc_time.hour     = Lib_NumToBCD(time->hour);
    rtc_time.minute   = Lib_NumToBCD(time->minute);
    rtc_time.second   = Lib_NumToBCD(time->second);

    ds1302_writetime(&rtc_time);

    /* 开启写保护 */
    ds1302_writebyte(WrControl, 0x80);

    return;
}

/*******************************************************************************
*功能：DS1302芯片初始化
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_DS1302Init(void)
{
    uint8 data;

    /*初始化管脚*/
    GPIO_PinModeSet(DS1302_PORT, DS1302_SCK_PIN, gpioModePushPull, 0);
    GPIO_PinModeSet(DS1302_PORT, DS1302_CE_PIN, gpioModePushPull, 0);

    /* 关闭写保护 */
    ds1302_writebyte(WrControl, 0x00);
    Lib_DelayUs(10);

    while (1)
    {
        data = ds1302_readbyte(RdTrickleCharge);

        if (data != 0xA6)
        {
            Lib_DelayUs(10);
            ds1302_writebyte(WrTrickleCharge, 0xA6);
        }
        else
        {
            break;
        }

    }

    Lib_DelayUs(10);

    /* 开启写保护 */
    ds1302_writebyte(WrControl, 0x80);

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年03月21日    巍图      创建文件
*******************************************************************************/
