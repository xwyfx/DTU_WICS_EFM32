/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年01月10日
**  名   称：Dri_Usart.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_leuart.h"
#include "em_usart.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_dma.h"

#include "Dri_Usart.h"
#include "Dri_Timer.h"
#include "Dri_Led.h"

#include "debug.h"


#define LEUART0_TX_CHANNEL            0                                          //通道0用于写发送数据
#define LEUART0_RX_CHANNEL0           1          //                                //通道1用于读接收数据
#define LEUART0_RX_CHANNEL1           2                                          //通道2用于读接收数据

#define LEUART1_TX_CHANNEL            3                                          //通道6用于写发送数据
#define LEUART1_RX_CHANNEL0           4                                          //通道7用于读接收数据
#define LEUART1_RX_CHANNEL1           5                                          //通道8用于读接收数据

#define USART0_TX_CHANNEL             6                                          //通道3用于写发送数据
#define USART0_RX_CHANNEL0            7                                          //通道4用于读接收数据
#define USART0_RX_CHANNEL1            8                                          //通道5用于读接收数据


/*-----------DMA部分变量---------*/
DMA_CB_TypeDef Gtcb[DMA_CHAN_COUNT];                                             /* 声明8个通道的回调结构体      */
#pragma data_alignment=256
DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMA_CHAN_COUNT * 2];

#define BUFF_OFFSET               6                                              //该值等于offsetof(_SysMsg, buff);

#define LEUART0_RX_BUFF_LEN       60
#define LEUART1_RX_BUFF_LEN       MAX_MODBUS_BUFF_LEN
#define USART0_RX_BUFF_LEN        MAX_GPRS_RX_BUFF_LEN

/*-----------串口接收发送缓存部分变量----------*/
uint8 Leuart0_ReceiveBuff[2][LEUART0_RX_BUFF_LEN + BUFF_OFFSET + 1];
uint8 Leuart0_SendBuff[MAX_BUFF_LEN + 10];
uint8 Leuart0RxChannel0Buff;
static bool Leuart0SendDone = true;

uint8 Leuart1_ReceiveBuff[2][LEUART1_RX_BUFF_LEN + BUFF_OFFSET + 1];
uint8 Leuart1_SendBuff[MAX_MODBUS_BUFF_LEN];
uint8 Leuart1RxChannel0Buff;

uint8 Usart0_ReceiveBuff[2][USART0_RX_BUFF_LEN + BUFF_OFFSET + 1];
uint8 Usart0_SendBuff[MAX_GPRS_TX_BUFF_LEN];
uint8 Usart0RxChannel0Buff;

extern void PHY_UsartSendDone(uint8 channel);
extern bool PHY_UsartReveiveDone(_SysMsg *msg);

/*-----------RS485收发控制--------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_RS485TxEnable(void)
{
    GPIO_PinOutSet(gpioPortA, 9);
    GPIO_PinOutSet(gpioPortA, 10);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_RS485RxEnable(void)
{
    GPIO_PinOutClear(gpioPortA, 9);
    GPIO_PinOutClear(gpioPortA, 10);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Fun_RS485IdleEnable(void)
{
    GPIO_PinOutClear(gpioPortA, 9);
    GPIO_PinOutSet(gpioPortA, 10);

    return;
}

/*-----------接收缓存处理----------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static uint32 Fun_GetUsartRxBuffLen(uint8 channel)
{
    uint32 len;

    if ((channel != LEUART0_RX_CHANNEL1) && (channel != LEUART1_RX_CHANNEL1) && (channel != USART0_RX_CHANNEL1))
    {
        return 0;
    }

    len = (dmaControlBlock[channel].CTRL & _DMA_CTRL_N_MINUS_1_MASK) >> _DMA_CTRL_N_MINUS_1_SHIFT;

    if (channel == LEUART0_RX_CHANNEL1)
    {
        if ((len + 1) >= LEUART0_RX_BUFF_LEN)
        {
            return 0;
        }

        len = LEUART0_RX_BUFF_LEN - len - 1;
    }
    else if (channel == LEUART1_RX_CHANNEL1)
    {
        if ((len + 1) >= LEUART1_RX_BUFF_LEN)
        {
            return 0;
        }

        len = LEUART1_RX_BUFF_LEN - len - 1;
    }
    else if (channel == USART0_RX_CHANNEL1)
    {
        if ((len + 1) >= USART0_RX_BUFF_LEN)
        {
            return 0;
        }

        len = USART0_RX_BUFF_LEN - len - 1;
    }
    else
    {
        len = 0;
    }

    return len;
}

/*******************************************************************************
*功能：获取串口空闲接收缓存
*参数：无
*返回：无
*说明：
*******************************************************************************/
static _SysMsg* Fun_GetUsartIdleRecvBuff(uint8 usart)
{
    switch (usart)
    {
        case LEUART0_CHANNEL:
            {
                if (((_SysMsg *)Leuart0_ReceiveBuff[0])->IsUsed == false)
                {
                    ((_SysMsg *)Leuart0_ReceiveBuff[0])->IsUsed = true;
                    ((_SysMsg *)Leuart0_ReceiveBuff[1])->IsUsed = false;

                    return (_SysMsg *)Leuart0_ReceiveBuff[0];
                }
                else
                {
                    ((_SysMsg *)Leuart0_ReceiveBuff[0])->IsUsed = false;
                    ((_SysMsg *)Leuart0_ReceiveBuff[1])->IsUsed = true;

                    return (_SysMsg *)Leuart0_ReceiveBuff[1];
                }

                break;
            }
        case USUART0_CHANNEL:
            {
                if (((_SysMsg *)Usart0_ReceiveBuff[0])->IsUsed == false)
                {
                    ((_SysMsg *)Usart0_ReceiveBuff[0])->IsUsed = true;
                    ((_SysMsg *)Usart0_ReceiveBuff[1])->IsUsed = false;

                    return (_SysMsg *)Usart0_ReceiveBuff[0];
                }
                else
                {
                    ((_SysMsg *)Usart0_ReceiveBuff[0])->IsUsed = false;
                    ((_SysMsg *)Usart0_ReceiveBuff[1])->IsUsed = true;

                    return (_SysMsg *)Usart0_ReceiveBuff[1];
                }

                break;
            }
        case LEUART1_CHANNEL:
            {
                if (((_SysMsg *)Leuart1_ReceiveBuff[0])->IsUsed == false)
                {
                    ((_SysMsg *)Leuart1_ReceiveBuff[0])->IsUsed = true;
                    ((_SysMsg *)Leuart1_ReceiveBuff[1])->IsUsed = false;

                    return (_SysMsg *)Leuart1_ReceiveBuff[0];
                }
                else
                {
                    ((_SysMsg *)Leuart1_ReceiveBuff[0])->IsUsed = false;
                    ((_SysMsg *)Leuart1_ReceiveBuff[1])->IsUsed = true;

                    return (_SysMsg *)Leuart1_ReceiveBuff[1];
                }
            }
    }

    return NULL;
}

/*******************************************************************************
*功能：获取串口接收缓存
*参数：无
*返回：无
*说明：
*******************************************************************************/
static _SysMsg* Fun_GetUsartRecvBuff(uint8 usart)
{
    switch (usart)
    {
        case LEUART0_CHANNEL:
            {
                if (((_SysMsg *)Leuart0_ReceiveBuff[0])->IsUsed == true)
                {
                    return (_SysMsg *)Leuart0_ReceiveBuff[0];
                }
                else if (((_SysMsg *)Leuart0_ReceiveBuff[1])->IsUsed == true)
                {
                    return (_SysMsg *)Leuart0_ReceiveBuff[1];
                }
                else
                {
                    return NULL;
                }

                break;
            }
        case LEUART1_CHANNEL:
            {
                if (((_SysMsg *)Leuart1_ReceiveBuff[0])->IsUsed == true)
                {
                    return (_SysMsg *)Leuart1_ReceiveBuff[0];
                }
                else if (((_SysMsg *)Leuart1_ReceiveBuff[1])->IsUsed == true)
                {
                    return (_SysMsg *)Leuart1_ReceiveBuff[1];
                }
                else
                {
                    return NULL;
                }

                break;
            }
        case USUART0_CHANNEL:
            {
                if (((_SysMsg *)Usart0_ReceiveBuff[0])->IsUsed == true)
                {
                    return (_SysMsg *)Usart0_ReceiveBuff[0];
                }
                else if (((_SysMsg *)Usart0_ReceiveBuff[1])->IsUsed == true)
                {
                    return (_SysMsg *)Usart0_ReceiveBuff[1];
                }
                else
                {
                    return NULL;
                }

                break;
            }
    }

    return NULL;
}

/*******************************************************************************
*功能：DMA发送完成回调函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_DmaSendFinishHandler(unsigned int channel, bool primary, void *user)
{
    switch (channel)
    {
        case LEUART0_TX_CHANNEL:
            {

                while (!(LEUART_StatusGet(LEUART0) & LEUART_STATUS_TXBL));

                /* LF register about to be modified require sync. busy check */
                /* Avoid deadlock if modifying the same register twice when freeze mode is */
                /* activated. */
                if (LEUART0->FREEZE & LEUART_FREEZE_REGFREEZE)
                {

                }
                else
                {
                    /* Wait for any pending previous write operation to have been completed */
                    /* in low frequency domain */
                    while (LEUART0->SYNCBUSY & LEUART_SYNCBUSY_TXDATA)
                    ;
                }

                channel = LEUART0_CHANNEL;
                Leuart0SendDone = true;

                break;
            }
        case LEUART1_TX_CHANNEL:
            {
                while (!(LEUART_StatusGet(LEUART1) & LEUART_STATUS_TXBL));

                /* LF register about to be modified require sync. busy check */
                /* Avoid deadlock if modifying the same register twice when freeze mode is */
                /* activated. */
                if (LEUART1->FREEZE & LEUART_FREEZE_REGFREEZE)
                {

                }
                else
                {
                    /* Wait for any pending previous write operation to have been completed */
                    /* in low frequency domain */
                    while (LEUART1->SYNCBUSY & LEUART_SYNCBUSY_TXDATA)
                    ;
                }

                uint32 i = 8000;
                while (i > 0)
                {
                    i--;
                }

                Fun_RS485RxEnable();

                channel = LEUART1_CHANNEL;

                break;
            }
        case USART0_TX_CHANNEL:
            {
                channel = USUART0_CHANNEL;

                break;
            }
        default:
            {
                return;
            }
    }

    PHY_UsartSendDone(channel);

    return;
}

/*-----------设置接收超时时间----------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SetLeuart0RxOverTime(unsigned int channel, bool primary, void *user)
{
    Dri_SetSTimer(LEUART0_RX_CHANNEL, 5, true);
    DMA_ActivateBasic(LEUART0_RX_CHANNEL0,                                      //配置基本传输
                      true,
                      false,
                      (void *)(uint32_t)&Leuart0RxChannel0Buff,
                      (void *)(uint32_t)&LEUART0->RXDATA,
                      0);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SetLeuart1RxOverTime(unsigned int channel, bool primary, void *user)
{
    Dri_SetSTimer(LEUART1_RX_CHANNEL, 3, true);                                 //10ms  认为超时
    DMA_ActivateBasic(LEUART1_RX_CHANNEL0,                                      //配置基本传输
                      true,
                      false,
                      (void *)(uint32_t)&Leuart1RxChannel0Buff,
                      (void *)(uint32_t)&LEUART1->RXDATA,
                      0);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SetUsart0RxOverTime(unsigned int channel, bool primary, void *user)
{
    Dri_SetSTimer(USART0_RX_CHANNEL, 5, true);                                  //1ms  认为超时
    DMA_ActivateBasic(USART0_RX_CHANNEL0,                                       // 配置基本传输
                      true,
                      false,
                      (void *)(uint32_t)&Usart0RxChannel0Buff,
                      (void *)(uint32_t)&USART0->RXDATA,
                      0);

    return;
}

/*--------------配置DMA接收缓存-------------*/
/*******************************************************************************
*功能：配置低速串口接收通道
*参数：无
*返回：无
*说明：用来接收数据的通道
*******************************************************************************/
static void Fun_SetLeuart0RxChannel(void)
{
    _SysMsg *msg;

    msg = Fun_GetUsartIdleRecvBuff(LEUART0_CHANNEL);

    DMA_ActivateBasic(LEUART0_RX_CHANNEL1,                                      //配置基本传输
                      true,
                      false,
                      (void *)(uint32_t)&msg->buff,
                      (void *)(uint32_t)&LEUART0->RXDATA,
                      LEUART0_RX_BUFF_LEN - 1);

    return;
}

/*******************************************************************************
*功能：配置Usart0接收通道
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SetLeuart1RxChannel(void)
{
    _SysMsg *msg;

    msg = Fun_GetUsartIdleRecvBuff(LEUART1_CHANNEL);

    DMA_ActivateBasic(LEUART1_RX_CHANNEL1,                                      //配置基本传输
                      true,
                      false,
                      (void *)(uint32_t)&msg->buff,
                      (void *)(uint32_t)&LEUART1->RXDATA,
                      LEUART1_RX_BUFF_LEN - 1);

    return;
}

/*******************************************************************************
*功能：配置Usart1接收通道
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SetUsart0RxChannel(void)
{
    _SysMsg *msg;

    msg = Fun_GetUsartIdleRecvBuff(USUART0_CHANNEL);
    memset(msg->buff, 0x00, USART0_RX_BUFF_LEN);

    DMA_ActivateBasic(USART0_RX_CHANNEL1,                                       //配置基本传输
                      true,
                      false,
                      (void *)(uint32_t)&msg->buff,
                      (void *)(uint32_t)&USART0->RXDATA,
                      USART0_RX_BUFF_LEN - 1);

    return;
}

/*---------------串口DMA初始化-------------*/
/*******************************************************************************
*功能：低速串口收、发DMA初始化
*参数：无
*返回：无
*说明：COM232口配置
*******************************************************************************/
static void Fun_Leuart0DmaInit(void)
{
  
  ////232口发送通道配置
    DMA_CfgChannel_TypeDef DmaChannel0Cfg =
    {                                                                           //DMA通道0配置结构体
        .highPri   = false,                                                     //主要描述符高优先级
        .enableInt = true,                                                      //使能中断
        .select    = DMAREQ_LEUART0_TXBL,                                       //请求信号
        .cb        = &(Gtcb[LEUART0_TX_CHANNEL])                                //&(Gtcb[CHANNEL0])  NULL                                /* 回调函数                     */
    };

    DMA_CfgDescr_TypeDef DmaChannel0CfgDescr =
    {                                                                           //DMA描述符0结构体
        .dstInc  = dmaDataIncNone,                                              //目标地址增加字节数
        .srcInc  = dmaDataInc1,                                                 //源地址增加字节数
        .size    = dmaDataSize1,                                                //传送一次的数据宽度
        .arbRate = dmaArbitrate1
    };

    
    ///232口接收通道1配置，专门用来通过超时来判断接收是否完成，即在设定的时间内没有中断发生，则认为接收完成
    DMA_CfgChannel_TypeDef DmaChannel1Cfg =
    {                                                                           //DMA通道1初始化结构体
        .highPri   = false,                                                     //主要描述符高优先
        .enableInt = true,                                                      //使能中断
        .select    = DMAREQ_LEUART0_RXDATAV,                                    //请求信号
        .cb        = &(Gtcb[LEUART0_RX_CHANNEL0])                               //回调函数
    };


    DMA_CfgDescr_TypeDef DmaChannel1CfgDescr =
    {                                                                           //DMA描述符1结构体
        .dstInc  = dmaDataInc1,                                                 //目标地址增加字节
        .srcInc  = dmaDataIncNone,                                              //源地址增加字节数
        .size    = dmaDataSize1,                                                //传送一次的数据宽
        .arbRate = dmaArbitrate1
    };

    
       ///232口接收通道2配置，真正接收数据的通道，不会产生中断
    DMA_CfgChannel_TypeDef DmaChannel2Cfg =
    {                                                                           //DMA通道2初始化结构体
        .highPri   = false,                                                     //主要描述符高优先
        .enableInt = true,                                                      //使能中断
        .select    = DMAREQ_LEUART0_RXDATAV,                                    //请求信号
        .cb        = &(Gtcb[LEUART0_RX_CHANNEL1])                               //回调函数
    };


    DMA_CfgDescr_TypeDef DmaChannel2CfgDescr = {                                //DMA描述符2结构体
        .dstInc  = dmaDataInc1,                                                 //目标地址增加字节
        .srcInc  = dmaDataIncNone,                                              //源地址增加字节数
        .size    = dmaDataSize1,                                                //传送一次的数据宽
        .arbRate = dmaArbitrate1
    };

    Gtcb[LEUART0_TX_CHANNEL].cbFunc   = Fun_DmaSendFinishHandler;//发送完成的回调
    Gtcb[LEUART0_TX_CHANNEL].userPtr  = NULL;

    Gtcb[LEUART0_RX_CHANNEL0].cbFunc  = Fun_SetLeuart0RxOverTime;//超时的回调
    Gtcb[LEUART0_RX_CHANNEL0].userPtr = NULL;

    Gtcb[LEUART0_RX_CHANNEL1].cbFunc  = NULL;
    Gtcb[LEUART0_RX_CHANNEL1].userPtr = NULL;

    DMA_CfgChannel(LEUART0_TX_CHANNEL, &DmaChannel0Cfg);                        //初始化通道0配置
    DMA_CfgDescr(LEUART0_TX_CHANNEL, true, &DmaChannel0CfgDescr);               //初始化通道0描述符

    DMA_CfgChannel(LEUART0_RX_CHANNEL0, &DmaChannel1Cfg);                       //初始化通道1配置
    DMA_CfgDescr(LEUART0_RX_CHANNEL0, true, &DmaChannel1CfgDescr);              //初始化通道1描述符

    DMA_CfgChannel(LEUART0_RX_CHANNEL1, &DmaChannel2Cfg);                       //初始化通道2配置
    DMA_CfgDescr(LEUART0_RX_CHANNEL1, true, &DmaChannel2CfgDescr);              //初始化通道2描述符

    /*使能完成中断*/
    DMA->IEN |= DMA_IEN_CH0DONE;                                                //使能DMA通道0中断
    DMA->IEN |= DMA_IEN_CH1DONE;
    DMA->IEN |= DMA_IEN_CH2DONE;
//232接收 超时判断开始
    DMA_ActivateBasic(LEUART0_RX_CHANNEL0,                                      // 配置基本传输
                      true,
                      false,
                      (void *)(uint32_t)&Leuart0RxChannel0Buff,
                      (void *)(uint32_t)&LEUART0->RXDATA,
                      0);

    Fun_SetLeuart0RxChannel();

    return;
}

/*******************************************************************************
*功能：Usart0收、发DMA初始化
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_Leuart1DmaInit(void)
{
    DMA_CfgChannel_TypeDef Leuart1TxChannelCfg =                                //DMA通道6配置结构体
    {
        .highPri   = false,                                                     //主要描述符高优先级
        .enableInt = true,                                                      //使能中断
        .select    = DMAREQ_LEUART1_TXBL,                                       //请求信号
        .cb        = &(Gtcb[LEUART1_TX_CHANNEL])                                //&(Gtcb[CHANNEL0])                               /* 回调函数                     */
    };

    DMA_CfgDescr_TypeDef Leuart1TxChannelCfgDescr =                             //DMA描述符6结构体
    {
        .dstInc  = dmaDataIncNone,                                              //目标地址增加字节数
        .srcInc  = dmaDataInc1,                                                 //源地址增加字节数
        .size    = dmaDataSize1,                                                //传送一次的数据宽度
        .arbRate = dmaArbitrate1
    };

    DMA_CfgChannel_TypeDef Leuart1RxChannel0Cfg =                               //DMA通道7初始化结构体
    {
        .highPri   = false,                                                     //主要描述符高优先
        .enableInt = true,                                                      //使能中断
        .select    = DMAREQ_LEUART1_RXDATAV,                                    //请求信号
        .cb        = &(Gtcb[LEUART1_RX_CHANNEL0])                               //回调函数
    };


    DMA_CfgDescr_TypeDef Leuart1RxChannel0CfgDescr =                            //DMA描述符7结构体
    {
        .dstInc  = dmaDataInc1,                                                 //目标地址增加字节
        .srcInc  = dmaDataIncNone,                                              //源地址增加字节数
        .size    = dmaDataSize1,                                                //传送一次的数据宽
        .arbRate = dmaArbitrate1
    };

    DMA_CfgChannel_TypeDef Leuart1RxChannel1Cfg =                               //DMA通道8初始化结构体
    {
        .highPri   = false,                                                     //主要描述符高优先
        .enableInt = true,                                                      //使能中断
        .select    = DMAREQ_LEUART1_RXDATAV,                                    //请求信号
        .cb        = &(Gtcb[LEUART1_RX_CHANNEL1])                               //回调函数
    };


    DMA_CfgDescr_TypeDef Leuart1RxChannel1CfgDescr =                            //DMA描述符8结构体
    {
        .dstInc  = dmaDataInc1,                                                 //目标地址增加字节
        .srcInc  = dmaDataIncNone,                                              //源地址增加字节数
        .size    = dmaDataSize1,                                                //传送一次的数据宽
        .arbRate = dmaArbitrate1
    };

    Gtcb[LEUART1_TX_CHANNEL].cbFunc   = Fun_DmaSendFinishHandler;
    Gtcb[LEUART1_TX_CHANNEL].userPtr  = NULL;

    Gtcb[LEUART1_RX_CHANNEL0].cbFunc  = Fun_SetLeuart1RxOverTime;
    Gtcb[LEUART1_RX_CHANNEL0].userPtr = NULL;

    Gtcb[LEUART1_RX_CHANNEL1].cbFunc  = NULL;
    Gtcb[LEUART1_RX_CHANNEL1].userPtr = NULL;

    DMA_CfgChannel(LEUART1_TX_CHANNEL, &Leuart1TxChannelCfg);                   //初始化通道6配置
    DMA_CfgDescr(LEUART1_TX_CHANNEL, true, &Leuart1TxChannelCfgDescr);          //初始化通道6描述符
    DMA_CfgChannel(LEUART1_RX_CHANNEL0, &Leuart1RxChannel0Cfg);                 //初始化通道7配置
    DMA_CfgDescr(LEUART1_RX_CHANNEL0, true, &Leuart1RxChannel0CfgDescr);        //初始化通道7述符
    DMA_CfgChannel(LEUART1_RX_CHANNEL1, &Leuart1RxChannel1Cfg);                 //初始化通道8配置
    DMA_CfgDescr(LEUART1_RX_CHANNEL1, true, &Leuart1RxChannel1CfgDescr);        //初始化通道8描述符

    /*使能完成中断*/
    DMA->IEN |= DMA_IEN_CH6DONE;
    DMA->IEN |= DMA_IEN_CH7DONE;                                                //使能DMA通道0中断
    DMA->IEN |= DMA_IEN_CH8DONE;

    DMA_ActivateBasic(LEUART1_RX_CHANNEL0,                                      //配置基本传输
                      true,
                      false,
                      (void *)(uint32_t)&Leuart1RxChannel0Buff,
                      (void *)(uint32_t)&LEUART1->RXDATA,
                      0);

    Fun_SetLeuart1RxChannel();

    return;
}

/*******************************************************************************
*功能：Usart1收、发DMA初始化
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_Usart0DmaInit(void)
{
    DMA_CfgChannel_TypeDef Usart0TxChannelCfg =                                 //Usart0发送通道配置结构体
    {
        .highPri   = false,                                                     //主要描述符高优先级
        .enableInt = true,                                                      //使能中断
        .select    = DMAREQ_USART0_TXBL,                                        //请求信号
        .cb        = &(Gtcb[USART0_TX_CHANNEL])                                 //&(Gtcb[CHANNEL0])                                  /* 回调函数                     */
    };

    DMA_CfgDescr_TypeDef Usart0TxChannelCfgDescr =                              //Usart0发送通道描述符结构体
    {
        .dstInc  = dmaDataIncNone,                                              //目标地址增加字节数
        .srcInc  = dmaDataInc1,                                                 //源地址增加字节数
        .size    = dmaDataSize1,                                                //传送一次的数据宽度
        .arbRate = dmaArbitrate1
    };

    DMA_CfgChannel_TypeDef Usart0RxChannel0Cfg =                                //Usart0接收0通道配置结构体
    {
        .highPri   = false,                                                     //主要描述符高优先
        .enableInt = true,                                                      //使能中断
        .select    = DMAREQ_USART0_RXDATAV,                                     //请求信号
        .cb        = &(Gtcb[USART0_RX_CHANNEL0])                                //回调函数
    };


    DMA_CfgDescr_TypeDef Usart0RxChannel0CfgDescr =                             //Usart0接收通道0描述符结构体
    {
        .dstInc  = dmaDataInc1,                                                 //目标地址增加字节
        .srcInc  = dmaDataIncNone,                                              //源地址增加字节数
        .size    = dmaDataSize1,                                                //传送一次的数据宽
        .arbRate = dmaArbitrate1
    };

    DMA_CfgChannel_TypeDef Usart0RxChannel1Cfg =                                //Usart0接收1通道配置结构体
    {
        .highPri   = false,                                                     //主要描述符高优先
        .enableInt = true,                                                      //使能中断
        .select    = DMAREQ_USART0_RXDATAV,                                     //请求信号
        .cb        = &(Gtcb[USART0_RX_CHANNEL1])                                //回调函数
    };


    DMA_CfgDescr_TypeDef Usart0RxChannel1CfgDescr =                             //Usart0接收通道1描述符结构体
    {
        .dstInc  = dmaDataInc1,                                                 //目标地址增加字节
        .srcInc  = dmaDataIncNone,                                              //源地址增加字节数
        .size    = dmaDataSize1,                                                //传送一次的数据宽
        .arbRate = dmaArbitrate1
    };

    Gtcb[USART0_TX_CHANNEL].cbFunc   = Fun_DmaSendFinishHandler;
    Gtcb[USART0_TX_CHANNEL].userPtr  = NULL;

    Gtcb[USART0_RX_CHANNEL0].cbFunc  = Fun_SetUsart0RxOverTime;
    Gtcb[USART0_RX_CHANNEL0].userPtr = NULL;

    Gtcb[USART0_RX_CHANNEL1].cbFunc  = NULL;
    Gtcb[USART0_RX_CHANNEL1].userPtr = NULL;

    DMA_CfgChannel(USART0_TX_CHANNEL, &Usart0TxChannelCfg);                     //初始化通道0配置
    DMA_CfgDescr(USART0_TX_CHANNEL, true, &Usart0TxChannelCfgDescr);            //初始化通道0描述符

    DMA_CfgChannel(USART0_RX_CHANNEL0, &Usart0RxChannel0Cfg);                   //初始化通道1配置
    DMA_CfgDescr(USART0_RX_CHANNEL0, true, &Usart0RxChannel0CfgDescr);          //初始化通道1描述符

    DMA_CfgChannel(USART0_RX_CHANNEL1, &Usart0RxChannel1Cfg);                   //初始化通道2配置
    DMA_CfgDescr(USART0_RX_CHANNEL1, true, &Usart0RxChannel1CfgDescr);          //初始化通道2描述符

    /*使能完成中断*/
    DMA->IEN |= DMA_IEN_CH3DONE;
    DMA->IEN |= DMA_IEN_CH4DONE;                                                //使能DMA通道0中断
    DMA->IEN |= DMA_IEN_CH5DONE;

    DMA_ActivateBasic(USART0_RX_CHANNEL0,                                       //配置基本传输
                      true,
                      false,
                      (void *)(uint32_t)&Usart0RxChannel0Buff,
                      (void *)(uint32_t)&USART0->RXDATA,
                      0);

    Fun_SetUsart0RxChannel();

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_Leuart0Init(void)
{
    /*开启时钟*/
    CMU_ClockEnable(cmuClock_HFLE, true);
    CMU_ClockEnable(cmuClock_LEUART0, true);
    CMU_ClockEnable(cmuClock_GPIO, true);

    /*配置管脚*/
    GPIO_PinModeSet(gpioPortD, 4, gpioModeWiredAndPullUp,  1);                  //配置PD4为开漏上拉            */
    GPIO_PinModeSet(gpioPortD, 5, gpioModeInputPull, 1);                        //配置PD5为输入上拉模式        */

    /*LEUART初始化结构体*/
    LEUART_Init_TypeDef tLeuartInit =
    {
        .enable   = leuartEnable,                                               //使能收发
        .refFreq  = 0,                                                          //一般设为0
        .baudrate = 9600,                                                       //波特率
        .databits = leuartDatabits8,                                            //数据位
        .parity   = leuartNoParity,                                             //奇偶校验
        .stopbits = leuartStopbits1                                             //停止位
    };

    LEUART_Reset(LEUART0);                                                      //复位LEUART
    LEUART_Init(LEUART0, &tLeuartInit);

    /*
     *  开启发送空闲时阻塞
     */
    LEUART0->CTRL  = LEUART0->CTRL & (~(_LEUART_CTRL_AUTOTRI_MASK)) |
            LEUART_CTRL_AUTOTRI;
    /*
    *  使能接收唤醒中断
    */
    LEUART0->CTRL  = LEUART0->CTRL & (~(_LEUART_CTRL_RXDMAWU_MASK)) |
            LEUART_CTRL_RXDMAWU;
    LEUART0->CTRL  = LEUART0->CTRL & (~(_LEUART_CTRL_TXDMAWU_MASK)) |
            LEUART_CTRL_TXDMAWU;
    /*
     *  使能发送接收管脚
     */
    LEUART0->ROUTE = LEUART_ROUTE_TXPEN |
            LEUART_ROUTE_RXPEN          |
            LEUART_ROUTE_LOCATION_LOC0;

    /*DMA初始化*/
    Fun_Leuart0DmaInit();

    /*为接收超时而注册软件定时器*/
    Dri_RegisterSTimer(LEUART0_RX_CHANNEL, Dri_Leuart0ReceivedDoneEvent);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_Leuart1Init(void)
{
    /*开启时钟*/
    /*开启时钟*/
    CMU_ClockEnable(cmuClock_HFLE, true);
    CMU_ClockEnable(cmuClock_LEUART1, true);
    CMU_ClockEnable(cmuClock_GPIO, true);

    /*控制管脚：全低为接收；全高为发送；PA9低，PA10高为空闲，可实现低功耗*/
    GPIO_PinModeSet(gpioPortA, 9, gpioModePushPull,  0);
    GPIO_PinModeSet(gpioPortA, 10, gpioModePushPull,  0);

    /*RS485低功耗模式*/
    GPIO_PinModeSet(gpioPortA, 9, gpioModePushPull,  0);
    GPIO_PinModeSet(gpioPortA, 10, gpioModePushPull,  1);

    /*配置管脚*/
    GPIO_PinModeSet(gpioPortC, 6, gpioModeWiredAndPullUp,  1);                  //配置PD4为开漏上拉            */
    GPIO_PinModeSet(gpioPortC, 7, gpioModeInputPull, 1);                        //配置PD5为输入上拉模式        */

    /*LEUART初始化结构体*/
    LEUART_Init_TypeDef LeuartInit =
    {
        .enable   = leuartEnable,                                               //使能收发
        .refFreq  = 0,                                                          //一般设为0
        .baudrate = 9600,                                                       //波特率
        .databits = leuartDatabits8,                                            //数据位
        .parity   = leuartNoParity,                                             //奇偶校验
        .stopbits = leuartStopbits1                                             //停止位
    };

    LEUART_Reset(LEUART1);                                                      //复位LEUART
    LEUART_Init(LEUART1, &LeuartInit);

    /*
     *  开启发送空闲时阻塞
     */
    LEUART1->CTRL  = LEUART0->CTRL & (~(_LEUART_CTRL_AUTOTRI_MASK)) |
            LEUART_CTRL_AUTOTRI;
    /*
    *  使能接收唤醒中断
    */
    LEUART1->CTRL  = LEUART0->CTRL & (~(_LEUART_CTRL_RXDMAWU_MASK)) |
            LEUART_CTRL_RXDMAWU;
    LEUART1->CTRL  = LEUART0->CTRL & (~(_LEUART_CTRL_TXDMAWU_MASK)) |
            LEUART_CTRL_TXDMAWU;
    /*
     *  使能发送接收管脚
     */
    LEUART1->ROUTE = LEUART_ROUTE_TXPEN |
            LEUART_ROUTE_RXPEN          |
            LEUART_ROUTE_LOCATION_LOC0;

    /*DMA初始化*/
    Fun_Leuart1DmaInit();

    /*为接收超时而注册软件定时器*/
    Dri_RegisterSTimer(LEUART1_RX_CHANNEL, Dri_Leuart1ReceivedDoneEvent);

    /*默认进入接收模式*/
    Fun_RS485RxEnable();

    return;
}


/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_Usart0Init(void)
{
    /*使能时钟*/
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_GPIO, true);
    CMU_ClockEnable(cmuClock_USART0, true);

    /*使能管脚*/
    GPIO_PinModeSet(gpioPortC, 0, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortC, 1, gpioModeInputPull, 1);

    USART_InitAsync_TypeDef UsartInit = {
        .enable       = usartEnable,                                            //使能USART发送与接收
        .refFreq      = 0,                                                      //使用当前时钟配置来配置波特率
        .baudrate     = 115200,                                                 //波特率配置
        .oversampling = usartOVS16,                                             //16x过采样率
        .databits     = usartDatabits8,                                         //8位数据位
        .parity       = usartNoParity,                                          //无校验位
        .stopbits     = usartStopbits1,                                         //1位停止位
        .mvdis        = false,                                                  //使能Majority
        .prsRxEnable  = false,                                                  //禁能PRS作为RX输入
        .prsRxCh      = usartPrsRxCh0                                           //选择PRS通道
    };
    USART_InitAsync(USART0, &UsartInit);                                        //调用初始化函数

    /*
     * 使能USART1的TX和RX
     */
    USART0->ROUTE = USART_ROUTE_TXPEN | USART_ROUTE_RXPEN | (5UL << 8);

    /*DMA初始化*/
    Fun_Usart0DmaInit();

    /*为接收超时而注册软件定时器*/
    Dri_RegisterSTimer(USART0_RX_CHANNEL, Dri_Usart0ReceivedDoneEvent);

    return;
}


/*---------------串口接收完成处理----------------*/
/*******************************************************************************
*功能：Leuart0接收完成中断
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_Leuart0ReceivedDoneEvent(void)
{
    uint32 len;
    _SysMsg *msg;

    len = Fun_GetUsartRxBuffLen(LEUART0_RX_CHANNEL1);

    if (len == 0)
    {
        Fun_SetLeuart0RxChannel();
        return;
    }

    msg = Fun_GetUsartRecvBuff(LEUART0_CHANNEL);

    if (msg == NULL)
    {
        Fun_SetLeuart0RxChannel();
        return;
    }

    Fun_SetLeuart0RxChannel();

    msg->buff[len] = 0;

    msg->port = LEUART0_CHANNEL;
    msg->len = len;
    PHY_UsartReveiveDone(msg);

    return;
}

/*******************************************************************************
*功能：Leuart1接收完成中断
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_Leuart1ReceivedDoneEvent(void)
{
    uint32 len;
    _SysMsg *msg;

    len = Fun_GetUsartRxBuffLen(LEUART1_RX_CHANNEL1);

    if (len == 0)
    {
        Fun_SetLeuart1RxChannel();
        return;
    }

    msg = Fun_GetUsartRecvBuff(LEUART1_CHANNEL);

    if (msg == NULL)
    {
        Fun_SetLeuart1RxChannel();
        return;
    }

    Fun_SetLeuart1RxChannel();

//    Fun_RS485IdleEnable();

    msg->buff[len] = 0;
    msg->port = LEUART1_CHANNEL;
    msg->len = len;
    PHY_UsartReveiveDone(msg);

    return;
}

/*******************************************************************************
*功能：Usart0接收完成中断
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_Usart0ReceivedDoneEvent(void)
{
    uint32 len;
    _SysMsg *msg;

    len = Fun_GetUsartRxBuffLen(USART0_RX_CHANNEL1);

    if (len == 0)
    {
        Fun_SetUsart0RxChannel();
        return;
    }

    msg = Fun_GetUsartRecvBuff(USUART0_CHANNEL);

    if (msg == NULL)
    {
        Fun_SetUsart0RxChannel();
        return;
    }

    Fun_SetUsart0RxChannel();

    msg->buff[len] = 0;
    msg->port = USUART0_CHANNEL;
    msg->len = len;
    PHY_UsartReveiveDone(msg);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Dri_UsartSend(uint8 *buff, uint16 len, uint16 port)
{
    if (len == 0)
    {
        return false;
    }

    switch (port)
    {
        case LEUART0_CHANNEL:                                                   //本地串口
            {
                if (DMA_ChannelEnabled(LEUART0_TX_CHANNEL) == true)             //查看dma是否忙
                {
                    while (Leuart0SendDone == false)
                    {
                    }
                }

                if (len >= sizeof(Leuart0_SendBuff))
                {
                    return false;
                }

                memcpy(Leuart0_SendBuff, buff, len);

                Leuart0SendDone = false;

                DMA_ActivateBasic(LEUART0_TX_CHANNEL,                           // 配置DMA基本传输
                                  true,
                                  false,
                                  (void *)(uint32)&LEUART0->TXDATA,
                                  (void *)(uint32)Leuart0_SendBuff,
                                  len - 1);
                break;
            }
        case LEUART1_CHANNEL:                                                   //RS485
            {
                if (DMA_ChannelEnabled(LEUART1_TX_CHANNEL) == true)             //查看dma是否忙
                {
                    return false;
                }

                if (len >= sizeof(Leuart1_SendBuff))
                {
                    return false;
                }

                Debug_Printf("RS485 Tx:", buff, len, DEBUG_RS485_TX, 0);

                Fun_RS485TxEnable();

                memcpy(Leuart1_SendBuff, buff, len);

                DMA_ActivateBasic(LEUART1_TX_CHANNEL,                           //配置DMA基本传输
                                  true,
                                  false,
                                  (void *)(uint32)&LEUART1->TXDATA,
                                  (void *)(uint32)Leuart1_SendBuff,
                                  len - 1);

                break;
            }
        case USUART0_CHANNEL:
            {
                if (DMA_ChannelEnabled(USART0_TX_CHANNEL) == true)              //查看dma是否忙
                {
                    return false;
                }

                Dri_LedToggle(1);

                if (len >= sizeof(Usart0_SendBuff))
                {
                    return false;
                }

                memcpy(Usart0_SendBuff, buff, len);

                /*调试信息输出*/
                if (SendAtCmd == false)
                {
                    SendAtCmd = true;
                    Debug_Printf("GPRS Tx:", buff, len, DEBUG_GPRS_TX, 0);
                }
                else
                {
                    Debug_Printf("GPRS Tx:", buff, len, DEBUG_GPRS_TX, 1);
                }

                /*调用串口底层发送*/
                DMA_ActivateBasic(USART0_TX_CHANNEL,                            // 配置DMA基本传输
                                  true,
                                  false,
                                  (void *)(uint32)&USART0->TXDATA,
                                  (void *)(uint32)Usart0_SendBuff,
                                  len - 1);
                break;
            }
        default:
            {
                return false;
            }
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_UsartInit(void)
{
    Fun_Leuart0Init();
    Fun_Leuart1Init();
    Fun_Usart0Init();

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年01月10日    巍图      创建文件
*******************************************************************************/

