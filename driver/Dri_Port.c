/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_Port.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_gpio.h"
#include "em_burtc.h"


#include "Dri_Dma.h"
#include "Dri_Port.h"
#include "Dri_Delay.h"



/*******************************************************************************
*功能：偶数通道中断处理函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
void  GPIO_ODD_IRQHandler(void)
{
    uint32 flg;

    CPU_SR  cpu_sr = 0;

    flg = GPIO_IntGet();

    if ((flg & ((uint32)1 << 15)) == (uint32)1 << 15)
    {
        GPIO_IntClear(1 << 15);

        /*---------*/
        
        /*---------*/
    }

    if ((flg & ((uint32)1 << 13)) == (uint32)1 << 13)
    {
        CPU_CRITICAL_ENTER();

        NVIC_DisableIRQ(TIMER1_IRQn);
        NVIC_ClearPendingIRQ(TIMER1_IRQn);

        GPIO_IntClear(1 << 13);
        GPIO_IntDisable(1 << 13);
        
        
        /*---------*/
        
        /*---------*/

        OSIntExit();

        CPU_CRITICAL_EXIT();
    }
    else
    {
        GPIO_IntClear(0xFFFFFFFF & (~(1 << 13)) & (~(1 << 15)) & (~(1 << 8)));
        GPIO_IntDisable(0xFFFFFFFF & (~(1 << 13)) & (~(1 << 15)) & (~(1 << 8)));
    }

    return;
}

/*******************************************************************************
*功能：偶数通道中断处理函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
void  GPIO_EVEN_IRQHandler(void)
{
    uint32 flg;

    flg = GPIO_IntGetEnabled();
    GPIO_IntClear(0xFFFFFFFF);

    if ((flg & ((uint32)1 << 8)) == (uint32)1 << 8)
    {
        /*---------*/
        
        /*---------*/
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_PortInit(void)
{
    /*初始化干簧管引脚*/
    GPIO_PinModeSet(gpioPortA, 8, gpioModePushPullDrive, 0);
    GPIO_PinOutClear(gpioPortA, 8);

    GPIO_PinModeSet(gpioPortA, 8, gpioModeInputPull, 0);

    _delay_us(2000);

    GPIO_IntClear(0xFFFFFFFF);
    GPIO_IntConfig(gpioPortA, 8, true, false, true);                            //下降沿捕获
    GPIO_IntClear(0xFFFFFFFF);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 Dri_GetPortLevel(uint16 port)
{
    if (GPIO_PinInGet(gpioPortA, 8))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_EnableInt(uint16 port, uint32 pin)
{
    NVIC_EnableIRQ(GPIO_EVEN_IRQn);

    return;
}


/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/
