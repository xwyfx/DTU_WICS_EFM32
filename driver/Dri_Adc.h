/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            IW(2008-2011)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_Adc.h
**  摘   要：
*******************************************************************************/
#ifndef _DRI_ADC_H
#define _DRI_ADC_H

extern int32 AD_Sample;

void Dri_AdcInit(void);
int8 AD_Temperature_Cal(void);
int16 AD_Temperature_Cal_Expand10(void);
void PHY_UpdataADTemperature(void);
void PHY_UpdataADTemperature_Test(void);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/
