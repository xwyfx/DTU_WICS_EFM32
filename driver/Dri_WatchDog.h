/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            IW(2008-2011)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_WatchDog.h
**  摘   要：
*******************************************************************************/
#ifndef _DRI_WATCHDOG_H
#define _DRI_WATCHDOG_H

void Dri_Reset(void);
void Dri_WatchdogTimerInit(void);
void Dri_StartWatchdogTimer(void);
void Dri_StopWatchdogTimer(void);
void Dri_FeedWatchdogTimer(void);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/
