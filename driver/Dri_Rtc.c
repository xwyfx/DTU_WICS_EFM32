/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            IW(2008-2011)
**  作   者：巍图
**  日   期：2017年01月13日
**  名   称：Dri_Rtc.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_rtc.h"
#include "em_cmu.h"

#include "Dri_Rtc.h"
#include "Dri_Timer.h"

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void RTC_IRQHandler(void)
{
    CPU_SR  cpu_sr = 0;

    CPU_CRITICAL_ENTER();
    RTC_IntClear(RTC_IF_COMP0);                                                 //清除比较中断标志

    Dri_TimerFiredHandler(RTC_TIMER_CHANNEL_0);

    CPU_CRITICAL_EXIT();

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：RTC计数器为递增模式
*******************************************************************************/
void Dri_RtcInit(void)
{
    RTC_Init_TypeDef tRtcInit =                                                 //RTC初始化结构体
    {
        .enable   = true,                                                       //使能控制
        .debugRun = false,                                                      //是否在调试时运行
        .comp0Top = false                                                       //是否开启比较匹配
    };

    CMU_ClockEnable(cmuClock_CORELE, true);                                     //使能低功耗内核时钟
    CMU_ClockEnable(cmuClock_RTC, true);                                        //使能RTC时钟
    RTC_Init(&tRtcInit);

    NVIC_SetPriority(RTC_IRQn, NVIC_EncodePriority(0, 0, 0));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Dri_StartRtcTimerChannel(uint8 channel, uint16 time)
{
    switch (channel)
    {
        case RTC_TIMER_CHANNEL_0:
        {
            uint32 tmp;

            tmp = RTC_CounterGet();
            tmp = tmp + time;
            tmp &= 0x00FFFFFF;

            RTC_IntClear(RTC_IF_COMP0);                                         //清除比较中断标志
            RTC_CompareSet(0, tmp);                                             //设定定时值
            RTC_IntEnable(RTC_IF_COMP0);                                        //使能比较中断
            NVIC_EnableIRQ(RTC_IRQn);                                           //使能中断向

            return true;
        }
        default:
        {
            return false;
        }
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Dri_StopRtcTimerChannel(uint8 channel)
{
    switch (channel)
    {
        case RTC_TIMER_CHANNEL_0:
        {
            RTC_IntDisable(RTC_IF_COMP0);                                       //使能比较中断
            NVIC_DisableIRQ(RTC_IRQn);                                          //使能中断向//使能中断向

            return true;
        }
        default:
        {
            return false;
        }
    }
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年01月13日    巍图      创建文件
*******************************************************************************/

