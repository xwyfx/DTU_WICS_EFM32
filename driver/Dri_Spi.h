/************************************************************************
**                 中科院沈阳自动化研究所 Copyright （c）
**                           农业灌溉系统
**  作    者：niuzetian
**  日    期：2013.9.25
**  文件名称：Dri_Spi.h
**  摘    要：Spi驱动的头文件
*************************************************************************/
#ifndef __DRI_SPI_H
#define __DRI_SPI_H

/*
 *  CS引脚控制
 */
#define SPICSHIGH()  do {                                                           \
                            GPIO->P[2].DOUTSET = 1 << 5;                            \
                        } while(0)
#define SPICSLOW()   do {                                                           \
                            GPIO->P[2].DOUTCLR = 1 << 5;                            \
                        } while(0)

/*********************************************************************************************************
** 函数声明
*********************************************************************************************************/

/*********************************************************************************************************
** Function name:      Dri_SpiInit
** Descriptions:       SPI总线始化
** input parameters:   none
** output parameters:  none
** Returned value:     none
** Created by:         Bianrufeng
** Created Date:       2012-11-23
*********************************************************************************************************/
extern void Dri_SpiInit(void);

/*********************************************************************************************************
** Function name:      spiByteSend
** Descriptions:       通过SPI总线发送一个字节数据
** input parameters:   ucData：需要发送的单个字节数据
** output parameters:  none
** Returned value:     none
** Created by:         Bianrufeng
** Created Date:       2012-11-23
*********************************************************************************************************/
extern uint8 Dri_SpiWriteByte(uint8 ucData);

/*********************************************************************************************************
** Function name:      spiNByteSend
** Descriptions:       通过SPI总线发送多个字节数据
** input parameters:   pucData：指向需要发送的数据指针
**                     ulBytesToSend：需要发送的字节数
** output parameters:  none
** Returned value:     none
** Created by:         Bianrufeng
** Created Date:       2012-11-23
*********************************************************************************************************/
extern void spiNByteSend(const uint8 *pucData, uint32 ulBytesToSend);

/*********************************************************************************************************
** Function name:      spiByteGet
** Descriptions:       通过SPI总线接收一个字节数据
** input parameters:   none
** output parameters:  none
** Returned value:     接收到的单个字节数据
** Created by:         Bianrufeng
** Created Date:       2012-11-23
*********************************************************************************************************/
extern uint8 Dri_SpiReadByte(void);

/*********************************************************************************************************
** Function name:      spiNByteGet
** Descriptions:       通过SPI总线接收多个字节数据
** input parameters:   pucData：指向需要接收的数据指针
**                     ulBytesToGet：需要接收的字节数
** output parameters:  none
** Returned value:     none
** Created by:         Bianrufeng
** Created Date:       2012-11-23
*********************************************************************************************************/
extern void spiNByteGet(uint8 *pucData, uint32 ulBytesToGet);

#ifdef __cplusplus
}
#endif                                                                  /* __cplusplus                  */

#endif                                                                  /* __SPI_DRIVER_H               */

/*********************************************************************************************************
  END FILE
*********************************************************************************************************/
