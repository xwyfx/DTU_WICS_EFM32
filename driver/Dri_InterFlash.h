/************************************************************************
**                                 巍图科技 Copyright （c）
**                             农业灌溉系统
**  作    者：软件设计室
**  日    期：2014年03月24日
**  文件名称：Dri_InterFlash.h
**  摘    要：
*************************************************************************/
#ifndef _DRI_INTERNALFLASH_H
#define _DRI_INTERNALFLASH_H

void Dri_InterFlashInit(void);
void Dri_InterFlashReadPage(uint32 addr, uint8 *buff);
void Dri_InterFlashRead(uint32 addr, uint8 *buff, uint32 len);
bool Dri_InterFlashWritePage(uint32 addr, uint8 *buff);
bool Dri_InterFlashWrite(uint32 addr, uint8 *buff, uint32 len);
bool Dri_InterFlashErasePage(uint32 page);

#endif

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0          2014年03月24日   巍图      创建文件
*******************************************************************************/

