/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年01月12日
**  名   称：Dri_BSP.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_cmu.h"
#include "em_chip.h"

#define ENABLE_HFXO

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ClockInit(void)
{
    CMU_ClockEnable(cmuClock_GPIO, true);

    /*
     *  开启LE时钟，只要使用到低频外设，都需要开启该时钟
     */
    CMU_ClockEnable(cmuClock_CORELE, true);

    /*选择内核时钟源*/
#ifdef ENABLE_HFXO
    CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);                            //外部晶振
#else
    CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFRCO);                           //内部RCO
    CMU_HFRCOBandSet(cmuHFRCOBand_28MHz);
#endif

    /*设置低功耗设备的时钟源*/
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);                           //选择LFXO作为LFA的振荡
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);                           //选择LFXO作为LFB的振荡

    SCB->VTOR = 0x1800;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_BSPInit(void)
{
    CHIP_Init();                                                                //芯片初始化

    Fun_ClockInit();                                                            //系统时钟初始化

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年01月12日    巍图      创建文件
*******************************************************************************/
