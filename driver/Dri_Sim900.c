/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年10月26日
**  名   称：Dri_Gprs.c
**  摘   要：
*******************************************************************************/
#include "system.h"
#include "Dri_Timer.h"
#include "Dri_Led.h"

#include "em_gpio.h"

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Gprs_StateIndicateTmrHandler(void)
{
    Dri_LedToggle(GPRS_STATE_LED);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_SetGprsStateIndicateLed(uint8 state)
{
    switch (state)
    {
        case SERVER_ON_LINE:
            {
                Dri_SetSTimer(GPRS_STATE_CHANNEL, 1000, false);

                break;
            }
        case SERVER_IDLE:
            {
                Dri_SetSTimer(GPRS_STATE_CHANNEL, 200, false);

                break;
            }
        case SERVER_OFF_LINE:
            {
                Dri_CloseSTimer(GPRS_STATE_CHANNEL);
                Dri_LedOn(GPRS_STATE_LED);

                break;
            }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_InitGprs(void)
{
    OS_ERR err;

    if (Gprs.reset == true)
    {
        //电源控制管脚
        GPIO_PinModeSet(gpioPortF, 5, gpioModePushPull, 0);
        OSTimeDly(30 * SYS_SECOND_TICK_NUM, OS_OPT_TIME_DLY, &err);

        GPIO_PinOutSet(gpioPortF, 5);
        OSTimeDly(2 * SYS_SECOND_TICK_NUM, OS_OPT_TIME_DLY, &err);

        //模拟开机键
        GPIO_PinModeSet(gpioPortC, 9, gpioModePushPull, 0);

        GPIO_PinOutClear(gpioPortC, 9);
        OSTimeDly(2 * SYS_SECOND_TICK_NUM, OS_OPT_TIME_DLY, &err);
        GPIO_PinOutSet(gpioPortC, 9);
        Lib_DelaySecond(2);
        OSTimeDly(2 * SYS_SECOND_TICK_NUM, OS_OPT_TIME_DLY, &err);
        GPIO_PinOutClear(gpioPortC, 9);
        Lib_DelaySecond(2);
        OSTimeDly(2 * SYS_SECOND_TICK_NUM, OS_OPT_TIME_DLY, &err);
    }

    Dri_RegisterSTimer(GPRS_STATE_CHANNEL, Gprs_StateIndicateTmrHandler);

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0      2016年10月26日     巍图      创建文件
*******************************************************************************/


