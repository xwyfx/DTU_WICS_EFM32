/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_Eep.h
**  摘   要：
*******************************************************************************/
#ifndef _DRI_EEP_H
#define _DRI_EEP_H

/*----------------返回值意义--------------*/
#define MODULE_PROCESS_COMPLETE                  0x00
#define MODULE_BUSY_IN_PROCESS                   0x01
#define MODULE_INPUT_ERROR_NO_EXECUTE            0x02
#define MODULE_REENTRANT_ERROR                   0x03
#define MODULE_ARBITRATE_NO_RESULT               0x04
#define MODULE_OTHER_ERROR                       0x05

void Dri_ReadEeprom(uint32 addr, uint8 *buff, uint16 len);
bool Dri_WriteEeprom(uint32 addr, uint8 *buff, uint16 len);
void Dri_EnableEepromPart(uint8 part, uint8 opt);
void Dri_EepromInit(void);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/
