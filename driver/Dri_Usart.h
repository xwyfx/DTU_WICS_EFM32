/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            IW(2008-2011)
**  作   者：巍图
**  日   期：2017年01月10日
**  名   称：Dri_Usart.h
**  摘   要：
*******************************************************************************/
#ifndef _DRI_USART_H
#define _DRI_USART_H

void Dri_Leuart0ReceivedDoneEvent(void);
void Dri_Leuart1ReceivedDoneEvent(void);
void Dri_Usart0ReceivedDoneEvent(void);
bool Dri_UsartSend(uint8 *buff, uint16 len, uint16 port);
void Dri_UsartInit(void);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年01月10日    巍图      创建文件
*******************************************************************************/

