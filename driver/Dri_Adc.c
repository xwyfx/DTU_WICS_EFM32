/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年09月21日
**  名   称：Dri_Adc.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_device.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_adc.h"
#include "em_lcd.h"
#include "em_chip.h"



extern uint8 GucCC2420PowerLevel;
int8 AD_Label = 0;
int32 AD_Sample;

static signed short temp_value[12] = { 201, 208, 461, 466, 459, 431, 419,  - 1,  - 30,  - 255,  - 209, 2 };


/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_AdcInit(void)
{
    CMU_ClockEnable(cmuClock_ADC0, true);                                       //使能ADC模块时钟              */

    ADC_Init_TypeDef tAdcInit = {
        .ovsRateSel = adcOvsRateSel2,                                           //ADC过采样配置
        .lpfMode    = adcLPFilterBypass,                                        //旁路输入滤波RC电路
        .warmUpMode = adcWarmupNormal,                                          //正常预热模式
        .timebase   = ADC_TimebaseCalc(0),                                      //基时间配置
        .prescale   = ADC_PrescaleCalc(7000000, 0),                             //ADC时钟分频配置
        .tailgate   = false                                                     //不使能Tailgate
    };

    ADC_InitSingle_TypeDef tSingleInit = {
        .prsSel     = adcPRSSELCh0,                                             //选择PRS通道0
        .acqTime    = adcAcqTime32,                                             //配置采集时间为16周期
        .reference  = adcRef1V25,                                               //使用VDD参考电压
        .resolution = adcRes12Bit,                                              //使用12位分辨率
        .input      = adcSingleInpTemp,                                         // 输入选择VDD / 3
        .diff       = false,                                                    //不采用差分采集模式
        .prsEnable  = false,                                                    //禁能PRS输入
        .leftAdjust = false,                                                    //使用右对准
        .rep        = false                                                     //不使用连续转换
    };
    ADC_Init(ADC0, &tAdcInit);

    ADC_InitSingle(ADC0, &tSingleInit);                                         //初始化ADC单次转换
    /*
    *  使能单次采集完成中断
    */
    ADC_IntDisable(ADC0, ADC_IF_SINGLE);
    ADC_Start(ADC0, adcStartSingle);
    for (uint16 i_dly = 0; i_dly < 3000; i_dly++) ;
    for (uint16 i_dly = 0; i_dly < 3000; i_dly++) ;
    ADC_DataSingleGet(ADC0);
    ADC_IntClear(ADC0, ADC_IF_SINGLE);

    ADC_IntEnable(ADC0, ADC_IF_SINGLE);
    NVIC_EnableIRQ(ADC0_IRQn);
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void ADC0_IRQHandler(void)
{
    ADC_IntClear(ADC0, ADC_IF_SINGLE);                                          //清ADC中断标志位
    AD_Label = 1;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
signed short AD_Temperature_Cal_Expand10_debug(void)
{
    static unsigned char counter = 0;

    return temp_value[(counter++) % 12];
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_UpdataADTemperature_Test(void)
{

}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
int8 AD_Temperature_Cal(void)
{
    volatile int32 temp_long_parameter = 0;
    volatile int32 temp_parameter_1V25;
    int32 temp_parameter_0;

    //ADC0_TEMP_0_READ_1V25
    temp_parameter_1V25 = *(volatile uint32_t *)(0x0FE081BEUL);
    temp_parameter_1V25 &= 0x0000FFFF;
    temp_parameter_1V25 >>= 4;
    //CAL_TEMP_0
    temp_parameter_0 = (uint8_t)(*(volatile uint32_t *)(0x0FE081B2UL));



    temp_long_parameter = ((temp_parameter_1V25 - (int32)AD_Sample) * ((int32)15625) + (int32)49152) / (int32)98304;

    temp_parameter_0 += temp_long_parameter;

    return temp_parameter_0;
}

/*******************************************************************************
*功能：
*参数：  无
*返回值：无
*说明：
*******************************************************************************/
int16 AD_Temperature_Cal_Expand10(void)
{
    volatile int32 temp_long_parameter = 0;
    volatile int32 temp_parameter_1V25;
    int32 temp_parameter_0;

    //ADC0_TEMP_0_READ_1V25
    temp_parameter_1V25 = *(volatile uint32_t *)(0x0FE081BEUL);
    temp_parameter_1V25 &= 0x0000FFFF;
    temp_parameter_1V25 >>= 4;
    //CAL_TEMP_0
    temp_parameter_0 = (uint8_t)(*(volatile uint32_t *)(0x0FE081B2UL));

    if (temp_parameter_1V25 >= (int32)AD_Sample)
    {
        temp_long_parameter = ((temp_parameter_1V25 - (int32)AD_Sample) * ((int32)15625) * 5 + (int32)24576) / (int32)49152;
    }
    else
    {
        temp_long_parameter = ((temp_parameter_1V25 - (int32)AD_Sample) * ((int32)15625) * 5 - (int32)24576) / (int32)49152;
    }
    temp_parameter_0 *= 10;
    temp_parameter_0 += temp_long_parameter;

    return temp_parameter_0;                                                    //单位是0.1摄氏度
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0      2016年09月21日     巍图      创建文件
*******************************************************************************/

