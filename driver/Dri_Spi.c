/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_Spi.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_cmu.h"
#include "em_usart.h"
#include "em_gpio.h"

#include "Dri_Spi.h"

extern uint8 radio_state;

/*********************************************************************************************************
** 宏定义
*********************************************************************************************************/
#define SPIPERIPH       USART2                                          //使用外设号配置
#define LOCATION       (1UL << 8)                                       //引脚映射配置

/*
 *  开启与SPI驱动相关时钟
 */
#define CLKINIT()    do {                                                       \
                            CMU_ClockEnable(cmuClock_HFPER, true);              \
                            CMU_ClockEnable(cmuClock_GPIO, true);               \
                            CMU_ClockEnable(cmuClock_USART2, true);             \
                        } while(0)
/*
 *  SPI驱动引脚配置
 */
#define PININIT()    do {                                                       \
                            GPIO_PinModeSet(gpioPortC, 2, gpioModePushPull, 1); \
                            GPIO_PinModeSet(gpioPortC, 3, gpioModeInput, 0);    \
                            GPIO_PinModeSet(gpioPortC, 4, gpioModePushPull, 1); \
                            GPIO_PinModeSet(gpioPortC, 5, gpioModePushPull, 1); \
                        } while(0)
/*
 *  完成其它杂项配置
 */
#define MISCINIT()   do {                                                       \
                        } while(0)

#define DUMMY_BYTE         0x00                                         //读数据时SPI发送的无效数据

/*
 *  选择是否在该驱动中开启CS引脚控制
 */
#define CSCONTROLON   false

/*******************************************************************************
*功能：SPI总线始化
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_SpiInit(void)
{
    CLKINIT();                                                                  //时钟初始化
    PININIT();                                                                  //引脚初始化
    MISCINIT();                                                                 //其它项初始化

    /*
     *  USART同步模式初始化结构体
     */
    const USART_InitSync_TypeDef tSpiInit =
    {
        .enable      = usartEnable,                                             //在完成初始化后使能
        /*
         *  设置参考时钟，若设为0则库函数会自动获取当前时钟配置作为参考时钟
         */
        .refFreq     = 0,
        .baudrate    = 7000000,                                                 //SPI通信速率配置为1 Mbits / s
        .databits    = usartDatabits8,                                          //8位数据位
        .master      = true,                                                    //配置SPI为主模式
        .msbf        = true,                                                    //配置通信数据传输为MSB在前
        .clockMode   = usartClockMode0,                                         //配置COPL = 0，CPHA = 0

#if defined(_EFM32_GIANT_FAMILY) || defined(_EFM32_TINY_FAMILY)
        .prsRxEnable = false,                                                   //关闭PRS作为RX输入
        .prsRxCh     = usartPrsRxCh0,                                           //此项配置PRS输入通道选择
        .autoTx      = false,                                                   //禁能自动发送
#endif
    };

    USART_InitSync(SPIPERIPH, &tSpiInit);                                       //调用SPI同步模式初始化函数

    /*
     *  使能SPI的MOSI、MISO、CLK
     */
    SPIPERIPH->ROUTE = USART_ROUTE_TXPEN | USART_ROUTE_RXPEN | USART_ROUTE_CLKPEN;
    /*
     *  清接收缓冲和接收移位寄存器，清发送缓冲和发送移位寄存器
     */
    SPIPERIPH->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;

    return;
}

/*******************************************************************************
*功能：通过SPI总线发送一个字节数据
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 Dri_SpiWriteByte(uint8 TxData)
{
    uint8  RxData = 0;
    /*
     *  清接收缓冲和接收移位寄存器，清发送缓冲和发送移位寄存器
     */
    SPIPERIPH->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;

#if CSCONTROLON
    SPICSLOW();
#endif

    while (!(SPIPERIPH->STATUS & USART_STATUS_TXBL) && (radio_state != 0));     //检查发送缓冲是否为空
    SPIPERIPH->TXDATA = (uint32)TxData;
    while (!(SPIPERIPH->STATUS & USART_STATUS_RXDATAV) && (radio_state != 0));  //检查是否接收到数据
    RxData = SPIPERIPH->RXDATA;

#if CSCONTROLON
    SPICSHIGH();
#endif

    if (radio_state == 0)
    {
#ifdef IW_TEST
        Test_Count.SpiErr++;
#endif
        RxData = 0;
    }

    return RxData;

}

/*******************************************************************************
*功能：通过SPI总线接收一个字节数据
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 Dri_SpiReadByte(void)
{
    uint32 RxData;
    /*
     *  清接收缓冲和接收移位寄存器，清发送缓冲和发送移位寄存器
     */
    SPIPERIPH->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;

#if CSCONTROLON
    SPICSLOW();
#endif

    while (!(SPIPERIPH->STATUS & USART_STATUS_TXBL) && (radio_state != 0));     // 检查发送缓冲是否为空
    SPIPERIPH->TXDATA = DUMMY_BYTE;
    while (!(SPIPERIPH->STATUS & USART_STATUS_RXDATAV) && (radio_state != 0));  // 检查是否接收到数据
    RxData = SPIPERIPH->RXDATA;

#if CSCONTROLON
    SPICSHIGH();
#endif

    if (radio_state == 0)
    {
#ifdef IW_TEST
        Test_Count.SpiErr++;
#endif
        RxData = 0;
    }

    return RxData;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/
