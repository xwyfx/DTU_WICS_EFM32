/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_Led.h
**  摘   要：
*******************************************************************************/
#ifndef _DRI_LED_H
#define _DRI_LED_H

void Dri_LedInit(void);                                                         //初始化LED
void Dri_LedToggle(uint8 num);                                                  //闪烁指定LED
void Dri_LedOn(uint8 num);                                                      //打开指定LED
void Dri_LedOff(uint8  num);                                                    //关闭指定LED

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/
