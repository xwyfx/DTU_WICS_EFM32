/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2017年02月27日
**  名   称：Dri_Eep.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "em_cmu.h"
#include "em_gpio.h"

#include "Dri_Delay.h"
#include "Dri_Eep.h"

/*---------------COMMAND---------------*/
#define EEP_SPI_WREN_CMD                                            0X06
#define EEP_SPI_WRDI_CMD                                            0X04
#define EEP_SPI_RDSR_CMD                                            0X05
#define EEP_SPI_WRSR_CMD                                            0X01
#define EEP_SPI_READ_CMD                                            0X03
#define EEP_SPI_WRITE_CMD                                           0X02
//自扩展命令
#define EEP_SPI_RESET_CMD                                           0X00
#define EEP_SPI_OPEN_MAM                                            0X81
#define EEP_SPI_LOCK_MAM                                            0X82

typedef struct
{
    unsigned firtInitLabel : 1;                                                 //0; 表示需要初始化内存锁
    unsigned ReentrantLabel : 1;                                                //0:表示可以进入；1:表示不可以重入
    unsigned counter_InputError : 8;
    unsigned counter_ReentrantError : 8;
    unsigned counter_ArbitrateError : 8;
    unsigned counter_OtherError : 8;
    unsigned counter_Busyfeedback : 8;
} EEP_state;

/*-----------------PORT----------------*/
#define EEP_SPI_SI_POS          11                                //以EEP芯片来看
#define EEP_SPI_SO_POS          10
#define EEP_SPI_CLK_POS         9
#define EEP_SPI_CS_POS          8
#define EEP_SPI_WP_POS          7
#define EEP_SPI_HOLD_POS        6

#define EEP_OUT_HIGH                                               GPIO_PinOutSet(gpioPortC, EEP_SPI_SI_POS)
#define EEP_OUT_LOW                                                GPIO_PinOutClear(gpioPortC, EEP_SPI_SI_POS)
#define EEP_IN_GET                                                 GPIO_PinInGet(gpioPortC, EEP_SPI_SO_POS)

#define EEP_CLK_HIGH                                               GPIO_PinOutSet(gpioPortC, EEP_SPI_CLK_POS)
#define EEP_CLK_LOW                                                GPIO_PinOutClear(gpioPortC, EEP_SPI_CLK_POS)
#define EEP_CS_HIGH                                                GPIO_PinOutSet(gpioPortC, EEP_SPI_CS_POS)
#define EEP_CS_LOW                                                 GPIO_PinOutClear(gpioPortC, EEP_SPI_CS_POS)

#define EEP_WP_EN                                                  GPIO_PinOutClear(gpioPortC, EEP_SPI_WP_POS)
#define EEP_WP_DIS                                                 GPIO_PinOutSet(gpioPortC, EEP_SPI_WP_POS)

#define EEP_HOLD_EN                                                GPIO_PinOutClear(gpioPortC, EEP_SPI_HOLD_POS)
#define EEP_HOLD_DIS                                               GPIO_PinOutSet(gpioPortC, EEP_SPI_HOLD_POS)

#define EEP_DELAY                                                  for(uint16 i_dly = 0; i_dly < 10; i_dly ++)           //2//3//根据5ms计算
#define EEP_DELAY_LITTLE                                           for(uint16 i_dly = 0; i_dly < 50; i_dly ++)          //debug

#define INT_EN 1
#define INT_DIS 0

#ifdef EEP_INT_CONSERVATIVE
    #define EEP_INT_CONSERVATIVE_EN                                   __enable_irq();
    #define EEP_INT_CONSERVATIVE_DIS                                  __disable_irq();
    #define EEP_INT_RADICAL_EN(ctl)                                  {}
    #define EEP_INT_RADICAL_DIS(ctl)                                 {}
#else
    #define EEP_INT_CONSERVATIVE_EN                                  {}
    #define EEP_INT_CONSERVATIVE_DIS                                 {}
    #define EEP_INT_RADICAL_EN(ctl)                                  if(ctl)CPU_CRITICAL_EXIT();
    #define EEP_INT_RADICAL_DIS(ctl)                                 CPU_SR_ALLOC(); if(ctl)CPU_CRITICAL_ENTER();
#endif

static EEP_state eepState = { 0, 0, 0, 0, 0, 0, 0 };

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
#pragma optimize=none
void EEP_DELAY_LONG(void)
{
    _delay_us(50 * 1000);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void spi_write_8bit(uint8 data, bool ctl)
{
    EEP_INT_RADICAL_DIS(ctl);

    for (uint8 i = 0; i < 8; i++)
    {
        EEP_CLK_LOW;
        EEP_DELAY;
        if ((data & 0x80) == 0x80)
        {
            EEP_OUT_HIGH;
        }
        else
        {
            EEP_OUT_LOW;
        }
        EEP_CLK_HIGH;
        EEP_DELAY;
        data = (data << 1);
    }

    EEP_INT_RADICAL_EN(ctl);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void spi_write_24bit(uint32 data, bool ctl)
{
    EEP_INT_RADICAL_DIS(ctl);

    for (uint8 i = 0; i < 24; i++)
    {
        EEP_CLK_LOW;
        EEP_DELAY;
        if ((data & 0x00800000) == 0x00800000)
        {
            EEP_OUT_HIGH;
        }
        else
        {
            EEP_OUT_LOW;
        }
        EEP_CLK_HIGH;
        EEP_DELAY;
        data = (data << 1);
    }

    EEP_INT_RADICAL_EN(ctl);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 spi_read_8bit(bool ctl)
{
    uint8 value;

    value = 0;
    
    EEP_INT_RADICAL_DIS(ctl);
    for (uint8 i = 0; i < 8; i++)
    {
        EEP_CLK_LOW;
        value = (value   << 1);
        EEP_DELAY;
        EEP_CLK_HIGH;
        if (EEP_IN_GET == 1)
        {
            value |= 0x01;
        }
        else
        {
            value &= ~0x01;
        }
        EEP_DELAY;
    }
    EEP_INT_RADICAL_EN(ctl);

    return  value;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
int8 judgeInputParameterLegal(uint8 cmd, uint32 addr, uint8 *buff, uint8 len)
{
    if ((cmd == EEP_SPI_READ_CMD) || (cmd == EEP_SPI_WRITE_CMD))
    {
        if ((buff == NULL) || (len == 0))
        {
            return false;
        }
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
int8 arbitrateProcess(uint8 cmd, uint8 *state)
{
    int8 result = true;

    if (cmd == EEP_SPI_RESET_CMD)
    {
        //EEP_INIT;
    }
    else if (cmd == EEP_SPI_RDSR_CMD)
    {
        if (*state & 0x01)
        {
            eepState.counter_Busyfeedback++;
            *state = MODULE_BUSY_IN_PROCESS;
        }
        else
        {
            *state = MODULE_PROCESS_COMPLETE;
        }
    }
    else if ((cmd == EEP_SPI_WRSR_CMD) || (cmd == EEP_SPI_WRITE_CMD))
    {
        *state = MODULE_BUSY_IN_PROCESS;
    }

    eepState.ReentrantLabel = 0;

    return result;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static uint8 Fun_EepCmdProcess(uint8 cmd, uint32 addr, uint8 *buff, uint8 len)
{
    uint8 state;
    uint8 i = 0;

    state = MODULE_PROCESS_COMPLETE;

    if (judgeInputParameterLegal(cmd, addr, buff, len) == false)
    {
        eepState.counter_InputError++;

        return MODULE_INPUT_ERROR_NO_EXECUTE;
    }

    if (eepState.ReentrantLabel == 1)                                           //防止函数被重入
    {
        eepState.counter_ReentrantError++;

        return MODULE_REENTRANT_ERROR;
    }

    eepState.ReentrantLabel = 1;

    EEP_INT_CONSERVATIVE_DIS;                                                   //关中断
    EEP_CLK_HIGH;
    EEP_DELAY;
    EEP_CS_LOW;
    EEP_DELAY;

    switch (cmd)
    {
        case EEP_SPI_WREN_CMD:
        {
            EEP_WP_DIS;
            EEP_HOLD_DIS;
            EEP_DELAY;
            spi_write_8bit(EEP_SPI_WREN_CMD, INT_EN);
            EEP_CS_HIGH;
            EEP_DELAY;

            break;
        }
        case EEP_SPI_WRDI_CMD:
        {
            spi_write_8bit(EEP_SPI_WRDI_CMD, INT_EN);
            EEP_CS_HIGH;
            EEP_DELAY;
            EEP_WP_EN;

            break;
        }
        case EEP_SPI_RDSR_CMD:
        {
            spi_write_8bit(EEP_SPI_RDSR_CMD, INT_EN);
            state = spi_read_8bit(INT_EN);
            EEP_CS_HIGH;
            EEP_DELAY;

            break;
        }
        case EEP_SPI_WRSR_CMD:
        {
            spi_write_8bit(EEP_SPI_WRSR_CMD, INT_EN);
            spi_write_8bit(*buff, INT_EN);
            EEP_CS_HIGH;
            EEP_DELAY;

            break;
        }
        case EEP_SPI_WRITE_CMD:
        {
            spi_write_8bit(EEP_SPI_WRITE_CMD, INT_EN);
            spi_write_24bit(addr, INT_EN);
            while (i < len)
            {
                spi_write_8bit(*(buff + i), INT_EN);
                i++;
            }

            EEP_CS_HIGH;
            EEP_DELAY;

            break;
        }
        case EEP_SPI_READ_CMD:
        {
            spi_write_8bit(EEP_SPI_READ_CMD, INT_EN);
            spi_write_24bit(addr, INT_EN);

            while (i < len)
            {
                *(buff + i) = spi_read_8bit(INT_EN);
                i++;
            }

            EEP_CS_HIGH;
            EEP_DELAY;

            break;
        }
        case EEP_SPI_RESET_CMD:
        {
            EEP_HOLD_EN;
            EEP_CLK_LOW;
            EEP_DELAY_LITTLE;
            EEP_CS_HIGH;
            EEP_DELAY_LITTLE;
            EEP_HOLD_DIS;

            break;
        }
        default:
        {
            eepState.counter_OtherError++;
            state = MODULE_OTHER_ERROR;
            break;
        }
    }

    EEP_INT_CONSERVATIVE_EN;                                                    //开中断

    if (arbitrateProcess(cmd, &state))
    {
        return state;
    }
    else
    {
        eepState.counter_ArbitrateError++;

        return MODULE_ARBITRATE_NO_RESULT;
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_EepromInit(void)
{
    char i, status;

    CMU_ClockEnable(cmuClock_GPIO, true);

    GPIO_PinModeSet(gpioPortC, EEP_SPI_SI_POS, gpioModePushPull, 1);            //D
    GPIO_PinModeSet(gpioPortC, EEP_SPI_SO_POS, gpioModeInput, 0);               //Q
    GPIO_PinModeSet(gpioPortC, EEP_SPI_CLK_POS, gpioModePushPull, 1);           //C
    GPIO_PinModeSet(gpioPortC, EEP_SPI_CS_POS, gpioModePushPull, 1);            // S低有效
    GPIO_PinModeSet(gpioPortC, EEP_SPI_WP_POS, gpioModePushPull, 1);            // W低有效
    GPIO_PinModeSet(gpioPortC, EEP_SPI_HOLD_POS, gpioModePushPull, 1);          // HOLD低有效

    EEP_CS_HIGH;
    EEP_DELAY;
    EEP_CS_LOW;
    eepState.ReentrantLabel = 0;


    /*禁止页面权限*/
    EEP_CLK_HIGH;
    EEP_CS_LOW;
    spi_write_8bit(EEP_SPI_RDSR_CMD, INT_EN);
    status = spi_read_8bit(INT_EN);
    EEP_CS_HIGH;

    if ((status & 0x84) != 0x84)                                                //写保护未禁止，高段保护未禁止
    {
        /*开放页面权限*/
        EEP_CLK_HIGH;
        EEP_CS_LOW;
        EEP_WP_DIS;
        EEP_HOLD_DIS;
        EEP_DELAY;
        spi_write_8bit(EEP_SPI_WREN_CMD, INT_EN);
        EEP_CS_HIGH;

        EEP_DELAY;

        EEP_CLK_HIGH;
        EEP_CS_LOW;
        spi_write_8bit(EEP_SPI_WRSR_CMD, INT_DIS);
        spi_write_8bit(0x84, INT_DIS);

        EEP_CS_HIGH;
    }

    for (i = 0; i < 4; i++)
    {
        EEP_DELAY_LONG();
    }

    return;
}

/*******************************************************************************
*功能：使能或禁止EEPROM保护段
*参数：part:0 - 2   opt:0，禁止  1，使能
*返回：无
*说明：
*******************************************************************************/
void Dri_EnableEepromPart(uint8 part, uint8 opt)
{
    uint8 temp8, i;

    if (opt == 1)
    {
        if (part == 0)
        {
            temp8 = 0x00;
        }

        if (Fun_EepCmdProcess(EEP_SPI_WREN_CMD, 0, 0, 0) != MODULE_PROCESS_COMPLETE)
        {
            return;
        }

        if (Fun_EepCmdProcess(EEP_SPI_WRSR_CMD, 0, &temp8, 0) != MODULE_PROCESS_COMPLETE)
        {
            return;
        }

        for (i = 0; i < 4; i++)
        {
            EEP_DELAY_LONG();
        }
    }

    if (opt == 0)
    {
        if (part == 0)
        {
            temp8 = 0x84;
        }

        if (Fun_EepCmdProcess(EEP_SPI_WREN_CMD, 0, 0, 0) != MODULE_PROCESS_COMPLETE)
        {
            return;
        }

        if (Fun_EepCmdProcess(EEP_SPI_WRSR_CMD, 0, &temp8, 0) != MODULE_PROCESS_COMPLETE)
        {
            return;
        }

        for (i = 0; i < 4; i++)
        {
            EEP_DELAY_LONG();
        }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Dri_ReadEeprom(uint32 addr, uint8 *buff, uint16 len)
{
    EEP_DELAY_LONG();

    Fun_EepCmdProcess(EEP_SPI_READ_CMD, addr, buff, len);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Dri_WriteEeprom(uint32 addr, uint8 *buff, uint16 len)
{
    EEP_DELAY_LONG();

    if ((Fun_EepCmdProcess(EEP_SPI_RDSR_CMD, 0, 0, 0) == MODULE_PROCESS_COMPLETE))
    {
//      Dri_EnableEepromPart(0, 1);

        if (Fun_EepCmdProcess(EEP_SPI_WREN_CMD, 0, 0, 0) != MODULE_PROCESS_COMPLETE)                           //写使能
        {
            return false;
        }

        Fun_EepCmdProcess(EEP_SPI_WRITE_CMD, addr, buff, len);

//      Dri_EnableEepromPart(0, 0);
    }
    else
    {
        return false;
    }

    return true;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年02月27日    巍图      创建文件
*******************************************************************************/
