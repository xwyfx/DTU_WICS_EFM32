/*******************************************************************************
**                 中科院沈阳自动化研究所 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作    者：巍图
**  日    期：2013年09月26日
**  文件名称：Dri_Timer.h
**  摘    要：
*******************************************************************************/
#ifndef _DRI_TIMER_H
#define _DRI_TIMER_H

/*----------STimer软件定时器部分------*/
#define STIMER_NUM                   4

typedef struct
{
    unsigned short count;
    unsigned short top;
    unsigned enable :1;
    unsigned fired  :1;
    unsigned shot   :1;
    void (*handler)(void);
}_STimerAttribute;

enum
{
    USART0_RX_CHANNEL  = 0,
    LEUART0_RX_CHANNEL = 1,
    LEUART1_RX_CHANNEL = 2,
    GPRS_STATE_CHANNEL = 3,
};

void Dri_RegisterSTimer(uint8 channel, void (*handler)(void));
void Dri_SetSTimer(uint8 channel, uint16 top, bool shot);
void Dri_CloseSTimer(uint8 channel);

/*----------LeTimer部分-------*/
enum
{
    LETIMER_CHANNEL_0 = 1,
    LETIMER_CHANNEL_1 = 2,
};

void Dri_LetimerInit(void);
bool Dri_StartLeTimerChannel(uint8 channel, uint16 time);
bool Dri_StopLeTimerChannel(uint8 channel);

/*----------Timer3部分----------*/
enum
{
    TIMER3_CHANNEL_0 = 5,
    TIMER3_CHANNEL_1 = 6,
};

uint16 Dri_GetTimeReg(void);
void Dri_StartTimer(uint8 channel, uint16 time);
void Dri_DisableTimer(uint8 channel);

void Dri_TimerFiredHandler(uint8 channel);
void Dri_TimerInit(void);
uint32 Dri_GetSysTick(void);
uint32 Dri_GetTickReg(void);

#endif

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0          2013年09月26日   巍图      创建文件
*******************************************************************************/

