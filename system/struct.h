/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月10日
**  名   称：struct.h
**  摘   要：
*******************************************************************************/
#ifndef _STRUCT_H
#define _STRUCT_H

/*gprs的连接配置信息*/
typedef struct
{
    uint8 ConnectMode;                                                          //连接模式：域名连接、ip连接
    char  port[6];                                                              //设定网络端口
    int8  domain[30];                                                           //服务器域名
    char  ip[20];                                                               //服务器IP

    bool  connected;                                                            //指示与服务器是否建立连接

    OS_TMR ConnectServerTmrId;                                                  //连接服务器定时器
    OS_TMR MaintainTmrId;                                                       //连接的维护定时器
    uint16 MaintainTmrCounter;

    uint8 TryConnectedNum;                                                      //尝试连接的次数
    uint8 status;                                                               //状态：是否连接成功
    uint16 RxCounter;
}_SERVER;

typedef struct
{
    char IMEI[15];
    uint16 CSQ;

    bool  CIPMux;                                                               //是否进行多路连接
    uint8 ServerNum;
    _SERVER server[MAX_SERVER_NUM];

    bool InsertedCard;

    //test用
    bool reset;
}_Gprs;

typedef struct
{
    bool   IsUsed;
    uint8  port;                                                                //缓存的来源
    uint8  protocol;
    uint16 len;                                                                 //缓存的长度
    uint8  buff[MAX_BUFF_LEN + 20];                                                  //缓存
}_SysMsg;

/*--------------接口命令格式-------------*/
/*报文头部格式*/
#pragma pack(1)
typedef struct
{
    uint8 version;
    uint8 cmd;
    uint16 len;
}_MsgHead;
#pragma pack()

/*接口命令总体格式*/
#pragma pack(1)
typedef struct
{
    _MsgHead head;
    uint8 buff[MAX_BUFF_LEN];
}_IF;
#pragma pack()

/*转发至关联DTU格式*/
#pragma pack(1)
typedef struct
{
    char   IMEI[15];
    uint8  seq;
    uint16 ValveState;
}_ForwardToBindDTU;
#pragma pack()

/*异常上报格式*/
#pragma pack(1)
typedef struct
{
    uint8 ExceptionNum;
    uint8 buff[MAX_BUFF_LEN];
}_ExceptionReport;
#pragma pack()

/*异常上报响应格式*/
#pragma pack(1)
typedef struct
{
    uint8 ExceptionNum;
}_ExceptionReportResponse;
#pragma pack()

/*配置报文格式*/
#pragma pack(1)
typedef struct
{
    uint8 len;
    uint8 cmd;
    uint8 data[15];
}_CfgCmd;
#pragma pack()

/*设置调试信息输出命令格式*/
typedef struct
{
    uint16 level;
}_DebugInfoOut;


/*配置报文格式*/
#pragma pack(1)
typedef struct
{
    char  ServerIP[20];                                                         //服务器ip
    char  ServerDomain[30];                                                     //服务器域名
    char  ServerPort[6];                                                        //服务器端口
    char  UpdateServerIP[20];                                                   //服务器ip
    char  UpdateServerDomain[30];                                               //服务器域名
    char  UpdateServerPort[6];                                                  //服务器端口

    uint16 ConnectedPump;                                                        //是否下接水泵
    uint16 ConnectedMutualInductor;                                              //是否判断互感器
    uint16 BindValveNum;                                                         //关联的电磁阀数量
    uint16 MainDeviceType;                                                       //主设备类型
    uint8 BindDtuNum;                                                           //关联的dtu数量
    char  BindDtuList[MAX_BIND_DTU_NUM][15];                                    //关联dtu

    uint16 PumpExcetionLimit;                                                    //水泵异常监测次数
    uint16 CircuitProtectExceptionLimit;                                         //电路保护异常监测次数
    uint16 ValveExceptionLimit;                                                  //阀门异常监测次数
    uint16 FlowMeterExceptionLimit;                                              //流量计异常监测次数
}_CfgMsg;
#pragma pack()


/*配置信息*/
typedef struct
{
    uint16  MainDeviceType;                                                      //主设备类型
    uint8  BindDtuNum;                                                          //关联的dtu数量
    uint16  BindValveNum;                                                        //关联的电磁阀数量
    char   BindDtuList[MAX_BIND_DTU_NUM][15];                                   //关联的DTU
    uint32 CollecitonPeriod;                                                    //采集周期    uint16 version;//版本号，暂时无用

    uint8  port_mode;                                                           //标记串口是使用485模式还是232模式

    _SERVER GprsConnectCfg[MAX_SERVER_NUM];                                     //Gprs的连接配置
    uint8  DefaultConnectIndex;                                                 //默认采用哪个域名、ip对进行连接，取值：0、1

    uint16  ConnectedPump;                                                        //是否下接水泵
    uint16  ConnectedMutualInductor;                                              //是否判断互感器

    uint16 PumpExcetionLimit;                                                    //水泵异常监测次数
    uint16 CircuitProtectExceptionLimit;                                         //电路保护异常监测次数
    uint16 ValveExceptionLimit;                                                  //阀门异常监测次数
    uint16 FlowMeterExceptionLimit;                                              //流量计异常监测次数

    uint8 ConnectServer;
    bool update;
    uint16 crc;
}_Cfg;

/*定义时间格式*/
typedef struct _Time
{
    uint16 year;                                                                //年
    uint8  month;                                                               //月
    uint8  day;                                                                 //日
    uint8  hour;                                                                //小时
    uint8  minute;                                                              //分钟
    uint8  second;                                                              //秒
}_Time;

/*------------------Modbus报文格式-------------*/

/*以太网格式头部*/
#pragma pack(1)
typedef struct
{
    uint16 SequenceNum;                                                         //序列号
    uint16 ProtocolType;                                                        //协议类型
    uint16 len;                                                                 //长度
    uint8  addr;                                                                //地址
}_MBAP;
#pragma pack()

/*485格式头部*/
#pragma pack(1)
typedef struct
{
    uint8  cmd;
    uint8  data[MAX_MODBUS_BUFF_LEN];
}_PDU;
#pragma pack()

/*Modbus命令体，兼容485和以太网模式*/
#pragma pack(1)
typedef union
{
    struct
    {
        uint8 addr;
        _PDU pdu;
    }UsartType;

    struct
    {
        _MBAP MBAP;
        _PDU pdu;
    }TcpType;
}_Modbus;
#pragma pack()

/*485发送缓存格式*/
#pragma pack(1)
typedef struct
{
    _Modbus buff;
    bool   IsUsed;
    uint16 len;
    uint8  type;
}_TxBuff;
#pragma pack()

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年05月10日   ggggggg      创建文件
*******************************************************************************/
