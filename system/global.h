/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月11日
**  名   称：global.h
**  摘   要：
*******************************************************************************/
#ifndef _GLOBAL_H
#define _GLOBAL_H

extern _Cfg  cfg;
extern _Gprs Gprs;
extern bool SendAtCmd;
extern uint16 SysFeedWatchdogCounter;
extern uint16 UpdateVersion;
extern bool SysParameterException;
extern uint16 SysDebugInfoOut;

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0          2016年05月11日   ggggggg      创建文件
*******************************************************************************/

