/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月10日
**  名   称：config.h
**  摘   要：
*******************************************************************************/
#ifndef _CONFIG_H
#define _CONFIG_H

#define CMD_VERSION                               1
#define ENABLE_PROTOCOL_MODBUS
//#define ENABLE_PROTOCOL_WT
#define SYS_SECOND_TICK_NUM                       10                           //每秒钟有几个系统tick
#define SYS_TICK_SCALE                            0xCCC                        //系统tick周期100ms

#define MAX_BUFF_LEN                              220                          //系统缓存最大长度
#define MAX_MODBUS_BUFF_LEN                       220

#define MAX_BIND_DTU_NUM                          1

#define DEFAULT_RESET_NUM                         0
#define DEFAULT_CONNECT_SERVER_INDEX              0

/*-------------测试信息输出调试定义------*/
enum
{
    DEBUG_RS485_RX  = 1,                                                        //RS485接收信息输出
    DEBUG_RS485_TX  = 2,                                                        //RS485发送信息输出
    DEBUG_GPRS_RX   = 4,                                                        //GPRS接收信息输出
    DEBUG_GPRS_TX   = 8,                                                        //GPRS发送信息输出
    DEBUG_TEST_INFO = 16,                                                       //调试信息输出
};

/*常规状态：报文发送结果*/
enum
{
    SEND_MSG_SUCCESS = 1,                                                       //发送成功
    SEND_MSG_FAIL    = 2,                                                       //发送失败
    RECEIVE_ERR      = 3,                                                       //接收错误
};

/*-------------设备类型定义------------*/
enum
{
    MAIN_DEVICE_PLC = 0,                                                        //主设备类型：PLC
    MAIN_DEVICE_MIC = 1,                                                        //主设备类型：MIC
};

/*-------------任务优先级定义------------*/
enum
{
    TASK_LEUART0_RX_PRIORITY      = 4,
    TASK_LEUART1_RX_PRIORITY      = 4,
    TASK_LEUART1_TX_PRIORITY      = 4,
    TASK_USART0_RX_PRIORITY       = 3,
    TASK_GPRS_SEND_PRIORITY       = 5,
    TASK_GPRS_PRIORITY            = 5,
};

/*-------------系统异常码定义------------*/
enum
{
    EXCEPTION_485_OVER_TIME      = 1,                                           //0x01  485通信超时异常；严重异常
    EXCEPTION_PUMP_WORK          = 2,                                           //0x02  水泵工作异常；严重异常
    EXCEPTION_FORWARD            = 3,                                           //0x03  转发异常；普通异常
    EXCEPTION_PARAMETER          = 4,                                           //0x04  参数异常；严重异常
    EXCEPTION_CIRCUIT_PROTECT    = 5,                                           //0x05  电路保护异常；严重异常
    EXCEPTION_NET_COMMUTE        = 6,                                           //0x06  网络通信异常；普通异常
    EXCEPTION_WIRELESS_COMMUTE   = 7,                                           //0x07  无线通信异常；普通异常
    EXCEPTION_COORDINATOR        = 8,                                           //0x08  协调器异常；普通异常
    EXCEPTION_VALVE              = 9,                                           //0x09  阀门异常；普通异常
    EXCEPTION_FLOWMETER          = 10,                                          //0x0A  流量计异常；普通异常
    EXCEPTION_CMD_LOCK           = 11,                                          //0x0B  指令锁定异常；普通异常
    EXCEPTION_LOGIC_LOCK         = 12,                                          //0x0C  逻辑锁定异常；普通异常
};

/*-------------异常处理方式--------------*/
enum
{
    EXCEPTION_MONITOR    = 1,                                                   //监测异常
    EXCEPTION_NO_MONITOR = 2,                                                   //不监测异常
};

/*-------------接口命令定义--------------*/
enum
{
    CMD_TRANSPARENT_TO_MAIN_DEVICE_1           = 1,                             //0x01 透传MODBUS数据给主设备
    CMD_HEART_BEACON                           = 2,                             //0x02 心跳包
    CMD_HEART_BEACON_RESPONSE                  = 2,                             //0x02 心跳包响应
    CMD_DEBUG_MSG                              = 4,                             //0x04 调试报文
    CMD_REGISTER_TO_SERVER                     = 11,                            //0x0B 向服务器注册
    CMD_REGISTER_TO_SERVER_RESPONSE            = 12,                            //0x0C 向服务器注册响应
    CMD_START_UPDATE                           = 16,                            //0x10

    CMD_SET_WORK_TASK                          = 23,                            //0x17 服务器下发灌溉任务
    CMD_SET_WORK_TASK_REPORT                   = 24,                            //0x18 灌溉任务报告
    CMD_SET_WORK_TASK_RESPONSE                 = 25,                            //0x19 对服务器下发的灌溉任务给予的响应
    CMD_RESET_EXCEPTION                        = 26,                            //0x1A 复位异常
    CMD_RESET_EXCEPTION_RESPONSE               = 27,                            //0x1B 复位异常指令响应
    CMD_WRITE_CONFIG                           = 28,                            //0x1C 配置指令
    CMD_WRITE_CONFIG_RESPONSE                  = 29,                            //0x1D 配置指令响应
    CMD_FORWARD_TO_BIND_DTU                    = 30,                            //0x1E 转发至关联DTU
    CMD_RECEIVED_SERVER_FORWARD_REQUEST        = 31,                            //0x1F PLC接收到服务器的转发请求
    CMD_RECEIVED_SERVER_FORWARD_REPONSE        = 32,                            //0x20
    CMD_FORWARD_TO_BIND_DTU_RESPOSE            = 43,                            //0x2B 转发至关联DTU的服务器响应
    CMD_REPORT_EXCEPTION                       = 34,                            //0x22 上报异常
    CMD_REPORT_EXCEPTION_RESPOSE               = 35,                            //0x23 上报异常响应
    CMD_READ_CONFIG                            = 36,                            //0x24 读取配置指令
    CMD_READ_CONFIG_RESPONSE                   = 37,                            //0x25 读取配置指令响应
    CMD_PRIVILEGE_CMD                          = 38,                            //0x26 特权指令
    CMD_PRIVILEGE_CMD_RESPOSE                  = 39,                            //0x27 特权指令响应
    CMD_CANCEL_PRIVILEGE_CMD                   = 40,                            //0x28 取消特权指令
    CMD_CANCEL_PRIVILEGE_CMD_RESPOSE           = 41,                            //0x29 取消特权指令响应

    CMD_RESUME_DEFAULT                         = 70,                            //0x46 恢复默认配置
    CMD_RESUME_DEFAULT_RESPOSE                 = 71,                            //0x47 恢复默认配置响应
    CMD_RESET                                  = 72,                            //0x48 重启
    CMD_RESET_RESPOSE                          = 73,                            //0x49 重启响应
    CMD_TRANSPARENT_NEGATIVE_RESPONSE          = 74,                            //0x4A 透明传输负响应
    CMD_SET_DEBUG_INFO                         = 75,                            //0x4B，使能调试信息输出
    CMD_SET_DEBUG_INFO_RESPONSE                = 76,                            //0x4C，使能调试信息输出响应

    CMD_TRANSPARENT_TO_MAIN_DEVICE_2           = 77,                            //0x4D 透传MODBUS数据给主设备
};

/*-------------Modbus模式定义--------------*/
/*命令模式*/
enum
{
    MODBUS_TCP = 1,
    MODBUS_RTU = 2,
};

/*Modbus Master模式下，发送报文的类型*/
enum
{
    /*为了兼容服务器，弄出两个透明传输*/
    SEND_TRANSPARENT_MSG_1    = 1,                                              //发送透明传输报文
    SEND_TRANSPARENT_MSG_2    = 7,                                              //发送透明传输报文

    SEND_POLL_MSG             = 2,                                              //发送轮询报文
    SEND_TASK_MSG             = 3,                                              //发送工作任务报文
    SEND_CLEAR_TASK_STATE_MSG = 4,
    SEND_PRIVILEGE_CMD        = 5,
    SEND_CTL_PUMP_CMD         = 6,                                              //发送控制水泵指令
};

/*-------------Gprs相关定义------------*/
#define MAX_GPRS_SEND_BUFF_NUM                    12                            //发送任务：发送缓存的个数
#define MAX_GPRS_TX_BUFF_LEN                      MAX_BUFF_LEN
#define MAX_GPRS_RX_BUFF_LEN                      540
#define MAX_SERVER_NUM                            2                             //Gprs最多连接服务器的数量

#define DEFAULT_DOMAIN                            "www.wituagri.com.cn"
#define DEFAULT_SERVER_IP                         "47.93.20.190"
#define DEFAULT_SERVER_PORT                       "8010"
#define DEFAULT_CONNECT_MODE                      GPRS_DOMAIN_MODE_CONNECT      //默认使用域名进行连接

#define DEFAULT_UPDATE_SERVER_IP                  "47.93.20.190"
#define DEFAULT_UPDATE_SERVER_DOMAIN              "www.wituagri.com.cn"
#define DEFAULT_UPDATE_SERVER_PORT                "9074"

/*Gprs在线状态*/
enum
{
    GPRS_IDLE_STATE      = 1,
    GPRS_POWER_UP_STATE  = 2,
    GPRS_INIT_STATE      = 3,                                                   //初始化
    GPRS_NORMAL_STATE    = 4,                                                   //正常
};

enum
{
    GPRS_IDLE                = 1,
    GPRS_POWER_UP            = 3,
    GPRS_SEND_TEST_AT        = 2,                                               //发送测试AT指令
    GPRS_GET_CSQ             = 4,                                               //获取信号强度
    GPRS_CLOSE_ECHO          = 5,                                               //关闭回显
    GPRS_SET_IPR             = 6,                                               //设置串口波特率
    GPRS_SET_IFC             = 7,                                               //设置串口硬件流
    GPRS_CHECK_INSERT_CARD   = 8,                                               //检查卡的插入状态
    GPRS_GET_IMEI            = 10,                                              //获取出厂序列号
    GPRS_SET_CIP_MUX         = 12,                                              //多连接
    GPRS_EXECUTE_CSTT        = 13,
    GPRS_EXECUTE_CIICR       = 14,
    GPRS_EXECUTE_CIFSR       = 15,
    GPRS_CMD_CIPSTATUS       = 16,
    GPRS_CMD_CIPSHUT         = 17,
    GPRS_CMD_CIPCLOSE        = 18,
    GPRS_CMD_CIPMODE         = 19,
    GPRS_CMD_ENTER_CMD_MODE  = 20,
};

/*发送缓存状态*/
enum
{
    BUFF_IDLE                = 1,
    BUFF_SEND_AT_CMD         = 2,
    BUFF_SEND_HEAD_MSG       = 3,
    BUFF_WAIT_READY_SEND     = 4,                                               //等待准备发送标志
    BUFF_SEND_BODY_MSG       = 5,
    BUFF_WAIT_SEND_OK        = 6,                                               //等待发送完成
    BUFF_WAIT_AT_CMD_OK      = 7,
    BUFF_WAIT_SERVER_RESPONSE = 8,
};

/*服务器的连接状态*/
enum
{
    SERVER_IDLE               = 1,
    SERVER_CONNECT_MODE_1     = 2,
    SERVER_CONNECT_MODE_2     = 3,
    SERVER_ON_LINE            = 4,
    SERVER_OFF_LINE           = 5,
};

/*Gprs的连接模式*/
enum
{
    GPRS_IP_MODE_CONNECT = 1,                                                   //使用IP模式进行连接
    GPRS_DOMAIN_MODE_CONNECT = 2,                                               //使用域名模式进行连接
};


/*-------------系统协议类型定义------------*/
enum
{
    PROTOCOL_MODBUS = 1,
    PROTOCOL_WT     = 2,
    PROTOCOL_NONE   = 0xFF,
};

#define  OS_ENTER_CRITICAL()          CPU_SR_ALLOC();CPU_CRITICAL_ENTER();
#define  OS_EXIT_CRITICAL()           CPU_CRITICAL_EXIT();


/*系统错误码*/
enum
{
    ERR_NONE                      = 0,                                          //无错误
    ERR_DEV_CFG_OVERLONG          = 1,                                          //设备配置过长
    ERR_REG_COUNT_OVERMUCH        = 2,                                          //寄存器数量过多
    ERR_UNABLE_GET_DEV_CFG        = 3,
    ERR_DEVICE_CONTROL_CFG        = 4,                                          //设备配置错误
    ERR_COMMUTE_OVER_TIME         = 5,                                          //读取超时
};

/*串口通道逻辑定义*/
enum
{
    LEUART0_CHANNEL      = 1,                                                   //用于leuart数据收发
    LEUART1_CHANNEL      = 2,                                                   //RTU与节点通信相关DMA通道
    USUART0_CHANNEL      = 3,                                                   //选带模块与节点通信相关DMA通道
};

/*串口通道命名*/
#define USART_232_CHANNEL     LEUART0_CHANNEL
#define USART_485_CHANNEL     LEUART1_CHANNEL
#define USART_GPRS_CHANNEL    USUART0_CHANNEL


/*-------------led定义------------*/
#define LED_OFF               0x00                                              //LED熄灭
#define LED_ON                0x01                                              //LED点亮
#define LED_TOGGLE            0x02                                              //LED闪烁

#define LED_NUM               4

enum
{
    LED1 = 1,
    LED2 = 2,
    LED3 = 3,
    LED4 = 4,
};

#define SYS_HEART_LED         LED3
#define GPRS_STATE_LED        LED4

#endif

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年05月10日   巍图      创建文件
*******************************************************************************/

