/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月10日
**  名   称：system.h
**  摘   要：
*******************************************************************************/
#ifndef _SYSTEM_H
#define _SYSTEM_H

#include <stdbool.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include "em_device.h"

#include "os.h"


#include "data_type.h"
#include "config.h"
#include "struct.h"

#include "lib.h"
#include "global.h"
//#include "debug.h"

#include "core_cmFunc.h"

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年05月10日   巍图      创建文件
*******************************************************************************/

