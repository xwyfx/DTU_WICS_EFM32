/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年10月26日
**  名   称：IF.c
**  摘   要：
*******************************************************************************/
#include "system.h"
#include "PHY_Flash.h"
#include "PHY_GprsSendTask.h"
#include "PHY_Usart.h"
#include "PHY_Reset.h"

#include "IF.h"
#include "CommProtocol.h"
#include "App_MIC.h"
#include "App_PLC.h"
#include "update.h"

#include "debug.h"

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_WriteCfg(_IF *msg, uint16 len, uint8 port)
{
    bool result;
    _CfgMsg CfgMsg;
    _CfgCmd *SigleCmd;
    uint16 CfgMsgPtr;
    uint8 CmdSeq;
    uint8 BindDtuNum;
    _SysMsg ResponseMsg;

    _IF *response;

    result = false;

    response = (_IF *)ResponseMsg.buff;

    memset(&CfgMsg, 0x00, sizeof(CfgMsg));

    CfgMsgPtr = 0;
    SigleCmd = (_CfgCmd *)&msg->buff[CfgMsgPtr];
    CmdSeq = 0;

    msg->head.len = Lib_htons(msg->head.len);

    while ((CfgMsgPtr + SigleCmd->len) <= msg->head.len)
    {
        if (CfgMsgPtr == msg->head.len)
        {
            break;
        }

        if (CmdSeq != SigleCmd->cmd)                                            //命令序号不连续
        {
            goto SendReponse;
        }

        switch (CmdSeq)
        {
            case 0:                                                             //服务器ip
                {
                    if (SigleCmd->len <= 0)
                    {
                        goto SendReponse;
                    }

                    memcpy(CfgMsg.ServerIP, SigleCmd->data, SigleCmd->len);

                    break;
                }
            case 1:                                                             //服务器域名
                {
                    if (SigleCmd->len <= 2)
                    {
                        goto SendReponse;
                    }

                    memcpy(CfgMsg.ServerDomain, SigleCmd->data, SigleCmd->len);

                    break;
                }
            case 2:                                                             //服务器端口
                {
                    if (SigleCmd->len <= 2)
                    {
                        goto SendReponse;
                    }

                    memcpy(CfgMsg.ServerPort, SigleCmd->data, SigleCmd->len);

                    break;
                }
            case 3:                                                             //升级服务器ip
                {
                    if (SigleCmd->len <= 2)
                    {
                        goto SendReponse;
                    }

                    memcpy(CfgMsg.UpdateServerIP, SigleCmd->data, SigleCmd->len);

                    break;
                }
            case 4:                                                             //升级服务器域名
                {
                    if (SigleCmd->len <= 2)
                    {
                        goto SendReponse;
                    }

                    memcpy(CfgMsg.UpdateServerDomain, SigleCmd->data, SigleCmd->len);

                    break;
                }
            case 5:                                                             //升级服务器端口
                {
                    if (SigleCmd->len <= 2)
                    {
                        goto SendReponse;
                    }

                    memcpy(CfgMsg.UpdateServerPort, SigleCmd->data, SigleCmd->len);

                    break;
                }
            case 6:                                                             //是否关联水泵
                {
                    if (SigleCmd->len != 2)
                    {
                        goto SendReponse;
                    }

                    CfgMsg.ConnectedPump = *((uint16 *)(SigleCmd->data));
                    CfgMsg.ConnectedPump = Lib_htons(CfgMsg.ConnectedPump);

                    /*非法值检查*/
                    if (CfgMsg.ConnectedPump != 0 && CfgMsg.ConnectedPump != 1)
                    {
                        goto SendReponse;
                    }

                    break;
                }
            case 7:                                                             //是否关联电磁阀
                {
                    if (SigleCmd->len != 2)
                    {
                        goto SendReponse;
                    }

                    CfgMsg.ConnectedMutualInductor = *((uint16 *)(SigleCmd->data));
                    CfgMsg.ConnectedMutualInductor = Lib_htons(CfgMsg.ConnectedMutualInductor);

                    /*非法值检查*/
                    if (CfgMsg.ConnectedMutualInductor != 0 && CfgMsg.ConnectedMutualInductor != 1)
                    {
                        goto SendReponse;
                    }

                    break;
                }
            case 8:                                                             //关联的电磁阀数量
                {
                    CfgMsg.BindValveNum = *((uint16 *)(SigleCmd->data));
                    CfgMsg.BindValveNum = Lib_htons(CfgMsg.BindValveNum);

                    break;
                }
            case 9:                                                             //下接主设备类型
                {
                    if (SigleCmd->len != 2)
                    {
                        goto SendReponse;
                    }

                    CfgMsg.MainDeviceType = *((uint16 *)(SigleCmd->data));
                    CfgMsg.MainDeviceType = Lib_htons(CfgMsg.MainDeviceType);

                    /*非法值检查*/
                    if (CfgMsg.MainDeviceType != MAIN_DEVICE_PLC && CfgMsg.MainDeviceType != MAIN_DEVICE_MIC)
                    {
                        goto SendReponse;
                    }

                    break;
                }
            case 10:                                                            //关联的DTU列表
                {
                    if (SigleCmd->len <= 0)
                    {
                        goto SendReponse;
                    }

                    if ((SigleCmd->len  % 15) != 0)
                    {
                        goto SendReponse;
                    }

                    BindDtuNum = SigleCmd->len / 15;

                    if (BindDtuNum > MAX_BIND_DTU_NUM)
                    {
                        goto SendReponse;
                    }

                    CfgMsg.BindDtuNum = BindDtuNum;
                    memcpy(CfgMsg.BindDtuList, SigleCmd->data, SigleCmd->len);

                    break;
                }
            case 11:                                                            //水泵异常监测异常次数
                {
                    CfgMsg.PumpExcetionLimit = *((uint16 *)(SigleCmd->data));
                    CfgMsg.PumpExcetionLimit = Lib_htons(CfgMsg.PumpExcetionLimit);

                    break;
                }
            case 12:                                                            //电路保护监测异常次数
                {
                    CfgMsg.CircuitProtectExceptionLimit = *((uint16 *)(SigleCmd->data));
                    CfgMsg.CircuitProtectExceptionLimit = Lib_htons(CfgMsg.CircuitProtectExceptionLimit);

                    break;
                }
            case 13:                                                            //阀门异常监测次数
                {
                    CfgMsg.ValveExceptionLimit = *((uint16 *)(SigleCmd->data));
                    CfgMsg.ValveExceptionLimit = Lib_htons(CfgMsg.ValveExceptionLimit);

                    break;
                }
            case 14:                                                            //流量计异常监测次数
                {
                    CfgMsg.FlowMeterExceptionLimit = *((uint16 *)(SigleCmd->data));
                    CfgMsg.FlowMeterExceptionLimit = Lib_htons(CfgMsg.FlowMeterExceptionLimit);

                    break;
                }
            default:
                {
                    goto SendReponse;
                }
        }

        CmdSeq++;
        CfgMsgPtr = CfgMsgPtr + SigleCmd->len + 2;
        SigleCmd = (_CfgCmd *)&msg->buff[CfgMsgPtr];                            //指向下一个命令
    }

    /*报文头部长度检查*/
    if (CfgMsgPtr != msg->head.len)
    {
        goto SendReponse;
    }

    /*对配置报文进行检查参数的互斥性*/

    /*下接水泵与互感器的互斥性*/
    if (CfgMsg.ConnectedPump == false)
    {
        if (CfgMsg.ConnectedMutualInductor == true)
        {
            goto SendReponse;
        }
    }

    /*下接水泵与关联电磁阀的互斥性*/
    if (CfgMsg.ConnectedPump == false)
    {
        if (CfgMsg.BindValveNum > 0)
        {
            goto SendReponse;
        }
    }

    /*下接水泵与主设备类型的互斥性*/
    if (CfgMsg.ConnectedPump == true)
    {
        if (CfgMsg.MainDeviceType == MAIN_DEVICE_MIC)
        {
            goto SendReponse;
        }
    }

    Debug_Statics(offsetof(_Statics, WtCfg), 1, sizeof(statics.WtCfg));

    result = true;

    /*主设备类型*/
    cfg.MainDeviceType = CfgMsg.MainDeviceType;

    /*关联dtu*/
    cfg.BindDtuNum = CfgMsg.BindDtuNum;

    /*关联电磁阀数量*/
    cfg.BindValveNum = CfgMsg.BindValveNum;

    memset(cfg.BindDtuList, 0x00, sizeof(cfg.BindDtuList));
    memcpy(cfg.BindDtuList, CfgMsg.BindDtuList, cfg.BindDtuNum * sizeof(CfgMsg.BindDtuList[0][0]));

    /*异常值门限*/
    cfg.CircuitProtectExceptionLimit = CfgMsg.CircuitProtectExceptionLimit;
    cfg.FlowMeterExceptionLimit      = CfgMsg.FlowMeterExceptionLimit;
    cfg.PumpExcetionLimit            = CfgMsg.PumpExcetionLimit;
    cfg.ValveExceptionLimit          = CfgMsg.ValveExceptionLimit;

    /*服务器ip、域名、端口*/
    memset(cfg.GprsConnectCfg[0].ip, 0x00, sizeof(cfg.GprsConnectCfg[0].ip));
    memcpy(cfg.GprsConnectCfg[0].ip, CfgMsg.ServerIP, sizeof(CfgMsg.ServerIP));

    memset(cfg.GprsConnectCfg[0].domain, 0x00, sizeof(cfg.GprsConnectCfg[0].domain));
    memcpy(cfg.GprsConnectCfg[0].domain, CfgMsg.ServerDomain, sizeof(cfg.GprsConnectCfg[0].domain));

    memset(cfg.GprsConnectCfg[0].port, 0x00, sizeof(cfg.GprsConnectCfg[0].port));
    memcpy(cfg.GprsConnectCfg[0].port, CfgMsg.ServerPort, sizeof(CfgMsg.ServerPort));

    cfg.GprsConnectCfg[0].connected = true;
    cfg.GprsConnectCfg[0].ConnectMode = GPRS_DOMAIN_MODE_CONNECT;

    /*升级服务器ip、域名、端口*/
    memset(cfg.GprsConnectCfg[1].ip, 0x00, sizeof(cfg.GprsConnectCfg[1].ip));
    memcpy(cfg.GprsConnectCfg[1].ip, CfgMsg.UpdateServerIP, sizeof(CfgMsg.UpdateServerIP));

    memset(cfg.GprsConnectCfg[1].domain, 0x00, sizeof(cfg.GprsConnectCfg[1].domain));
    memcpy(cfg.GprsConnectCfg[1].domain, CfgMsg.UpdateServerDomain, sizeof(CfgMsg.UpdateServerDomain));

    memset(cfg.GprsConnectCfg[1].port, 0x00, sizeof(cfg.GprsConnectCfg[1].port));
    memcpy(cfg.GprsConnectCfg[1].port, CfgMsg.UpdateServerPort, sizeof(CfgMsg.UpdateServerPort));

    cfg.GprsConnectCfg[1].connected = true;
    cfg.GprsConnectCfg[1].ConnectMode = GPRS_DOMAIN_MODE_CONNECT;

    /*下接水泵、电磁阀*/
    cfg.ConnectedPump           = CfgMsg.ConnectedPump;                         //是否下接水泵
    cfg.ConnectedMutualInductor = CfgMsg.ConnectedMutualInductor;               //是否判断互感器

    memcpy(cfg.BindDtuList, CfgMsg.BindDtuList, sizeof(CfgMsg.BindDtuList));
    PHY_WriteConfig();

    if (IF_ReadCfgFromFlash() == false)
    {
        result = false;
    }

SendReponse:

/*发送响应*/
    response->head.version = CMD_VERSION;
    response->head.cmd = CMD_WRITE_CONFIG_RESPONSE;
    response->head.len = 1;

    response->head.len = Lib_htons(response->head.len);
    response->buff[0] = result;

    ResponseMsg.port = port;
    ResponseMsg.protocol = PROTOCOL_NONE;
    ResponseMsg.len = offsetof(_IF, buff) + 1;

    if (port == USART_GPRS_CHANNEL)
    {
        PHY_RequestGprsSendMsg((uint8 *)response, offsetof(_IF, buff) + 1, CMD_WRITE_CONFIG_RESPONSE, 0xFF, 1, 0, false);
    }
    else
    {
        PHY_UsartSendMsg(&ResponseMsg);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ReadCfg(uint8 port)
{
    _CfgCmd *SigleCmd;
    uint16 CfgMsgPtr;
    uint8 CmdSeq;
    _SysMsg msg;
    _IF *response;
    uint16 tmp;

    response = (_IF *)msg.buff;

    memset(&msg, 0x00, sizeof(msg));

    CfgMsgPtr = 0;
    SigleCmd = (_CfgCmd *)&response->buff[CfgMsgPtr];
    CmdSeq = 0;

    while (CmdSeq <= 14)
    {
        SigleCmd->cmd = CmdSeq;

        switch (CmdSeq)
        {
            case 0:                                                             //服务器ip
                {
                    SigleCmd->len = strlen(cfg.GprsConnectCfg[0].ip);
                    memcpy(SigleCmd->data, cfg.GprsConnectCfg[0].ip, SigleCmd->len);

                    break;
                }
            case 1:                                                             //服务器域名
                {
                    SigleCmd->len = strlen((char const *)cfg.GprsConnectCfg[0].domain);
                    memcpy(SigleCmd->data, cfg.GprsConnectCfg[0].domain, SigleCmd->len);

                    break;
                }
            case 2:                                                             //服务器端口
                {
                    SigleCmd->len = strlen(cfg.GprsConnectCfg[0].port);
                    memcpy(SigleCmd->data, cfg.GprsConnectCfg[0].port, SigleCmd->len);

                    break;
                }
            case 3:                                                             //升级服务器ip
                {
                    SigleCmd->len = strlen(cfg.GprsConnectCfg[1].ip);
                    memcpy(SigleCmd->data, cfg.GprsConnectCfg[1].ip, SigleCmd->len);

                    break;
                }
            case 4:                                                             //升级服务器域名
                {
                    SigleCmd->len = strlen((char const *)cfg.GprsConnectCfg[1].domain);
                    memcpy(SigleCmd->data, cfg.GprsConnectCfg[1].domain, SigleCmd->len);

                    break;
                }
            case 5:                                                             //升级服务器端口
                {
                    SigleCmd->len = strlen(cfg.GprsConnectCfg[1].port);
                    memcpy(SigleCmd->data, cfg.GprsConnectCfg[1].port, SigleCmd->len);

                    break;
                }
            case 6:                                                             //是否关联水泵
                {
                    SigleCmd->len = sizeof(cfg.ConnectedPump);
                    tmp = cfg.ConnectedPump;
                    tmp = Lib_htons(tmp);
                    memcpy(SigleCmd->data, &tmp, SigleCmd->len);

                    break;
                }
            case 7:                                                             //是否关联电磁阀
                {
                    SigleCmd->len = sizeof(cfg.ConnectedMutualInductor);

                    tmp = cfg.ConnectedMutualInductor;
                    tmp = Lib_htons(tmp);
                    memcpy(SigleCmd->data, &tmp, SigleCmd->len);

                    break;
                }
            case 8:                                                             //关联的电磁阀数量
                {
                    SigleCmd->len = sizeof(cfg.BindValveNum);

                    tmp = cfg.BindValveNum;
                    tmp = Lib_htons(tmp);
                    memcpy(SigleCmd->data, &tmp, SigleCmd->len);

                    break;
                }
            case 9:                                                             //下接主设备类型
                {
                    SigleCmd->len = sizeof(cfg.MainDeviceType);

                    tmp = cfg.MainDeviceType;
                    tmp = Lib_htons(tmp);
                    memcpy(SigleCmd->data, &tmp, SigleCmd->len);

                    break;
                }
            case 10:                                                            //关联的DTU列表
                {
                    SigleCmd->len = sizeof(cfg.BindDtuList[0]) * cfg.BindDtuNum;
                    memcpy(SigleCmd->data, cfg.BindDtuList, SigleCmd->len);

                    break;
                }
            case 11:                                                            //水泵异常监测异常次数
                {
                    SigleCmd->len = sizeof(cfg.PumpExcetionLimit);

                    tmp = cfg.PumpExcetionLimit;
                    tmp = Lib_htons(tmp);
                    memcpy(SigleCmd->data, &tmp, SigleCmd->len);

                    break;
                }
            case 12:                                                            //电路保护监测异常次数
                {
                    SigleCmd->len = sizeof(cfg.CircuitProtectExceptionLimit);

                    tmp = cfg.CircuitProtectExceptionLimit;
                    tmp = Lib_htons(tmp);
                    memcpy(SigleCmd->data, &tmp, SigleCmd->len);

                    break;
                }
            case 13:                                                            //阀门异常监测次数
                {
                    SigleCmd->len = sizeof(cfg.ValveExceptionLimit);

                    tmp = cfg.ValveExceptionLimit;
                    tmp = Lib_htons(tmp);
                    memcpy(SigleCmd->data, &tmp, SigleCmd->len);

                    break;
                }
            case 14:                                                            //流量计异常监测次数
                {
                    SigleCmd->len = sizeof(cfg.FlowMeterExceptionLimit);

                    tmp = cfg.FlowMeterExceptionLimit;
                    tmp = Lib_htons(tmp);
                    memcpy(SigleCmd->data, &tmp, SigleCmd->len);

                    break;
                }
        }

        CmdSeq++;
        CfgMsgPtr = CfgMsgPtr + SigleCmd->len + 2;
        SigleCmd = (_CfgCmd *)&response->buff[CfgMsgPtr];                       //指向下一个命令
    }

    /*发送响应*/
    response->head.version = CMD_VERSION;
    response->head.cmd = CMD_READ_CONFIG_RESPONSE;
    response->head.len = Lib_htons(CfgMsgPtr);

    msg.port = port;
    msg.protocol = PROTOCOL_NONE;
    msg.len = offsetof(_IF, buff) + CfgMsgPtr;

    Debug_Statics(offsetof(_Statics, ResRdCfg), 1, sizeof(statics.ResRdCfg));

    if (port == USART_GPRS_CHANNEL)
    {
        PHY_RequestGprsSendMsg((uint8 *)response, offsetof(_IF, buff) + CfgMsgPtr, CMD_READ_CONFIG_RESPONSE, 0xFF, 1, 0, false);
    }
    else
    {
        PHY_UsartSendMsg(&msg);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ResumeDefaultCfg(void)
{
    uint8 i;

    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        cfg.GprsConnectCfg[i].connected = false;

        memset(cfg.GprsConnectCfg[i].ip, 0x00, sizeof(cfg.GprsConnectCfg[i].ip));
        memset(cfg.GprsConnectCfg[i].port, 0x00, sizeof(cfg.GprsConnectCfg[i].port));
        cfg.GprsConnectCfg[i].connected = false;

        memcpy(cfg.GprsConnectCfg[i].domain, DEFAULT_DOMAIN, sizeof(DEFAULT_DOMAIN));
        memcpy(cfg.GprsConnectCfg[i].ip, DEFAULT_SERVER_IP, sizeof(DEFAULT_SERVER_IP));
        memcpy(cfg.GprsConnectCfg[i].port, DEFAULT_SERVER_PORT, sizeof(DEFAULT_SERVER_PORT));

        cfg.GprsConnectCfg[i].ConnectMode = DEFAULT_CONNECT_MODE;
    }

    memcpy(cfg.GprsConnectCfg[0].domain, DEFAULT_DOMAIN, sizeof(DEFAULT_DOMAIN));
    memcpy(cfg.GprsConnectCfg[0].ip, DEFAULT_SERVER_IP, sizeof(DEFAULT_SERVER_IP));
    memcpy(cfg.GprsConnectCfg[0].port, DEFAULT_SERVER_PORT, sizeof(DEFAULT_SERVER_PORT));

    memcpy(cfg.GprsConnectCfg[1].domain, DEFAULT_UPDATE_SERVER_DOMAIN, sizeof(DEFAULT_UPDATE_SERVER_DOMAIN));
    memcpy(cfg.GprsConnectCfg[1].ip, DEFAULT_UPDATE_SERVER_IP, sizeof(DEFAULT_UPDATE_SERVER_IP));
    memcpy(cfg.GprsConnectCfg[1].port, DEFAULT_UPDATE_SERVER_PORT, sizeof(DEFAULT_UPDATE_SERVER_PORT));

    cfg.GprsConnectCfg[0].connected = true;

    cfg.MainDeviceType = MAIN_DEVICE_MIC;
    cfg.BindDtuNum = 1;
    cfg.BindValveNum = 0;
    cfg.CircuitProtectExceptionLimit = 2;
    cfg.ConnectedMutualInductor = false;
    cfg.ConnectedPump = false;
    cfg.FlowMeterExceptionLimit = 2;
    cfg.PumpExcetionLimit = 2;
    cfg.ValveExceptionLimit = 2;

    memset(&cfg.BindDtuList[0][0], '0', sizeof(cfg.BindDtuList[0]));
    cfg.BindDtuList[0][0] = '1';

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Fun_ServerStartUpdate(_IF *msg, uint16 len)
{
    uint8 version;
    OS_ERR err;

    version = msg->buff[0];
    PHY_WriteConnectServerIndex(1);
    PHY_WriteUpdateVersion(version);

    msg->head.version = CMD_VERSION;
    msg->head.cmd = 17;

    len = 1;
    msg->head.len = Lib_htons(len);
    msg->buff[0] = 1;

    len = offsetof(_IF, buff) + len;
    PHY_RequestGprsSendMsg((uint8 *)&msg, len, 17, 0xFF, 1, 0, false);
    OSTimeDly(10, OS_OPT_TIME_DLY, &err);

    PHY_Reset(0);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SetDebugInfoOut(_SysMsg *msg)
{
    bool result;
    _DebugInfoOut *DebugInfoOut;
    _IF *SrcMsg;
    _IF *response;
    uint16 len;

    result = true;

    SrcMsg = (_IF *)msg->buff;
    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);

    /*报文参数检查*/
    if (SrcMsg->head.len != sizeof(_DebugInfoOut))
    {
        result = false;
    }

    if ((offsetof(_IF, buff) + sizeof(_DebugInfoOut)) != msg->len)
    {
        result = false;
    }

    if (result == false)
    {
        return;
    }

    DebugInfoOut = (_DebugInfoOut *)SrcMsg->buff;

    SysDebugInfoOut = DebugInfoOut->level;

    /*回复响应，无负载*/
    response = (_IF *)msg->buff;

    response->head.version = CMD_VERSION;
    response->head.cmd = CMD_SET_DEBUG_INFO_RESPONSE;
    response->head.len = 0;

    len = offsetof(_IF, buff);

    if (msg->port == USART_GPRS_CHANNEL)
    {
        PHY_RequestGprsSendMsg((uint8 *)response, len, CMD_SET_DEBUG_INFO_RESPONSE, 0xFF, 1, 0, false);
    }
    else
    {
        msg->len = len;
        msg->protocol = PROTOCOL_NONE;

        PHY_UsartSendMsg(msg);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool IF_ReadCfgFromFlash(void)
{
    bool result;
    uint8 i;
    uint8 len;
    uint8 count;

    result = false;

    PHY_ReadConfig();

    /*ip合法性：1、包含三个“.”；2、每个字段最大为三位数*/
    len = 0;
    count = 0;
    for (i = 0; i < sizeof(cfg.GprsConnectCfg[0].ip) && i < strlen(cfg.GprsConnectCfg[0].ip); i++)
    {
        if (cfg.GprsConnectCfg[0].ip[i] == '.')
        {
            len = 0;
            count++;
        }
        else
        {
            len++;
            if (len >= 4)
            {
                goto CheckFail;
            }
        }
    }
    if (count != 3)
    {
        goto CheckFail;
    }

    len = 0;
    count = 0;
    for (i = 0; i < sizeof(cfg.GprsConnectCfg[1].ip) && i < strlen(cfg.GprsConnectCfg[1].ip); i++)
    {
        if (cfg.GprsConnectCfg[1].ip[i] == '.')
        {
            len = 0;
            count++;
        }
        else
        {
            len++;
            if (len >= 4)
            {
                goto CheckFail;
            }
        }
    }
    if (count != 3)
    {
        goto CheckFail;
    }

    /*端口号合法性：1、长度最大为5字节；2、每个字符必须为数字；3、首字节为非零*/
    if (strlen(cfg.GprsConnectCfg[0].port) >= 6)
    {
        goto CheckFail;
    }

    if (cfg.GprsConnectCfg[0].port[0] == '0')
    {
        goto CheckFail;
    }

    for (i = 0; i < strlen(cfg.GprsConnectCfg[0].port); i++)
    {
        if (!(cfg.GprsConnectCfg[0].port[i] >= '0' && cfg.GprsConnectCfg[0].port[i] <= '9'))
        {
            goto CheckFail;
        }
    }

    if (strlen(cfg.GprsConnectCfg[1].port) >= 6)
    {
        goto CheckFail;
    }

    if (cfg.GprsConnectCfg[1].port[0] == '0')
    {
        goto CheckFail;
    }

    for (i = 0; i < strlen(cfg.GprsConnectCfg[1].port); i++)
    {
        if (!(cfg.GprsConnectCfg[1].port[i] >= '0' && cfg.GprsConnectCfg[1].port[i] <= '9'))
        {
            goto CheckFail;
        }
    }

    if (cfg.ConnectedPump != 0 && cfg.ConnectedPump != 1)                       //检查是否关联水泵
    {
        goto CheckFail;
    }

    if (cfg.ConnectedMutualInductor != 0 && cfg.ConnectedMutualInductor != 1)   //检查是否关联电磁阀
    {
        goto CheckFail;
    }

    if (cfg.MainDeviceType != MAIN_DEVICE_PLC && cfg.MainDeviceType != MAIN_DEVICE_MIC) //检查下接主设备类型
    {
        goto CheckFail;
    }

    /*对配置报文进行检查参数的互斥性*/

    /*下接水泵与互感器的互斥性*/
    if (cfg.ConnectedPump == false)
    {
        if (cfg.ConnectedMutualInductor == true)
        {
            goto CheckFail;
        }
    }

    /*下接水泵与关联电磁阀的互斥性*/
    if (cfg.ConnectedPump == false)
    {
        if (cfg.BindValveNum > 0)
        {
            goto CheckFail;
        }
    }

    /*下接水泵与主设备类型的互斥性*/
    if (cfg.ConnectedPump == true)
    {
        if (cfg.MainDeviceType == MAIN_DEVICE_MIC)
        {
            goto CheckFail;
        }
    }

    result = true;


CheckFail:
    if (result == false)
    {
        Fun_ResumeDefaultCfg();

        PHY_WriteConfig();
    }

    uint8 index = PHY_ReadConnectServerIndex();
    index = index % MAX_SERVER_NUM;
    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        if (i == index)
        {
            cfg.GprsConnectCfg[i].connected = true;
            cfg.ConnectServer = i;
        }
        else
        {
            cfg.GprsConnectCfg[i].connected = false;
        }

        cfg.GprsConnectCfg[i].ConnectMode = DEFAULT_CONNECT_MODE;
    }

    if (index == 1)
    {
        cfg.update = true;
        PHY_WriteConnectServerIndex(0);
    }
    else
    {
        cfg.update = false;
    }

    if (cfg.update == true)
    {
        UpdateVersion = PHY_ReadUpdateVersion();
    }

    //test
//    cfg.MainDeviceType = MAIN_DEVICE_MIC;

    return result;
}


/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void IF_CmdServer(_SysMsg *SrcMsg)
{
    _IF *msg;

    /*由于服务器暂时没有做crc，因此注释*/
//    if (CPR_ReceivedProcess(SrcMsg) == true)
//    {
//        if (SrcMsg->protocol != PROTOCOL_WT)
//        {
//            return;
//        }
//    }
//    else
//    {
//        return;
//    }

    msg = (_IF *)SrcMsg->buff;

    if (msg->head.version != CMD_VERSION)
    {
        return;
    }

    Gprs.server[cfg.ConnectServer].RxCounter++;

    switch (msg->head.cmd)
    {
        case CMD_SET_WORK_TASK:                                                 //设置子任务
            {
                if (SrcMsg->len <= sizeof(msg->head))
                {
                    break;
                }

                App_SetMicWorkTask(msg, SrcMsg->len);
                App_SetPlcWorkTask(msg, SrcMsg->len);

                break;
            }
        case CMD_REGISTER_TO_SERVER_RESPONSE:
            {
                App_MicRegisterToServerResponse(msg, SrcMsg->len);
                App_PlcRegisterToServerResponse(msg, SrcMsg->len);

                break;
            }
        case CMD_HEART_BEACON_RESPONSE:
            {
                PHY_HeartBeaconResponse(msg, SrcMsg->len);

                break;
            }
        case CMD_RESET_EXCEPTION:                                               //复位异常
            {
                App_MicResetException(SrcMsg, SrcMsg->len);
                App_PlcResetException(SrcMsg, SrcMsg->len);

                break;
            }
        case CMD_TRANSPARENT_TO_MAIN_DEVICE_1:                                    //透明传输
        case CMD_TRANSPARENT_TO_MAIN_DEVICE_2:
            {
                App_MicTransparentToMainDevice(msg, SrcMsg->len);
                App_PlcTransparentToMainDevice(msg, SrcMsg->len);

                break;
            }
        case CMD_RECEIVED_SERVER_FORWARD_REQUEST:                               //接收到服务器转发的请求
            {
                App_PlcReceivedServerForwardRequest(msg, SrcMsg->len);

                break;
            }
        case CMD_FORWARD_TO_BIND_DTU_RESPOSE:                                   //server->源dtu 响应
            {
                App_MicForwardToBindDtuRespose(msg, SrcMsg->len);

                break;
            }
        case CMD_REPORT_EXCEPTION_RESPOSE:                                      //异常响应
            {
                App_MicReportExceptionRespose(msg, SrcMsg->len);
                App_PlcReportExceptionRespose(msg, SrcMsg->len);

                break;
            }
        case CMD_PRIVILEGE_CMD:                                                 //特权指令
            {
                App_MicPrivilegeCmd(msg, SrcMsg->len);
                App_PlcPrivilegeCmd(msg, SrcMsg->len);

                break;
            }
        case CMD_CANCEL_PRIVILEGE_CMD:                                          //取消特权指令
            {
                App_MicCancelPrivilegeCmd(msg, SrcMsg->len);
                App_PlcCancelPrivilegeCmd(msg, SrcMsg->len);

                break;
            }
        case CMD_WRITE_CONFIG:
            {
                Fun_WriteCfg(msg, SrcMsg->len, SrcMsg->port);

                break;
            }
        case CMD_READ_CONFIG:
            {
                Debug_Statics(offsetof(_Statics, RdCfg),  1, sizeof(statics.RdCfg));

                Fun_ReadCfg(SrcMsg->port);

                break;
            }
        case CMD_RESUME_DEFAULT:
            {
                Fun_ResumeDefaultCfg();
                PHY_WriteConfig();

                _IF *response;
                OS_ERR err;

                response = (_IF *)SrcMsg->buff;
                response->head.version = CMD_VERSION;
                response->head.cmd = CMD_RESUME_DEFAULT_RESPOSE;
                response->head.len = 0;

                SrcMsg->protocol = PROTOCOL_NONE;
                SrcMsg->len = offsetof(_IF, buff);

                if (SrcMsg->port == USART_GPRS_CHANNEL)
                {
                    PHY_RequestGprsSendMsg((uint8 *)response, offsetof(_IF, buff), CMD_RESUME_DEFAULT_RESPOSE, 0xFF, 1, 0, false);
                }
                else
                {
                    PHY_UsartSendMsg(SrcMsg);
                }

                OSTimeDly(15 * SYS_SECOND_TICK_NUM, OS_OPT_TIME_DLY, &err);

                PHY_Reset(0);

                break;
            }
        case CMD_RESET:
            {
                _IF *response;
                OS_ERR err;

                response = (_IF *)SrcMsg->buff;
                response->head.version = CMD_VERSION;
                response->head.cmd = CMD_RESET_RESPOSE;
                response->head.len = 0;

                SrcMsg->protocol = PROTOCOL_NONE;
                SrcMsg->len = offsetof(_IF, buff);

                if (SrcMsg->port == USART_GPRS_CHANNEL)
                {
                    PHY_RequestGprsSendMsg((uint8 *)response, offsetof(_IF, buff), CMD_RESET_RESPOSE, 0xFF, 1, 0, false);
                }
                else
                {
                    PHY_UsartSendMsg(SrcMsg);
                }

                OSTimeDly(15 * SYS_SECOND_TICK_NUM, OS_OPT_TIME_DLY, &err);

                PHY_Reset(0);

                break;
            }
        case CMD_START_UPDATE:
            {
                Fun_ServerStartUpdate(msg, SrcMsg->len);

                break;
            }
        case CMD_SET_DEBUG_INFO:
            {
                Fun_SetDebugInfoOut(SrcMsg);

                break;
            }
        default:
            {
                if (cfg.update == true)
                {
                    App_UpdateMessageHandler(msg);
                }

                break;
            }
    }

    return;
}
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0      2016年10月26日     巍图      创建文件
*******************************************************************************/

