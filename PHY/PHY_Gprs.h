/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月12日
**  名   称：PHY_Sim900.h
**  摘   要：
*******************************************************************************/
#ifndef _PHY_Gprs_H
#define _PHY_Gprs_H

enum
{
    GPRS_TMR_INIT_OVER_TIME         = 20,                                       //初始化超时时间
    GPRS_SEND_HEART_BEAT_PERIOD     = 20,                                       //发送心跳包周期
    GPRS_TRY_CONNECT_PERIOD         = 10,
    GPRS_TMR_SEND_DATA_OVER_TIME    = 30,                                       //发送超时时间
    GPRS_PROTECT_TMR_PERIOD         = 30,                                       //180通信保护时间
    GPRS_CHECK_COMMUTE_PERIOD       = 180,
};


/*预期响应报文的检测方式*/
enum
{
    CHECK_RESPONSE_BODY       = 1,                                              //对接收报文进行全部检测
    CHECK_RESPONSE_HEAD       = 2,                                              //只检测报文头部
    CHECK_AND_RESPONSE_HEAP   = 4,                                              //与关系：检测报文尾部
    CHECK_MAX_LEN             = 8,                                              //检测报文最大长度
    CHECK_AND_CONTAINS_STRING = 16,                                             //与关系：检测是否包含指定字符串
    CHECK_OR_RESPONSE_HEAP    = 32,                                             //或关系：检测报文尾部
    CHECK_OR_CONTAINS_STRING  = 64,                                             //或关系：检测包含指定字符串
};
void PHY_GprsInit(void);
bool PHY_StartGprs(void);

void PHY_GprsReveivedHandler(uint8 *buff, uint16 len);
void PHY_GprsReceivedDone(_SysMsg *msg);
bool PHY_GprsCompareRecvMsgToExpectResponse(_SysMsg *msg);
bool PHY_GprsCIPSend(_GprsSendBuff *msg);
bool PHY_GprsSendATCommand(char *cmd, uint16 len);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2017年06月25日    巍图      创建文件
*******************************************************************************/

