/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年11月15日
**  名   称：PHY_GprsSendTask.h
**  摘   要：
*******************************************************************************/
#ifndef _PHY_GPRSSENDTASK_H
#define _PHY_GPRSSENDTASK_H


/*预期响应*/
typedef struct _ExpectResponse
{
    char   response[40];
    char   head[20];
    char   heap[20];
    uint32 opt;
    uint16 len;
}_ExpectResponse;

typedef struct _GprsSendBuff
{
    uint8  buff[MAX_GPRS_TX_BUFF_LEN];
    uint8  NeedResponse;
    uint8  server;
    uint16 len;
    uint8  state;
    uint8  ServerIndex;                                                         //想要发送给哪个服务器，0xFF：全部服务器都要发送
    int8   SendServerSeq;                                                       //使用多连接时，且报文需要发送给所有的服务器时，指示发送服务器的顺序
    uint32 SendStamp;

    OS_TMR ResendTmr;                                                           //重传定时器
    uint8  ResendNum;                                                           //重传次数
    uint32 ResendTime;
    uint8  cmd;                                                                 //命令号
}_GprsSendBuff;

void PHY_GprsSendTaskInit(void);
void PHY_HeartBeaconResponse(_IF *SrcMsg, uint16 len);
bool PHY_RequestGprsSendMsg(uint8 *buff, uint16 len, uint8 cmd, uint8 server, uint8 ResendNum, uint16 ResendTime, bool NeedResponse);
_GprsSendBuff* PHY_SearchGprsSendBuff(uint8 cmd, uint8 *buff, uint16 len);
void PHY_GprsOnlineEvent(uint8 server);
void PHY_ResumeGprsSendBuff(void);

#endif

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年11月15日    巍图      创建文件
*******************************************************************************/
