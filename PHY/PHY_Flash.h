/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月10日
**  名   称：PHY_Flash.h
**  摘   要：
*******************************************************************************/
#ifndef _PHY_FLASH_H
#define _PHY_FLASH_H

void PHY_ReadConfig(void);
void PHY_WriteConfig(void);

void PHY_ReadExternFlash(uint16 addr, uint8 *buff, uint16 len);
bool PHY_WriteExternFlash(uint16 addr, uint8 *buff, uint16 len);

uint16 PHY_ReadResetNum(void);
bool PHY_WriteResetNum(uint16 ResetNum);

uint8 PHY_ReadConnectServerIndex(void);
bool PHY_WriteConnectServerIndex(uint8 index);

uint16 PHY_ReadUpdateVersion(void);
bool PHY_WriteUpdateVersion(uint16 UpdateVersion);

bool PHY_InterFlashWrite(uint32 addr, uint8 *buff, uint16 len);
bool PHY_InterFlashRead(uint32 addr, uint8 *buff, uint16 len);
bool PHY_InterFlashErasePage(uint32 page);

#endif
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年05月10日   ggggggg      创建文件
*******************************************************************************/

