/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年10月26日
**  名   称：PHY_Gprs.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "PHY_Usart.h"
#include "PHY_Timer.h"
#include "PHY_Reset.h"

#include "PHY_GprsSendTask.h"
#include "PHY_Gprs.h"
#include "Dri_Sim900.h"
#include "update.h"

#include "debug.h"

#define GPRS_MAX_CONSECUTIVE_START_NUM    4
#define GPRS_MAX_TRY_CONNECT_NUM          4                      //最大尝试连接次数
#define GPRS_TASK_STACK_LEN               (MAX_BUFF_LEN * 3)

typedef struct _Gprs_ErrorResponse
{
    char   response[100];
    char   head[20];
    char   heap[20];
    uint16 opt;
    uint16 len;
}_Gprs_ErrorResponse;

typedef struct _Monitor
{
    OS_TMR TmrId;
    uint16 RetryInterval;
    uint32 OverTimeCounter;
    bool state;
}_Monitor;

typedef struct _GprsProtect
{
    OS_TMR TmrId;
}_GprsProtect;

static _Monitor GprsMonitor;
static _GprsProtect GprsProtect;

static uint8 ConnectToServerStage;                                              //Gprs连接到服务器的阶段
static uint8 GprsState;                                                         //只有初始化、正常工作两种状态
static _ExpectResponse Gprs_ExpectResponse;                                     //发送报文的预期响应
uint8 TryEnterAtCmdModeCounter;

static OS_TCB GprsTaskId;
static CPU_STK GprsTaskStack[GPRS_TASK_STACK_LEN];
static OS_SEM GprsTaskSemaphore;

bool PHY_StartGprs(void);
void PHY_GprsReset(void);

static void Fun_SendHeartBeat(uint8 server);
static uint8 Fun_GetSendServerIndex(_GprsSendBuff *msg);
static void Fun_ResigerToServer(uint8 server);
static void Fun_GprsStateMachineProcess(uint8 stage);

extern void IF_CmdServer(_SysMsg *msg);

/*-------------------状态指示模块---------------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Fun_SetGprsStateIndicate(uint8 state)
{
    Dri_SetGprsStateIndicateLed(state);

    return;
}

/*-------------------超时监控模块---------------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_StartMonitorTmr(void)
{
    OS_ERR err;

    OSTmrStart(&GprsMonitor.TmrId, &err);
    GprsMonitor.state = true;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_StopMonitorTmr(void)
{
    OS_ERR err;

    OSTmrStop(&GprsMonitor.TmrId, OS_OPT_TMR_NONE, NULL, &err);
    GprsMonitor.state = false;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ResetMonitorModule(uint32 top, uint16 interval)
{
    GprsMonitor.OverTimeCounter = top;
    GprsMonitor.RetryInterval = interval;

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_GprsMonitorTmrHandler(void *p_tmr, void *p_arg)
{
    OS_ERR err;

    /*投递信号量，通知发送任务*/
    OSSemPost(&GprsTaskSemaphore, OS_OPT_POST_1, &err);

    return;
}

/*-------------------保护模块-------------------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_GprsProtectTmrHandler(void *p_tmr, void *p_arg)
{
    uint8 i;

    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        if (Gprs.server[i].status == SERVER_ON_LINE)
        {
            break;
        }
    }

    Debug_Statics(offsetof(_Statics, ProtectTmr), 1, sizeof(statics.ProtectTmr));

    if (i >= MAX_SERVER_NUM)
    {
        Debug_Statics(offsetof(_Statics, TrigProtect), 1, sizeof(statics.TrigProtect));
        PHY_StartGprs();
    }

    return;
}

/*******************************************************************************
*功能：更新缓存：sim卡的插入状态
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_RenewGprsInsertedCardState(_SysMsg *msg)
{
    /*检查卡的插入状态响应：\r\n+CSMINS:0,1\r\n\r\nOK\r\n*/
    if (msg->buff[13] == '1')
    {
        Gprs.InsertedCard = true;
    }
    else
    {
        Gprs.InsertedCard = false;
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_RenewGprsIMEI(_SysMsg *msg)
{
    /*获取IMEI响应：\r\n864161027647044\r\n\r\nOK\r\n*/
    memcpy(Gprs.IMEI, &msg->buff[2], sizeof(Gprs.IMEI));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_RenewGprsCSQ(_SysMsg *msg)
{
    /*获取信号强度响应：\r\n+CSQ: 20,0\r\n\r\nOK\r\n*/
    /*从第七个字节开始转换*/
    Gprs.CSQ = atoi((char const *)&msg->buff[7]);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_RenewMultiConnectStatus(uint8 ConnectServerIndex)
{
    if (ConnectServerIndex >= MAX_SERVER_NUM)
    {
        PHY_Reset(0);

        return false;
    }

    if (Gprs.server[ConnectServerIndex].status == SERVER_IDLE)
    {
        Gprs.server[ConnectServerIndex].status = SERVER_CONNECT_MODE_1;
        Gprs.server[ConnectServerIndex].ConnectMode = cfg.GprsConnectCfg[ConnectServerIndex].ConnectMode;
    }
    else if (Gprs.server[ConnectServerIndex].status == SERVER_CONNECT_MODE_1)
    {
        if (Gprs.server[ConnectServerIndex].TryConnectedNum >= GPRS_MAX_TRY_CONNECT_NUM)
        {
            if (Gprs.server[ConnectServerIndex].ConnectMode == GPRS_IP_MODE_CONNECT)
            {
                Gprs.server[ConnectServerIndex].ConnectMode = GPRS_DOMAIN_MODE_CONNECT;
            }
            else
            {
                Gprs.server[ConnectServerIndex].ConnectMode = GPRS_IP_MODE_CONNECT;
            }

            Gprs.server[ConnectServerIndex].status = SERVER_CONNECT_MODE_2;
            Gprs.server[ConnectServerIndex].TryConnectedNum = 0;
        }
    }
    else if (Gprs.server[ConnectServerIndex].status == SERVER_CONNECT_MODE_2)
    {
        if (Gprs.server[ConnectServerIndex].TryConnectedNum >= GPRS_MAX_TRY_CONNECT_NUM)
        {
            return false;
        }
    }
    else
    {
        PHY_Reset(0);

        return false;
    }

    return true;
}

/*--------------------------------------------------------------*/

/*******************************************************************************
*功能：接收数据包的完整性校验
*参数：无
*返回：无
*说明：1、判断是否以\r\n作为结尾；2、判断中间数据包中是否含有\r\n
*******************************************************************************/
static bool Fun_CheckRecvMsg(_SysMsg *msg)
{
    if (msg->len < 2)
    {
        return false;
    }

    if (msg->buff[0] == '\r' && msg->buff[1] == '\n' && msg->buff[2] == '>')
    {
        return true;
    }

    return true;
}

/*******************************************************************************
*功能：发送AT测试指令
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SendAtTestCmd(void)
{
    char response[] = "AT\r\r\nOK\r\n";
    char ResponseHeap[] = "\r\nOK\r\n";
    char cmd[] = "AT\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    memcpy(Gprs_ExpectResponse.heap, ResponseHeap, sizeof(ResponseHeap));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_OR_CONTAINS_STRING | CHECK_OR_RESPONSE_HEAP;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CmdCIPSHUT(void)
{
    char response[] = "\r\nSHUT OK\r\n";
    char cmd[] = "AT+CIPSHUT\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CmdCIPCLOSE(void)
{
    char response[] = "\r\nCLOSE OK\r\n";
    char ResponseHeap[] = "\r\nERROR\r\n";
    char cmd[] = "AT+CIPCLOSE\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    memcpy(Gprs_ExpectResponse.heap, ResponseHeap, sizeof(ResponseHeap));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_OR_CONTAINS_STRING | CHECK_OR_RESPONSE_HEAP;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CmdCIPMODE(void)
{
    char response[] = "\r\nOK\r\n";
    char cmd[] = "AT+CIPMODE=1\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_EnterCmdMode(void)
{
    char response[] = "\r\nOK\r\n";
    char cmd[] = "+++";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

    Lib_DelaySecond(1);
    PHY_GprsSendATCommand(cmd, strlen(cmd));
    Lib_DelaySecond(1);

    return;
}
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CloseGprsEcho(void)
{
    char response[] = "ATE0\r\r\nOK\r\n";
    char ResponseHeap[] = "\r\nOK\r\n";
    char cmd[] = "ATE0\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    memcpy(Gprs_ExpectResponse.heap, ResponseHeap, sizeof(ResponseHeap));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_OR_CONTAINS_STRING | CHECK_OR_RESPONSE_HEAP;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_GetGprsCSQ(void)
{
    char ResponseHead[] = "\r\n+CSQ:";
    char ResponseHeap[] = "\r\n\r\nOK\r\n";
    char cmd[] = "AT+CSQ\r\n";

    memcpy(Gprs_ExpectResponse.head, ResponseHead, sizeof(ResponseHead));
    memcpy(Gprs_ExpectResponse.heap, ResponseHeap, sizeof(ResponseHeap));

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_HEAD | CHECK_AND_RESPONSE_HEAP;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：设置本地串口通信没有硬件流控制
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SetIFC(void)
{
    char response[] = "\r\nOK\r\n";
    char cmd[] = "AT+IFC=0,0\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：查询产品序列号
*参数：无
*返回：无
*说明：在关闭回显的情况下，读取IMEI的响应的长度是25字节
*******************************************************************************/
static void Fun_GetIMEI(void)
{
    char ResponseHead[] = "\r\n";
    char ResponseHeap[] = "\r\n\r\nOK\r\n";
    char cmd[] = "AT+CGSN\r\n";

    memcpy(Gprs_ExpectResponse.head, ResponseHead, sizeof(ResponseHead));
    memcpy(Gprs_ExpectResponse.heap, ResponseHeap, sizeof(ResponseHeap));

    Gprs_ExpectResponse.len = 25;
    Gprs_ExpectResponse.opt = CHECK_RESPONSE_HEAD | CHECK_AND_RESPONSE_HEAP | CHECK_MAX_LEN;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：获取Sim卡的插入状态
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_GetCSMINS(void)
{
    char ResponseHead[] = "\r\n+CSMINS:";
    char ResponseHeap[] = "\r\n\r\nOK\r\n";
    char cmd[] = "AT+CSMINS?\r\n";

    memcpy(Gprs_ExpectResponse.head, ResponseHead, sizeof(ResponseHead));
    memcpy(Gprs_ExpectResponse.heap, ResponseHeap, sizeof(ResponseHeap));

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_HEAD | CHECK_AND_RESPONSE_HEAP;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SetCIPMux(void)
{
    char response[] = "\r\nOK\r\n";
    char cmd[] = "AT+CIPMUX=1\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CmdCIPSTATUS(void)
{
    char response[] = "\r\nOK\r\n";
    char cmd[] = "AT+CIPSTATUS\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ExecuteCSTT(void)
{
    char response[] = "\r\nOK\r\n";
    char cmd[] = "AT+CSTT=\"CMNET\"\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ExecuteCIICR(void)
{
    char response[] = "\r\nOK\r\n";
    char cmd[] = "AT+CIICR\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
    Gprs_ExpectResponse.len = strlen(response);

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}


/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ExecuteCIFSR(void)
{
    char ResponseHead[] = "\r\n";
    char ResponseHeap[] = "\r\n";
    char response[] = ".";
    char cmd[] = "AT+CIFSR\r\n";

    memcpy(Gprs_ExpectResponse.response, response, sizeof(response));

    memcpy(Gprs_ExpectResponse.head, ResponseHead, sizeof(ResponseHead));
    memcpy(Gprs_ExpectResponse.heap, ResponseHeap, sizeof(ResponseHeap));

    Gprs_ExpectResponse.opt = CHECK_RESPONSE_HEAD | CHECK_AND_RESPONSE_HEAP | CHECK_AND_CONTAINS_STRING;

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_StartConnectServer(void)
{
    uint8 i;
    OS_ERR err;

    GprsState = GPRS_NORMAL_STATE;
    Fun_StopMonitorTmr();

    /*启动连接过程*/
    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        if (Gprs.server[i].connected == true)
        {
            OSTmrStop(&Gprs.server[i].ConnectServerTmrId, OS_OPT_TMR_NONE, NULL, &err);
            Gprs.server[i].ConnectServerTmrId.Dly = (1 + i * GPRS_TRY_CONNECT_PERIOD) * SYS_SECOND_TICK_NUM;
            Gprs.server[i].ConnectServerTmrId.Period = GPRS_TRY_CONNECT_PERIOD * SYS_SECOND_TICK_NUM;
            OSTmrStart(&Gprs.server[i].ConnectServerTmrId, &err);
        }
    }

    return;
}
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_CIPStart(uint8 ServerIndex)
{
    char response[] = "CONNECT";
    _ExpectResponse ExpectResponse;

    char cmd[60];

    /*封装预期响应*/
    memcpy(ExpectResponse.response, response, sizeof(response));
    ExpectResponse.len = strlen(response);
    ExpectResponse.opt = CHECK_AND_CONTAINS_STRING;

    memset(cmd, 0x00, sizeof(cmd));

    if (Gprs.server[ServerIndex].ConnectMode == GPRS_IP_MODE_CONNECT)
    {
        if (Gprs.CIPMux == true)
        {
            sprintf(cmd, "AT+CIPSTART=%d,\"TCP\",\"%s\",\"%s\"\r\n",
                    ServerIndex, Gprs.server[ServerIndex].ip, Gprs.server[ServerIndex].port);
        }
        else
        {
            sprintf(cmd, "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"\r\n",
                    Gprs.server[ServerIndex].ip, Gprs.server[ServerIndex].port);
        }
    }
    else                                                                        //使用域名方式连接
    {
        if (Gprs.CIPMux == true)
        {
            sprintf(cmd, "AT+CIPSTART=%d,\"TCP\",\"%s\",\"%s\"\r\n",
                    ServerIndex, Gprs.server[ServerIndex].domain, Gprs.server[ServerIndex].port);
        }
        else
        {
            sprintf(cmd, "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"\r\n",
                    Gprs.server[ServerIndex].domain, Gprs.server[ServerIndex].port);
        }
    }

    PHY_GprsSendATCommand(cmd, strlen(cmd));

    return;
}

/*----------------------------初始化过程处理-----------------------------*/
/*******************************************************************************
*功能：Gprs状态机处理
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_GprsStateMachineProcess(uint8 stage)
{
    switch (stage)
    {
        case GPRS_IDLE:
            {
                break;
            }
        case GPRS_POWER_UP:                                                     //上电初始化
            {
                Fun_StopMonitorTmr();
                Dri_InitGprs();
                Fun_StartMonitorTmr();

                Fun_SetGprsStateIndicate(SERVER_IDLE);

                ConnectToServerStage = GPRS_CMD_ENTER_CMD_MODE;
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 2);

                Fun_EnterCmdMode();

                TryEnterAtCmdModeCounter = 0;

                break;
            }
        case GPRS_CMD_ENTER_CMD_MODE:
            {
                if (TryEnterAtCmdModeCounter < 3)
                {
                    TryEnterAtCmdModeCounter++;
                    Fun_EnterCmdMode();
                }
                else
                {
                    Fun_SendAtTestCmd();

                    ConnectToServerStage = GPRS_SEND_TEST_AT;
                    Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME * 100, 2);
                }

                break;
            }
        case GPRS_SEND_TEST_AT:
            {
                Fun_SendAtTestCmd();

                break;
            }
        case GPRS_CMD_CIPSHUT:
            {
                Fun_CmdCIPSHUT();

                break;
            }
        case GPRS_CMD_CIPCLOSE:
            {
                Fun_CmdCIPCLOSE();

                break;
            }
        case GPRS_CLOSE_ECHO:
            {
                Fun_CloseGprsEcho();

                break;
            }
        case GPRS_SET_IFC:
            {
                Fun_SetIFC();

                break;
            }
        case GPRS_CHECK_INSERT_CARD:                                            //检查卡的插入状态
            {
                Fun_GetCSMINS();

                break;
            }
        case GPRS_GET_IMEI:                                                     //查询产品序列号
            {
                Fun_GetIMEI();

                break;
            }
        case GPRS_GET_CSQ:
            {
                Fun_GetGprsCSQ();

                break;
            }
        case GPRS_CMD_CIPSTATUS:
            {
                Fun_CmdCIPSTATUS();

                break;
            }
        case GPRS_SET_CIP_MUX:                                                  //设置多路连接
            {
                Fun_SetCIPMux();

                break;
            }
        case GPRS_EXECUTE_CSTT:
            {
                Fun_ExecuteCSTT();

                break;
            }
        case GPRS_EXECUTE_CIICR:
            {
                Fun_ExecuteCIICR();

                break;
            }
        case GPRS_EXECUTE_CIFSR:
            {
                Fun_ExecuteCIFSR();

                break;
            }
        case GPRS_CMD_CIPMODE:
            {
                Fun_CmdCIPMODE();

                break;
            }
        default:
            {
                break;
            }
    }

    return;
}

/*******************************************************************************
*功能：处理初始化阶段的响应
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_ProcessInitStageResponse(_SysMsg *msg)
{
    bool result;

    result = true;

    switch (ConnectToServerStage)
    {
        case GPRS_IDLE:
            {
                break;
            }
        case GPRS_POWER_UP:
            {
                break;
            }
        case GPRS_CMD_ENTER_CMD_MODE:
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                ConnectToServerStage = GPRS_SEND_TEST_AT;                       //下一阶段是设置串口硬件流
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 1);

                break;
            }
        case GPRS_SEND_TEST_AT:                                                 //发送AT测试指令
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                ConnectToServerStage = GPRS_CLOSE_ECHO;                         //下一阶段是设置串口硬件流
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 1);

                break;
            }

        case GPRS_CLOSE_ECHO:                                                   //关闭回显
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                ConnectToServerStage = GPRS_SET_IFC;                            //下一阶段是设置串口硬件流
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 1);

                break;
            }
        case GPRS_SET_IFC:                                                      //设置串口硬件流
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                ConnectToServerStage = GPRS_CHECK_INSERT_CARD;                  //下一阶段是检测卡的插入状态
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 1);

                break;
            }
        case GPRS_CHECK_INSERT_CARD:                                            //检查卡的插入状态
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                Fun_RenewGprsInsertedCardState(msg);
                if (Gprs.InsertedCard == false)                                 //检测到卡未插入
                {
                    PHY_StartGprs();

                    break;
                }

                ConnectToServerStage = GPRS_GET_IMEI;                           //下一阶段是获取IMEI
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 1);

                break;
            }
        case GPRS_GET_IMEI:                                                     //获取出厂序列号
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                Fun_RenewGprsIMEI(msg);

                ConnectToServerStage = GPRS_GET_CSQ;                            //下一阶段是获取信号强度
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 1);

                break;
            }
        case GPRS_GET_CSQ:
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                Fun_RenewGprsCSQ(msg);

                if (Gprs.CIPMux == true)
                {
                    ConnectToServerStage = GPRS_CMD_CIPCLOSE;
                }
                else
                {
                    ConnectToServerStage = GPRS_CMD_CIPSHUT;
                }

                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 1);

                break;
            }
        case GPRS_CMD_CIPSHUT:
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                ConnectToServerStage = GPRS_CMD_CIPSTATUS;
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 2);

                break;
            }
        case GPRS_CMD_CIPCLOSE:
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                if (Gprs.CIPMux == true)
                {
                    ConnectToServerStage = GPRS_SET_CIP_MUX;                    //下一阶段是设置多连接
                }
                else
                {
                    ConnectToServerStage = GPRS_CMD_CIPSTATUS;                  //下一阶段是设置CSTT
                }

                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 2);

                break;
            }
        case GPRS_CMD_CIPSTATUS:
            {
                if (strstr((char const *)msg->buff, "IP INITIAL") != NULL
                    || strstr((char const *)msg->buff, "IP STATUS") != NULL
                    || strstr((char const *)msg->buff, "TCP CLOSED") != NULL)
                {
                    ConnectToServerStage = GPRS_CMD_CIPMODE;                    //下一阶段是设置CIFSR
                    Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 2);

                    return false;
                }

                break;
            }
        case GPRS_SET_CIP_MUX:
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                ConnectToServerStage = GPRS_EXECUTE_CSTT;                       //下一阶段是设置CSTT
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 2);

                break;
            }
        case GPRS_EXECUTE_CSTT:
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                ConnectToServerStage = GPRS_EXECUTE_CIICR;                      //下一阶段是设置CIICR
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 2);

                break;
            }
        case GPRS_EXECUTE_CIICR:
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                ConnectToServerStage = GPRS_EXECUTE_CIFSR;                      //下一阶段是设置CIFSR
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 2);

                break;
            }
        case GPRS_EXECUTE_CIFSR:
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                ConnectToServerStage = GPRS_CMD_CIPMODE;                        //下一阶段是设置CIFSR
                Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 2);

                break;
            }
        case GPRS_CMD_CIPMODE:
            {
                if (PHY_GprsCompareRecvMsgToExpectResponse(msg) == false)
                {
                    result = false;

                    break;
                }

                Fun_StartConnectServer();

                break;
            }
    }

    return result;
}

/*******************************************************************************
*功能：对于gprs的响应，维护连接
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_ProcessMaintainConnect(_SysMsg *msg)
{
    uint8 i;

    /*收到服务器关闭连接*/
    if (strstr((char const *)msg->buff, "CLOSED") != NULL)
    {
        if (Gprs.CIPMux == true)                                                //多连接状态的报文解析方式
        {
            for (i = 0; i < MAX_SERVER_NUM; i++)
            {
                if ((msg->buff[2] - '0') == i)
                {
                    Gprs.server[i].status = SERVER_IDLE;
                }
            }
        }
        else
        {
            for (i = 0; i < MAX_SERVER_NUM; i++)                                //单连接状态的报文解析方式
            {
                if (Gprs.server[i].connected == true)
                {
                    Gprs.server[i].status = SERVER_IDLE;

                    break;
                }
            }
        }

        Debug_Statics(offsetof(_Statics, offline),  1, sizeof(statics.offline));

        /*收到服务器断开连接后，立即启动再次连接*/
        PHY_StartGprs();

        return true;
    }

    /*上线事件*/
    if (strstr((char const *)msg->buff, "CONNECT") != NULL)
    {
        if (strcmp((char const *)msg->buff, "\r\nCONNECT\r\n") == 0
            || strstr((char const *)msg->buff, "ALREADY CONNECT") != NULL)
        {
            Gprs.server[cfg.ConnectServer].status = SERVER_ON_LINE;

            App_StartUpdate();

            Fun_ResigerToServer(cfg.ConnectServer);

            Fun_SetGprsStateIndicate(SERVER_ON_LINE);

            Debug_Statics(offsetof(_Statics, online), 1, sizeof(statics.online));

            return true;
        }
    }

    return false;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：服务器连接状态维护定时器回调函数
*******************************************************************************/
static void Fun_ServerConnectStatusTmrHandler(void *p_tmr, void *p_arg)
{
    uint8 i;
    uint8 ServerSeq;

    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        if (&Gprs.server[i].ConnectServerTmrId == p_tmr)
        {
            break;
        }
    }

    if (i >= MAX_SERVER_NUM)
    {
        return;
    }

    ServerSeq = i;
    if (Gprs.server[i].connected == false)
    {
        return;
    }

    if (GprsState == GPRS_NORMAL_STATE)
    {
        if (Gprs.server[ServerSeq].status != SERVER_ON_LINE)
        {
            if (Fun_RenewMultiConnectStatus(ServerSeq) == true)
            {
                Gprs.server[ServerSeq].TryConnectedNum++;
                Fun_CIPStart(ServerSeq);
            }
            else
            {
                Gprs.server[ServerSeq].status = SERVER_IDLE;
                Gprs.server[ServerSeq].TryConnectedNum = 0;
            }

            return;
        }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：服务器连接状态维护定时器回调函数
*******************************************************************************/
static void Fun_MaintainTmrHandler(void *p_tmr, void *p_arg)
{
    uint8 i;
    uint8 ServerSeq;

    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        if (&Gprs.server[i].MaintainTmrId == p_tmr)
        {
            break;
        }
    }

    if (i >= MAX_SERVER_NUM)
    {
        return;
    }

    ServerSeq = i;
    if (Gprs.server[i].connected == false)
    {
        return;
    }

    Gprs.server[i].MaintainTmrCounter++;

    /*到达发送心跳周期，进行发送心跳包*/
    if (GprsState == GPRS_NORMAL_STATE
        && Gprs.server[ServerSeq].status == SERVER_ON_LINE
        && (Gprs.server[i].MaintainTmrCounter % GPRS_SEND_HEART_BEAT_PERIOD) == 0)
    {
        Fun_SendHeartBeat(ServerSeq);
    }

    /*到达检查通信状态周期，进行检查通信状态*/
    if ((Gprs.server[i].MaintainTmrCounter % GPRS_CHECK_COMMUTE_PERIOD) == 0)
    {
        Debug_Statics(offsetof(_Statics, MaintainTmr), 1, sizeof(statics.MaintainTmr));

        if (Gprs.server[i].RxCounter == 0)
        {
            PHY_StartGprs();
        }
        else
        {
            Gprs.server[i].RxCounter = 0;
        }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static uint8 Fun_GetSendServerIndex(_GprsSendBuff *msg)
{
    uint8 SendServerSeq;
    uint8 i;

    SendServerSeq = 0xFF;

    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        if (msg->ServerIndex & ((uint16)1 << i))
        {
            SendServerSeq = i;
            msg->ServerIndex &= ~((uint16)1 << i);

            break;
        }
    }

    return SendServerSeq;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_SendHeartBeat(uint8 server)
{
    _IF msg;
    uint16 len;

    msg.head.version = CMD_VERSION;

    msg.head.cmd = CMD_HEART_BEACON;
    msg.head.len = 0;
    len = offsetof(_IF, buff);

    PHY_RequestGprsSendMsg((uint8 *)&msg, len, CMD_HEART_BEACON, 0xFF, 1, 0, true);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ResigerToServer(uint8 server)
{
    PHY_GprsOnlineEvent(server);

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_GprsSendATCommand(char *cmd, uint16 len)
{
    return PHY_UsartSend(USART_GPRS_CHANNEL, (uint8 *)cmd, len);
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_GprsCompareRecvMsgToExpectResponse(_SysMsg *msg)
{
    uint16 i;
    uint16 len;
    bool result;

    result = true;

    /*预期响应体检测：只检测响应体（必须满足）*/
    if ((Gprs_ExpectResponse.opt & CHECK_RESPONSE_BODY) == CHECK_RESPONSE_BODY)
    {
        if (msg->len != Gprs_ExpectResponse.len)
        {
            return false;
        }

        if (!(memcmp(msg->buff, Gprs_ExpectResponse.response, msg->len) == 0))
        {
            return false;
        }
    }

    /*预期响应头检测：只检测响应头部（必须满足）*/
    if ((Gprs_ExpectResponse.opt & CHECK_RESPONSE_HEAD) == CHECK_RESPONSE_HEAD)
    {
        len = strlen(Gprs_ExpectResponse.head);

        if (!(memcmp(msg->buff, Gprs_ExpectResponse.head, len) == 0))
        {
            return false;
        }
    }

    /*预期响应尾检测：只检测响应尾部（必须满足）*/
    if ((Gprs_ExpectResponse.opt & CHECK_AND_RESPONSE_HEAP) == CHECK_AND_RESPONSE_HEAP)
    {
        len = strlen(Gprs_ExpectResponse.heap);

        for (i = 0; i < len; i++)
        {
            if (Gprs_ExpectResponse.heap[i] != msg->buff[msg->len - len + i])
            {
                return false;
            }
        }
    }

    /*预期响应长度检测*/
    if ((Gprs_ExpectResponse.opt & CHECK_MAX_LEN) == CHECK_MAX_LEN)
    {
        if (Gprs_ExpectResponse.len != msg->len)
        {
            return false;
        }
    }

    /*预期响应包含指定字符串检测（必须满足）*/
    if ((Gprs_ExpectResponse.opt & CHECK_AND_CONTAINS_STRING) == CHECK_AND_CONTAINS_STRING)
    {
        if (strstr((char const *)msg->buff, Gprs_ExpectResponse.response) == NULL)
        {
            return false;
        }
    }

    /*预期响应包含指定字符串检测：或关系*/
    if ((Gprs_ExpectResponse.opt & CHECK_OR_CONTAINS_STRING) == CHECK_OR_CONTAINS_STRING)
    {
        if (strstr((char const *)msg->buff, Gprs_ExpectResponse.response) != NULL)
        {
            return true;
        }
        else
        {
            result = false;
        }
    }

    /*预期响应尾检测*/
    if ((Gprs_ExpectResponse.opt & CHECK_OR_RESPONSE_HEAP) == CHECK_OR_RESPONSE_HEAP)
    {
        len = strlen(Gprs_ExpectResponse.heap);

        for (i = 0; i < len; i++)
        {
            if (Gprs_ExpectResponse.heap[i] != msg->buff[msg->len - len + i])
            {
                break;
            }
        }

        if (i >= len)
        {
            return true;
        }
        else
        {
            result = false;
        }
    }

    return result;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_GprsReceivedDone(_SysMsg *msg)
{
    /*根据接收的第一个字符来判断是按照数字还是ASCII来输出（协议的一个字节为版本号，是数字）*/
    if (msg->buff[0] < 5)
    {
        Debug_Printf("GPRS Rx:", msg->buff, msg->len, DEBUG_GPRS_RX, 0);
    }
    else
    {
        Debug_Printf("GPRS Rx:", msg->buff, strlen((const char *)msg->buff), DEBUG_GPRS_RX, 1);
    }

    if (Fun_CheckRecvMsg(msg) == false)                                         //接收数据包的完整性校验
    {
        return;
    }

    switch (GprsState)
    {
        case GPRS_INIT_STATE:
            {
                if (ConnectToServerStage == GPRS_POWER_UP)
                {
                    break;
                }

                if (Fun_ProcessInitStageResponse(msg) == true)                  //处理初始化阶段的响应
                {
                    if (GprsState == GPRS_INIT_STATE)
                    {
                        Fun_StartMonitorTmr();                                  //重启监控定时器
                        Fun_GprsStateMachineProcess(ConnectToServerStage);      //进行下一步的操作指令
                    }
                }

                break;
            }
        case GPRS_NORMAL_STATE:
            {
                if (Fun_ProcessMaintainConnect(msg) == true)
                {
                    break;
                }

                IF_CmdServer(msg);

                break;
            }
        default:
            {
                PHY_Reset(0);                                                   //发生错误，重启

                break;
            }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_GprsCIPSend(_GprsSendBuff *msg)
{
    char cmd[60];
    uint8 SendServerSeq;
    OS_ERR err;

    SendServerSeq = 0xFF;

    memset(cmd, 0x00, sizeof(cmd));

    if (Gprs.CIPMux == true)                                                    //多服务器需要发送
    {
        if (msg->state == BUFF_SEND_HEAD_MSG)                                   //多连接第一步：发送数据头部
        {
            SendServerSeq = Fun_GetSendServerIndex(msg);
            msg->SendServerSeq = SendServerSeq;

            sprintf(cmd, "AT+CIPSEND=%d,%d\r\n", msg->SendServerSeq, msg->len);

            /*封装预期响应*/
            char response[] = "\r\n> ";

            memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
            Gprs_ExpectResponse.len = strlen(response);

            Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

            /*进行发送：发送命令头部，然后等待响应*/
            PHY_GprsSendATCommand(cmd, strlen(cmd));
            msg->state = BUFF_WAIT_READY_SEND;
        }
        else                                                                    //多连接第二步：发送数据体
        {
            /*封装预期响应*/
            char response[] = "SEND OK";

            memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
            Gprs_ExpectResponse.len = strlen(response);

            Gprs_ExpectResponse.opt = CHECK_AND_CONTAINS_STRING;

            /*进行发送：发送数据体*/
            PHY_GprsSendATCommand((char *)msg->buff, msg->len);
            msg->state = BUFF_WAIT_SEND_OK;
            msg->SendStamp = OSTimeGet(&err);
        }
    }
    else                                                                        //只发送给单个服务器
    {
        if (msg->state == BUFF_SEND_HEAD_MSG)
        {
            SendServerSeq = Fun_GetSendServerIndex(msg);
            msg->SendServerSeq = SendServerSeq;

            /*第一步：发送命令头*/

            /*封装预期响应*/
            char response[] = "\r\n> ";

            memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
            Gprs_ExpectResponse.len = strlen(response);
            Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

            /*进行发送：发送命令头部，然后等待响应*/
            sprintf(cmd, "AT+CIPSEND=%d\r\n", msg->len);
            PHY_GprsSendATCommand(cmd, strlen(cmd));
            msg->state = BUFF_WAIT_READY_SEND;
        }
        else
        {
            /*第二步：发送数据体*/
            /*封装预期响应*/
            char response[] = "\r\nSEND OK\r\n";

            memcpy(Gprs_ExpectResponse.response, response, sizeof(response));
            Gprs_ExpectResponse.len = strlen(response);

            Gprs_ExpectResponse.opt = CHECK_RESPONSE_BODY;

            /*进行发送：发送数据体*/
            PHY_GprsSendATCommand((char *)msg->buff, msg->len);
            msg->state = BUFF_SEND_BODY_MSG;
            msg->SendStamp = OSTimeGet(&err);
        }
    }

    return true;
}

/*******************************************************************************
*功能：启动Gprs
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_StartGprs(void)
{
    uint8 i;
    OS_ERR err;

    ConnectToServerStage = GPRS_IDLE;

    /*恢复默认值*/
    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        Gprs.server[i].status = SERVER_IDLE;
        Gprs.server[i].TryConnectedNum = 0;
        Gprs.server[i].RxCounter = 0;
    }

    Debug_Statics(offsetof(_Statics, GprsStart),  1, sizeof(statics.GprsStart));

    GprsState = GPRS_INIT_STATE;
    ConnectToServerStage = GPRS_POWER_UP;

    Fun_ResetMonitorModule(GPRS_TMR_INIT_OVER_TIME, 2);

    OSTmrStop(&GprsMonitor.TmrId, OS_OPT_TMR_NONE, NULL, &err);
    GprsMonitor.TmrId.Dly = 1 * SYS_SECOND_TICK_NUM;
    GprsMonitor.TmrId.Period = 1 * SYS_SECOND_TICK_NUM;
    OSTmrStart(&GprsMonitor.TmrId, &err);
    GprsMonitor.state = true;

    Fun_SetGprsStateIndicate(SERVER_OFF_LINE);

    PHY_ResumeGprsSendBuff();

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_GprsTaskHandler(void *p_arg)
{
    OS_ERR err;

    while (true)
    {
        OSSemPend(&GprsTaskSemaphore, 0, OS_OPT_PEND_BLOCKING, NULL, &err);

        if (GprsMonitor.state == false)
        {
            continue;
        }

        if (GprsMonitor.OverTimeCounter <= 1)
        {
            PHY_StartGprs();                                                    //超时后，重新上电初始化
        }

        if (GprsState == GPRS_INIT_STATE)
        {
            Debug_Statics(offsetof(_Statics, OverTimeNum), 1, sizeof(statics.OverTimeNum));

            if ((GprsMonitor.OverTimeCounter % GprsMonitor.RetryInterval) == 0)
            {
                Fun_GprsStateMachineProcess(ConnectToServerStage);
            }
        }

        if (GprsMonitor.OverTimeCounter > 0)
        {
            GprsMonitor.OverTimeCounter--;
        }

        continue;
    }
}

/*******************************************************************************
*功能：启动gprs
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_GprsInit(void)
{
    uint16 i;
    uint16 ConnectServerNum;                                                    //待连接的服务器数量
    OS_ERR err;

    /*初始化Gprs的状态变量*/
    memcpy(&Gprs.server, &cfg.GprsConnectCfg, sizeof(Gprs.server));

    /*确定待连接的服务器数量*/
    ConnectServerNum = 0;
    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        if (cfg.GprsConnectCfg[i].connected == true)
        {
            ConnectServerNum++;

            OSTmrCreate(&Gprs.server[i].ConnectServerTmrId,
                        NULL,
                        0 * SYS_SECOND_TICK_NUM,
                        GPRS_TRY_CONNECT_PERIOD * SYS_SECOND_TICK_NUM,
                        OS_OPT_TMR_PERIODIC,
                        Fun_ServerConnectStatusTmrHandler,
                        NULL,
                        &err);
        }

        OSTmrCreate(&Gprs.server[i].MaintainTmrId,
                    NULL,
                    2 * SYS_SECOND_TICK_NUM,
                    1 * SYS_SECOND_TICK_NUM,
                    OS_OPT_TMR_PERIODIC,
                    Fun_MaintainTmrHandler,
                    NULL,
                    &err);
        OSTmrStart(&Gprs.server[i].MaintainTmrId, &err);

        Gprs.server[i].TryConnectedNum = 0;
        Gprs.server[i].status = SERVER_IDLE;
        Gprs.server[i].RxCounter = 0;
    }

    if (ConnectServerNum >= 2)
    {
        Gprs.CIPMux = true;
    }
    else
    {
        Gprs.CIPMux = false;
    }

    Gprs.ServerNum = ConnectServerNum;
    Gprs.InsertedCard = false;

    OSTmrCreate(&GprsMonitor.TmrId,
                NULL,
                0,
                GPRS_TMR_INIT_OVER_TIME * SYS_SECOND_TICK_NUM,
                OS_OPT_TMR_PERIODIC,
                Fun_GprsMonitorTmrHandler,
                NULL,
                &err);

    OSTmrCreate(&GprsProtect.TmrId,
                NULL,
                300 * SYS_SECOND_TICK_NUM,
                300 * SYS_SECOND_TICK_NUM,
                OS_OPT_TMR_PERIODIC,
                Fun_GprsProtectTmrHandler,
                NULL,
                &err);
    OSTmrStart(&GprsProtect.TmrId, &err);                                       //启动保护定时器

    /*gprs任务初始化*/
    OSSemCreate(&GprsTaskSemaphore, NULL, 0, &err);

    OSTaskCreate((OS_TCB    *)&GprsTaskId,
                 (CPU_CHAR  *)NULL,
                 (OS_TASK_PTR)Fun_GprsTaskHandler,
                 (void      *)0,
                 (OS_PRIO)TASK_GPRS_PRIORITY,
                 (CPU_STK   *)&GprsTaskStack[0],
                 (CPU_STK_SIZE)GPRS_TASK_STACK_LEN / 10,
                 (CPU_STK_SIZE)GPRS_TASK_STACK_LEN,
                 (OS_MSG_QTY)0,
                 (OS_TICK)0,
                 (void      *)0,
                 (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR    *)&err);

    //test
    Gprs.reset = true;
    SendAtCmd = true;

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0      2016年10月26日     巍图      创建文件
*******************************************************************************/



