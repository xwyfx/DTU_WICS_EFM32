/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月12日
**  名   称：PHY_Usart.c
**  摘   要：
*******************************************************************************/
#include "system.h"
#include "Dri_Usart.h"

#include "PHY_GprsSendTask.h"
#include "PHY_Gprs.h"
#include "PHY_Usart.h"

#include "CommProtocol.h"

#include "debug.h"

#define USART_RX_OVER_TIME                          (1 * SYS_SECOND_TICK_NUM)

#define LEUSART0_RECEIVE_TASK_STACK_LEN             (MAX_BUFF_LEN + 70)

#define LEUSART1_RECEIVE_TASK_STACK_LEN             (MAX_BUFF_LEN * 2)
#define LEUSART1_SEND_TASK_STACK_LEN                (MAX_BUFF_LEN * 2)
#define MAX_LEUART1_SEND_BUFF_NUM                   17

#define USART0_RECEIVE_TASK_STACK_LEN               (MAX_GPRS_RX_BUFF_LEN * 2)
#define USART0_SEND_TASK_STACK_LEN                  MAX_GPRS_TX_BUFF_LEN


/*485发送任务格式*/
typedef struct
{
    _TxBuff TxBuff[MAX_LEUART1_SEND_BUFF_NUM];
    uint8 SendIndex;
}_485SendTask;

/*----------------Leuart0相关变量----------------*/
static OS_TCB  Leuart0ReceiveTaskId;
#pragma data_alignment=8
static CPU_STK Leuart0ReceiveTaskStack[LEUSART0_RECEIVE_TASK_STACK_LEN];
static _SysMsg *Leuart0ReceiveMsg;
static OS_SEM  Leuart0ReceiveSemaphore;

/*----------------Leuart1相关变量----------------*/
static OS_TCB  Leuart1ReceiveTaskId;
#pragma data_alignment=8
static CPU_STK Leuart1ReceiveTaskStack[LEUSART1_RECEIVE_TASK_STACK_LEN];
static _SysMsg *Leuart1ReceiveMsg;
static OS_SEM  Leuart1ReceiveSemaphore;

static OS_TCB  Leuart1SendTaskId;
#pragma data_alignment=8
static CPU_STK Leuart1SendTaskStack[LEUSART1_SEND_TASK_STACK_LEN];
static OS_SEM  Leuart1SendSemaphore;
static _485SendTask Leuart1SendTask;

/*----------------Usart0相关变量----------------*/
static OS_TCB Usart0ReceiveTaskId;
#pragma data_alignment=8
static CPU_STK Usart0ReceiveTaskStack[USART0_RECEIVE_TASK_STACK_LEN];
static _SysMsg *Usart0ReceiveMsg;
static OS_SEM Usart0ReceiveSemaphore;


bool PHY_UsartSend(uint8 channel, uint8 *buff, uint16 len);

/*----------------外部接口函数----------------*/
extern void App_MicRS485ReceiveHandler(_SysMsg *msg, _TxBuff *SrcMsg, uint8 result);
extern void App_PlcRS485ReceiveHandler(_SysMsg *msg, _TxBuff *SrcMsg, uint8 result);
extern void IF_CmdServer(_SysMsg *msg);

/*******************************************************************************
*功能：申请串口2发送缓存
*参数：无
*返回：无
*说明：
*******************************************************************************/
static _TxBuff* Fun_MallocUsart2SendBuff(void)
{
    uint8 i;
    CPU_SR_ALLOC();

    for (i = 0; i < MAX_LEUART1_SEND_BUFF_NUM; i++)
    {
        if (Leuart1SendTask.TxBuff[i].IsUsed == false)
        {
            CPU_CRITICAL_ENTER();
            Leuart1SendTask.TxBuff[i].IsUsed = true;
            CPU_CRITICAL_EXIT();

            return &Leuart1SendTask.TxBuff[i];
        }
    }

    return NULL;
}

/*******************************************************************************
*功能：释放串口2发送缓存
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ReleaseUsart2SendBuff(uint8 opt)
{
    uint8 index;
    CPU_SR_ALLOC();

    index = Leuart1SendTask.SendIndex;

    switch (opt)
    {
        case SEND_MSG_FAIL:
            {
                if (index != 0xFF)
                {
                    App_MicRS485ReceiveHandler(NULL, &Leuart1SendTask.TxBuff[index], SEND_MSG_FAIL);
                    App_PlcRS485ReceiveHandler(NULL, &Leuart1SendTask.TxBuff[index], SEND_MSG_FAIL);

                    Leuart1SendTask.TxBuff[Leuart1SendTask.SendIndex].len = 0;
                    Leuart1SendTask.TxBuff[Leuart1SendTask.SendIndex].type = 0;

                    CPU_CRITICAL_ENTER();
                    Leuart1SendTask.TxBuff[Leuart1SendTask.SendIndex].IsUsed = false;
                    Leuart1SendTask.SendIndex = 0xFF;
                    CPU_CRITICAL_EXIT();

                    return;
                }

                break;
            }
        case SEND_MSG_SUCCESS:
            {
                if (index != 0xFF)
                {
                    CPU_CRITICAL_ENTER();
                    Leuart1SendTask.TxBuff[index].IsUsed = false;
                    Leuart1SendTask.SendIndex = 0xFF;
                    CPU_CRITICAL_EXIT();

                    return;
                }

                break;
            }
        case RECEIVE_ERR:
            {
                break;
            }
        default:
            {
                break;
            }
    }

    return;
}
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_Leuart1SendNextMsg(void)
{
    uint8 i, j;
    bool result;

    /*纠错处理*/
    if (Leuart1SendTask.SendIndex >= MAX_LEUART1_SEND_BUFF_NUM)
    {
        Leuart1SendTask.SendIndex = 0;
    }

    result = false;

    Leuart1SendTask.SendIndex++;
    Leuart1SendTask.SendIndex = Leuart1SendTask.SendIndex % MAX_LEUART1_SEND_BUFF_NUM;

    for (i = 0, j = Leuart1SendTask.SendIndex; i < MAX_LEUART1_SEND_BUFF_NUM; i++, j++, j = j % MAX_LEUART1_SEND_BUFF_NUM)
    {
        if (Leuart1SendTask.TxBuff[j].IsUsed == true)
        {
            Leuart1SendTask.SendIndex = j;
            result = true;

            break;
        }
    }

    /*纠错处理*/
    if (Leuart1SendTask.SendIndex >= MAX_LEUART1_SEND_BUFF_NUM)
    {
        result = false;
    }

    if (result == false)
    {
        Leuart1SendTask.SendIndex = 0xFF;
    }

    return result;
}

/*******************************************************************************
*功能：判断串口是否接收完成
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_UsartReveiveDone(_SysMsg *msg)
{
    OS_ERR err;
    CPU_SR_ALLOC();

    if (msg == NULL)
    {
        return false;
    }

    switch (msg->port)
    {
        case USART_232_CHANNEL:
            {
                if (Leuart0ReceiveMsg != NULL)
                {
                    Debug_Statics(offsetof(_Statics, L0ProSlow), 1, sizeof(statics.L0ProSlow));
                    OSSemPost(&Leuart0ReceiveSemaphore, OS_OPT_POST_FIFO, &err);

                    break;
                }

                CPU_CRITICAL_ENTER();
                Leuart0ReceiveMsg = msg;
                CPU_CRITICAL_EXIT();

                OSSemPost(&Leuart0ReceiveSemaphore, OS_OPT_POST_FIFO, &err);

                break;
            }
        case USART_485_CHANNEL:
            {
                if (Leuart1ReceiveMsg != NULL)
                {
                    Debug_Statics(offsetof(_Statics, L1ProSlow), 1, sizeof(statics.L1ProSlow));
                    OSSemPost(&Leuart1ReceiveSemaphore, OS_OPT_POST_FIFO, &err);

                    break;
                }

                CPU_CRITICAL_ENTER();
                Leuart1ReceiveMsg = msg;
                CPU_CRITICAL_EXIT();

                OSSemPost(&Leuart1ReceiveSemaphore, OS_OPT_POST_FIFO, &err);

                break;
            }
        case  USART_GPRS_CHANNEL:
            {
                if (Usart0ReceiveMsg != NULL)
                {
                    Debug_Statics(offsetof(_Statics, U0ProSlow), 1, sizeof(statics.U0ProSlow));
                    OSSemPost(&Usart0ReceiveSemaphore, OS_OPT_POST_1, &err);
                    break;
                }

                CPU_CRITICAL_ENTER();
                Usart0ReceiveMsg = msg;
                CPU_CRITICAL_EXIT();

                OSSemPost(&Usart0ReceiveSemaphore, OS_OPT_POST_1, &err);

                break;
            }
        default:
            {
                return false;
            }
    }

    return true;
}

/*******************************************************************************
*功能：Leuart0接收任务处理函数
*参数：无
*返回：无
*说明：RS232接收处理
*******************************************************************************/
void Fun_Leuart0ReceiveTaskProcess(void *p_arg)
{
    OS_ERR err;
    CPU_SR_ALLOC();

    while (true)
    {
        OSSemPend(&Leuart0ReceiveSemaphore,
                  (OS_TICK)0,
                  OS_OPT_PEND_BLOCKING,
                  (CPU_TS)NULL,
                  &err);

        if (Leuart0ReceiveMsg == NULL)
        {
            err = OS_ERR_NONE;
            continue;
        }

        IF_CmdServer(Leuart0ReceiveMsg);

        CPU_CRITICAL_ENTER();
        Leuart0ReceiveMsg = NULL;
        CPU_CRITICAL_EXIT();
    }
}

/*******************************************************************************
*功能：Leuart1接收任务处理函数
*参数：无
*返回：无
*说明：RS485接收处理
*******************************************************************************/
void Fun_Leuart1ReceiveTaskProcess(void *p_arg)
{
    OS_ERR err;
    bool result;
    CPU_SR_ALLOC();

    while (true)
    {
        OSSemPend(&Leuart1ReceiveSemaphore,
                  (OS_TICK)0,
                  OS_OPT_PEND_BLOCKING,
                  (CPU_TS)NULL,
                  &err);

        result = false;

        if (Leuart1ReceiveMsg == NULL)
        {
            err = OS_ERR_NONE;
            continue;
        }

        Debug_Printf("RS485 Rx:", Leuart1ReceiveMsg->buff, Leuart1ReceiveMsg->len, 1, 0);

        if (Leuart1SendTask.SendIndex != 0xFF)
        {
            if (CPR_ReceivedProcess(Leuart1ReceiveMsg) == true)
            {
                if (Leuart1ReceiveMsg->protocol == PROTOCOL_MODBUS)
                {
                    result = true;

                    App_MicRS485ReceiveHandler(Leuart1ReceiveMsg, &Leuart1SendTask.TxBuff[Leuart1SendTask.SendIndex], SEND_MSG_SUCCESS);
                    App_PlcRS485ReceiveHandler(Leuart1ReceiveMsg, &Leuart1SendTask.TxBuff[Leuart1SendTask.SendIndex], SEND_MSG_SUCCESS);
                    Fun_ReleaseUsart2SendBuff(SEND_MSG_SUCCESS);
                }
            }
        }

        if (result == false)
        {
            Fun_ReleaseUsart2SendBuff(RECEIVE_ERR);
        }

        CPU_CRITICAL_ENTER();
        Leuart1ReceiveMsg = NULL;
        CPU_CRITICAL_EXIT();
    }
}

/*******************************************************************************
*功能：Leuart1发送任务处理函数
*参数：无
*返回：无
*说明：RS485
*******************************************************************************/
void Fun_Leuart1SendTaskProcess(void *p_arg)
{
    OS_ERR err;
    uint8 index;
    CPU_TS ts;

    while (true)
    {
        OSSemPend(&Leuart1SendSemaphore,
                  (OS_TICK)0,
                  OS_OPT_PEND_BLOCKING,
                  &ts,
                  &err);

        while (Fun_Leuart1SendNextMsg() == true)
        {
            index = Leuart1SendTask.SendIndex;
            PHY_UsartSend(USART_485_CHANNEL, (uint8 *)&Leuart1SendTask.TxBuff[index].buff, Leuart1SendTask.TxBuff[index].len);
            OSTimeDly(USART_RX_OVER_TIME, OS_OPT_TIME_DLY, &err);

            /*执行三次重传操作*/
            if (Leuart1SendTask.SendIndex != 0xFF)
            {
                PHY_UsartSend(USART_485_CHANNEL, (uint8 *)&Leuart1SendTask.TxBuff[index].buff, Leuart1SendTask.TxBuff[index].len);
                OSTimeDly(USART_RX_OVER_TIME, OS_OPT_TIME_DLY, &err);

                if (Leuart1SendTask.SendIndex != 0xFF)
                {
                    PHY_UsartSend(USART_485_CHANNEL, (uint8 *)&Leuart1SendTask.TxBuff[index].buff, Leuart1SendTask.TxBuff[index].len);
                    OSTimeDly(USART_RX_OVER_TIME, OS_OPT_TIME_DLY, &err);
                }

                /*三次发送后，依然没有接收到响应*/
                if (Leuart1SendTask.SendIndex != 0xFF)
                {
                    Fun_ReleaseUsart2SendBuff(SEND_MSG_FAIL);
                }
            }
        }
    }
}

/*******************************************************************************
*功能：Usart0接收任务处理函数
*参数：无
*返回：无
*说明：gprs
*******************************************************************************/
void Fun_Usart0ReceiveTaskProcess(void *p_arg)
{
    OS_ERR err;
    CPU_SR_ALLOC();

    while (true)
    {
        OSSemPend(&Usart0ReceiveSemaphore,
                  (OS_TICK)0,
                  OS_OPT_PEND_BLOCKING,
                  (CPU_TS)NULL,
                  &err);

        if (Usart0ReceiveMsg == NULL)
        {
            err = OS_ERR_NONE;

            continue;
        }

        PHY_GprsReceivedDone(Usart0ReceiveMsg);

        CPU_CRITICAL_ENTER();
        Usart0ReceiveMsg = NULL;
        CPU_CRITICAL_EXIT();
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_UsartSendDone(uint8 channel)
{
    switch (channel)
    {
        case USART_232_CHANNEL:
            {
                break;
            }
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_UsartSend(uint8 channel, uint8 *buff, uint16 len)
{
    return Dri_UsartSend(buff, len, channel);
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_UsartSendMsg(_SysMsg *msg)
{
    _TxBuff *TxBuff;
    _TxBuff *SendMsg;
    OS_ERR err;

    if (CPR_SendProcess(msg) == false)
    {
        return false;
    }

    switch (msg->port)
    {
        case USART_485_CHANNEL:
            {
                if (msg->len >= sizeof(TxBuff->buff))
                {
                    OSSemPost(&Leuart1SendSemaphore, OS_OPT_POST_1, &err);

                    return false;
                }

                TxBuff = Fun_MallocUsart2SendBuff();
                if (TxBuff == NULL)
                {
                    Debug_Statics(offsetof(_Statics, No485SendBuff), 1, sizeof(statics.No485SendBuff));

                    OSSemPost(&Leuart1SendSemaphore, OS_OPT_POST_1, &err);

                    return false;
                }

                SendMsg = (_TxBuff *)msg->buff;

                memcpy(&TxBuff->buff, &SendMsg->buff, msg->len);
                TxBuff->len = msg->len;
                TxBuff->type = SendMsg->type;

                OSSemPost(&Leuart1SendSemaphore, OS_OPT_POST_1, &err);

                break;
            }
        default:
            {
                return Dri_UsartSend(msg->buff, msg->len, msg->port);
            }
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_UsartInit(void)
{
    OS_ERR err;
    uint8 i;

    /*Leuart0（RS232）串口接收任务初始化*/
    OSSemCreate(&Leuart0ReceiveSemaphore, NULL, 0, &err);

    Leuart0ReceiveMsg = NULL;

    OSTaskCreate((OS_TCB    *)&Leuart0ReceiveTaskId,                 /* Create the start task                                */
                 (CPU_CHAR  *)NULL,
                 (OS_TASK_PTR)Fun_Leuart0ReceiveTaskProcess,
                 (void      *)0,
                 (OS_PRIO)TASK_LEUART0_RX_PRIORITY,
                 (CPU_STK   *)&Leuart0ReceiveTaskStack[0],
                 (CPU_STK_SIZE)LEUSART0_RECEIVE_TASK_STACK_LEN / 10,
                 (CPU_STK_SIZE)LEUSART0_RECEIVE_TASK_STACK_LEN,
                 (OS_MSG_QTY)0,
                 (OS_TICK)0,
                 (void      *)0,
                 (OS_OPT)(OS_OPT_TASK_STK_CHK),
                 (OS_ERR    *)&err);

    /*LeUart1（RS485）串口接收任务初始化*/
    OSSemCreate(&Leuart1ReceiveSemaphore, NULL, 0, &err);

    OSTaskCreate((OS_TCB    *)&Leuart1ReceiveTaskId,                 /* Create the start task                                */
                 (CPU_CHAR  *)NULL,
                 (OS_TASK_PTR)Fun_Leuart1ReceiveTaskProcess,
                 (void      *)0,
                 (OS_PRIO)TASK_LEUART1_RX_PRIORITY,
                 (CPU_STK   *)&Leuart1ReceiveTaskStack[0],
                 (CPU_STK_SIZE)LEUSART1_RECEIVE_TASK_STACK_LEN / 10,
                 (CPU_STK_SIZE)LEUSART1_RECEIVE_TASK_STACK_LEN,
                 (OS_MSG_QTY)0,
                 (OS_TICK)0,
                 (void      *)0,
                 (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR    *)&err);

    /*Leuart1（RS485）串口发送任务初始化*/
    OSSemCreate(&Leuart1SendSemaphore, NULL, 0, &err);

    OSTaskCreate((OS_TCB    *)&Leuart1SendTaskId,                 /* Create the start task                                */
                 (CPU_CHAR  *)NULL,
                 (OS_TASK_PTR)Fun_Leuart1SendTaskProcess,
                 (void      *)0,
                 (OS_PRIO)TASK_LEUART1_TX_PRIORITY,
                 (CPU_STK   *)&Leuart1SendTaskStack[0],
                 (CPU_STK_SIZE)LEUSART1_SEND_TASK_STACK_LEN / 10,
                 (CPU_STK_SIZE)LEUSART1_SEND_TASK_STACK_LEN,
                 (OS_MSG_QTY)0,
                 (OS_TICK)0,
                 (void      *)0,
                 (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR    *)&err);


    /*初始化Leuart1发送缓存*/
    Leuart1SendTask.SendIndex = 0xFF;
    for (i = 0; i < MAX_LEUART1_SEND_BUFF_NUM; i++)
    {
        Leuart1SendTask.TxBuff[i].IsUsed = false;
        Leuart1SendTask.TxBuff[i].len = 0;
        Leuart1SendTask.TxBuff[i].type = 0;
    }

    /*Usart0（Gprs）串口接收任务初始化*/
    OSSemCreate(&Usart0ReceiveSemaphore, NULL, 0, &err);

    OSTaskCreate((OS_TCB    *)&Usart0ReceiveTaskId,                 /* Create the start task                                */
                 (CPU_CHAR  *)NULL,
                 (OS_TASK_PTR)Fun_Usart0ReceiveTaskProcess,
                 (void      *)0,
                 (OS_PRIO)TASK_USART0_RX_PRIORITY,
                 (CPU_STK   *)&Usart0ReceiveTaskStack[0],
                 (CPU_STK_SIZE)USART0_RECEIVE_TASK_STACK_LEN / 10,
                 (CPU_STK_SIZE)USART0_RECEIVE_TASK_STACK_LEN,
                 (OS_MSG_QTY)0,
                 (OS_TICK)0,
                 (void      *)0,
                 (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR    *)&err);

    return;
}
/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年05月12日   ggggggg      创建文件
*******************************************************************************/

