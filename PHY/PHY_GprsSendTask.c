/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年11月15日
**  名   称：PHY_GprsSendTask.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "PHY_GprsSendTask.h"
#include "PHY_Gprs.h"
#include "PHY_Reset.h"

#include "debug.h"

#define GPRS_SEND_TASK_STACK_LEN          (256)

/*发送任务管理模块*/
typedef struct _Gprs_SendTask
{
    _GprsSendBuff msg[MAX_GPRS_SEND_BUFF_NUM];                                  //发送报文缓存
    uint8 index;                                                                //指示当前正在发送的缓存
}_Gprs_SendTask;

static _Gprs_SendTask GprsSendTask;                                             //Gprs发送缓存
static OS_TCB GprsSendTaskId;
#pragma data_alignment=8
static CPU_STK GprsSendTaskStack[GPRS_SEND_TASK_STACK_LEN];
static OS_TMR SendBuffOverTimeTmrId;                                            //发送缓存超时监控定时器
static OS_SEM GprsSendTaskSemaphore;                                            //发送任务信号量
static uint16 SendIndex;

extern _Gprs Gprs;

/*局部函数定义*/
static void Fun_GprsSendResultProcess(uint8 index, uint8 result);
static void Fun_ReleaseGprsSendBuff(_GprsSendBuff *TxBuff);

extern bool App_MicGprsSendResultHandler(_GprsSendBuff *TxBuff, uint8 SendResult);
extern bool App_PlcGprsSendResultHandler(_GprsSendBuff *TxBuff, uint8 SendResult);
extern void App_ReportExceptionToServer(uint16 ExceptionNum, uint8 *buff, uint16 len);

/*----------------统计分析部分---------------*/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Fun_StaticsSend(uint16 cmd)
{
    switch (cmd)
    {
        case CMD_HEART_BEACON:                                                  //发送心跳
        {
            Debug_Statics(offsetof(_Statics, beat), 1, sizeof(statics.beat));

            break;
        }
        case CMD_REGISTER_TO_SERVER:                                            //发送注册
        {
            Debug_Statics(offsetof(_Statics, Register), 1, sizeof(statics.Register));

            break;
        }
        case CMD_SET_WORK_TASK_REPORT:
        {
            Debug_Statics(offsetof(_Statics, TaskReport), 1, sizeof(statics.TaskReport));

            break;
        }
        case CMD_SET_WORK_TASK_RESPONSE:
        {
            Debug_Statics(offsetof(_Statics, ResTask), 1, sizeof(statics.ResTask));

            break;
        }
        case CMD_RESET_EXCEPTION_RESPONSE:
        {
            Debug_Statics(offsetof(_Statics, ResResetException), 1, sizeof(statics.ResResetException));

            break;
        }
        case CMD_WRITE_CONFIG_RESPONSE:
        {
            Debug_Statics(offsetof(_Statics, ResWtCfg), 1, sizeof(statics.ResWtCfg));

            break;
        }
        case CMD_FORWARD_TO_BIND_DTU:
        {
            Debug_Statics(offsetof(_Statics, forward), 1, sizeof(statics.forward));

            break;
        }
        case CMD_FORWARD_TO_BIND_DTU_RESPOSE:
        {
            Debug_Statics(offsetof(_Statics, MicRxServerForwardRes), 1, sizeof(statics.MicRxServerForwardRes));

            break;
        }
        case CMD_READ_CONFIG_RESPONSE:
        {
            break;
        }
        case CMD_PRIVILEGE_CMD_RESPOSE:
        {
            Debug_Statics(offsetof(_Statics, ResPrivilege), 1, sizeof(statics.ResPrivilege));

            break;
        }
        case CMD_CANCEL_PRIVILEGE_CMD_RESPOSE:
        {
            Debug_Statics(offsetof(_Statics, ResCancelPrivilege), 1, sizeof(statics.ResCancelPrivilege));

            break;
        }
        case CMD_REPORT_EXCEPTION:                                              //发送异常
        {
            Debug_Statics(offsetof(_Statics, ReportException), 1, sizeof(statics.ReportException));

            break;
        }
    }

    return;
}

/*******************************************************************************
*功能：清空发送缓存
*参数：无
*返回：无
*说明：发送缓存超时定时器
*******************************************************************************/
static void Fun_SendBuffOverTimeTmrHandler(void *id, void *p_arg)
{
    uint8 i;
    uint32 tick;
    OS_ERR err;

    if (id != &SendBuffOverTimeTmrId)
    {
        PHY_Reset(0);

        return;
    }

    tick = OSTimeGet(&err);

    /*检查发送缓存，超时则清空*/
    for (i = 0; i < MAX_GPRS_SEND_BUFF_NUM; i++)
    {
        if (GprsSendTask.msg[i].state != BUFF_IDLE)
        {
            if (abs(tick - GprsSendTask.msg[i].SendStamp) > GPRS_TMR_SEND_DATA_OVER_TIME)
            {
                if (GprsSendTask.msg[i].ResendNum == 0
                    && GprsSendTask.msg[i].ResendTime == 0)
                {
                    Fun_GprsSendResultProcess(i, SEND_MSG_FAIL);
                }
            }
        }
    }

    return;
}

/*******************************************************************************
*功能：gprs重传定时器回调函数
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_GprsResendTmrHanler(void *p_tmr, void *p_arg)
{
    _GprsSendBuff *TxBuff;
    OS_ERR err;
    uint8 i;

    for (i = 0; i < MAX_GPRS_SEND_BUFF_NUM; i++)
    {
        if (&GprsSendTask.msg[i].ResendTmr == p_tmr)
        {
            break;
        }
    }

    if (i >= MAX_GPRS_SEND_BUFF_NUM)
    {
        return;
    }

    TxBuff = &GprsSendTask.msg[i];

    if (TxBuff->ResendNum > 0)
    {
        TxBuff->ServerIndex = TxBuff->server;
        TxBuff->state = BUFF_SEND_BODY_MSG;

        /*投递信号量，通知发送任务*/
        OSSemPost(&GprsSendTaskSemaphore, OS_OPT_POST_1, &err);
    }
    else
    {
        /*重传类型的报文，重传次数耗尽，通知发送方，清空缓存*/
        Fun_GprsSendResultProcess(i, SEND_MSG_FAIL);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 Fun_GetSendServerIndex(_GprsSendBuff *msg)
{
    uint8 SendServerSeq;
    uint8 i;

    SendServerSeq = 0xFF;

    for (i = 0; i < MAX_SERVER_NUM; i++)
    {
        if (msg->ServerIndex & ((uint16)1 << i))
        {
            SendServerSeq = i;
            msg->ServerIndex &= ~((uint16)1 << i);

            break;
        }
    }

    return SendServerSeq;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_GprsSendTaskHandler(void *p_arg)
{
    uint8 i, j;
    OS_ERR err;
    _IF *msg;
    bool PendTask;

    PendTask = true;

    while (true)
    {
        if (PendTask == true)
        {
            OSSemPend(&GprsSendTaskSemaphore, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        }

        PendTask = false;

        OSTimeDly(2 * SYS_SECOND_TICK_NUM, OS_OPT_TIME_DLY, &err);

        SendIndex = SendIndex % MAX_GPRS_SEND_BUFF_NUM;

        for (i = SendIndex, j = 0;
             j < MAX_GPRS_SEND_BUFF_NUM;
             i++, j++)
        {
            i = i % MAX_GPRS_SEND_BUFF_NUM;

            /*不需要重传、别需要等待响应的报文，将其清空*/
            if (GprsSendTask.msg[i].state != BUFF_IDLE
                && GprsSendTask.msg[i].ResendNum == 0
                && GprsSendTask.msg[i].NeedResponse == false)
            {
                Fun_ReleaseGprsSendBuff(&GprsSendTask.msg[i]);
            }

            /*发送常规数据*/
            if (GprsSendTask.msg[i].state == BUFF_SEND_BODY_MSG
                && GprsSendTask.msg[i].ResendNum > 0)
            {
                SendAtCmd = false;
                PHY_GprsCIPSend(&GprsSendTask.msg[i]);
                SendAtCmd = true;

                GprsSendTask.msg[i].SendStamp = OSTimeGet(&err);
                GprsSendTask.msg[i].state = BUFF_WAIT_SERVER_RESPONSE;

                if (GprsSendTask.msg[i].ResendNum > 0)
                {
                    GprsSendTask.msg[i].ResendNum--;
                }

                GprsSendTask.index = i;

                break;
            }
        }

        SendIndex = i;

        if (j >= MAX_GPRS_SEND_BUFF_NUM)
        {
            GprsSendTask.index = 0xFF;

            PendTask = true;
        }
        else
        {
            PendTask = false;

            /*根据发送内容进行统计*/
            msg = (_IF *)&GprsSendTask.msg[i].buff;
            Fun_StaticsSend(msg->head.cmd);
            /*测试结束*/
        }
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_GprsSendResultProcess(uint8 index, uint8 result)
{
    if (index >= MAX_GPRS_SEND_BUFF_NUM)
    {
        return;
    }

    if (result == SEND_MSG_FAIL)
    {
        if (GprsSendTask.msg[index].ResendTime == 0 || GprsSendTask.msg[index].ResendNum == 0)
        {
            if (App_MicGprsSendResultHandler(&GprsSendTask.msg[index], SEND_MSG_FAIL) == true)
            {
                return;
            }

            if (App_PlcGprsSendResultHandler(&GprsSendTask.msg[index], SEND_MSG_FAIL) == true)
            {
                return;
            }

            Fun_ReleaseGprsSendBuff(&GprsSendTask.msg[index]);
        }
    }

    /*成功指的是接收到server的响应*/
    if (result == SEND_MSG_SUCCESS)
    {
        App_MicGprsSendResultHandler(&GprsSendTask.msg[index], SEND_MSG_SUCCESS);
        App_PlcGprsSendResultHandler(&GprsSendTask.msg[index], SEND_MSG_SUCCESS);

        Fun_ReleaseGprsSendBuff(&GprsSendTask.msg[index]);
    }

    return;
}


/*******************************************************************************
*功能：释放GPRS发送缓存
*参数：无
*返回：无
*说明：
*******************************************************************************/
static void Fun_ReleaseGprsSendBuff(_GprsSendBuff *TxBuff)
{
    OS_ERR err;
    CPU_SR_ALLOC();

    OSTmrStop(&TxBuff->ResendTmr, OS_OPT_TMR_NONE, NULL, &err);

    TxBuff->cmd = 0xFF;
    TxBuff->len = 0;
    TxBuff->ResendNum = 0;
    TxBuff->ResendTime = 0;
    TxBuff->ResendTmr.Dly = 60 * SYS_SECOND_TICK_NUM;
    TxBuff->ResendTmr.Period = 5 * 60 * SYS_SECOND_TICK_NUM;

    TxBuff->SendServerSeq =  0xFE;
    TxBuff->ServerIndex = 0xFE;

    CPU_CRITICAL_ENTER();
    TxBuff->state = BUFF_IDLE;
    CPU_CRITICAL_EXIT();

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_GprsOnlineEvent(uint8 server)
{
    uint8 i;
    _IF msg;
    uint16 len;

    for (i = 0; i < MAX_GPRS_SEND_BUFF_NUM; i++)
    {
        if (GprsSendTask.msg[i].cmd == CMD_REGISTER_TO_SERVER
            && GprsSendTask.msg[i].state == BUFF_WAIT_SERVER_RESPONSE)
        {
            Fun_ReleaseGprsSendBuff(&GprsSendTask.msg[i]);

            break;
        }
    }

    msg.head.version = CMD_VERSION;
    msg.head.cmd = CMD_REGISTER_TO_SERVER;
    msg.head.len = Lib_htons(sizeof(Gprs.IMEI));

    memcpy(msg.buff, Gprs.IMEI, sizeof(Gprs.IMEI));

    len = offsetof(_IF, buff) + sizeof(Gprs.IMEI);
    PHY_RequestGprsSendMsg((uint8 *)&msg, len, CMD_REGISTER_TO_SERVER, server, 5, 20, true);

    if (SysParameterException == true)
    {
        App_ReportExceptionToServer(EXCEPTION_PARAMETER, NULL, 0);
    }

    return;
}

/*******************************************************************************
*功能：清空所有gprs发送缓存
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_ResumeGprsSendBuff(void)
{
    uint16 i;

    for (i = 0; i < MAX_GPRS_SEND_BUFF_NUM; i++)
    {
        Fun_ReleaseGprsSendBuff(&GprsSendTask.msg[i]);
    }

    return;
}

/*******************************************************************************
*功能：心跳响应
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_HeartBeaconResponse(_IF *SrcMsg, uint16 len)
{
    _GprsSendBuff *TxBuff;

    if (SrcMsg->head.len != 0)
    {
        return;
    }

    SrcMsg->head.len = Lib_htons(SrcMsg->head.len);
    if (len != SrcMsg->head.len + offsetof(_IF, buff))
    {
        return;
    }

    TxBuff = PHY_SearchGprsSendBuff(CMD_HEART_BEACON, NULL, 0);

    Debug_Statics(offsetof(_Statics, ResBeat), 1, sizeof(statics.ResBeat));

    if (TxBuff == NULL)
    {
        return;
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_RequestGprsSendMsg(uint8 *buff, uint16 len, uint8 cmd, uint8 server, uint8 ResendNum, uint16 ResendTime, bool NeedResponse)
{
    bool result;
    OS_ERR err;
    uint8 i;
    uint16 ServerIndex;
    CPU_SR_ALLOC();

    /*投递信号量，通知发送任务*/
    OSSemPost(&GprsSendTaskSemaphore, OS_OPT_POST_1, &err);

    if (!(server == 0xFF || server < MAX_SERVER_NUM))
    {
        result = false;

        Debug_Statics(offsetof(_Statics, GprsSendParaErr), 1, sizeof(statics.GprsSendParaErr));

        return result;
    }


    if (len >= MAX_GPRS_TX_BUFF_LEN)
    {
        Debug_Statics(offsetof(_Statics, GprsSendParaErr), 1, sizeof(statics.GprsSendParaErr));

        return false;
    }

    result = false;
    ServerIndex = 0;

    /*定发送的服务器索引*/
    if (server == 0xFF)
    {
        for (i = 0; i < MAX_SERVER_NUM; i++)
        {
            if (Gprs.server[i].connected == true)
            {
                ServerIndex |= (uint16)1 << i;
                result = true;
            }
        }
    }
    else
    {
        if (server >= MAX_SERVER_NUM)
        {
            Debug_Statics(offsetof(_Statics, GprsSendParaErr), 1, sizeof(statics.GprsSendParaErr));

            return false;
        }

        ServerIndex = 1 << server;
    }

    /*遍历发送缓存：投递*/
    for (i = 0; i < MAX_GPRS_SEND_BUFF_NUM; i++)
    {
        if (GprsSendTask.msg[i].state == BUFF_IDLE)
        {
            memset(GprsSendTask.msg[i].buff, 0x00, sizeof(GprsSendTask.msg[i].buff));

            memcpy(GprsSendTask.msg[i].buff, buff, len);
            GprsSendTask.msg[i].len = len;

            GprsSendTask.msg[i].NeedResponse = NeedResponse;
            GprsSendTask.msg[i].cmd = cmd;
            GprsSendTask.msg[i].ResendNum = ResendNum;
            GprsSendTask.msg[i].ResendTime = ResendTime;
            GprsSendTask.msg[i].ServerIndex = ServerIndex;
            GprsSendTask.msg[i].server = ServerIndex;

            CPU_CRITICAL_ENTER();
            GprsSendTask.msg[i].state = BUFF_SEND_BODY_MSG;
            CPU_CRITICAL_EXIT();

            break;
        }
    }

    /*获取发送缓存失败，无空闲发送缓存*/
    if (i >= MAX_GPRS_SEND_BUFF_NUM)
    {
        Debug_Statics(offsetof(_Statics, NoGprsSendBuff), 1, sizeof(statics.NoGprsSendBuff));

        return false;
    }
    else
    {
        OSTmrStop(&GprsSendTask.msg[i].ResendTmr, OS_OPT_TMR_NONE, NULL, &err);

        if (ResendNum > 1 && ResendTime > 0)
        {
            GprsSendTask.msg[i].ResendTmr.Dly = ResendTime * SYS_SECOND_TICK_NUM;
            GprsSendTask.msg[i].ResendTmr.Period = ResendTime * SYS_SECOND_TICK_NUM;

            OSTmrStart(&GprsSendTask.msg[i].ResendTmr, &err);
        }

        return true;
    }
}

/*******************************************************************************
*功能：搜索gprs发送缓存
*参数：无
*返回：无
*说明：接收到报文的预期响应后，删除源报文
*******************************************************************************/
_GprsSendBuff* PHY_SearchGprsSendBuff(uint8 CmdNum, uint8 *buff, uint16 len)
{
    uint8 i;
    _IF *cmd;

    for (i = 0; i < MAX_GPRS_SEND_BUFF_NUM; i++)
    {
        if (GprsSendTask.msg[i].cmd == CmdNum && GprsSendTask.msg[i].state == BUFF_WAIT_SERVER_RESPONSE)
        {
            if(buff == NULL)
            {
                Fun_GprsSendResultProcess(i, SEND_MSG_SUCCESS);
                
                return &GprsSendTask.msg[i];
            }
            else
            {
                cmd = (_IF *)GprsSendTask.msg[i].buff;
                if(memcmp(buff, cmd->buff, len) == 0)
                {
                    Fun_GprsSendResultProcess(i, SEND_MSG_SUCCESS);
                
                    return &GprsSendTask.msg[i];
                }
                else
                {
                    continue;
                }
            }

        }
    }

    /*异常：统计*/
    Debug_Statics(offsetof(_Statics, SearchGprsBuffErr), 1, sizeof(statics.SearchGprsBuffErr));

    return NULL;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_GprsSendTaskInit(void)
{
    uint8 i;
    OS_ERR err;

    /*初始化gprs发送任务*/
    memset(&GprsSendTask, 0x00, sizeof(GprsSendTask));

    GprsSendTask.index = 0xFF;

    for (i = 0; i < MAX_GPRS_SEND_BUFF_NUM; i++)
    {
        GprsSendTask.msg[i].len = 0;
        GprsSendTask.msg[i].ServerIndex = 0xFF;
        GprsSendTask.msg[i].SendServerSeq =  -1;
        GprsSendTask.msg[i].state = BUFF_IDLE;

        GprsSendTask.msg[i].cmd = 0xFF;
        GprsSendTask.msg[i].ResendNum = 0;

        /*
        重传周期：5分钟
        */
        OSTmrCreate(&GprsSendTask.msg[i].ResendTmr,
                    NULL,
                    3 * SYS_SECOND_TICK_NUM,
                    5 * 60 * SYS_SECOND_TICK_NUM,
                    OS_OPT_TMR_PERIODIC,
                    Fun_GprsResendTmrHanler,
                    NULL,
                    &err);
    }

    OSTmrCreate(&SendBuffOverTimeTmrId,
                NULL,
                1 * SYS_SECOND_TICK_NUM,
                1 * SYS_SECOND_TICK_NUM,
                OS_OPT_TMR_PERIODIC,
                Fun_SendBuffOverTimeTmrHandler,
                NULL,
                &err);
    OSTmrStart(&SendBuffOverTimeTmrId, &err);

    /*gprs发送任务初始化*/
    OSSemCreate(&GprsSendTaskSemaphore, NULL, 0, &err);

    OSTaskCreate((OS_TCB    *)&GprsSendTaskId,                 /* Create the start task                                */
                 (CPU_CHAR  *)NULL,
                 (OS_TASK_PTR)Fun_GprsSendTaskHandler,
                 (void      *)0,
                 (OS_PRIO)TASK_GPRS_SEND_PRIORITY,
                 (CPU_STK   *)&GprsSendTaskStack[0],
                 (CPU_STK_SIZE)GPRS_SEND_TASK_STACK_LEN / 10,
                 (CPU_STK_SIZE)GPRS_SEND_TASK_STACK_LEN,
                 (OS_MSG_QTY)0,
                 (OS_TICK)0,
                 (void      *)0,
                 (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR    *)&err);

    SendIndex = 0;

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年11月15日    巍图      创建文件
*******************************************************************************/
