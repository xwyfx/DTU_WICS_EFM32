/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**  作   者：巍图
**  日   期：2016年05月10日
**  名   称：PHY_Flash.c
**  摘   要：
*******************************************************************************/
#include "system.h"

#include "Dri_Led.h"
#include "Dri_I2C.h"
#include "Dri_Timer.h"
#include "Dri_Usart.h"
#include "Dri_WatchDog.h"
#include "Dri_InterFlash.h"


/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_WriteExternFlash(uint32 addr, uint8 *buff, uint16 len)
{
    bool result;

    result = false;

    result = Dri_I2CWrite(addr, buff, len);

    return result;
}

/*--每页长度为264字节------*/
#define CONFIGED                          0x5A5A
#define ADDR_MAIN_DEVICE_TYPE             0                                           //主设备类型
#define ADDR_SERVER_0_IP                  50
#define ADDR_SERVER_0_DOMAIN              100
#define ADDR_SERVER_0_PORT                150
#define ADDR_SERVER_1_IP                  200
#define ADDR_SERVER_1_DOMAIN              300
#define ADDR_SERVER_1_PORT                350
#define ADDR_BIND_DTU_NUM                 400
#define ADDR_BIND_VALVE_NUM               450
#define ADDR_PUMP_EXCETION_LIMIT          500
#define ADDR_CIRCUIT_PROTECT_EXCEPTIONLIMIT 550
#define ADDR_VALVE_EXCEPTION_LIMIT        600
#define ADDR_FLOWMETER_EXCEPTION_LIMIT    650
#define ADDR_CONNECTED_PUMP               700
#define ADDR_CONNECTED_MUTUAL_INDUCTOR    750
#define ADDR_BIND_DTU_LIST                800
#define ADDR_RESET_COUNTER                1058
#define ADDR_SERVER_INDEX                 1100
#define ADDR_UPDATE_VERSION               1150

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_GetCfg(uint32 addr, uint8 *buff, uint8 len)
{
    uint16 IsConfig;

    Dri_I2CRead(addr, buff, len);
    Dri_I2CRead(addr + len, (uint8 *)&IsConfig, 2);

    if (IsConfig != CONFIGED)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
static bool Fun_SetCfg(uint32 addr, uint8 *buff, uint8 len)
{
    uint16 IsConfig;
    uint8 data[255];

    if (len >= sizeof(data))
    {
        return false;
    }

    IsConfig = CONFIGED;

    memcpy(data, buff, len);
    memcpy(&data[len], (uint8 *)&IsConfig, 2);
    Dri_I2CWrite(addr, data, len + 2);

    if (Fun_GetCfg(addr, data, len) == false)
    {
        return false;
    }

    if (memcmp(buff, data, len) != 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 PHY_ReadMainDeviceType(void)
{
    uint16 data;

    if (Fun_GetCfg(ADDR_MAIN_DEVICE_TYPE, (uint8*)&data, sizeof(data)) == false)
    {
        data = 0xFF;
    }

    return data;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_WriteMainDeviceType(void)
{
    return Fun_SetCfg(ADDR_MAIN_DEVICE_TYPE, (uint8*)&cfg.MainDeviceType, sizeof(cfg.MainDeviceType));
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_ReadServerIP(void)
{
    uint8 *buff;
    uint8 len;

    buff = (uint8 *)cfg.GprsConnectCfg[0].ip;
    len = sizeof(cfg.GprsConnectCfg[0].ip);

    if (Fun_GetCfg(ADDR_SERVER_0_IP, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)cfg.GprsConnectCfg[1].ip;
    len = sizeof(cfg.GprsConnectCfg[1].ip);

    if (Fun_GetCfg(ADDR_SERVER_1_IP, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_WriteServerIP(void)
{
    uint8 *buff;
    uint8 len;

    buff = (uint8 *)cfg.GprsConnectCfg[0].ip;
    len = sizeof(cfg.GprsConnectCfg[0].ip);

    if (Fun_SetCfg(ADDR_SERVER_0_IP, buff, len) == false)
    {
        return false;
    }

    buff = (uint8 *)cfg.GprsConnectCfg[1].ip;
    len = sizeof(cfg.GprsConnectCfg[1].ip);

    if (Fun_SetCfg(ADDR_SERVER_1_IP, buff, len) == false)
    {
        return false;
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_ReadServerDomain(void)
{
    uint8 *buff;
    uint8 len;

    buff = (uint8 *)cfg.GprsConnectCfg[0].domain;
    len = sizeof(cfg.GprsConnectCfg[0].domain);

    if (Fun_GetCfg(ADDR_SERVER_0_DOMAIN, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)cfg.GprsConnectCfg[1].domain;
    len = sizeof(cfg.GprsConnectCfg[1].domain);

    if (Fun_GetCfg(ADDR_SERVER_1_DOMAIN, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_WriteServerDomain(void)
{
    uint8 *buff;
    uint8 len;

    buff = (uint8 *)cfg.GprsConnectCfg[0].domain;
    len = sizeof(cfg.GprsConnectCfg[0].domain);

    if (Fun_SetCfg(ADDR_SERVER_0_DOMAIN, buff, len) == false)
    {
        return false;
    }

    buff = (uint8 *)cfg.GprsConnectCfg[1].domain;
    len = sizeof(cfg.GprsConnectCfg[1].domain);

    if (Fun_SetCfg(ADDR_SERVER_1_DOMAIN, buff, len) == false)
    {
        return false;
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_ReadServerPort(void)
{
    uint8 *buff;
    uint8 len;

    buff = (uint8 *)cfg.GprsConnectCfg[0].port;
    len = sizeof(cfg.GprsConnectCfg[0].port);

    if (Fun_GetCfg(ADDR_SERVER_0_PORT, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)cfg.GprsConnectCfg[1].port;
    len = sizeof(cfg.GprsConnectCfg[1].port);

    if (Fun_GetCfg(ADDR_SERVER_1_PORT, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_WriteServerPort(void)
{
    uint8 *buff;
    uint8 len;

    buff = (uint8 *)cfg.GprsConnectCfg[0].port;
    len = sizeof(cfg.GprsConnectCfg[0].port);

    if (Fun_SetCfg(ADDR_SERVER_0_PORT, buff, len) == false)
    {
        return false;
    }

    buff = (uint8 *)cfg.GprsConnectCfg[1].port;
    len = sizeof(cfg.GprsConnectCfg[1].port);

    if (Fun_SetCfg(ADDR_SERVER_1_PORT, buff, len) == false)
    {
        return false;
    }

    return true;
}

/*******************************************************************************
*功能：读取配置信息
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_ReadConfig(void)
{
    uint8 *buff;
    uint8 len;
    uint32 addr;

    cfg.MainDeviceType = PHY_ReadMainDeviceType();

    PHY_ReadServerIP();
    PHY_ReadServerDomain();
    PHY_ReadServerPort();

    buff = (uint8 *)&cfg.BindDtuNum;
    len = sizeof(cfg.BindDtuNum);
    addr = ADDR_BIND_DTU_NUM;
    if (Fun_GetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.BindValveNum;
    len = sizeof(cfg.BindValveNum);
    addr = ADDR_BIND_VALVE_NUM;
    if (Fun_GetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.PumpExcetionLimit;
    len = sizeof(cfg.PumpExcetionLimit);
    addr = ADDR_PUMP_EXCETION_LIMIT;
    if (Fun_GetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.CircuitProtectExceptionLimit;
    len = sizeof(cfg.CircuitProtectExceptionLimit);
    addr = ADDR_CIRCUIT_PROTECT_EXCEPTIONLIMIT;
    if (Fun_GetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.ValveExceptionLimit;
    len = sizeof(cfg.ValveExceptionLimit);
    addr = ADDR_VALVE_EXCEPTION_LIMIT;
    if (Fun_GetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.FlowMeterExceptionLimit;
    len = sizeof(cfg.FlowMeterExceptionLimit);
    addr = ADDR_FLOWMETER_EXCEPTION_LIMIT;
    if (Fun_GetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.ConnectedPump;
    len = sizeof(cfg.ConnectedPump);
    addr = ADDR_CONNECTED_PUMP;
    if (Fun_GetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.ConnectedMutualInductor;
    len = sizeof(cfg.ConnectedMutualInductor);
    addr = ADDR_CONNECTED_MUTUAL_INDUCTOR;
    if (Fun_GetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.BindDtuList;
    len = sizeof(cfg.BindDtuList);
    addr = ADDR_BIND_DTU_LIST;
    if (Fun_GetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void PHY_WriteConfig(void)
{
    uint8 *buff;
    uint8 len;
    uint32 addr;

    cfg.crc = CRC16_Process((uint8 *)&cfg, sizeof(cfg) - 2);

    PHY_WriteMainDeviceType();
    PHY_WriteServerIP();
    PHY_WriteServerDomain();
    PHY_WriteServerPort();

    buff = (uint8 *)&cfg.BindDtuNum;
    len = sizeof(cfg.BindDtuNum);
    addr = ADDR_BIND_DTU_NUM;
    if (Fun_SetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.BindValveNum;
    len = sizeof(cfg.BindValveNum);
    addr = ADDR_BIND_VALVE_NUM;
    if (Fun_SetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.PumpExcetionLimit;
    len = sizeof(cfg.PumpExcetionLimit);
    addr = ADDR_PUMP_EXCETION_LIMIT;
    if (Fun_SetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.CircuitProtectExceptionLimit;
    len = sizeof(cfg.CircuitProtectExceptionLimit);
    addr = ADDR_CIRCUIT_PROTECT_EXCEPTIONLIMIT;
    if (Fun_SetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.ValveExceptionLimit;
    len = sizeof(cfg.ValveExceptionLimit);
    addr = ADDR_VALVE_EXCEPTION_LIMIT;
    if (Fun_SetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.FlowMeterExceptionLimit;
    len = sizeof(cfg.FlowMeterExceptionLimit);
    addr = ADDR_FLOWMETER_EXCEPTION_LIMIT;
    if (Fun_SetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.ConnectedPump;
    len = sizeof(cfg.ConnectedPump);
    addr = ADDR_CONNECTED_PUMP;
    if (Fun_SetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.ConnectedMutualInductor;
    len = sizeof(cfg.ConnectedMutualInductor);
    addr = ADDR_CONNECTED_MUTUAL_INDUCTOR;
    if (Fun_SetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    buff = (uint8 *)&cfg.BindDtuList;
    len = sizeof(cfg.BindDtuList);
    addr = ADDR_BIND_DTU_LIST;
    if (Fun_SetCfg(addr, buff, len) == false)
    {
        memset(buff, 0xFF, len);
    }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 PHY_ReadResetNum(void)
{
    uint16 ResetNum;

    if (Fun_GetCfg(ADDR_RESET_COUNTER, (uint8 *)&ResetNum, 2) == false)
    {
        ResetNum = DEFAULT_RESET_NUM;
    }

    return ResetNum;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_WriteResetNum(uint16 ResetNum)
{
    if (Fun_SetCfg(ADDR_RESET_COUNTER, (uint8 *)&ResetNum, 2) == false)
    {
        return false;
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 PHY_ReadConnectServerIndex(void)
{
    uint8 index;

    if (Fun_GetCfg(ADDR_SERVER_INDEX, (uint8 *)&index, 1) == false)
    {
        index = DEFAULT_CONNECT_SERVER_INDEX;
    }

    if (index >= MAX_SERVER_NUM)
    {
        index = DEFAULT_CONNECT_SERVER_INDEX;
    }

    return index;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_WriteConnectServerIndex(uint8 index)
{
    if (Fun_SetCfg(ADDR_SERVER_INDEX, (uint8 *)&index, 1) == false)
    {
        return false;
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 PHY_ReadUpdateVersion(void)
{
    uint16 UpdateVersion;

    if (Fun_GetCfg(ADDR_UPDATE_VERSION, (uint8 *)&UpdateVersion, 2) == false)
    {
        UpdateVersion = 0;
    }

    return UpdateVersion;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_WriteUpdateVersion(uint16 UpdateVersion)
{
    if (Fun_SetCfg(ADDR_UPDATE_VERSION, (uint8 *)&UpdateVersion, 2) == false)
    {
        return false;
    }

    return true;
}
/*******************************************************************
*功能：写入设备异常附码
*参数：需要存储的数据，共8个字节。
*返回值：无
*******************************************************************/
void PHY_WriteDeveiceExceptionHandler(uint32 addr, uint32 num)
{
//    uint8 temp[8];
//
//    for (uint8 i = 0; i < 4; i++)
//    {
//        temp[i] = (uint8)(addr >> (i * 8));
//    }
//
//    for (uint8 i = 0; i < 4; i++)
//    {
//        temp[4 + i] = (uint8)(num >> (i * 8));
//    }

//  EEP_DELAY_LONG();
//  if (EEP_ACTION_JUDGE)
//  {
//      Dri_WriteEeprom(EEP_HARDWARE_EXCEPTION_INDEX, (uint8 *)temp, sizeof(temp));
//  }

    return;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_InterFlashWrite(uint32 addr, uint8 *buff, uint16 len)
{
    Dri_InterFlashWrite(addr, buff, len);

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_InterFlashRead(uint32 addr, uint8 *buff, uint16 len)
{
    Dri_InterFlashRead(addr, buff, len);

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool PHY_InterFlashErasePage(uint32 page)
{
    Dri_InterFlashErasePage(page * 0x800);

    return true;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年05月10日   ggggggg      创建文件
*******************************************************************************/

