/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作    者：巍图
**  日    期：2015年08月17日
**  文件名称：ModbusInterface.c
**  摘    要：
*******************************************************************************/
#include "system.h"
#include "Modbus.h"

__no_init uint16 MbReg0[MAX_MODBUS_SLAVE_NUM][REG_0_SIZE];
__no_init uint16 MbReg1[MAX_MODBUS_SLAVE_NUM][REG_1_SIZE];
__no_init uint16 MbReg3[MAX_MODBUS_SLAVE_NUM][REG_3_SIZE];
__no_init uint16 MbReg4[MAX_MODBUS_SLAVE_NUM][REG_4_SIZE];

/*******************************************************************************
*功能：初始化Modbus
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Modbus_SlaveInit(void)
{
    uint16 i;
    uint16 j;

    for (i = 0; i < MAX_MODBUS_SLAVE_NUM; i++)
    {
        for (j = 0; j < REG_3_SIZE; j++)
        {
            MbReg3[i][j] = 0xFFFF;
        }
    }

    return;
}

/*******************************************************************************
*功能：处理来自modbus的1号命令
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dealwith_CMD1(uint16 slave, _PDU *pdu, uint16 PduLen)
{
    uint16 StartAddr;
    uint16 BitNum;
    uint16 RegNum;
    uint16 StartReg;
    uint16 StartBit;
    uint16 i;
    uint16 len;
    uint16 bit;
    _CMD1PDU *Cmd1Pdu;
    _CMD1Reply *CMD1Reply;


    if (PduLen != sizeof(_CMD1PDU))
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 3;
        len = 2;

        return len;
    }

    Cmd1Pdu = (_CMD1PDU *)pdu;
    CMD1Reply = (_CMD1Reply *)pdu;

    StartAddr = Lib_htons(Cmd1Pdu->StartAddr);
    BitNum = Lib_htons(Cmd1Pdu->CoilsNum);

    StartReg = StartAddr / 16;
    StartBit = StartAddr % 16;

    if (BitNum < 1 || BitNum > 1999)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 3;
        len = 2;

        return len;
    }

    if ((StartAddr + BitNum) >= (REG_0_SIZE * 16) || slave >= MAX_MODBUS_SLAVE_NUM)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 2;
        len = 2;

        return len;
    }

    RegNum = BitNum / 16;
    if ((BitNum % 16) != 0)
    {
        RegNum++;
    }

    for (i = 0; i < RegNum; i++)
    {
        CMD1Reply->data[i] = 0;
    }

    /*****************************
     * 1、将某个寄存器的某个位取出来
     * 2、依次放置在应答报文中，应答报文的位索引从0开始
     ****************************/
    for (i = 0; i < BitNum; i++)
    {
        bit = MbReg0[slave][StartReg + (StartBit + i) / 16] & ((uint16)1 << ((StartBit + i) % 16));

        if (bit)
        {
            CMD1Reply->data[i / 16] |= (uint16)1 << (i % 16);
        }
    }

    CMD1Reply->cmd = Cmd1Pdu->cmd;

    if ((BitNum % 16) > 8 || (BitNum % 16) == 0)
    {
        CMD1Reply->ByteNum = RegNum * 2;

        len = RegNum * 2;
    }
    else
    {
        CMD1Reply->ByteNum = RegNum * 2 - 1;

        len = RegNum * 2 - 1;
    }

    len = len + offsetof(_CMD1Reply, data);

    return len;
}

/*******************************************************************************
*功能：处理来自modbus的2号命令
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dealwith_CMD2(uint16 slave, _PDU *pdu, uint16 PduLen)
{
    uint16 StartAddr;
    uint16 BitNum;
    uint16 RegNum;
    uint16 StartReg;
    uint16 StartBit;
    uint16 i;
    uint16 len;
    uint16 bit;
    _CMD2PDU *Cmd2Pdu;
    _CMD2Reply *CMD2Reply;

    if (PduLen != sizeof(_CMD2PDU))
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 3;
        len = 2;

        return len;
    }

    Cmd2Pdu = (_CMD2PDU *)pdu;
    CMD2Reply = (_CMD2Reply *)pdu;

    StartAddr = Lib_htons(Cmd2Pdu->StartAddr);
    BitNum = Lib_htons(Cmd2Pdu->InputNum);

    StartReg = StartAddr / 16;
    StartBit = StartAddr % 16;

    if (BitNum < 1 || BitNum > 1999)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 3;
        len = 2;

        return len;
    }

    if ((StartAddr + BitNum) >= (REG_1_SIZE * 16) || slave >= MAX_MODBUS_SLAVE_NUM)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 2;
        len = 2;

        return len;
    }

    RegNum = BitNum / 16;
    if ((BitNum % 16) != 0)
    {
        RegNum++;
    }

    for (i = 0; i < RegNum; i++)
    {
        CMD2Reply->data[i] = 0;
    }

    /*****************************
     * 1、将某个寄存器的某个位取出来
     * 2、依次放置在应答报文中，应答报文的位索引从0开始
     ****************************/
    for (i = 0; i < BitNum; i++)
    {
        bit = MbReg1[slave][StartReg + (StartBit + i) / 16] & ((uint16)1 << ((StartBit + i) % 16));

        if (bit)
        {
            CMD2Reply->data[i / 16] |= (uint16)1 << (i % 16);
        }
    }

    CMD2Reply->cmd = Cmd2Pdu->cmd;

    if ((BitNum % 16) > 8 || (BitNum % 16) == 0)
    {
        CMD2Reply->ByteNum = RegNum * 2;

        len = RegNum * 2;
    }
    else
    {
        CMD2Reply->ByteNum = RegNum * 2 - 1;

        len = RegNum * 2 - 1;
    }

    len = len + offsetof(_CMD2Reply, data);

    return len;
}



/***************************************************************************
**  函数名称：Dealwith_CMD3

**  参数说明：01 03 00 00 00 0F 00 00

**  备注：处理来自modbus的3号命令，即读取保存寄存器。
****************************************************************************/
/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dealwith_CMD3(uint16 slave, _PDU *pdu, uint16 PduLen)
{
    uint16 StartAddr;
    uint16 RegNum;
    uint16 len;
    uint16 i;
    _CMD3PDU *Cmd3Pdu;
    _CMD3Reply *CMD3Reply;

    if (PduLen != sizeof(_CMD3PDU))
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 3;
        len = 2;

        return len;
    }

    Cmd3Pdu = (_CMD3PDU *)pdu;
    CMD3Reply = (_CMD3Reply *)pdu;

    StartAddr = Lib_htons(Cmd3Pdu->StartAddr);
    RegNum = Lib_htons(Cmd3Pdu->RegNum);

    if (RegNum < 1 || RegNum >= 125)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 3;
        len = 2;

        return len;
    }

    if ((StartAddr + RegNum) >= REG_3_SIZE || slave >= MAX_MODBUS_SLAVE_NUM)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 2;
        len = 2;

        return len;
    }

    for (i = 0; i < RegNum; i++)
    {
        CMD3Reply->data[i] = MbReg3[slave][StartAddr + i];
    }

    CMD3Reply->cmd = Cmd3Pdu->cmd;

    CMD3Reply->ByteNum = RegNum * 2;

    len = RegNum * 2;

    len = len + offsetof(_CMD3Reply, data);

    return len;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dealwith_CMD4(uint16 slave, _PDU *pdu, uint16 PduLen)
{
    return 0;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dealwith_CMD5(uint16 slave, _PDU *pdu, uint16 PduLen)
{
    return 0;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dealwith_CMD6(uint16 slave, _PDU *pdu, uint16 PduLen, uint16 *addr, uint16 *RegValue)
{
    uint16 RegAddr;
    uint16 len;
    _CMD6PDU *Cmd6Pdu;
    _CMD6Reply *CMD6Reply;

    Cmd6Pdu = (_CMD6PDU *)pdu;
    CMD6Reply = (_CMD6Reply *)pdu;

    RegAddr = Lib_htons(Cmd6Pdu->RegAddr);
    *addr = RegAddr;

    if (RegAddr >= REG_3_SIZE || slave >= MAX_MODBUS_SLAVE_NUM)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 2;
        len = 2;

        return len;
    }

    MbReg3[slave][RegAddr] = Cmd6Pdu->RegValue;
    *RegValue = Lib_htons(Cmd6Pdu->RegValue);

    CMD6Reply->cmd = Cmd6Pdu->cmd;
    CMD6Reply->RegAddr = Cmd6Pdu->RegAddr;
    CMD6Reply->RegValue = MbReg3[slave][RegAddr];

    len = sizeof(_CMD6Reply);

    return len;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dealwith_CMD15(uint16 slave, _PDU *pdu, uint16 PduLen)
{
    return 0;
}

/*******************************************************************************
*功能：Modbus 16号命令
*参数：无
*返回：无写多个寄存器
*说明：
*******************************************************************************/
uint16 Dealwith_CMD16(uint16 slave, _PDU *pdu, uint16 PduLen, uint16 *addr, uint16 *RegNum, uint16 *RegValue)
{
    uint16 StartAddr;
    uint16 ByteNum;
    uint16 len;
    uint16 i;
    _CMD16PDU *Cmd16Pdu;
    _CMD16Reply *CMD16Reply;

    Cmd16Pdu = (_CMD16PDU *)pdu;
    CMD16Reply = (_CMD16Reply *)pdu;

    StartAddr = Lib_htons(Cmd16Pdu->StartAddr);
    *addr = StartAddr;
    *RegNum = Lib_htons(Cmd16Pdu->RegNum);
    ByteNum = Cmd16Pdu->ByteNum;

    if (*RegNum < 1 || *RegNum >= 124 || ByteNum != (*RegNum * 2))
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 3;
        len = 2;

        return len;
    }

    if ((StartAddr + *RegNum) >= REG_3_SIZE || slave >= MAX_MODBUS_SLAVE_NUM)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 2;
        len = 2;

        return len;
    }

    for (i = 0; i < *RegNum; i++)
    {
        MbReg3[slave][StartAddr + i] = Cmd16Pdu->data[i];
        RegValue[i] = Lib_htons(Cmd16Pdu->data[i]);
    }

    CMD16Reply->cmd = Cmd16Pdu->cmd;
    CMD16Reply->StartAddr = Cmd16Pdu->StartAddr;
    CMD16Reply->RegNum = Cmd16Pdu->RegNum;

    len = sizeof(_CMD16Reply);

    return len;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dealwith_CMD111(uint16 slave, _PDU *pdu, uint16 PduLen)
{
    uint32 StartAddr;
    uint16 RegNum;
    uint16 len;
    uint16 i;
    uint32 tmp;
    uint32 tmp0;
    uint32 tmp1;
    uint32 tmp2;
    uint32 tmp3;
    _CMD111PDU *Cmd111Pdu;
    _CMD111Reply *CMD111Reply;

    if (PduLen != sizeof(_CMD111PDU))
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 3;
        len = 2;

        return len;
    }

    Cmd111Pdu = (_CMD111PDU *)pdu;
    CMD111Reply = (_CMD111Reply *)pdu;

    tmp = Cmd111Pdu->StartAddr;
    tmp0 = (tmp & 0x000000FF) << 24;
    tmp1 = (tmp & 0x0000FF00) >> 8 << 16;
    tmp2 = (tmp & 0x00FF0000) >> 16 << 8;
    tmp3 = (tmp & 0xFF000000) >> 24;
    StartAddr = tmp0 + tmp1 + tmp2 + tmp3;
    RegNum = Lib_htons(Cmd111Pdu->RegNum);

    if (RegNum < 1 || RegNum >= 125)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 3;
        len = 2;

        return len;
    }

    if ((StartAddr + RegNum) >= REG_3_SIZE || slave >= MAX_MODBUS_SLAVE_NUM)
    {
        pdu->cmd |= 0x80;
        pdu->data[0] = 2;
        len = 2;

        return len;
    }

    for (i = 0; i < RegNum; i++)
    {
        CMD111Reply->data[i] = MbReg3[slave][StartAddr + i];
    }

    CMD111Reply->cmd = Cmd111Pdu->cmd;

    CMD111Reply->ByteNum = RegNum * 2;

    len = RegNum * 2;

    len = len + offsetof(_CMD111Reply, data);

    return len;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint16 Dealwith_CMD112(uint16 slave, _PDU *pdu, uint16 PduLen)
{
    return 0;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
void Modbus_SlaveReceived(_SysMsg *msg)
{
    _Modbus *Modbus;
    _PDU *pdu;
    uint16 PduLen;
    uint16 len;
    uint16 HeadLen;                                                             //报文头部长度
    uint16 slave;                                                               //从站地址
    uint16 addr;
    uint16 RegNum;
    uint16 RegValue[128];
    uint16 cmd;

    Modbus = (_Modbus *)msg->buff;

    if (msg->src == SRC_ETH)                                                    //Modbus TCP
    {
        pdu = &Modbus->_TcpType.pdu;
        HeadLen = sizeof(_MBAP);
        PduLen = msg->len - HeadLen;
        slave = Modbus->_TcpType.MBAP.addr;
    }
    else if (msg->src == SRC_USART)                                             //Modbus RTU
    {
        pdu = &Modbus->_UsartType.pdu;
        HeadLen = sizeof(Modbus->_UsartType.addr);
        PduLen = msg->len - HeadLen;
        slave = Modbus->_UsartType.addr;
    }
    else
    {
        return;
    }

    if ((slave < 1) || (slave > MAX_MODBUS_SLAVE_NUM))
    {
        return;
    }
    slave -= 1;
    cmd = pdu->cmd;

    switch (pdu->cmd)
    {
        case CMD_MODBUS_READ_COILS:                                             // 1号命令
        {
            len = Dealwith_CMD1(slave, pdu, PduLen);

            break;
        }
        case CMD_MODBUS_READ_DISCRETE_INPUTS:                                   // 2号命令
        {
            len = Dealwith_CMD2(slave, pdu, PduLen);

            break;
        }
        case CMD_MODBUS_READ_HOLDING_REGISTERS:                                 // 3号命令
        {
            len = Dealwith_CMD3(slave, pdu, PduLen);

            break;
        }
        case CMD_MODBUS_READ_INPUT_REGISTERS:                                   // 4号命令
        {
            len = Dealwith_CMD4(slave, pdu, PduLen);

            break;
        }

        case CMD_MODBUS_WRITE_SINGLE_COILS:                                     // 5号命令
        {
            len = Dealwith_CMD5(slave, pdu, PduLen);

            break;
        }
        case CMD_MODBUS_WRITE_SINGLE_REGISTER:                                  // 6号命令
        {
            len = Dealwith_CMD6(slave, pdu, PduLen, &addr, RegValue);
            RegNum = 1;

            break;
        }
        case CMD_MODBUS_WRITE_MULTIPLE_COILS:                                   // 15号命令
        {
            len = Dealwith_CMD15(slave, pdu, PduLen);

            break;
        }
        case CMD_MODBUS_WRITE_MULTIPLE_REGISTERS:                               // 16号命令
        {
            len = Dealwith_CMD16(slave, pdu, PduLen, &addr, &RegNum, RegValue);

            break;
        }
        case CMD_MODBUS_READ_HOLDING_REGISTERS_EXTEND:
        {
            len = Dealwith_CMD111(slave, pdu, PduLen);

            break;
        }
        case CMD_MODBUS_READ_ENERGY:
        {
            len = Dealwith_CMD112(slave, pdu, PduLen);

            break;
        }
        default:
        {
            return;
        }
    }

    msg->len = HeadLen + len;
    MF_Send(msg);

    if (cmd == CMD_MODBUS_WRITE_SINGLE_REGISTER || cmd == CMD_MODBUS_WRITE_MULTIPLE_REGISTERS)
    {
        MF_SetModbusReg(slave, addr, RegNum, RegValue);
    }

    return;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0          2015年08月17日   巍图      创建文件
*******************************************************************************/
/*
设计思路：
1、本文件只作为Modbus通用命令处理，主要完成的功能是读取
2、写入命令在完成写入操作后，提交厂商处理
*/
