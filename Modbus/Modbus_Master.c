/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年11月07日
**  名   称：Modbus_Master.c
**  摘   要：
*******************************************************************************/
#include "system.h"
#include "Modbus.h"

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Modbus_ReadReg(_Modbus *buff, uint8 slave, uint16 StartAddr, uint8 RegNum, uint8 opt, uint16 *len)
{
    _CMD3PDU *Cmd3Pdu;

    *len = 0;

    if (opt != MODBUS_RTU && opt != MODBUS_TCP)
    {
        return false;
    }

    buff->UsartType.pdu.cmd = CMD_MODBUS_READ_HOLDING_REGISTERS;

    Cmd3Pdu = (_CMD3PDU *)buff->UsartType.pdu.data;

    Cmd3Pdu->RegNum = Lib_htons(RegNum);
    Cmd3Pdu->StartAddr = Lib_htons(StartAddr);

    *len = 0;

    if (opt == MODBUS_RTU)
    {
        buff->UsartType.addr = slave;

        *len = 6;
    }
    else
    {
        buff->TcpType.MBAP.addr = slave;
    }

    return true;
}

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
bool Modbus_WriteReg(_Modbus *buff, uint8 slave, uint16 addr, uint16 *value, uint8 RegNum, uint8 opt, uint16 *len)
{
    _CMD6PDU  *Cmd6Pdu;
    _CMD16PDU *Cmd16Pdu;
    uint8 i;

    if (opt != MODBUS_RTU && opt != MODBUS_TCP)
    {
        return false;
    }

    *len = 0;

    addr = Lib_htons(addr);

    if (RegNum == 1)
    {
        if (opt == MODBUS_RTU)
        {
            Cmd6Pdu = (_CMD6PDU *)buff->UsartType.pdu.data;

            buff->UsartType.addr = slave;
            buff->UsartType.pdu.cmd = CMD_MODBUS_WRITE_SINGLE_REGISTER;

            Cmd6Pdu->RegAddr = addr;
            Cmd6Pdu->RegValue = *value;
            Cmd6Pdu->RegValue = Lib_htons(Cmd6Pdu->RegValue);

            *len = 6;
        }
        else
        {
            buff->TcpType.MBAP.addr = slave;
            buff->TcpType.pdu.cmd = CMD_MODBUS_WRITE_SINGLE_REGISTER;
            memcpy(buff->TcpType.pdu.data, value, 2);
        }
    }
    else if (RegNum > 1)
    {
        if (opt == MODBUS_RTU)
        {
            Cmd16Pdu = (_CMD16PDU *)buff->UsartType.pdu.data;

            buff->UsartType.addr = slave;
            buff->UsartType.pdu.cmd = CMD_MODBUS_WRITE_MULTIPLE_REGISTERS;

            Cmd16Pdu->StartAddr = addr;
            Cmd16Pdu->RegNum = Lib_htons(RegNum);
            Cmd16Pdu->ByteNum = RegNum * 2;

            for (i = 0; i < RegNum; i++)
            {
                Cmd16Pdu->data[i] = Lib_htons(value[i]);
            }

            *len = 7 + RegNum * 2;
        }
        else
        {
            Cmd16Pdu = (_CMD16PDU *)buff->TcpType.pdu.data;

            buff->TcpType.MBAP.addr = addr;
            buff->TcpType.pdu.cmd = CMD_MODBUS_WRITE_MULTIPLE_REGISTERS;

            Cmd16Pdu->StartAddr = addr;
            Cmd16Pdu->RegNum = RegNum;
            Cmd16Pdu->ByteNum = RegNum * 2;

            for (i = 0; i < RegNum; i++)
            {
                buff->TcpType.pdu.data[i] = Lib_htons(value[i]);
            }
        }
    }

    return true;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年11月07日    巍图      创建文件
*******************************************************************************/
