/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年11月07日
**  名   称：Modbus.c
**  摘   要：
*******************************************************************************/
#include "system.h"
#include "Modbus.h"

/*******************************************************************************
*功能：
*参数：无
*返回：无
*说明：
*******************************************************************************/
uint8 Modbus_CheckResponse(_Modbus *requset, _Modbus *respone, uint16 len)
{
    _CMD3Reply  *Cmd3Reply;
    _CMD3PDU    *Cmd3Request;

    _CMD6Reply  *Cmd6Reply;
    _CMD6PDU    *Cmd6Request;

    _CMD16Reply *Cmd16Reply;
    _CMD16PDU   *Cmd16Request;

    /*检查：负响应*/
    if (len == 3 && respone->UsartType.pdu.cmd == requset->UsartType.pdu.cmd | 0x80)
    {
        return MODBUS_ERR_RESPONSE;
    }

    /*检查：命令号*/
    if (respone->UsartType.pdu.cmd != requset->UsartType.pdu.cmd)
    {
        return MODBUS_NON_REQUEST_CMD_ERR;
    }

    switch (requset->UsartType.pdu.cmd)
    {
        /*3#命令检查：读保持寄存器*/
        case CMD_MODBUS_READ_HOLDING_REGISTERS:
        {
            Cmd3Reply = (_CMD3Reply *)respone->UsartType.pdu.data;
            Cmd3Request = (_CMD3PDU *)requset->UsartType.pdu.data;

            /*检查：响应字节长度、响应数据包总长度*/
            if (Cmd3Reply->ByteNum != Lib_htons(Cmd3Request->RegNum) * 2
                || len != (Lib_htons(Cmd3Request->RegNum) * 2 + 1 + 1 + 1))     //1：1字节地址；1：1字节命令号；1：1字节”字节数“
            {
                return MODBUS_RESPONSE_LEN_ERR;
            }

            break;
        }
            /*6#命令检查：写单个寄存器*/
        case CMD_MODBUS_WRITE_SINGLE_REGISTER:
        {
            Cmd6Reply = (_CMD6Reply *)respone->UsartType.pdu.data;
            Cmd6Request = (_CMD6PDU *)requset->UsartType.pdu.data;

            if (Cmd6Reply->RegAddr != Cmd6Request->RegAddr)
            {
                return MODBUS_NON_REQUEST_REG_ERR;
            }

            if (len != 6)
            {
                return MODBUS_RESPONSE_LEN_ERR;
            }

            break;
        }
            /*16#命令检查：写多个寄存器*/
        case CMD_MODBUS_WRITE_MULTIPLE_REGISTERS:
        {
            Cmd16Reply = (_CMD16Reply *)respone->UsartType.pdu.data;
            Cmd16Request = (_CMD16PDU *)requset->UsartType.pdu.data;

            if (Cmd16Reply->StartAddr != Cmd16Request->StartAddr)
            {
                return MODBUS_NON_REQUEST_REG_ERR;
            }

            if (Cmd16Reply->RegNum != Cmd16Request->RegNum)
            {
                return MODBUS_NON_REQUEST_REG_ERR;
            }

            break;
        }
    }

    return MOBUS_CORRECT_RESPONSE;
}

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年11月07日    巍图      创建文件
*******************************************************************************/

