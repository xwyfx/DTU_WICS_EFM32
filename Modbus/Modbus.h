/*******************************************************************************
**                        巍图科技 Copyright （c）
**                           农业灌溉系统
**                            WT(2008 - 2017)
**  作   者：巍图
**  日   期：2016年11月07日
**  名   称：Modbus.h
**  摘   要：
*******************************************************************************/
/*Modbus命令定义*/
enum
{
    CMD_MODBUS_READ_COILS                    = 1,                               //读线圈状态
    CMD_MODBUS_READ_DISCRETE_INPUTS          = 2,                               //读离散输出量
    CMD_MODBUS_READ_HOLDING_REGISTERS        = 3,                               //读保持寄存器
    CMD_MODBUS_READ_INPUT_REGISTERS          = 4,                               //读输入寄存器
    CMD_MODBUS_WRITE_SINGLE_COILS            = 5,                               //写单个线圈
    CMD_MODBUS_WRITE_SINGLE_REGISTER         = 6,                               //写单个寄存器
    CMD_MODBUS_WRITE_MULTIPLE_COILS          = 15,                              //写多个线圈
    CMD_MODBUS_WRITE_MULTIPLE_REGISTERS      = 16,                              //写多个寄存器
    CMD_MODBUS_READ_HOLDING_REGISTERS_EXTEND = 111,                             //写多个寄存器
    CMD_MODBUS_READ_ENERGY                   = 112
};

/*Modbus接收报文检查结果*/
enum
{
    MOBUS_CORRECT_RESPONSE = 1,                                                 //正确响应
    MODBUS_ERR_RESPONSE = 2,                                                    //负响应
    MODBUS_NON_REQUEST_CMD_ERR = 3,                                             //返回响应的功能码与发送请求的功能码不一致
    MODBUS_RESPONSE_LEN_ERR = 4,                                                //返回响应的长度不对
    MODBUS_NON_REQUEST_REG_ERR = 5,
};

/*1号命令体*/
#pragma pack(1)
typedef struct
{
    uint8  cmd;                                                                 //命令号
    uint16  StartAddr;                                                          //起始地址
    uint16 CoilsNum;                                                            //线圈数
}_CMD1PDU;
#pragma pack()

/*1号命令响应*/
#pragma pack(1)
typedef struct
{
    uint8  ByteNum;                                                             //字节数
    uint16 data[256];
}_CMD1Reply;
#pragma pack()

/*2号命令体*/
#pragma pack(1)
typedef struct
{
    uint16 StartAddr;                                                           //起始地址
    uint16 InputNum;                                                            //输入线圈数
}_CMD2PDU;
#pragma pack()

/*2号命令响应*/
#pragma pack(1)
typedef struct
{
    uint8  ByteNum;                                                             //字节数
    uint16 data[256];
}_CMD2Reply;
#pragma pack()

/*3号命令体*/
#pragma pack(1)
typedef struct
{
    uint16 StartAddr;                                                           //起始地址
    uint16 RegNum;                                                              //寄存器数量
}_CMD3PDU;
#pragma pack()

/*3号命令响应*/
#pragma pack(1)
typedef struct
{
    uint8  ByteNum;                                                             //字节数
    uint16 data[256];
}_CMD3Reply;
#pragma pack()

/*4号命令体*/
#pragma pack(1)
typedef struct
{
    uint16 StartAddr;                                                           //起始地址
    uint16 RegNum;                                                              //寄存器数量
}_CMD4PDU;
#pragma pack()

/*4号命令响应*/
#pragma pack(1)
typedef struct
{
    uint8  ByteNum;                                                             //字节数
    uint16 data[256];
}_CMD4Reply;
#pragma pack()

/*6号命令体*/
#pragma pack(1)
typedef struct
{
    uint16 RegAddr;                                                             //寄存器地址
    uint16 RegValue;                                                            //寄存器值
}_CMD6PDU;
#pragma pack()


/*6号命令响应*/
#pragma pack(1)
typedef struct
{
    uint16 RegAddr;                                                             //寄存器地址
    uint16 RegValue;                                                            //寄存器值
}_CMD6Reply;
#pragma pack()

/*16号命令体*/
#pragma pack(1)
typedef struct
{
    uint16 StartAddr;                                                           //起始地址
    uint16 RegNum;                                                              //寄存器数量
    uint8  ByteNum;                                                             //字节数
    uint16 data[256];                                                           //负载
}_CMD16PDU;
#pragma pack()

/*16号命令响应*/
#pragma pack(1)
typedef struct
{
    uint16 StartAddr;                                                           //起始地址
    uint16 RegNum;                                                              //寄存器数量
}_CMD16Reply;
#pragma pack()

/*111号命令体*/
#pragma pack(1)
typedef struct
{
    uint32 StartAddr;                                                           //起始地址
    uint16 RegNum;                                                              //寄存器数量//字节数
}_CMD111PDU;
#pragma pack()

/*111号命令响应*/
#pragma pack(1)
typedef struct
{
    uint8  ByteNum;                                                             //寄存器数量
    uint16 data[256];                                                           //负载
}_CMD111Reply;
#pragma pack()

#include "Modbus_Master.h"

uint8 Modbus_CheckResponse(_Modbus *requset, _Modbus *respone, uint16 len);

/*******************************************************************************
* 版本号         修改日期         修改者         修改内容
* 1.0.0       2016年11月07日    巍图      创建文件
*******************************************************************************/
