透明传输（plc）
01 01 00 08 02 03 00 04 00 01 C5 F8   //读一个寄存器

透明传输（plc）
01 01 00 08 02 03 00 04 00 64 05 D3   //读100个寄存器

透明传输（mic）
01 01 00 08 C8 03 00 05 00 01 85 92   //读一个寄存器

透明传输（mic）
01 01 00 08 C8 03 00 04 00 64 14 79   //读100个寄存器


01 0C 00 00   //注册响应

01 02 00 00   //心跳响应

01 4B 00 02 00 00  //关闭调试信息输出
01 4B 00 02 01 00  //RS485接收信息输出
01 4B 00 02 02 00  //RS485发送信息输出
01 4B 00 02 04 00  //GPRS接收信息输出
01 4B 00 02 08 00  //GPRS发送信息输出
01 4B 00 02 0C 00  //组合设置：GPRS收发信息输出
01 4B 00 02 1C 00  //组合设置：GPRS收发信息输出、调试信息输出
01 4B 00 02 10 00  //调试信息输出
01 4B 00 02 03 00  //组合设置：（1+2=3）RS485收发输出
01 4B 00 02 13 00  //组合设置：（1+2+16=19）RS485收发输出、调试信息输出
01 4B 00 02 1F 00  //打开所有调试信息

01 1A 00 02 01 01   //复位异常，监控 总线超时 
01 1A 00 02 01 02   //复位异常，不监控 总线超时 

01 1A 00 03 01 03
01 1A 00 02 01 03  //错误数据包，预期无响应

01 1A 00 02 02 01   //复位异常，监控 水泵严重异常 
01 1A 00 02 02 02   //复位异常，不监控 水泵严重异常 

01 1A 00 02 03 01   //复位异常，监控 转发异常 
01 1A 00 02 03 02   //复位异常，不监控 转发异常 

01 1A 00 02 05 01   //复位异常，监控 电路保护严重异常 
01 1A 00 02 05 02   //复位异常，不监控 电路保护严重异常 

01 1A 00 02 06 01   //复位异常，监控 GPRS通信异常 
01 1A 00 02 06 02   //复位异常，不监控 GPRS通信异常

01 1A 00 02 07 01   //复位异常，监控 无线通信异常 
01 1A 00 02 07 02   //复位异常，不监控 无线通信异常

01 1A 00 02 08 01   //复位异常，监控 协调器异常 
01 1A 00 02 08 02   //复位异常，不监控 协调器异常

01 1A 00 02 09 01   //复位异常，监控 阀门异常 
01 1A 00 02 09 02   //复位异常，不监控 阀门异常

01 1A 00 02 0A 01   //复位异常，监控 流量计异常 
01 1A 00 02 0A 02   //复位异常，不监控 流量计异常

01 1A 00 02 0B 01   //复位异常，监控 指令锁定异常 
01 1A 00 02 0B 02   //复位异常，不监控 指令锁定异常

01 1A 00 02 0C 01   //复位异常，监控 逻辑锁定异常 
01 1A 00 02 0C 02   //复位异常，不监控 逻辑锁定异常

01 26 00 08 02 03 00 04 00 64 05 D3  //特权指令：读一个寄存器


/*子任务测试*/
01 17 00 0D 55 00 08 01 00 02 03 01 40 00 01 84 11   //设置子任务 1个任务、任务ID：0x55、任务类型：定时任务、超时时间：无；地块：0（注：报文负载为随意编写的、不能向真实PLC中写入）
01 17 00 0D 55 00 08 01 00 02 03 01 54 00 01 C4 15   //设置子任务 1个任务、任务ID：0x55、任务类型：定时任务、超时时间：无；地块：1（注：报文负载为随意编写的、不能向真实PLC中写入）
01 17 00 0D 88 00 08 02 00 02 03 01 54 00 01 C4 15   //设置子任务 1个任务、任务ID：0x88、任务类型：定量任务、超时时间：无；地块：1（注：报文负载为随意编写的、不能向真实PLC中写入）
01 17 00 0D 77 00 08 01 00 02 03 01 68 00 01 04 19   //设置子任务 1个任务、任务ID：0x77、任务类型：定时任务、超时时间：无；地块：2（注：报文负载为随意编写的、不能向真实PLC中写入）
01 17 00 0D 99 00 08 01 01 02 03 01 7C 00 01 44 1D   //设置子任务 1个任务、任务ID：0x99、任务类型：定时任务、超时时间：1分钟；地块：3（注：报文负载为随意编写的、不能向真实PLC中写入）
01 17 00 0D AA 00 08 01 01 02 03 01 90 00 01 85 E8   //设置子任务 1个任务、任务ID：0xAA、任务类型：定时任务、超时时间：1分钟；地块：4（注：报文负载为随意编写的、不能向真实PLC中写入）

01 17 00 19 55 00 08 01 00 02 03 01 40 00 01 84 11 00 08 01 00 02 03 01 40 00 01 84 11  //设置子任务 2个子任务，任务ID：0x55、任务类型：定时任务、超时时间：无

清空子任务状态寄存器：
02 06 0167 0000 39DA




灌溉任务报告：
01 18 0004 55 00 0200   注释：55：任务id、00：任务执行结果（成功）、0200：子任务状态寄存器
01 18 0004 99 01 0000   注释：99：任务id、01：任务执行结果（超时）、0000：子任务状态寄存器
01 18 0004 AA 00 0200   注释：AA：任务id、00：任务执行结果（成功）、0200：子任务状态寄存器
01 18 0007 55 00 0200 00 0200  注释：55：任务id、00：任务执行结果（成功）、0200：子任务状态寄存器、00：任务执行结果（成功）、0200：子任务状态寄存器

设置子任务响应：
01 19 0001 01           注释：01：成功

特权指令响应
01 27 0001 01           注释：01：成功


取消特权指令
01 28 00 00

取消特权指令响应
01 29 00 00

注册响应
01 0C 00 00

上报异常响应
01 23 00 01 06   负载：异常码

PLC收到服务器转发的其他dtu的转发请求
01 1F 00 12 31 00 00 00 00 00 00 00 00 00 00 00 00 00 00 88 00 01

解释：
01 1F 00 12           报文头部
31 00 00 00 00 00 00 00 00 00 00 00 00 00 00   IMEI
88   序号
01 00  阀位状态

PLC收到服务器转发的其他dtu的转发请求 --- 阀门打开
01 1F 00 12 31 00 00 00 00 00 00 00 00 00 00 00 00 00 00 88 01 00

PLC收到服务器转发的其他dtu的转发请求 --- 阀门关闭
01 1F 00 12 31 00 00 00 00 00 00 00 00 00 00 00 00 00 00 88 00 00


PLC收到转发请求后，给服务器的响应
01 20 00 12 31 00 00 00 00 00 00 00 00 00 00 00 00 00 00 88 01 00

解释：
01 20 00 12           报文头部
31 00 00 00 00 00 00 00 00 00 00 00 00 00 00   IMEI
88   序号
01 00  阀位状态

读配置
01 24 00 00


恢复默认配置
01 46 00 00

恢复默认配置响应
01 47 00 00